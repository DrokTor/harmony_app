<?php

namespace app\modules\deviation\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class LibraryController extends Controller
{
    public $layout='deviation';
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
      $page= $this->renderPartial('guide');
      return $this->render('library',['page'=>$page]);
    }

    public function actionGuide()
    {
      $page= $this->renderPartial('guide');
      return $this->render('library',['page'=>$page]);
    }

    public function actionFlowchart()
    {
      $page= $this->renderPartial('flowchart');
      return $this->render('library',['page'=>$page]);
    }

    public function actionRefrences()
    {
      $page= $this->renderPartial('refrences');
      return $this->render('library',['page'=>$page]);
    }

    public function actionDefinitions()
    {
      $page= $this->renderPartial('definitions');
      return $this->render('library',['page'=>$page]);
    }


}
