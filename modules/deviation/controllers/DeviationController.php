<?php

namespace app\modules\deviation\controllers;

use Yii;
use Psy\VarDumper\Dumper;
use app\models\user;
use app\modules\deviation\models\DevDeviation;
use app\modules\deviation\models\DevDraft;
use app\modules\deviation\models\DevDeclaration;
use app\modules\deviation\models\DevInvestigation;
use app\modules\deviation\models\DevConclusion;
use app\modules\deviation\models\DevDeviationSearch;
use app\modules\deviation\models\DevBatches;
use app\modules\deviation\models\DevSignatories;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use app\modules\deviation\models\Files;
use yii\web\UploadedFile;
use app\modules\deviation\models\DevFilters;
use app\modules\deviation\models\DevActions;
use app\modules\deviation\models\DevRpn;
use app\modules\deviation\models\DevPreRpn;
use app\modules\deviation\models\DevReasons;
use app\modules\deviation\models\DevApprovals;
use app\models\Profiles;
use app\models\Process;
use app\models\Assignments;
use yii\base\Event;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\modules\deviation\models\DevLevel;
use app\modules\deviation\models\DevSeverity;
use app\modules\deviation\models\DevOccurence;
use app\modules\deviation\models\DevDetectability;
use app\modules\deviation\models\DevEstimatedLvl;
use app\modules\deviation\models\CapaDeclaration;
use app\modules\deviation\models\CapaImplementation;
use app\modules\deviation\models\CapaEfficacity;
use app\modules\deviation\models\CapaDraft;
use kartik\mpdf\Pdf;
use yii\helpers\VarDumper;
use yii\log\Logger;


/**
 * DeviationController implements the CRUD actions for DevDeviation model.
 */
class DeviationController extends Controller
{
    //'deviation';//
    public $layout = '@app/modules/deviation/views/layouts/deviation';
    public $url= '/deviation/deviation/';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [

                ],
            ],
            /*'access' => [
                  'class' => AccessControl::className(),
                  //'only' => ['login', 'logout', 'signup'],
                  'rules' => [
                      [
                          'allow' => true,
                          'actions' => ['login', 'signup'],
                          'roles' => ['?'],
                      ],
                      [
                          'allow' => true,
                          'actions' => ['index','draft','create','view','investigation','investigation-data',
                          'conclusion','declaration-data','conclusion-data','data','download'],
                          'roles' => ['@'],
                      ],
                  ],
              ],*/
        ];





    }

/*
    public function beforeAction($action)
    {
      Yii::$app->getResponse()->redirect('/site/login')->send();
       dd($this->redirect('/new-draftddd'));
       die();
        //print_r(Yii::getAlias('@dev'));
         echo Yii::getAlias('@app').'<br>';
        echo Yii::getAlias('@webroot').'<br>';
        echo Yii::getAlias('@web').'<br>';
        echo Yii::$app->request->hostInfo.'<br>';
        exit();
        parent::beforeAction();
        Event::on(View::className(), View::EVENT_BEFORE_RENDER, function() {
        $userId=  Yii::$app->user->identity->id_user;

        $inv=DevDeviation::find()->where(['stateIn'=>DECLARATION])->count();
        $clsd=DevDeviation::find()->where(['stateIn'=>INVESTIGATION])->count();
        //$drft=DevDeviation::find()->where(['stateIn'=>DRAFT,'creator'=>$userId])->count();
        $drft=DevDraft::find()->count();//->where(['creator'=>$userId])
        $capadrft=CapaDraft::find()->count();//->where(['creator'=>$userId])
//print_r($drft);exit();
        $this->view->params['draft'] = $drft;
        $this->view->params['capadraft'] = $capadrft;
        Yii::$app->view->params['investigate'] = $inv;
        $this->view->params['close'] = $clsd;
        });

        $act= $this->action->id;
        $act=str_replace('-',' ',$act);
        $auth = \Yii::$app->authManager;
        //print_r($act);exit();
        //\Yii::$app->user->can($act)
        $id=Yii::$app->user->getId();
        //print_r($auth->getPermission($act)->name);echo'<br>';print_r($act);exit();
        //$id && ! \yii\rbac\ManagerInterface::checkAccess($id,$act)
        $allow= \Yii::$app->user->can($act);
        try {
            if (! $allow ){
              //throw  new ForbiddenHttpException('You are not allowed to access this page');
              return $this->render('//site/_not_allowed');
            }
        } catch (ErrorException $e) {
             echo 'not allowed';
             print_r($e);
        }

        if (! $allow ) {

            //throw new ForbiddenHttpException('You are not allowed to access this page');

            // create post
            //header("HTTP/1.0 400 Relation Restriction");
        }else  return true;

    } /*
    public function init()
    {

      // REMOVE COMMENT TAG TO CONTINUE WORKING

      $auth = \Yii::$app->authManager;
      $actions= DevActions::find()->all();
      $roles= Profiles::find()->all();
      $assignments= Assignments::find()->With('profiles','actions')->all();
      //print_r($roles[0]->attributes);exit();
      foreach( $actions as $action )
      {


          if(!$auth->getPermission($action->nm_action))
          {
            $permission=$auth->createPermission($action->nm_action);
            $permission->description=$action->nm_action;
            $auth->add($permission);
          }


      }
      foreach( $roles as $role )
      {
          if(!$auth->getRole($role->nm_profile))
          {
            $role = $auth->createRole($role->nm_profile);
            $auth->add($role);
          }
      }
      foreach( $assignments as $assignment )
      {
          if(!$auth->hasChild($auth->getRole($assignment->profiles['nm_profile']),$auth->getPermission($assignment->actions['nm_action'])))
          {
            $auth->addChild($auth->getRole($assignment->profiles['nm_profile']),$auth->getPermission($assignment->actions['nm_action']));
            //print_r( $assignment->actions['nm_action'] );echo '<br>';
          }

      }
      //   $auth->assign($auth->getRole('operator'), '2');
      //   $auth->assign($auth->getRole('Quality expert'), '1');
      //print_r($auth->getChildren('Quality expert'));exit();
    }*/
    /**
     * Lists all DevDeviation models.
     * @return mixed
     */

/*
    public function actionUpload()
    {
        $model = new Files();

        if (Yii::$app->request->isPost) {
            $model->files = UploadedFile::getInstance($model, 'files');
            if ($model->upload()) {
                // file is uploaded successfully
                return;
            }
        }

        return $this->render('_upload', ['model' => $model]);
    }*/
    public function actionNotAllowed()
    {
      return $this->render('//site/_not_allowed');
    }
    public function actionListDeviations()
    {
      //dd($this->id);
        /*$dev=DevDeviation::findOne('409');
        dd($dev->signatories['0']->statenm['nm_sign_state']);*/
        //print_r( Yii::$app->request->post()); //exit();
        $searchModel = new DevDeviationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataFilters= new DevFilters();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'devFilters'=> $dataFilters,
        ]);
    }
    public function actionListDrafts()
    {

        //$searchModel = new DevDeviationSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataFilters= new DevFilters();
        $isAdmin=\Yii::$app->authManager;
        $user=\yii::$app->user->getId();
        $isAdmin=$isAdmin->getAssignment('administrator',$user);

        $where=(!$isAdmin)?['creator'=>$user]:'1';
        $dataProvider= new ActiveDataProvider([
          'query'=> DevDraft::find()->where($where)->orderBy('id_declaration DESC'),
        ]);

        return $this->render('list_drafts', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            //'devFilters'=> $dataFilters,
        ]);
    }

    /**
     * Displays a single DevDeviation model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewDeviation($id)
    {

        $dataProvider = new ActiveDataProvider([
                        'query' => DevDeviation::find()->with('user','state','declaration')->where(['id_deviation'=>$id]),
                         'sort'=>false,
                    ]);
      $LVL_model=DevRpn::find()->with('sev','occ','det')->where(['id_deviation'=>$id])->one();

      $model=$this->findModel($id);

      if($model->stateIn==DRAFT )
      {
      $draft=true;
      }
      else
      {
        $draft=false;
        $reason= DevReasons::findOne(['id_deviation'=>$id]);
        //print_r($reason);exit();
        if($model->stateIn==CLOSED && isset($reason) && $reason->close_reason)
        {

          $decClosed=$reason->close_reason;
          $by=$reason->id_user;
        }
        elseif(/*$model->stateIn==CONCLUSION  &&*/ isset($reason) && $reason->skip_investigation)
        {

          $investSkipped=$reason->skip_investigation;//:'';
          $by=$reason->id_user;
          //echo   $testInvest->skip_investigation;exit();
        }
        elseif($model->stateIn==MISSINGINFO  && $reason->require_info)
        {
          $requireInfo=$reason->require_info;//:'';
          $by=$reason->id_user;
          $draft=true;
        }

        if(isset($by))
        {
          $getUser= User::findOne($by);
          $by=$getUser->username;
        }


      }


        if((($model->stateIn==DRAFT || $model->stateIn==MISSINGINFO) && ! \Yii::$app->user->can('view draft')))
        {
            throw new ForbiddenHttpException('You are not allowed to access this page');
        }
        else
        {

          return $this->render('view', [

              'dataProvider' => $dataProvider,
              'model' => $model,
              'draft'=>$draft,
              'levelModel'=>$LVL_model,
              'decClosed'=>isset($decClosed)?$decClosed:null,
              'investSkipped'=>isset($investSkipped)?$investSkipped:null,
              'requireInfo'=>isset($requireInfo)?$requireInfo:null,
              'by'=>isset($by)?$by:null,


          ]);
        }


    }

    public function actionSaveMissing($id=null){
      if($post= Yii::$app->request->post())
      {//dd($post);
        $model_file= new Files();

        if(isset($post['draft']))
        {
          $id= $id? $id : $post['DevDeviation']['id_deviation'] ;
          //dd($id);
          $model =   DevDeclaration::findOne(['id_deviation'=>$id]);
          $model->attributes = $post['DevDeclaration'];
          //dd($model);
          $model_file->files = UploadedFile::getInstances($model_file, 'files');



          if(  $model->save() && $model_file->upload($model->id_deviation,'deviation'))
          {
            Yii::info(\Yii::t('app','saved deviation declaration draft.'), 'user');// log draft

            //return $this->redirect(['new-draft','id'=>$model->id_declaration]);
            //dd($model->id_declaration);
            $model_file->list=files::findFiles($model->id_deviation, 'deviation');
            $model_file->url=$this->url;
            $model_file->type='declaration/draft';
            $model_file->id=$id;
            //dd($model_file);
            $session = Yii::$app->session;
            $session->setFlash('draftSaved', 'Draft successfully saved !');

            return $this->redirect( ['view-deviation','id'=>$id]);
          }

        }elseif(isset($post['launch']) && !empty($post['launch']))
        {
          $post=Yii::$app->request->post();
          //echo "add dev to process";
          $model=  DevDeclaration::findOne(['id_deviation'=>$post['DevDeviation']['id_deviation']]);
          //$model->load($post['DevDraft']);
          //print_r($post['DevDraft']);exit();
          $model->attributes =  $post['DevDeclaration'];
          //$model->deviation_title=$post['DevDraft']['deviation_title'];
          //echo '<br>';
          //dd($model);
          $model_file->files = UploadedFile::getInstances($model_file, 'files');
          //dd($model);
          if($model->validate() && $model_file->validate())
          {

            if(   $model->save()  && $model_file->upload($model->id_deviation,'deviation') )
            {
              $deviation = DevDeviation::setState($id,APPROVAL);

              $id?$model_file->approveFiles($model->id_deviation,$id,'deviation','declaration/draft'):'';

              Yii::info(\Yii::t('app','Launched a new deviation for approval.'), 'user'); // log launch deviation

                //print_r($batch);exit();
                //DELETE Draft from draft DB

                //$this->findModel($id)->delete();

                  $user= User::findIdentity($post['launch']);

                  Yii::$app->mailer->compose()
                  ->setFrom('deviation@harmony.com')
                  ->setTo($user->email)
                  ->setSubject(\Yii::t('app','Deviation approval N° ').$id)
                  //->setTextBody('Plain text content')
                  ->setHtmlBody('<h1>'.$model->deviation_title.'</h1><p> <a href="'.Yii::getAlias('@devUrl').'/view-deviation?id='.$id.'">'.\Yii::t('app','The deviation {idlast}</a> is waiting for your approval.',['idlast'=>$id]).'</p>')
                  ->send();

                Yii::info(\Yii::t('app','Approval sent to ').'['.$user->username.'].', 'user'); // log launch deviation

                  $sign = DevSignatories::findOne($user->id_user);
                  //dd($sign);
                  if($sign!=null)
                  DevSignatories::add($id,$user->id_user);


                return $this->redirect(['view-deviation', 'id' => $model->id_deviation]);
            }
            elseif($initCheck !== NULL && $initCheck !== 'success')
            {

             print_r($initCheck);

            }
          }else
          {
            /*
            $modelDraft= new DevDraft();
            $modelDraft->attributes=$model->attributes;
            $modelDraft->addErrors($model->getErrors());

//dd($model_file->getErrors());
            $modelFile = new Files();
            $modelFile->list=files::findFiles($id?$id:'null', 'declaration/draft');
            $modelFile->url=$this->url;
            $modelFile->type='declaration/draft';
            $modelFile->id=$id;
            $modelFile->addErrors($model_file->getErrors());
            //dd($modelFile);

            return $this->render('create', [
               'model' => $modelDraft,'model_file'=>$modelFile,'id'=>$modelDraft->id_declaration,
            ]);
            */
          } //print_r ($model->errors);

        }
      }
    }
    /**
     * Creates a new DevDeviation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionNewDraft($id=null)
    {

      if($post= Yii::$app->request->post())
      {

        $model_file= new Files();

        if(isset($post['draft']))
        {//dd($post);
          $id=($post['newDraftID']!='/' && $post['newDraftID']!='' )?$post['newDraftID']:$id;
          $model =  ($mod=DevDraft::findOne(['id_declaration'=>$id]))? $mod : new DevDraft();
          //dd($id);
          $model_file->files = UploadedFile::getInstances($model_file, 'files');

          if($model == new DevDraft())
          {
            $model->creator=Yii::$app->user->identity->id_user;
          }

          if($model->load($post) && $model->save() && $model_file->upload($model->id_declaration,'declaration/draft'))
          {
            Yii::info(\Yii::t('app','saved deviation declaration draft.'), 'user');// log draft

            //return $this->redirect(['new-draft','id'=>$model->id_declaration]);
            //dd($model->id_declaration);
            $model_file->list=files::findFiles($model->id_declaration, 'declaration/draft');
            $model_file->url=$this->url;
            $model_file->type='declaration/draft';
            $model_file->id=$id;
            //dd($model_file);
            $session = Yii::$app->session;
            $session->setFlash('draftSaved', 'Draft successfully saved !');

            return $this->render('create', [
               'model' => $model,'model_file'=>$model_file,'id'=>$model->id_declaration,
            ]);
          }
          elseif($model == new DevDraft())
          {
            $model=DevDeclaration::findOne(['id_deviation'=>$post['DevDeviation']['id_deviation']]) ;
            //dd($post);

            if($model->load($post) && $model->save() && $model_file->upload($model->id_declaration,'declaration/draft'))
            {
              Yii::info(\Yii::t('app','saved deviation declaration draft.'), 'user');// log draft
              //dd($model);
              return $this->redirect(['view-deviation','id'=>$model->id_deviation]);
            }
          }
          else {

//dd($model_file->errors);
            $model_file->list=files::findFiles($id, 'declaration/draft');
            $model_file->url=$this->url;
            $model_file->type='declaration/draft';
            $model_file->id=$id;

            return $this->render('create',['model'=>$model,'model_file'=>$model_file,'id'=>$model->id_declaration,]);
            //return $this->redirect(['new-draft','id'=>$model->id_declaration]);
            //return $this->runAction('new-draft',['id'=>$model->id_declaration, 'errors'=>'$model_file->errors']);



          }
        }
        elseif(isset($post['launch']) && !empty($post['launch']))
        {
          $post=Yii::$app->request->post();
          //echo "add dev to process";
          //dd($post);
          $model= new DevDeclaration();
          //$model->load($post['DevDraft']);
          //print_r($post['DevDraft']);exit();
          $model->attributes =  isset($post['DevDraft'])?$post['DevDraft']: $post['DevDeclaration'];
          //$model->deviation_title=$post['DevDraft']['deviation_title'];
          //echo '<br>';
          $model_file->files = UploadedFile::getInstances($model_file, 'files');
          //dd($model);
          if($model->validate() && $model_file->validate())
          {
            $initCheck=$model->init_dev( );
            $idlast = Yii::$app->db->getLastInsertID();
            //echo $initCheck;exit();
            if( $initCheck==='success' && $model->save()  && $model_file->upload($model->id_deviation,'deviation') )
            {

              $id?$model_file->approveFiles($model->id_deviation,$id,'deviation','declaration/draft'):'';

              Yii::info(\Yii::t('app','Launched a new deviation for approval.'), 'user'); // log launch deviation

                //print_r($batch);exit();
                //DELETE Draft from draft DB

                //$this->findModel($id)->delete();
                ($md=DevDraft::findOne(['id_declaration'=>$id]))? $md->delete() : '';

                  $user= User::findIdentity($post['launch']);

                  Yii::$app->mailer->compose()
                  ->setFrom('deviation@harmony.com')
                  ->setTo($user->email)
                  ->setSubject(\Yii::t('app','Deviation approval N° ').$idlast)
                  //->setTextBody('Plain text content')
                  ->setHtmlBody('<h1>'.$model->deviation_title.'</h1><p> <a href="'.Yii::getAlias('@devUrl').'/view-deviation?id='.$idlast.'">'.\Yii::t('app','The deviation {idlast}</a> is waiting for your approval.',['idlast'=>$idlast]).'</p>')
                  ->send();

                Yii::info(\Yii::t('app','Approval sent to ').'['.$user->username.'].', 'user'); // log launch deviation

                  DevSignatories::add($idlast,$user->id_user);


                return $this->redirect(['view-deviation', 'id' => $model->id_deviation]);
            }
            elseif($initCheck !== NULL && $initCheck !== 'success')
            {

             print_r($initCheck);

            }
          }else
          {

            $modelDraft= new DevDraft();
            $modelDraft->attributes=$model->attributes;
            $modelDraft->addErrors($model->getErrors());

//dd($model_file->getErrors());
            $modelFile = new Files();
            $modelFile->list=files::findFiles($id?$id:'null', 'declaration/draft');
            $modelFile->url=$this->url;
            $modelFile->type='declaration/draft';
            $modelFile->id=$id;
            $modelFile->addErrors($model_file->getErrors());
            //dd($modelFile);

            return $this->render('create', [
               'model' => $modelDraft,'model_file'=>$modelFile,'id'=>$modelDraft->id_declaration,
            ]);
          } //print_r ($model->errors);

        }
        elseif(isset($post['discard']) && !empty($post['discard']))
        {
          ($md=DevDraft::findOne(['id_declaration'=>$id]))? $md->delete() : '';

          $session = Yii::$app->session;
          $session->setFlash('draftDiscarded', 'Draft successfully deleted !');

          return $this->redirect('new-draft');
        }
        else
        {
          echo "error on submit";
        }


      }
      else
      {

        $model = ($mod=DevDraft::findOne(['id_declaration'=>$id]))? $mod : new DevDraft();
        $model_file = new Files();
        $model_file->files=UploadedFile::getInstances($model_file, 'files');// I uncommented the next two lines, and I think it is the right thing to do, I probably commented them because a bug that I solved today (list files not empty when new draft)
        $model_file->list=files::findFiles($id?$id:'null', 'declaration/draft');
        $model_file->url=$this->url;
        $model_file->type='declaration/draft';
        $model_file->id=$id;
        //$model_file->save();

        return $this->render('create', [
           'model' => $model,'model_file'=>$model_file,'id'=>$id,
        ]);
        /*if($model->creator==\Yii::$app->user->getId() || \Yii::$app->user->identity->isAdmin())
        {


          $model_file = new Files();
          //$model_file->files=UploadedFile::getInstances($model_file, 'files');//
          //$model_file->list=files::findFiles($id, 'declaration/draft');
          $model_file->url=$this->url;
          $model_file->type='declaration/draft';
          $model_file->id=$id;

          return $this->render('create', [
             'model' => $model,'model_file'=>$model_file
          ]);

        }
        else {
          return $this->redirect('not-allowed');
        }*/
      }



    }


    public function actionApproveDeviation($id)
    {
      //print_r(Yii::$app->request->post());exit();
      $post=Yii::$app->request->post();
      //dd(date('Y-m-d H:i'));
      $model=DevDeviation::find()->with('declaration')->where(['id_deviation'=>$id])->one();
      $findreason=DevReasons::findOne(['id_deviation'=>$id]);
      $reason= $findreason?  $findreason:new DevReasons();
      $user_id=Yii::$app->user->identity->id_user;
      $username=Yii::$app->user->identity->username;

      if(Yii::$app->request->post('approve'))
      {
        $dirtyModel=DevDeclaration::edit($id,$post);
        //$dirty=$dirtyModel->getDirtyAttributes();
        //print_r($post['DevDeclaration']);
        //print_r($declaration);echo"<br>";
        //print_r($declaration->attributes);echo"<br>";
        //print_r($declaration->oldAttributes);exit();
        $model->stateIn=DECLARATION;
        $model->save();
        $approvals=new DevApprovals();
        $approvals->manager=$user_id;
        $approvals->deviation_id=$id;
        $approvals->approvalDate=date('Y-m-d H:i');
        $approvals->save();




        /* populate batch deviation table  */
        $dec_model=DevDeclaration::findOne(['id_deviation'=>$model->id_deviation]);
        //dd($dec_model);
        $update='';
        $relations=DevDeclaration::getRelations();
        //d($relations);
        //d($dirty);
        foreach ($dirtyModel['dirty'] as $key => $value) {
         $update.='('.$dec_model->getAttributeLabel($key);
         $update.=' _old : '.(array_key_exists($key,$relations)?$dirtyModel['oldModel']->$relations[$key][0]->$relations[$key][1]:$dirtyModel['oldModel']->$key).' - ';
         $update.=' _new : '.(array_key_exists($key,$relations)?$dec_model->$relations[$key][0]->$relations[$key][1]:$value).') - ';
        }
        //dd($update);
        Yii::info(\Yii::t('app','Approved deviation N° ').$id.' .'.$update, 'user'); // log launch deviation


        //dd($dec_model->location);
        $batch=$dec_model->batch_number;
        $batch=explode('#',$batch);



        foreach($batch as $num)
        {
          $batch_model= new DevBatches();
          $batch_model->id_deviation=$model->id_deviation;
          $batch_model->id_batch=$num;
          $batch_model->save();
        }

        $user=User::findIdentity($post['assign']);
        DevSignatories::add($id,$user->id_user);

        Yii::$app->mailer->compose()
        ->setFrom('deviation@harmony.com')
        ->setTo($user->email)
        ->setSubject(\Yii::t('app','Quality expertise N° ').$id)
        ->setTextBody('Plain text content')
        ->setHtmlBody('<h1>'.$model->declaration['deviation_title'].'</h1><p><a href="'.Yii::getAlias('@devUrl').'/view-deviation?id='.$id.'">'.\Yii::t('app','The deviation {id}</a> is waiting for your quality expertise.',['id'=>$id]).'</p>')
        ->send();

        Yii::info(\Yii::t('app','Required quality expertise from [{username}] for deviation N° {id} .',['username'=>$user->username,'id'=>$id]), 'user'); // log launch deviation




        $estimated= new DevEstimatedLvl();
        $estimated->id_deviation=$id;
        $estimated->id_level=Yii::$app->request->post('approve');
        $estimated->save();

        Yii::info(\Yii::t('app','Rated deviation N° {id} as level [{lvl}].',['id'=>$id,'lvl'=>$estimated->lvl->nm_level]), 'user'); // log launch deviation

      }
      elseif(Yii::$app->request->post('moreInfo'))
      {

        DevDeclaration::edit($id,$post);
        $user=User::findOne(['id_user'=>$model->creator]);

        $decModel=DevDeclaration::findOne(['id_deviation'=>$model->id_deviation]);
        $manager=\Yii::$app->user->identity;

        $model->stateIn=MISSINGINFO; //DRAFT
        $model->save();
        $reason->id_deviation=$id;
        $reason->require_info=Yii::$app->request->post('moreInfo');
        $reason->id_user=  Yii::$app->user->identity->id_user;
        $reason->save();

        Yii::info(\Yii::t('app','Required more information for deviation N° {id} [reason: {reason} ].',['id'=>$id,'reason'=>$reason->require_info]), 'user'); // log launch deviation

        Yii::$app->mailer->compose()
        ->setFrom('deviation@harmony.com')
        ->setTo($user->email)
        ->setSubject(\Yii::t('app','Required more information for deviation N° {id}.',['id'=>$id]))
        //->setTextBody('Plain text content')
        ->setHtmlBody('<h1><a href="'.Yii::getAlias('@devUrl').'/view-deviation?id='.$id.'">'.$decModel->deviation_title.'</a></h1><p> '.\Yii::t('app','Required more information for deviation N° {id} by: {manager} <br><strong>Reason:</strong> {reason}',['id'=>$id,'manager'=>$manager->username, 'reason'=>$reason->require_info]).'</p>')
        ->send();
      }
      elseif(Yii::$app->request->post('discardDev'))
      {
        //$this->runAction('discard-draft',['id'=>$id]);
        DevDeclaration::edit($id,$post);
        $user=User::findOne(['id_user'=>$model->creator]);

        $decModel=DevDeclaration::findOne(['id_deviation'=>$model->id_deviation]);
        $manager=\Yii::$app->user->identity;

        $model->stateIn=CLOSED;
        $model->save();
        $reason->id_deviation=$id;
        $reason->close_reason=Yii::$app->request->post('discardDev');
        $reason->id_user=  $manager->id_user;
        $reason->save();

        $approvals=new DevApprovals();
        $approvals->manager=$manager->id_user;
        $approvals->deviation_id=$id;
        $approvals->approvalDate=date('Y-m-d H:i');
        $approvals->save();

        Yii::info(\Yii::t('app','Deviation discarded [reason: {reason} ].',['reason'=>$reason->close_reason]), 'user'); // log launch deviation


        Yii::$app->mailer->compose()
        ->setFrom('deviation@harmony.com')
        ->setTo($user->email)
        ->setSubject(\Yii::t('app','Deviation N° {id} was discarded.',['id'=>$id]))
        //->setTextBody('Plain text content')
        ->setHtmlBody('<h1><a href="'.Yii::getAlias('@devUrl').'/view-deviation?id='.$id.'">'.$decModel->deviation_title.'</a></h1><p> '.\Yii::t('app','Deviation N° {id} was discarded by: {manager} <br><strong>Reason:</strong> {reason}',['id'=>$id,'manager'=>$manager->username, 'reason'=>$reason->close_reason]).'</p>')
        ->send();

        //return $this->redirect(['list-deviations']);
      }
      else {
        echo "Error in step Approval";
      }
      return $this->redirect(['view-deviation', 'id' => $id]);
    }


    public function actionViewDeclaration()
    {
      if (Yii::$app->request->isAjax  ) //|| 1
      {
      //echo "lala";
      //$id=330;//for tests only
      $id=Yii::$app->request->post()['id'];
      $model=  DevDeclaration::find()->With('type','plan','location','process','equipment','product')->where(['id_deviation' => $id])->one();
      //print_r($model); exit();
      $dev_model= DevDeviation::findOne(['id_deviation' => $id]);

      //$lvl= new DevLevel();
/*debugging
      if(!$model->save())
      {
      //print_r( $model->errors);print_r($model);exit();
    }else print_r($model);
    /*debugging*/
       $files=new files();
       //$files->files=UploadedFile::getInstances($files, 'files');//
       $files->list=files::findFiles($id, 'deviation');
       $files->url=$this->url;
       $files->type='deviation';
       $files->id=$id;

       if(($dev_model->stateIn==DRAFT || $dev_model->stateIn==MISSINGINFO ) && \Yii::$app->user->can('view draft'))
       {
         if($dev_model->creator==\Yii::$app->user->getId())
         $page='_view-draft';
         else
         $page='_view-declaration';
         return $this->renderAjax($page,['model'=> $model,'model_file'=>$files,'dev_model'=>$dev_model,'elvl'=>null, 'state'=>$dev_model->stateIn || null ]);
       }
       elseif($dev_model->stateIn!=DRAFT && $dev_model->stateIn!=MISSINGINFO )
       {
         $dataLvl=ArrayHelper::map(DevLevel::find()->asArray()->all(),'id_level','nm_level');
         $elvl=DevEstimatedLvl::findOne(['id_deviation'=>$id]);

         if($elvl)
         switch ($elvl->id_level)
         {
           case 1 : $elvl='eLvlMinor';break;
           case 2 : $elvl='eLvlMedium';break;
           case 3 : $elvl='eLvlMajor';break;
         }
         else
         {
           $elvl='eNorate';
         }
        // print_r($dataLvl);exit();

        if( $dev_model->stateIn==APPROVAL && \Yii::$app->user->can('approve deviation'))
        {
          return $this->renderAjax('_approval',['model'=> $model,'model_file'=>$files,'dev_model'=>$dev_model,'levelData'=>$dataLvl,'elvl'=>$elvl  ]);
          //return $this->renderAjax('_view-draft',['model'=> $model,'model_file'=>$files,'dev_model'=>$dev_model ]);
        }
        elseif($dev_model->stateIn==APPROVAL)
        {
          return $this->renderAjax('_view-declaration',['model'=> $model,'model_file'=>$files,'dev_model'=>$dev_model,'levelData'=>$dataLvl,'elvl'=>$elvl  ]);

        }
        else
        {
          return $this->renderAjax('_view-declaration',['model'=> $model,'model_file'=>$files,'dev_model'=>$dev_model,'levelData'=>$dataLvl,'elvl'=>$elvl  ]);
        }

       }

        //print_r ($model3);
      }
    }

    /*
    paste conclusion
    */



    public function actionRateDeviation($id)
    {
      //print_r(Yii::$app->request->post());
      $post=Yii::$app->request->post();
      $model= DevRpn::findOne(['id_deviation'=>$id])?DevRpn::findOne(['id_deviation'=>$id]) : new DevRpn();
      //print_r($model);
      $model->id_deviation=$id;
      $sev= $model->id_severity=$post['severity'];
      $occ= $model->id_occurence=$post['occurence'];
      $det= $model->id_detectability=$post['detectability'];

       $rpn=$sev*$occ*$det;

        if($rpn>=12)
        {
          $rpn=3;
        }
        elseif($rpn>=6)
        {
          $rpn=2;
        }
        else
        {
          $rpn=1;
        }
        //print_r($rpn);
        $model->id_level=$rpn;
        if($model->save())
        {
          return $this->redirect(['view-deviation', 'id' =>$id]);
        }else{
          echo "error";
        }


    }


    public function actionDownloadFiles()
    {

      $name = $_GET['file'];
      $id = $_GET['id'];
      $type = $_GET['type'];
      //$upload_path = Yii::$app->params[Yii::getAlias('@web').'/deviation/deviation/download'];
      $upload_path =Yii::getAlias('@app').'/modules/deviation/uploads/'.$type.'/'.$id.'/';
      if( file_exists( $upload_path.$name ) ){
      //Yii::$app->getRequest()->sendFile( $name  );
      \Yii::$app->response->sendFile($upload_path.$name );
        //, file_get_contents( $upload_path.$name )
      }
      else{
         throw new NotFoundHttpException('The requested page does not exist.');
      }



    }

    /**
     * Updates an existing DevDeviation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     *//*
    public function actionUpdate($id)
    {


        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_deviation]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DevDeviation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     *//**/
    public function actionDiscardDraft($id)
    {

        $this->findModel($id)->delete();
        DevDeclaration::findOne(['id_deviation'=>$id])->delete();
        Yii::info(\Yii::t('app','Draft discarded.'), 'user'); // log launch deviation


        return $this->redirect(['list-deviations']);
    }
    public function actionDeleteFile($id,$name,$type)
    {
      //echo 'ba';
      files::DeleteFile($id,$name,$type);
      Yii::info(\Yii::t('app','File [{name}] deleted in {module} N° {id}.',['name'=>$name,'id'=>$id]), 'user'); // log launch deviation

      //return false;
      //echo findFiles(Yii::getAlias('@app').'/modules/deviation/uploads/'.$id.'/')->count();
    }
    public function actionForm()
    {


        return $this->redirect(['_form']);
    }

    public function actionDeviationPdf($id)
    {

        $dataProvider = new ActiveDataProvider([
                        'query' => DevDeviation::find()->with('user','state','declaration')->where(['id_deviation'=>$id]),
                         'sort'=>false,
                    ]);
        $LVL_model=DevRpn::find()->with('sev','occ','det')->where(['id_deviation'=>$id])->one();
        $reason= DevReasons::findOne(['id_deviation'=>$id]);
        $approvals= DevApprovals::findOne(['deviation_id'=>$id]);

        $dev=DevDeviation::findOne($id);
        $dec=DevDeclaration::findOne(['id_deviation'=>$id]);
        $inv=DevInvestigation::findOne(['id_deviation'=>$id]);
        $ishikawa=($inv)? $inv->selectedIshikawa(true):'';
        $conc=DevConclusion::findOne(['id_deviation'=>$id]);
        $batches= DevBatches::find()->With('decision')->where(['id_deviation'=>$id])->all();
        $signatories= DevSignatories::find()->with('user','notice')->where(['id_deviation'=>$id]);//->all();
        $signProviders= new ActiveDataProvider(['query'=>$signatories,'pagination'=>false,'sort'=>false]);
        $rpnProviders= new ActiveDataProvider(['query'=>DevRpn::find()->with('lvl','sev','occ','det')->where(['id_deviation'=>$id]),'pagination'=>false,'sort'=>false]);

        $files=new Files();
        $files->list='null';

        $elvl=DevEstimatedLvl::findOne(['id_deviation'=>$id]);
        //$elvl=$this->serveRiskClass($elvl);
        //Vardumper::dump($elvl->id_level,50,true);die();

        $plvl=DevPreRpn::findOne(['id_deviation'=>$id]);
        //$plvl=$this->serveRiskClass($plvl);


        $content = $this->renderPartial('_view-pdf', [

            'dataProvider' => $dataProvider,
            'rpnProvider' => $rpnProviders,
//            'model' => $model,
            'levelModel'=>$LVL_model,
            /*'decClosed'=>isset($decClosed)?$decClosed:null,
            'investSkipped'=>isset($investSkipped)?$investSkipped:null,
            'requireInfo'=>isset($requireInfo)?$requireInfo:null,
            'by'=>isset($by)?$by:null*/
            'reason'=>$reason,
            'ishikawa'=>$ishikawa,
            'approvals'=>$approvals,
            'dec'=> $dec,'model_file'=>$files,'dev_model'=>$dev,'levelData'=>'null','elvl'=>$elvl,
            'inv'=> $inv,'model_file'=>$files,'plvl'=>$plvl,
            'conc'=> $conc, 'batches'=>$batches, 'signProviders'=>$signProviders
        ]);



        $stylesheet = file_get_contents('../modules/deviation/assets/css/dev_pdf.css');

        $pdf = new Pdf([
        'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
        'content' => $content,//$this->renderPartial('privacy'),
        'cssFile' => false,//assets/ff8c3ab0/css/bootstrap.css',
        'cssInline' => $stylesheet,//.'td{color:red;background-color:red;}',//$stylesheet,
        'options' => [
        'title' => 'Deviation rapport',
        'subject' => 'Deviation rapport PDF'
        ],
        'methods' => [
        'SetHeader' => ['Generated By: Harmony QMS||Generated On: ' . date("r")],
        'SetFooter' => ['|Page {PAGENO}/{nbpg}|'],
        ]
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        return $pdf->render();


    }
    public function serveRiskClass($risk)
    {

      switch ($risk->id_level)
      {
        case 1 : $risk='eLvlMinor';break;
        case 2 : $risk='eLvlMedium';break;
        case 3 : $risk='eLvlMajor';break;
        default: $risk='eNorate';
      }

      return $risk;
    }



    /**
     * Finds the DevDeviation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DevDeviation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
      if (($model = DevDeviation::find()->With('user','state')->where(['id_deviation' => $id])->one()  ) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
