<?php

namespace  app\modules\deviation\controllers;

use Yii;
use app\models\user;
use app\modules\deviation\models\DevDeviation;
use app\modules\deviation\models\DevDraft;
use app\modules\deviation\models\DevDeclaration;
use app\modules\deviation\models\DevInvestigation;
use app\modules\deviation\models\DevConclusion;
use app\modules\deviation\models\DevDeviationSearch;
use app\modules\deviation\models\DevBatches;
use app\modules\deviation\models\DevSignatories;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use app\modules\deviation\models\Files;
use yii\web\UploadedFile;
use app\modules\deviation\models\DevFilters;
use app\modules\deviation\models\DevActions;
use app\modules\deviation\models\DevRpn;
use app\modules\deviation\models\DevPreRpn;
use app\modules\deviation\models\DevReasons;
use app\models\Profiles;
use app\models\Process;
use app\models\Assignments;
use yii\base\Event;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\modules\deviation\models\DevLevel;
use app\modules\deviation\models\DevSeverity;
use app\modules\deviation\models\DevOccurence;
use app\modules\deviation\models\DevDetectability;
use app\modules\deviation\models\DevEstimatedLvl;
use app\modules\deviation\models\CapaDeclaration;
use app\modules\deviation\models\CapaImplementation;
use app\modules\deviation\models\CapaEfficacity;
use app\modules\deviation\models\CapaDraft;
use kartik\mpdf\Pdf;
use yii\log\DbTarget;
/**
 *
 */
class CapaController extends Controller
{
    /**
     * @inheritdoc
     */

     public $layout = 'deviation';
     public $url='/deviation/capa/';

    public function behaviors()
    {
        return [
            /*'access' => [
                'class' => AccessControl::className(),
                'only' => [

                ],
                'rules' => [
                ],
            ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    /**
     * Displays homepage.
     *
     * @return mixed
     */


     /*
     public function beforeAction($action)
     {


       Event::on(View::className(), View::EVENT_BEFORE_RENDER, function() {
       $userId=  Yii::$app->user->identity->id_user;

       $inv=DevDeviation::find()->where(['stateIn'=>DECLARATION])->count();
       $clsd=DevDeviation::find()->where(['stateIn'=>INVESTIGATION])->count();
       //$drft=DevDeviation::find()->where(['stateIn'=>DRAFT,'creator'=>$userId])->count();
       $drft=DevDraft::find()->count();//->where(['creator'=>$userId])
       $capadrft=CapaDraft::find()->count();//->where(['creator'=>$userId])
//print_r($drft);exit();
       $this->view->params['draft'] = $drft;
       $this->view->params['capadraft'] = $capadrft;
       Yii::$app->view->params['investigate'] = $inv;
       $this->view->params['close'] = $clsd;
       });
         $act= $this->action->id;
         $act=str_replace('-',' ',$act);
         $auth = \Yii::$app->authManager;
         $id=Yii::$app->user->getId();
         $allow= \Yii::$app->user->can($act);
         try {
             if (! $allow ){
               throw  new ForbiddenHttpException('You are not allowed to access this page');
             }
         } catch (ErrorException $e) {
              echo 'not allowed';
              print_r($e);
         }

         return true;

     }*/

     public function actionNotAllowed()
     {
       return $this->render('//site/_not_allowed');
     }

    public function actionIndex()
    {

        return $this->runAction('list-capas');
    }

    public function actionListCapas()
    {

      $querycapas= CapaDeclaration::find()->orderBy('id DESC');

      $capas= new ActiveDataProvider([
        'query'=>$querycapas,
      ]);

      return $this->render('capasList',[
        'dataProvider'=>$capas,

      ]);
    }

    public function actionListDrafts()
    {

        //$searchModel = new CapaSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataFilters= new DevFilters();

        $dataProvider= new ActiveDataProvider([
          'query'=> CapaDraft::find()->orderBy('id DESC'),
        ]);

        return $this->render('list_drafts', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            //'devFilters'=> $dataFilters,
        ]);
    }

    public function actionCreateCapa($id=null,$draft=null)
    {
      $capa= new CapaDeclaration();
      $model_file= new Files();
      //print_r('$capa->errors');exit();
      //dd(Yii::$app->request->post());
      if(($post= Yii::$app->request->post()) && isset($post['capa']) && !empty($post['capa']) )
      {//dd($post);
        $capa->attributes=isset($post['CapaDraft'])?$post['CapaDraft']:$post['CapaDeclaration'];
        $capa->user_id=  Yii::$app->user->identity->id_user;
        $capa->creation_date=date('y-m-d');
        $capa->state=CAPADECLARATION;

        $model_file->files = UploadedFile::getInstances($model_file, 'files');
        if($capa->validate() && $capa->save() && $model_file->upload($capa->id,'capa'))
        {

          $model_file->approveFiles($capa->id,$draft,'capa','capa/draft');
          Yii::info(\Yii::t('app','Created CAPA planification N° ').$capa->id.'.', 'user'); // log launch deviation

          //return 'sucess';
          $user= User::findIdentity($post['capa']);

          Yii::$app->mailer->compose()
          ->setFrom('deviation@harmony.com')
          ->setTo($user->email)
          ->setSubject('CAPA assignment N°'.$capa->id)
          //->setTextBody('Plain text content')
          ->setHtmlBody('<h1>'.$capa->title.'</h1><p> <a href="'.Yii::getAlias('@capaUrl').'/view-capa?id='.$capa->id.'">'.\Yii::t('app','The CAPA').' '.$capa->id.'</a>'.\Yii::t('app','is waiting to be implemented').'.</p>')
          ->send();

          Yii::info(\Yii::t('app','CAPA Implementation N° ').$capa->id.\Yii::t('app',' assigned to ').$user->username.' .', 'user'); // log launch deviation

          return $this->redirect(['view-capa','id'=>$capa->id]);


        }
        else
        {//dd($post);
          $capaDraft=($drft=CapaDraft::findOne($draft))?$drft: new CapaDraft();
          $capaDraft->attributes=$capa->attributes;
          $capaDraft->scenario=DRAFT;
          $capaDraft->addErrors($capa->errors);
          $SectorManagers=ArrayHelper::map(Assignments::find()->with('user')->where(['item_name'=>'Sector manager'])->asArray()->all(),'user_id','user.username');

          $model_file = new Files();
          $model_file->files=UploadedFile::getInstances($model_file, 'files');//
          $model_file->list=files::findFiles($drft?$capaDraft->id:null, 'capa/draft');
          $model_file->url=$this->url;
          $model_file->type='capa/draft';
          $model_file->id=$capaDraft->id?$capaDraft->id:null;

           return $this->render('draftCapa',[
            'model'=>$capaDraft,
            'draft'=>$draft,
            'model_file'=>$model_file,
            'SectorManagers'=>$SectorManagers,
          ]);
        } //print_r($capa->errors);
      }
      elseif(isset($post['draft']))
      {

        $capa= ($capa=CapaDraft::findOne($draft))?$capa:new CapaDraft();//dd($draft);
        ($capa->isNewRecord && isset($post['CapaDeclaration']))?$capa->attributes=$post['CapaDeclaration']:$capa->load($post);

        $model_file->files = UploadedFile::getInstances($model_file, 'files');


        if( $capa->save() && $model_file->upload($capa->id,'capa/draft'))
        {
          Yii::info(\Yii::t('app','saved CAPA planification draft.'), 'user');// log draft

          $capaD= new CapaDeclaration();
          $capaD->load($post);//=$capa->attributes;
          return $this->redirect(['capa-draft','draft'=>$capa->id]);
          /*return $this->render(['create-capa?id='.$capa->deviation_id,
            'model'=>$capaD,
          ]);*/
        }else
        {
          $capa->scenario=DRAFT;
          $capa->addErrors($capa->errors);
          $SectorManagers=ArrayHelper::map(Assignments::find()->with('user')->where(['item_name'=>'Sector manager'])->asArray()->all(),'user_id','user.username');

          $model_fileN = new Files();
          $model_fileN->files=UploadedFile::getInstances($model_fileN, 'files');//
          $model_fileN->list=files::findFiles(isset($capa->id)?$capa->id:null, 'capa/draft');
          $model_fileN->url=$this->url;
          $model_fileN->type='capa/draft';
          $model_fileN->id=$capa->id?$capa->id:null;
          $model_fileN->addErrors($model_file->errors);

           return $this->render('draftCapa',[
            'model'=>$capa,
            'draft'=>$draft,
            'model_file'=>$model_fileN,
            'SectorManagers'=>$SectorManagers,
          ]);
        }  //print_r($model_file->errors);
      }
      else
      {
              $capa->deviation_id=$id;
              $capa->scenario=DRAFT;
              $SectorManagers=ArrayHelper::map(Assignments::find()->with('user')->where(['item_name'=>'Sector manager'])->asArray()->all(),'user_id','user.username');

              $model_file = new Files();

              return $this->render('createCapa',[
                'model'=>$capa,
                'model_file'=>$model_file,
                'SectorManagers'=>$SectorManagers,
              ]);
      }
    }

    public function actionCapaDraft($draft)
    {
      $capa= CapaDraft::findOne($draft);
      $capaDec= new CapaDeclaration();
      if($capa)
      {
        //$capaDec->attributes=$capa->attributes;
        $SectorManagers=ArrayHelper::map(Assignments::find()->with('user')->where(['item_name'=>'Sector manager'])->asArray()->all(),'user_id','user.username');

        $model_file = new Files();
        $model_file->files=UploadedFile::getInstances($model_file, 'files');//
        $model_file->list=files::findFiles($capa->id, 'capa/draft');
        $model_file->url=$this->url;
        $model_file->type='capa/draft';
        $model_file->id=$capa->id;

        return $this->render('draftCapa',[
          'model'=>$capa,
          'draft'=>$capa->id,
          'model_file'=>$model_file,
          'SectorManagers'=>$SectorManagers,
        ]);
      }
      else {
        dd($capa->getErrors());
      }
    }

    public function actionViewCapa($id,$implementation=null,$impfile=null,$efficacity=null,$effile=null)
    {//dd($implementation);
      $querycapa= CapaDeclaration::find()->with('capaType','capaState','creator')->where(['id'=>$id]);
      $modelPlan= $querycapa->one();
      //print_r($modelPlan);exit();
      $state=$modelPlan->state;

      $model_file = new Files();
      $model_file->files=UploadedFile::getInstances($model_file, 'files');//
      $model_file->list=files::findFiles($modelPlan->id, 'capa');
      $model_file->url=$this->url;
      $model_file->type='capa/draft';
      $model_file->id=$modelPlan->id;
//dd($state);
      $modelPlan= $this->renderPartial('_view-capa-plan',[
      'modelPlan'=> $modelPlan,
      'model_file'=>$model_file
       ]);
      $modelImplement= CapaImplementation::findOne(['capa_id'=>$id]);
      $stepJump=true;
    //dd($modelImplement);
      $planned=isset($modelImplement->efficacity_review_date)?$modelImplement->efficacity_review_date:null;
      if($modelImplement && ($state>=CAPAIMPLEMENTATION))
      {
        $stepJump=false;

        $imp_file = new Files();
        $imp_file->list=files::findFiles($modelImplement->id, 'implementation','capa');

        $modelImplement= $this->renderPartial('_view-capa-implement',['model'=>$modelImplement, 'model_file'=>$imp_file]);
      }
      elseif($implementation)
      {//dd($implementation);
        $implementation->scenario=CapaImplementation::SAVE;
        $imp_file = new Files();
        //$imp_file->files=UploadedFile::getInstances($imp_file, 'files');//
        $imp_file->list=files::findFiles($implementation->id, 'implementation','capa');
        $imp_file->url=$this->url;
        $imp_file->type='implementation';
        $imp_file->id=$implementation->id;
        $impfile?$imp_file->addErrors($impfile->errors):'';

        if(\Yii::$app->user->can('capa implementation'))
        $modelImplement=$this->renderPartial('_capa-implementation',['model'=>$implementation,'model_file'=>$imp_file]);
        else
        $modelImplement=$this->renderPartial('_view-capa-implement',['model'=>$implementation,'model_file'=>$imp_file]);
      }
      else
      {
        $model=$modelImplement?$modelImplement:new CapaImplementation();
        $model->capa_id=$id;
        $model->scenario=CapaImplementation::SAVE;

        $imp_file = new Files();

        if($modelImplement)
        {

        $imp_file->list=files::findFiles($modelImplement->id, 'implementation','capa');
        $imp_file->url=$this->url;
        $imp_file->type='implementation';
        $imp_file->id=$modelImplement->id;
        $impfile?$imp_file->addErrors($impfile->errors):'';

        }

        //$modelImplement=$this->renderPartial('_capa-implementation',['model'=>$model,'model_file'=>$imp_file]);
        if(\Yii::$app->user->can('capa implementation'))
        $modelImplement=$this->renderPartial('_capa-implementation',['model'=>$model,'model_file'=>$imp_file]);
        else
        $modelImplement=$this->renderPartial('_view-capa-implement',['model'=>$model,'model_file'=>$imp_file]);

      }

      $modelEffic= CapaEfficacity::find()->with('reviewType')->where(['capa_id'=>$id])->One();
      if($modelEffic && $modelImplement && ($state==CAPAEFFICACITYREVIEW))
      {

        $ef_file = new Files();
        $ef_file->list=files::findFiles($modelEffic->id, 'efficacity','capa');
        $modelEffic=$this->renderPartial('_view-capa-efficacity',['model'=>$modelEffic, 'model_file'=>$ef_file]);
      }
      elseif(!$stepJump && $efficacity)
      {
        $efficacity->scenario=CapaEfficacity::SAVE;

        $ef_file = new Files();
        $ef_file->list=files::findFiles($efficacity->id, 'efficacity','capa');
        $ef_file->url=$this->url;
        $ef_file->type='efficacity';
        $ef_file->id=$efficacity->id;
        $effile?$ef_file->addErrors($effile->errors):'';

        if(\Yii::$app->user->can('capa efficacity'))
        $modelEffic=$this->renderPartial('_capa-efficacity',['model'=>$efficacity, 'model_file'=>$ef_file]);
        else
        $modelEffic=$this->renderPartial('_view-capa-efficacity',['model'=>$efficacity, 'model_file'=>$ef_file]);
      }
      elseif(!$stepJump && $planned)
      {
        $model=$modelEffic?$modelEffic:new CapaEfficacity();
        $model->capa_id=$id;
        $ef_file = new Files();
        if($modelEffic)
        {

          $ef_file->list=files::findFiles($modelEffic->id, 'efficacity','capa');
          $ef_file->url=$this->url;
          $ef_file->type='efficacity';
          $ef_file->id=$modelEffic->id;
          $effile?$ef_file->addErrors($effile->errors):'';
        }
        if(\Yii::$app->user->can('capa efficacity'))
        $modelEffic=$this->renderPartial('_capa-efficacity',['model'=>$model, 'model_file'=>$ef_file]);
        else
        $modelEffic=$this->renderPartial('_view-capa-efficacity',['model'=>$model, 'model_file'=>$ef_file]);

        //$modelEffic=$this->renderPartial('_capa-efficacity',['model'=>$model,'model_file'=>$ef_file]);
      }
      elseif(!$stepJump && !$planned)
      {
        $modelEffic= \Yii::t('app',"The efficacity review step must was not planned.");
      }
      else
      {
        $modelEffic= \Yii::t('app',"The CAPA implementation step must be done first.");
      }




      $capas= new ActiveDataProvider([
        'query'=>$querycapa,
        'sort'=>false,
      ]);

      return $this->render('viewCapa',[
        'dataProvider'=>$capas,
        'modelPlan'=>$modelPlan,
        'modelImplement'=>$modelImplement,
        'modelEffic'=>$modelEffic,


      ]);
    }

    public function actionCapaImplementation()
    {
      if(($post= Yii::$app->request->post()) && (isset($post['closeCapa']) || isset($post['save'])) )
      {//dd($post);
        $implement= ($imp=CapaImplementation::findOne(['capa_id'=>$post['CapaImplementation']['capa_id']]))?$imp:new CapaImplementation();
        $implement->scenario= isset($post['save'])? CapaImplementation::SAVE:CapaImplementation::IMPLEMENT;

        $model_file = new Files();
        $model_file->files=UploadedFile::getInstances($model_file, 'files');//


        if( $implement->load($post) && $implement->save() && $model_file->upload($implement->id,'implementation'))
        {
          if(!isset($post['save']))
          {
          $capa= CapaDeclaration::findOne($implement->capa_id);
          $capa->state=CAPAIMPLEMENTATION;
          $capa->save();

          Yii::info(\Yii::t('app','Finished CAPA implementation N° ').$implement->capa_id.'.', 'user'); // log launch deviation

          }//return 'sucess';
          else {

          Yii::info(\Yii::t('app','Saved CAPA implementation N° ').$implement->capa_id.'.', 'user'); // log launch deviation

          }
          return $this->redirect(['view-capa','id'=>$implement->capa_id]);

        }
        else
        {
          //dd($implement->errors);
          $implement->addErrors($implement->errors);
          $model_file->addErrors($model_file->errors);
          return $this->runAction('view-capa',['id'=>$implement->capa_id,'implementation'=>$implement,'impfile'=>$model_file]);
        }
      }

    }

    public function actionCapaEfficacity()
    {
        //print_r(Yii::$app->request->post());exit();
      if(($post= Yii::$app->request->post()) && isset($post['closeEfficacity'])  || isset($post['save']))
      {
        //$efficacity= new CapaEfficacity();
        $efficacity= ($eff=CapaEfficacity::findOne(['capa_id'=>$post['CapaEfficacity']['capa_id']]))?$eff:new CapaEfficacity();
        $efficacity->scenario= isset($post['save'])? CapaEfficacity::SAVE:CapaEfficacity::EFFICACITY;

        $model_file = new Files();
        $model_file->files=UploadedFile::getInstances($model_file, 'files');

        if($efficacity->load($post) && $efficacity->save() && $model_file->upload($efficacity->id,'efficacity'))
        {
          if(!isset($post['save']))
          {
          $capa= CapaDeclaration::findOne($efficacity->capa_id);
          $capa->state=CAPAEFFICACITYREVIEW;
          $capa->save();

          Yii::info(\Yii::t('app','Finished CAPA efficacity review N° ').$efficacity->capa_id.'.', 'user'); // log launch deviation

          }
          else {
            Yii::info(\Yii::t('app','Saved CAPA efficacity review N° ').$efficacity->capa_id.'.', 'user'); // log launch deviation
          }
          return $this->redirect(['view-capa','id'=>$efficacity->capa_id]);

        }
        else
        {

          $efficacity->addErrors($efficacity->errors);
          $model_file->addErrors($model_file->errors);

          return $this->runAction('view-capa',['id'=>$efficacity->capa_id,'efficacity'=>$efficacity,'effile'=>$model_file]);
        }
      }
    }

    public function actionCapaPdf($id)
    {

        $query=CapaDeclaration::find()->with('creator','capaType','imp','eff')->where(['id'=>$id]);

        $dataProvider = new ActiveDataProvider([
                        'query' => $query,
                        'sort'=>false,
                    ]);

        $files= new Files();
        $files->list='';

        $capa= $query->one();
        //dd($capa);
        $content = $this->renderPartial('_view-pdf', [

            'dataProvider' => $dataProvider,
            'dec'=> $capa,'imp'=>$capa->imp,'eff'=>$capa->eff,'model_file'=>$files
        ]);



        $stylesheet = file_get_contents('../modules/deviation/assets/css/dev_pdf.css');

        $pdf = new Pdf([
        'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
        'content' => $content,//$this->renderPartial('privacy'),
        'cssFile' => false,//assets/ff8c3ab0/css/bootstrap.css',
        'cssInline' => $stylesheet,//.'td{color:red;background-color:red;}',//$stylesheet,
        'options' => [
        'title' => 'CAPA rapport',
        'subject' => 'Capa rapport PDF'
        ],
        'methods' => [
        'SetHeader' => ['Generated By: Harmony QMS||Generated On: ' . date("r")],
        'SetFooter' => ['|Page {PAGENO}/{nbpg}|'],
        ]
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        return $pdf->render();


    }

    public function actionDeleteFile($id,$name,$type)
    {
      //echo 'ba';
      files::DeleteFile($id,$name,$type);
      Yii::info(\Yii::t('app','File [{name}] deleted in {module} N° {id}.',['name' => $name,'id'=>$id, 'module'=>'CAPA']), 'user'); // log launch deviation

      //return false;
      //echo findFiles(Yii::getAlias('@app').'/modules/deviation/uploads/'.$id.'/')->count();
    }

    public function actionDownloadFiles()
    {
      //download?name=AILET+Sample+Form1&id=53&file=1381053929_pita.gif
      //$model = new Download;
      $name = $_GET['file'];
      $id = $_GET['id'];
      $type = $_GET['type'];
      //$upload_path = Yii::$app->params[Yii::getAlias('@web').'/deviation/deviation/download'];
      $upload_path =Yii::getAlias('@app').'/modules/deviation/uploads/'.$type.'/'.$id.'/';
      if( file_exists( $upload_path.$name ) ){
      //Yii::$app->getRequest()->sendFile( $name  );
      \Yii::$app->response->sendFile($upload_path.$name );
        //, file_get_contents( $upload_path.$name )
      }
      else{
         throw new NotFoundHttpException('The requested page does not exist.');
      }



    }
}
