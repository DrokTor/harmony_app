<?php

namespace app\modules\deviation\controllers;

use Yii;
use app\models\user;
use app\modules\deviation\models\DevDeviation;
use app\modules\deviation\models\DevDraft;
use app\modules\deviation\models\DevDeclaration;
use app\modules\deviation\models\DevInvestigation;
use app\modules\deviation\models\DevConclusion;
use app\modules\deviation\models\DevDeviationSearch;
use app\modules\deviation\models\DevBatches;
use app\modules\deviation\models\DevSignatories;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use app\modules\deviation\models\Files;
use yii\web\UploadedFile;
use app\modules\deviation\models\DevFilters;
use app\modules\deviation\models\DevActions;
use app\modules\deviation\models\DevRpn;
use app\modules\deviation\models\DevPreRpn;
use app\modules\deviation\models\DevReasons;
use app\models\Profiles;
use app\models\Process;
use app\models\Assignments;
use yii\base\Event;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\modules\deviation\models\DevLevel;
use app\modules\deviation\models\DevSeverity;
use app\modules\deviation\models\DevOccurence;
use app\modules\deviation\models\DevDetectability;
use app\modules\deviation\models\DevEstimatedLvl;
use app\modules\deviation\models\CapaDeclaration;
use app\modules\deviation\models\CapaImplementation;
use app\modules\deviation\models\CapaEfficacity;
use app\modules\administration\models\AdmDevObjectives;
use kartik\mpdf\Pdf;




class DataController extends Controller
{
    /**
     * @inheritdoc
     */
    public $layout = 'deviation';

    public function init()
    {
      define('DEVOVER', '30');
      define('EFFOVER', '1');
      parent::init();

    }
    public function behaviors()
    {
        return [
            /*'access' => [
                'class' => AccessControl::className(),
                'only' => [

                ],
                'rules' => [
                ],
            ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
/*
    public function beforeAction($action)
    {



        $act= $this->action->id;
        $act=str_replace('-',' ',$act);
        $auth = \Yii::$app->authManager;
        $id=Yii::$app->user->getId();
        $allow= \Yii::$app->user->can($act);
        try {
            if (! $allow ){
              throw  new ForbiddenHttpException('You are not allowed to access this page');
            }
        } catch (ErrorException $e) {
             echo 'not allowed';
             print_r($e);
        }

       return true;


    }
    /**
     * Displays homepage.
     *
     * @return mixed
     */

     public function actionNotAllowed()
     {
       return $this->render('//site/_not_allowed');
     }
     public function actionDataPdf($t='y',$month=null)
     {

       $DevNbr=$ClsdRate=$avg=0;
       if($t==null)
       $selectedMonth=($month!=null)?$month:date('m')[1];
       else $selectedMonth=null;
       $yearly=['YEAR(creation_date)'=>date('Y') ];
       //print_r($selectedMonth[1]);exit();
       //init
       $month=(isset($month))?['MONTH(creation_date)'=>$month,'YEAR(creation_date)'=>date('Y') ]:['MONTH(creation_date)'=>date('m'),'YEAR(creation_date)'=>date('Y') ];
       $time=($t=='y')?  $yearly:$month;
       //print_r($time);exit();
       $monthnum=null;
       if($time==$yearly )
       {
       $DevNbr= DevDeviation::find()->where($time)->all();




       $monthnum=[];
       for($i=1;$i<=12;++$i)
       {
         $monthnum[$i]=0;
       }
         foreach($DevNbr as $dev)
         {
           $date=date_format(date_create($dev['creation_date']),'m');
           //echo $date.'<br>';
           ++$monthnum[intval($date)];
         }
       $monthnum=implode($monthnum,',');
     }

         $content = $this->render('_data-pdf', [
           'monthnum'=>$monthnum

         ]);
         //return $content;


         $stylesheet = file_get_contents('../modules/deviation/assets/css/dev_pdf.css');

         $pdf = new Pdf([
         'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
         'content' => $content,//$this->renderPartial('privacy'),
         'cssFile' => false,//assets/ff8c3ab0/css/bootstrap.css',
         'cssInline' => $stylesheet,//.'td{color:red;background-color:red;}',//$stylesheet,
         'options' => [
         'title' =>  \Yii::t('app','Data analysis'),
         'subject' =>  \Yii::t('app','Data analysis PDF')
         ],
         'methods' => [
         'SetHeader' => [\Yii::t('app','Generated By: Harmony QMS||Generated On: ') . date("r")],
         'SetFooter' => ['|Page {PAGENO}|'],
         ]
         ]);

         Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
         $headers = Yii::$app->response->headers;
         $headers->add('Content-Type', 'application/pdf');

         return $pdf->render();


     }

     public function actionIndex()
     {

         //return $this->runAction('data-analysis');
         //return $this->runAction('indicators');
         return $this->redirect('indicators');
     }
    /*public function actionDataAnalysis($t=null,$month=null)
    {
      //dd(Yii::$app->timezone);
      //Yii::$app->language='fr_FR';
      $DevNbr=$ClsdRate=$avg=0;
      if($t==null || $t== 'm')
      $selectedMonth=($month!=null)?$month:date('m')[1];
      else $selectedMonth=null;
      $yearly=['YEAR(creation_date)'=>date('Y') ];
      //print_r($selectedMonth[1]);exit();
      //init
      $month=(isset($month))?['MONTH(creation_date)'=>$month,'YEAR(creation_date)'=>date('Y') ]:['MONTH(creation_date)'=>date('m'),'YEAR(creation_date)'=>date('Y') ];
      $time=($t=='y')?  $yearly:$month;

      $closing= DevDeviation::find()->joinWith('conclusion')->where($time)->andWhere(['stateIn'=>CLOSED])->all();  //DevConclusion::find()->all();

      if($closing != null)
      {
        $avg=0;
      foreach($closing as $clsed)
      {
          $avg+=$clsed->conclusion['deviation_duration'];
      }
      $avg= ceil($avg/count($closing));
      }


      if($t=='y')
      {


      // average anum by process
      $params['select']=['month(creation_date) as month','nm_process', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['inner join','dev_process','dev_declaration.process_step=dev_process.id_process']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='month,nm_process';

      $processAVGAnum= $this->chartAVGAnum($params);
      // average anum end

      // average anum by product
      $params['select']=['month(creation_date) as month','nm_product', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['inner join','dev_product','dev_declaration.product_code=dev_product.id_product']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='month,nm_product';

      $productAVGAnum= $this->chartAVGAnum($params);
      // average anum end

      // average anum by equipment
      $params['select']=['month(creation_date) as month','nm_equipment', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['inner join','dev_equipment','dev_declaration.equipment_involved=dev_equipment.id_equipment']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='month,nm_equipment';

      $equipmentAVGAnum= $this->chartAVGAnum($params);
      // average anum end

      // average anum by loclaity
      $params['select']=['month(creation_date) as month','nm_locality', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['inner join','dev_locality','dev_declaration.locality=dev_locality.id_locality']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='month,nm_locality';

      $localityAVGAnum= $this->chartAVGAnum($params);
      // average anum end


      // average anum end

      // average anum
      /*$params['select']=['MONTH(dev_deviation.creation_date) as month', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=null;//'dev_declaration';
      $params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='month';*/

/*
      $raw = (new \yii\db\Query())
      ->select(['MONTH(dev_deviation.creation_date) as month','COUNT(id_deviation) as aggregate'])
      ->from('dev_deviation')
      ->where($time)
      ->groupBy('month')
      ->all();

      $monthnum= $this->chartByNbr($raw,false);
      //dd($raw);

      $raw = (new \yii\db\Query())
      ->select(['MONTH(dev_deviation.creation_date) as month','COUNT(id_deviation) as aggregate'])
      ->from('dev_deviation')
      ->where($time)
      ->groupBy('month')
      ->all();

      $monthadd= $this->chartByNbr($raw,true);
      //dd($raw);

      // monthly closing rate
      $raw = (new \yii\db\Query())
      ->select(['MONTH(date) as month','closing_rate as aggregate'])
      ->from('dev_monthly_closing')
      ->where(['YEAR(date)'=>date('Y')])
      ->groupBy('month')
      ->all();
      //dd($raw);
      $closAnum= $this->chartByNbr($raw);
      //$closAnum='20,60,80,2,70,2,20';
      //dd($closingAnum);


      $raw = (new \yii\db\Query())
      ->select(['MONTH(dev_deviation.creation_date) as month', 'CEIL(AVG(deviation_duration)) as aggregate '])
      ->from('dev_conclusion')
      ->leftJoin('dev_deviation','dev_conclusion.id_deviation=dev_deviation.id_deviation')
      ->where($time)
      ->groupBy('month')
      ->all();

      $avgAnum= $this->chartByNbr($raw);
      // average anum end

      //d($avgAnum);
      //dd($closingAnum);

      // capa anum
      $raw = (new \yii\db\Query())
      ->select( ['MONTH(creation_date) as month','COUNT(id) as aggregate'])
      ->from('capa_declaration')
      ->where($params['where'])
      ->groupBy('month')
      ->all();

      $capaAnum= $this->chartByQuery($raw);
      //dd($capaAnum);
      // capa anum end


      // process anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_process', 'COUNT(`process_step`)'];
      $params['join']='dev_declaration';
      $params['table']='dev_process';
      $params['on']='id_process=process_step';
      $params['where']=$time;
      $params['by']='month,nm_process';

      $processAnum= $this->chartByAnum($params);


      // end process anum


      // severity anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_level', 'COUNT(dev_rpn.id_level)'];
      $params['join']='dev_rpn';
      $params['table']='dev_level';
      $params['on']='dev_rpn.id_level=dev_level.id_level';
      $params['where']=$time;
      $params['by']='month,nm_level';

      $severityAnum= $this->chartByAnum($params);
      //severity anum end

      // Method anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_method', 'COUNT(dev_investigation.id_method)'];
      $params['join']='dev_investigation';
      $params['table']='dev_method';
      $params['on']='dev_method.id_method=dev_investigation.id_method';
      $params['where']=$time;
      $params['by']='month,nm_method';

      $methodAnum= $this->chartByAnum($params);
      //Method anum end
      //dd($methodAnum);
      // ihsikawa anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_ishikawa', 'COUNT(dev_investigation_ishikawa.id)'];
      $params['join']='dev_investigation_ishikawa';
      $params['table']='dev_ishikawa';
      $params['on']='dev_ishikawa.id_ishikawa=dev_investigation_ishikawa.id_ishikawa';
      $params['where']=$time;
      $params['by']='month,nm_ishikawa';

      $ishikawa= $this->chartByAnum($params);
      //dd($ishikawa);
      //ihsikawa anum end

      // locality anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_locality', 'COUNT(locality)'];
      $params['join']='dev_declaration';
      $params['table']='dev_locality';
      $params['on']='dev_declaration.locality=dev_locality.id_locality';
      $params['where']=$time;
      $params['by']='month,nm_locality';

      $localityAnum= $this->chartByAnum($params);
      // locality anum end

      // equipment anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_equipment', 'COUNT(equipment_involved)'];
      $params['join']='dev_declaration';
      $params['table']='dev_equipment';
      $params['on']='dev_declaration.equipment_involved=dev_equipment.id_equipment';
      $params['where']=$time;
      $params['by']='month,nm_equipment';

      $equipmentAnum= $this->chartByAnum($params);
      // equimpent anum end

      // product anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_product', 'COUNT(product_code)'];
      $params['join']='dev_declaration';
      $params['table']='dev_product';
      $params['on']='dev_declaration.product_code=dev_product.id_product';
      $params['where']=$time;
      $params['by']='month,nm_product';

      $productAnum= $this->chartByAnum($params);
      // product anum end

      // decision anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_decision', 'COUNT(dev_deviation.id_deviation)'];
      $params['table']='dev_decision';
      $params['join']='dev_batches';
      $params['on']='dev_decision.id_decision=dev_batches.id_decision';
      $params['where']=$time;
      $params['by']='month,nm_decision';

      $decisionAnum= $this->chartByAnum($params);
      // decision anum end

      // decision anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_type', 'COUNT(dev_type.id_type)'];
      $params['table']='dev_type';
      $params['join']='dev_declaration';
      $params['on']='dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='month,nm_type';

      $categoryAnum= $this->chartByAnum($params);
      // decision anum end



      return $this->render('_tb',['monthnum'=>$monthnum,'monthadd'=>$monthadd,'closAnum'=>$closAnum,'capaAnum'=>$capaAnum,'processAnum'=>$processAnum,'severityAnum'=>$severityAnum,'methodAnum'=>$methodAnum,'localityAnum'=>$localityAnum,'avgAnum'=>$avgAnum,
              'equipmentAnum'=>$equipmentAnum,'productAnum'=>$productAnum,'decisionAnum'=>$decisionAnum,'categoryAnum'=>$categoryAnum,'ishikawa'=>$ishikawa,

              'processAVGAnum'=>$processAVGAnum,'productAVGAnum'=>$productAVGAnum,'equipmentAVGAnum'=>$equipmentAVGAnum,'localityAVGAnum'=>$localityAVGAnum,
              /*'manpower'=>$manpower, 'material'=>$material, 'method'=>$method, 'milieu'=>$milieu, 'machine'=>$machine,*/


/*
            ]);
      }
      elseif ($t=='m') {
      // process start

      // average month by process
      $params['select']=['nm_process', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['right join','dev_process','dev_declaration.process_step=dev_process.id_process']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='nm_process';

      $avgByProcess= $this->chartAVG($params);
      // average month end

      // average anum by product
      $params['select']=['nm_product', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['right join','dev_product','dev_declaration.product_code=dev_product.id_product']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='nm_product';

      $avgByProduct= $this->chartAVG($params);
      // average anum end


      // average anum by locality
      $params['select']=['nm_locality', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['right join','dev_locality','dev_declaration.locality=dev_locality.id_locality']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='nm_locality';

      $avgByLocality= $this->chartAVG($params);
      // average anum end

      // average anum by equipment
      $params['select']=['nm_equipment', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['right join','dev_equipment','dev_declaration.equipment_involved=dev_equipment.id_equipment']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='nm_equipment';

      $avgByEquipment= $this->chartAVG($params);

      $params['select']=['nm_process', 'COUNT(`process_step`)'];
      $params['join']='dev_declaration';
      $params['table']='dev_process';
      $params['on']='id_process=process_step';
      $params['where']=$time;
      $params['by']='nm_process';

      $processHash= $this->chartByparam($params);
      //print_r($processHash);exit();
      // process END

      // locality start
      $params['select']=['nm_locality', 'COUNT(`locality`)'];
      $params['table']='dev_locality';
      $params['join']='dev_declaration';
      $params['on']='id_locality=locality';
      $params['where']=$time;
      $params['by']='nm_locality';

      $localityHash= $this->chartByparam($params);

      // locality end

      // equimpent start
      $params['select']=['nm_equipment', 'COUNT(`equipment_involved`)'];
      $params['join']='dev_declaration';
      $params['table']='dev_equipment';
      $params['on']='id_equipment=equipment_involved';
      $params['where']=$time;
      $params['by']='nm_equipment';

      $equipmentHash= $this->chartByparam($params);

      // equipment end

      // product start
      $params['select']=['nm_product', 'COUNT(`product_code`)'];
      $params['join']='dev_declaration';
      $params['table']='dev_product';
      $params['on']='id_product=product_code';
      $params['where']=$time;
      $params['by']='nm_product';

      $productHash= $this->chartByparam($params);

      // product end

      // product start
      $params['select']=['nm_ishikawa', 'COUNT(`dev_investigation_ishikawa`.`id_ishikawa`)'];
      $params['table']='dev_ishikawa';
      $params['join']='dev_investigation_ishikawa';
      $params['on']='dev_ishikawa.id_ishikawa=dev_investigation_ishikawa.id_ishikawa';
      $params['where']=$time;
      $params['by']='nm_ishikawa';

      $ishikawatHash= $this->chartByparam($params);

      // product end

      // method start
      $params['select']=['nm_method', 'COUNT(`dev_investigation`.`id_method`)'];
      $params['table']='dev_method';
      $params['join']='dev_investigation';
      $params['on']='dev_method.id_method=dev_investigation.id_method';
      $params['where']=$time;
      $params['by']='nm_method';

      $methodHash= $this->chartByparam($params);

      // method end

      // decision start
      $params['select']=['nm_decision', 'COUNT(dev_batches.`id_deviation`)'];
      $params['table']='dev_decision';
      $params['join']='dev_batches';
      $params['on']='dev_decision.id_decision=dev_batches.id_decision';
      $params['where']=$time;
      $params['by']='dev_decision.id_decision';

      $decisionHash= $this->chartByparam($params);
      //print_r($decisionHash);exit();
      // decision end

      $Qulty= DevDeviation::find()->joinWith('declaration')->where(['id_type'=>'1'])->andWhere($time)->count();
      $hse= DevDeviation::find()->joinWith('declaration')->where(['id_type'=>'2'])->andWhere($time)->count();

      $manpower= DevDeviation::find()->innerJoin('dev_investigation_ishikawa','dev_deviation.id_deviation=dev_investigation_ishikawa.id_deviation')->where(['id_ishikawa'=>'1'])->andWhere($time)->count();
      $material= DevDeviation::find()->innerJoin('dev_investigation_ishikawa','dev_deviation.id_deviation=dev_investigation_ishikawa.id_deviation')->where(['id_ishikawa'=>'2'])->andWhere($time)->count();
      $method= DevDeviation::find()->innerJoin('dev_investigation_ishikawa','dev_deviation.id_deviation=dev_investigation_ishikawa.id_deviation')->where(['id_ishikawa'=>'3'])->andWhere($time)->count();
      $milieu= DevDeviation::find()->innerJoin('dev_investigation_ishikawa','dev_deviation.id_deviation=dev_investigation_ishikawa.id_deviation')->where(['id_ishikawa'=>'4'])->andWhere($time)->count();
      $machine= DevDeviation::find()->innerJoin('dev_investigation_ishikawa','dev_deviation.id_deviation=dev_investigation_ishikawa.id_deviation')->where(['id_ishikawa'=>'5'])->andWhere($time)->count();

      $low= DevRpn::find()->joinWith('deviation')->where(['id_level'=>'1'])->andWhere($time)->count();
      $hight= DevRpn::find()->joinWith('deviation')->where(['id_level'=>'2'])->andWhere($time)->count();
      $critical= DevRpn::find()->joinWith('deviation')->where(['id_level'=>'3'])->andWhere($time)->count();

      return $this->render('_tb',[/*'monthnum'=>$monthnum,'DevNbr'=>$DevNbr ,'DevNbr2'=>$DevNbr ,'Clsd'=>$ClsdRate,
              'qlt'=>$Qulty,'hse'=>$hse,'avg'=>$avg, 'selectedMonth'=>$selectedMonth, 'process'=>$processHash,'locality'=>$localityHash,'equipment'=>$equipmentHash,
              'product'=>$productHash,'ishikawa'=>$ishikawatHash, 'methodHash'=>$methodHash,'decision'=>$decisionHash,
              'avgByProcess'=>$avgByProcess,'avgByProduct'=>$avgByProduct,'avgByLocality'=>$avgByLocality,'avgByEquipment'=>$avgByEquipment,

                'low'=>$low,   'hight'=>$hight, 'critical'=>$critical

            ]);
      }
      else {

          $objectives= AdmDevObjectives::find()->all();
          //dd($objectives);
         //select COUNT(`id_deviation`) from `dev_deviation`
         //where (select DATEDIFF(CURDATE(),`creation_date`))<30
         $devover=(new \yii\db\Query())
         ->select("COUNT(`id_deviation`) as overdue")
         ->from('dev_deviation')
         ->where(['>',"DATEDIFF(CURDATE(),`creation_date`)",DEVOVER])
         ->one();

         $capaover=CapaDeclaration::find()->where(['<','state',CAPAIMPLEMENTATION])->andWhere(['<','implementation_date',date('Y-m-d')])->count();
         $efficover=CapaImplementation::find()->where(['>','DATEDIFF("'.date('Y-m-d').'",efficacity_review_date)','0'])->count();//->createcommand()->rawSql;//
         //dd($efficover);

         //$capaover=$capaover->createCommand()->getRawSql();
         //dd($capaover);
        ///$devover= DevDeviation::find()->select(["COUNT(`id_deviation`)"])->where(['<',"DATEDIFF(CURDATE(),`creation_date`)",'30'])->all();
        //dd($devover);
        $Clsd= DevDeviation::find()->where(['stateIn'=>CLOSED])->andWhere($time)->count();
        $DevNbr= DevDeviation::find()->where($time)->count();

        $CapaNbr= CapaDeclaration::find()->where($time)->count();
        $capaimp= CapaDeclaration::find()->where(['>=','state',CAPAIMPLEMENTATION])->andWhere($time)->count();
        $capaeff= CapaDeclaration::find()->where(['=','state',CAPAEFFICACITYREVIEW])->andWhere($time)->count();

        if($Clsd != 0)
        {//print_r($Clsd);exit();
        //print_r($DevNbr);exit();
        $ClsdRate=round($Clsd*100/$DevNbr);
        }

        if($capaeff != 0 && $capaimp !=0)
        {//print_r($Clsd);exit();
        //print_r($DevNbr);exit();
        $capaeff=round($capaeff*100/$capaimp);//$CapaNbr total capas vs only implemented
        }

        if($capaimp != 0)
        {//print_r($Clsd);exit();
        //print_r($DevNbr);exit();
        $capaimp=round($capaimp*100/$CapaNbr);
        }


        return $this->render('_tb',['objectives'=>$objectives,'DevNbr'=>$DevNbr ,'Clsd'=>$ClsdRate,'devover'=>$devover['overdue'],
                'avg'=>$avg,'capaNbr'=>$CapaNbr, 'selectedMonth'=>$selectedMonth, 'capaover'=>$capaover,
                'capaimp'=>$capaimp,'capaeff'=>$capaeff,'capaeff'=>$capaeff,'efficover'=>$efficover,
              ]);
      }


    }*/


    public function actionIndicators($month=null)
    {
      //dd($month);
      $DevNbr=$ClsdRate=$avg=0;
      $selectedMonth=($month!=null)?$month:date('m')[1];

      $month=(isset($month))?['MONTH(creation_date)'=>$month,'YEAR(creation_date)'=>date('Y') ]:['MONTH(creation_date)'=>date('m'),'YEAR(creation_date)'=>date('Y') ];
      $time= $month;

      $closing= DevDeviation::find()->joinWith('conclusion')->where($time)->andWhere(['stateIn'=>CLOSED])->all();  //DevConclusion::find()->all();

      if($closing != null)
      {
        $avg=0;
      foreach($closing as $clsed)
      {
          $avg+=$clsed->conclusion['deviation_duration'];
      }
      $avg= ceil($avg/count($closing));
      }

      $objectives= AdmDevObjectives::find()->all();
      //dd($objectives);
     //select COUNT(`id_deviation`) from `dev_deviation`
     //where (select DATEDIFF(CURDATE(),`creation_date`))<30
     $devover=(new \yii\db\Query())
     ->select("COUNT(`id_deviation`) as overdue")
     ->from('dev_deviation')
     ->where(['>',"DATEDIFF(CURDATE(),`creation_date`)",DEVOVER])
     ->one();

     $capaover=CapaDeclaration::find()->where(['<','state',CAPAIMPLEMENTATION])->andWhere(['<','implementation_date',date('Y-m-d')])->count();
     $efficover=CapaImplementation::find()->where(['>','DATEDIFF("'.date('Y-m-d').'",efficacity_review_date)','0'])->count();//->createcommand()->rawSql;//
     //dd($efficover);

     //$capaover=$capaover->createCommand()->getRawSql();
     //dd($capaover);
    ///$devover= DevDeviation::find()->select(["COUNT(`id_deviation`)"])->where(['<',"DATEDIFF(CURDATE(),`creation_date`)",'30'])->all();
    //dd($devover);
    $Clsd= DevDeviation::find()->where(['stateIn'=>CLOSED])->andWhere($time)->count();
    $DevNbr= DevDeviation::find()->where($time)->count();

    $CapaNbr= CapaDeclaration::find()->where($time)->count();
    $capaimp= CapaDeclaration::find()->where(['>=','state',CAPAIMPLEMENTATION])->andWhere($time)->count();
    $capaeff= CapaDeclaration::find()->where(['=','state',CAPAEFFICACITYREVIEW])->andWhere($time)->count();

    if($Clsd != 0)
    {//print_r($Clsd);exit();
    //print_r($DevNbr);exit();
    $ClsdRate=round($Clsd*100/$DevNbr);
    }

    if($capaeff != 0 && $capaimp !=0)
    {//print_r($Clsd);exit();
    //print_r($DevNbr);exit();
    $capaeff=round($capaeff*100/$capaimp);//$CapaNbr total capas vs only implemented
    }

    if($capaimp != 0)
    {//print_r($Clsd);exit();
    //print_r($DevNbr);exit();
    $capaimp=round($capaimp*100/$CapaNbr);
    }


    return $this->render('_tb',['objectives'=>$objectives,'DevNbr'=>$DevNbr ,'Clsd'=>$ClsdRate,'devover'=>$devover['overdue'],
            'avg'=>$avg,'capaNbr'=>$CapaNbr, 'selectedMonth'=>$selectedMonth, 'capaover'=>$capaover,
            'capaimp'=>$capaimp,'capaeff'=>$capaeff,'capaeff'=>$capaeff,'efficover'=>$efficover,
          ]);
    }
    public function actionMonthlyAnalysis($month=null)
    {

      $DevNbr=$ClsdRate=$avg=0;
      $selectedMonth=($month!=null)?$month:date('m')[1];

      $month=(isset($month))?['MONTH(creation_date)'=>$month,'YEAR(creation_date)'=>date('Y') ]:['MONTH(creation_date)'=>date('m'),'YEAR(creation_date)'=>date('Y') ];
      $time= $month;

      $closing= DevDeviation::find()->joinWith('conclusion')->where($time)->andWhere(['stateIn'=>CLOSED])->all();  //DevConclusion::find()->all();

      if($closing != null)
      {
        $avg=0;
      foreach($closing as $clsed)
      {
          $avg+=$clsed->conclusion['deviation_duration'];
      }
      $avg= ceil($avg/count($closing));
      }
      // process start

      // average month by process
      $params['select']=['nm_process', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['right join','dev_process','dev_declaration.process_step=dev_process.id_process']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='nm_process';

      $avgByProcess= $this->chartAVG($params);
      // average month end

      // average anum by product
      $params['select']=['nm_product', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['right join','dev_product','dev_declaration.product_code=dev_product.id_product']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='nm_product';

      $avgByProduct= $this->chartAVG($params);
      // average anum end


      // average anum by locality
      $params['select']=['nm_locality', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['right join','dev_locality','dev_declaration.locality=dev_locality.id_locality']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='nm_locality';

      $avgByLocality= $this->chartAVG($params);
      // average anum end

      // average anum by equipment
      $params['select']=['nm_equipment', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['right join','dev_equipment','dev_declaration.equipment_involved=dev_equipment.id_equipment']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='nm_equipment';

      $avgByEquipment= $this->chartAVG($params);

      $params['select']=['nm_process', 'COUNT(`process_step`)'];
      $params['join']='dev_declaration';
      $params['table']='dev_process';
      $params['on']='id_process=process_step';
      $params['where']=$time;
      $params['by']='nm_process';

      $processHash= $this->chartByparam($params);
      //print_r($processHash);exit();
      // process END

      // locality start
      $params['select']=['nm_locality', 'COUNT(`locality`)'];
      $params['table']='dev_locality';
      $params['join']='dev_declaration';
      $params['on']='id_locality=locality';
      $params['where']=$time;
      $params['by']='nm_locality';

      $localityHash= $this->chartByparam($params);

      // locality end

      // equimpent start
      $params['select']=['nm_equipment', 'COUNT(`equipment_involved`)'];
      $params['join']='dev_declaration';
      $params['table']='dev_equipment';
      $params['on']='id_equipment=equipment_involved';
      $params['where']=$time;
      $params['by']='nm_equipment';

      $equipmentHash= $this->chartByparam($params);

      // equipment end

      // product start
      $params['select']=['nm_product', 'COUNT(`product_code`)'];
      $params['join']='dev_declaration';
      $params['table']='dev_product';
      $params['on']='id_product=product_code';
      $params['where']=$time;
      $params['by']='nm_product';

      $productHash= $this->chartByparam($params);

      // product end

      // product start
      $params['select']=['nm_ishikawa', 'COUNT(`dev_investigation_ishikawa`.`id_ishikawa`)'];
      $params['table']='dev_ishikawa';
      $params['join']='dev_investigation_ishikawa';
      $params['on']='dev_ishikawa.id_ishikawa=dev_investigation_ishikawa.id_ishikawa';
      $params['where']=$time;
      $params['by']='nm_ishikawa';

      $ishikawatHash= $this->chartByparam($params);

      // product end

      // method start
      $params['select']=['nm_method', 'COUNT(`dev_investigation`.`id_method`)'];
      $params['table']='dev_method';
      $params['join']='dev_investigation';
      $params['on']='dev_method.id_method=dev_investigation.id_method';
      $params['where']=$time;
      $params['by']='nm_method';

      $methodHash= $this->chartByparam($params);

      // method end

      // decision start
      $params['select']=['nm_decision', 'COUNT(dev_batches.`id_deviation`)'];
      $params['table']='dev_decision';
      $params['join']='dev_batches';
      $params['on']='dev_decision.id_decision=dev_batches.id_decision';
      $params['where']=$time;
      $params['by']='dev_decision.id_decision';

      $decisionHash= $this->chartByparam($params);
      //print_r($decisionHash);exit();
      // decision end

      $Qulty= DevDeviation::find()->joinWith('declaration')->where(['id_type'=>'1'])->andWhere($time)->count();
      $hse= DevDeviation::find()->joinWith('declaration')->where(['id_type'=>'2'])->andWhere($time)->count();

      $manpower= DevDeviation::find()->innerJoin('dev_investigation_ishikawa','dev_deviation.id_deviation=dev_investigation_ishikawa.id_deviation')->where(['id_ishikawa'=>'1'])->andWhere($time)->count();
      $material= DevDeviation::find()->innerJoin('dev_investigation_ishikawa','dev_deviation.id_deviation=dev_investigation_ishikawa.id_deviation')->where(['id_ishikawa'=>'2'])->andWhere($time)->count();
      $method= DevDeviation::find()->innerJoin('dev_investigation_ishikawa','dev_deviation.id_deviation=dev_investigation_ishikawa.id_deviation')->where(['id_ishikawa'=>'3'])->andWhere($time)->count();
      $milieu= DevDeviation::find()->innerJoin('dev_investigation_ishikawa','dev_deviation.id_deviation=dev_investigation_ishikawa.id_deviation')->where(['id_ishikawa'=>'4'])->andWhere($time)->count();
      $machine= DevDeviation::find()->innerJoin('dev_investigation_ishikawa','dev_deviation.id_deviation=dev_investigation_ishikawa.id_deviation')->where(['id_ishikawa'=>'5'])->andWhere($time)->count();

      $low= DevRpn::find()->joinWith('deviation')->where(['id_level'=>'1'])->andWhere($time)->count();
      $hight= DevRpn::find()->joinWith('deviation')->where(['id_level'=>'2'])->andWhere($time)->count();
      $critical= DevRpn::find()->joinWith('deviation')->where(['id_level'=>'3'])->andWhere($time)->count();

      return $this->render('_tb',[/*'monthnum'=>$monthnum,*/'DevNbr'=>$DevNbr ,'DevNbr2'=>$DevNbr ,'Clsd'=>$ClsdRate,
              'qlt'=>$Qulty,'hse'=>$hse,'avg'=>$avg, 'selectedMonth'=>$selectedMonth, 'process'=>$processHash,'locality'=>$localityHash,'equipment'=>$equipmentHash,
              'product'=>$productHash,'ishikawa'=>$ishikawatHash, 'methodHash'=>$methodHash,'decision'=>$decisionHash,
              'avgByProcess'=>$avgByProcess,'avgByProduct'=>$avgByProduct,'avgByLocality'=>$avgByLocality,'avgByEquipment'=>$avgByEquipment,

                'low'=>$low,   'hight'=>$hight, 'critical'=>$critical

            ]);

    }

    public function actionYearlyAnalysis()
    {

      $DevNbr=$ClsdRate=$avg=0;

      $yearly=['YEAR(creation_date)'=>date('Y') ];
      //print_r($selectedMonth[1]);exit();
      //init
      //$month=(isset($month))?['MONTH(creation_date)'=>$month,'YEAR(creation_date)'=>date('Y') ]:['MONTH(creation_date)'=>date('m'),'YEAR(creation_date)'=>date('Y') ];
      //$time=($t=='y')?  $yearly:$month;
      $time= $yearly;

      $closing= DevDeviation::find()->joinWith('conclusion')->where($time)->andWhere(['stateIn'=>CLOSED])->all();  //DevConclusion::find()->all();

      if($closing != null)
      {
        $avg=0;
      foreach($closing as $clsed)
      {
          $avg+=$clsed->conclusion['deviation_duration'];
      }
      $avg= ceil($avg/count($closing));
      }

      // average anum by process
      $params['select']=['month(creation_date) as month','nm_process', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['inner join','dev_process','dev_declaration.process_step=dev_process.id_process']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='month,nm_process';

      $processAVGAnum= $this->chartAVGAnum($params);
      // average anum end

      // average anum by product
      $params['select']=['month(creation_date) as month','nm_product', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['inner join','dev_product','dev_declaration.product_code=dev_product.id_product']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='month,nm_product';

      $productAVGAnum= $this->chartAVGAnum($params);
      // average anum end

      // average anum by equipment
      $params['select']=['month(creation_date) as month','nm_equipment', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['inner join','dev_equipment','dev_declaration.equipment_involved=dev_equipment.id_equipment']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='month,nm_equipment';

      $equipmentAVGAnum= $this->chartAVGAnum($params);
      // average anum end

      // average anum by loclaity
      $params['select']=['month(creation_date) as month','nm_locality', 'CEIL(AVG(deviation_duration)) as aggregate '];
      $params['table']='dev_conclusion';
      $params['join']=[['inner join','dev_declaration','dev_declaration.id_deviation=dev_conclusion.id_deviation'],
                      ['inner join','dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation'],
                    ['inner join','dev_locality','dev_declaration.locality=dev_locality.id_locality']];//'dev_declaration';
      //$params['on']='';//'dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='month,nm_locality';

      $localityAVGAnum= $this->chartAVGAnum($params);
      // average anum end

      $raw = (new \yii\db\Query())
      ->select(['MONTH(dev_deviation.creation_date) as month','COUNT(id_deviation) as aggregate'])
      ->from('dev_deviation')
      ->where($time)
      ->groupBy('month')
      ->all();

      $monthnum= $this->chartByNbr($raw,false);
      //dd($raw);

      $raw = (new \yii\db\Query())
      ->select(['MONTH(dev_deviation.creation_date) as month','COUNT(id_deviation) as aggregate'])
      ->from('dev_deviation')
      ->where($time)
      ->groupBy('month')
      ->all();

      $monthadd= $this->chartByNbr($raw,true);
      //dd($raw);

      // monthly closing rate
      $raw = (new \yii\db\Query())
      ->select(['MONTH(date) as month','closing_rate as aggregate'])
      ->from('dev_monthly_closing')
      ->where(['YEAR(date)'=>date('Y')])
      ->groupBy(['month','aggregate'])
      ->all();
      //dd($raw);
      $closAnum= $this->chartByNbr($raw);
      //$closAnum='20,60,80,2,70,2,20';
      //dd($closAnum);


      $raw = (new \yii\db\Query())
      ->select(['MONTH(dev_deviation.creation_date) as month', 'CEIL(AVG(deviation_duration)) as aggregate '])
      ->from('dev_conclusion')
      ->leftJoin('dev_deviation','dev_conclusion.id_deviation=dev_deviation.id_deviation')
      ->where($time)
      ->groupBy('month')
      ->all();

      $avgAnum= $this->chartByNbr($raw);
      // average anum end

      //d($avgAnum);
      //dd($closingAnum);

      // capa anum
      $raw = (new \yii\db\Query())
      ->select( ['MONTH(creation_date) as month','COUNT(id) as aggregate'])
      ->from('capa_declaration')
      ->where($params['where'])
      ->groupBy('month')
      ->all();

      $capaAnum= $this->chartByQuery($raw);
      //dd($capaAnum);
      // capa anum end


      // process anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_process', 'COUNT(`process_step`)'];
      $params['join']='dev_declaration';
      $params['table']='dev_process';
      $params['on']='id_process=process_step';
      $params['where']=$time;
      $params['by']='month,nm_process';

      $processAnum= $this->chartByAnum($params);


      // end process anum


      // severity anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_level', 'COUNT(dev_rpn.id_level)'];
      $params['join']='dev_rpn';
      $params['table']='dev_level';
      $params['on']='dev_rpn.id_level=dev_level.id_level';
      $params['where']=$time;
      $params['by']='month,nm_level';

      $severityAnum= $this->chartByAnum($params);
      //severity anum end

      // Method anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_method', 'COUNT(dev_investigation.id_method)'];
      $params['join']='dev_investigation';
      $params['table']='dev_method';
      $params['on']='dev_method.id_method=dev_investigation.id_method';
      $params['where']=$time;
      $params['by']='month,nm_method';

      $methodAnum= $this->chartByAnum($params);
      //Method anum end
      //dd($methodAnum);
      // ihsikawa anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_ishikawa', 'COUNT(dev_investigation_ishikawa.id)'];
      $params['join']='dev_investigation_ishikawa';
      $params['table']='dev_ishikawa';
      $params['on']='dev_ishikawa.id_ishikawa=dev_investigation_ishikawa.id_ishikawa';
      $params['where']=$time;
      $params['by']='month,nm_ishikawa';

      $ishikawa= $this->chartByAnum($params);
      //dd($ishikawa);
      //ihsikawa anum end

      // locality anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_locality', 'COUNT(locality)'];
      $params['join']='dev_declaration';
      $params['table']='dev_locality';
      $params['on']='dev_declaration.locality=dev_locality.id_locality';
      $params['where']=$time;
      $params['by']='month,nm_locality';

      $localityAnum= $this->chartByAnum($params);
      // locality anum end

      // equipment anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_equipment', 'COUNT(equipment_involved)'];
      $params['join']='dev_declaration';
      $params['table']='dev_equipment';
      $params['on']='dev_declaration.equipment_involved=dev_equipment.id_equipment';
      $params['where']=$time;
      $params['by']='month,nm_equipment';

      $equipmentAnum= $this->chartByAnum($params);
      // equimpent anum end

      // product anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_product', 'COUNT(product_code)'];
      $params['join']='dev_declaration';
      $params['table']='dev_product';
      $params['on']='dev_declaration.product_code=dev_product.id_product';
      $params['where']=$time;
      $params['by']='month,nm_product';

      $productAnum= $this->chartByAnum($params);
      // product anum end

      // decision anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_decision', 'COUNT(dev_deviation.id_deviation)'];
      $params['table']='dev_decision';
      $params['join']='dev_batches';
      $params['on']='dev_decision.id_decision=dev_batches.id_decision';
      $params['where']=$time;
      $params['by']='month,nm_decision';

      $decisionAnum= $this->chartByAnum($params);
      // decision anum end

      // decision anum
      $params['select']=['MONTH(dev_deviation.creation_date) as month','nm_type', 'COUNT(dev_type.id_type)'];
      $params['table']='dev_type';
      $params['join']='dev_declaration';
      $params['on']='dev_declaration.id_type=dev_type.id_type';
      $params['where']=$time;
      $params['by']='month,nm_type';

      $categoryAnum= $this->chartByAnum($params);
      // decision anum end



      return $this->render('_tb',['monthnum'=>$monthnum,'monthadd'=>$monthadd,'closAnum'=>$closAnum,'capaAnum'=>$capaAnum,'processAnum'=>$processAnum,'severityAnum'=>$severityAnum,'methodAnum'=>$methodAnum,'localityAnum'=>$localityAnum,'avgAnum'=>$avgAnum,
              'equipmentAnum'=>$equipmentAnum,'productAnum'=>$productAnum,'decisionAnum'=>$decisionAnum,'categoryAnum'=>$categoryAnum,'ishikawa'=>$ishikawa,

              'processAVGAnum'=>$processAVGAnum,'productAVGAnum'=>$productAVGAnum,'equipmentAVGAnum'=>$equipmentAVGAnum,'localityAVGAnum'=>$localityAVGAnum,
              /*'manpower'=>$manpower, 'material'=>$material, 'method'=>$method, 'milieu'=>$milieu, 'machine'=>$machine,*/



            ]);
    }
    protected function chartAVGAnum($params)
    {

      $raw = (new \yii\db\Query())
      ->select($params['select'])
      ->from($params['table']);

      foreach ($params['join'] as $join )
      {
        $raw =$raw->join($join[0],$join[1],$join[2]);
      }

      $raw =$raw->where($params['where'])
      ->groupBy($params['by'])
      ->all();


        $data=ArrayHelper::map($raw,$params['select'][1],'aggregate', 'month' );

        //$data=ArrayHelper::map($raw,'month', 'aggregate'  );
        //$arr=[['id' => '123', 'name' => 'aaa']];
        //$data=ArrayHelper::map($arr,'id', 'name' );

        //print_r($params['select'][1]);
        //print_r($data);exit();

    //print_r($data);exit();

      $hash='';
      foreach ($data as $key => $value)
      {
          $hash.=$key.'=>';
          foreach ($value as $param => $count)
          {
            # code...
            $hash.= $param.'-'.$count.'#';
          }
          $hash=rtrim($hash,'#');
          $hash.='|';
      }



      $hash=rtrim($hash,'|');
      //echo $hash.'<br>';
      //print_r($hash);exit();
      // process END


      return $hash;


      //print_r($hash);exit();
    }

    protected function chartAVG($params)
    {
      $raw = (new \yii\db\Query())
      ->select($params['select'])
      ->from($params['table']);

      foreach ($params['join'] as $join )
      {
        $raw =$raw->join($join[0],$join[1],$join[2]);
      }

      $raw =$raw->where($params['where'])
      ->groupBy($params['by'])
      ->all();

      $data=ArrayHelper::map($raw,$params['select'][0], 'aggregate'  );

      //print_r($data);exit();
      $hash='';

      foreach ($data as $key => $value)
      {
          //is_null($value)?$value=0;
          $hash.=$key.'-'.$value.'#';
          //$hash.=$value.',';

      }
      $hash=rtrim($hash,'#');
      // if($params['select'][0]=='nm_locality')
      // {print_r($hash);exit();}
      return $hash;
    }
    protected function chartByNbr($raw, $add=false)
    {
/*
      $raw = (new \yii\db\Query())
      ->select($params['select'])
      //->from('dev_deviation')->rightJoin($params['table'],$params['table'].'.id_deviation=dev_deviation.id_deviation')//'dev_process', 'id_process=process_step')//->innerJoin('dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation')
      ->from($params['table'])//'dev_process', 'id_process=process_step')//->innerJoin('dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation')
      ->leftJoin('dev_deviation',$params['table'].'.id_deviation=dev_deviation.id_deviation')
      ->where($params['where'])
      ->groupBy($params['by'])
      ->all();*/

      $data=ArrayHelper::map($raw,'month', 'aggregate'  );

      $hash='';
      $previous=0;

      if($add)
      {
        foreach ($data as $key => $value)
        {
            //$hash.=$key.'-'.$value.'#';
            $hash.=$value+$previous.',';
            $previous+=$value;
        }
      }
      else
      {
        foreach ($data as $key => $value)
        {
            //$hash.=$key.'-'.$value.'#';
            $hash.=$value.',';
         }
      }

      $hash=rtrim($hash,',');
      //print_r($hash);exit();
      return $hash;
    }

    protected function chartByQuery($raw)
    {

      $data=ArrayHelper::map($raw,'month', 'aggregate'  );

      $hash='';
      foreach ($data as $key => $value)
      {
          //$hash.=$key.'-'.$value.'#';
          $hash.=$value.',';

      }
      $hash=rtrim($hash,',');
      //print_r($hash);exit();
      return $hash;
    }

    protected function chartByparam($params)//$select,$table,$join,$on,$by)
    {
      $raw = (new \yii\db\Query())
      ->select($params['select'])
      //->from('dev_deviation')->rightJoin($params['table'],$params['table'].'.id_deviation=dev_deviation.id_deviation')//'dev_process', 'id_process=process_step')//->innerJoin('dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation')
      ->from($params['table'])->leftJoin($params['join'],$params['on'])//'dev_process', 'id_process=process_step')//->innerJoin('dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation')
      ->leftJoin('dev_deviation',$params['join'].'.id_deviation=dev_deviation.id_deviation')
      ->where($params['where'])
      ->groupBy($params['by'])
      ->all();
      //->createCommand();

//print_r($raw->sql);exit();
      $data=ArrayHelper::map($raw, $params['select'][0], $params['select'][1] );
//print_r($data);
      $hash='';
      foreach ($data as $key => $value)
      {
          $hash.=$key.'-'.$value.'#';

      }
      $hash=rtrim($hash,'#');
      //echo $hash.'<br>';
      //print_r($processHash);exit();
      // process END

      return $hash;
    }

    protected function chartByAnum($params)
    {
      $raw = (new \yii\db\Query())
      ->select($params['select'])
      //->from('dev_deviation')->rightJoin($params['table'],$params['table'].'.id_deviation=dev_deviation.id_deviation')//'dev_process', 'id_process=process_step')//->innerJoin('dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation')
      ->from($params['table'])
      ->leftJoin($params['join'],$params['on']) //'dev_process', 'id_process=process_step')//->innerJoin('dev_deviation','dev_declaration.id_deviation=dev_deviation.id_deviation')
      ->innerJoin('dev_deviation',$params['join'].'.id_deviation=dev_deviation.id_deviation')
      ->where($params['where'])
      ->groupBy($params['by'])
      ->all();


        $data=ArrayHelper::map($raw,$params['select'][1], $params['select'][2],'month' );

        //$data=ArrayHelper::map($raw,'month', 'aggregate'  );
        //$arr=[['id' => '123', 'name' => 'aaa']];
        //$data=ArrayHelper::map($arr,'id', 'name' );

        //print_r($params['select'][1]);
        //print_r($data);exit();

//print_r($data);exit();

      $hash='';
      foreach ($data as $key => $value)
      {
          $hash.=$key.'=>';
          foreach ($value as $param => $count)
          {
            # code...
            $hash.= $param.'-'.$count.'#';
          }
          $hash=rtrim($hash,'#');
          $hash.='|';
      }



      $hash=rtrim($hash,'|');
      //echo $hash.'<br>';
      //print_r($processHash);exit();
      // process END


      return $hash;


      //print_r($hash);exit();
    }
}
