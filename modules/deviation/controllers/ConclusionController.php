<?php

namespace app\modules\deviation\controllers;

use Yii;
use Psy\VarDumper\Dumper;
use app\models\User;
use app\modules\deviation\models\DevDeviation;
use app\modules\deviation\models\DevDraft;
use app\modules\deviation\models\DevDeclaration;
use app\modules\deviation\models\DevInvestigation;
use app\modules\deviation\models\DevConclusion;
use app\modules\deviation\models\DevDeviationSearch;
use app\modules\deviation\models\DevBatches;
use app\modules\deviation\models\DevSignatories;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use app\modules\deviation\models\Files;
use yii\web\UploadedFile;
use app\modules\deviation\models\DevFilters;
use app\modules\deviation\models\DevActions;
use app\modules\deviation\models\DevRpn;
use app\modules\deviation\models\DevPreRpn;
use app\modules\deviation\models\DevReasons;
use app\models\Profiles;
use app\models\Process;
use app\models\Assignments;
use yii\base\Event;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\modules\deviation\models\DevLevel;
use app\modules\deviation\models\DevSeverity;
use app\modules\deviation\models\DevOccurence;
use app\modules\deviation\models\DevDetectability;
use app\modules\deviation\models\DevEstimatedLvl;
use app\modules\deviation\models\CapaDeclaration;
use app\modules\deviation\models\CapaImplementation;
use app\modules\deviation\models\CapaEfficacity;
use kartik\mpdf\Pdf;
use yii\helpers\VarDumper;



Class ConclusionController extends Controller
{
  public $layout = 'deviation';

  public function behaviors()
  {
      return [
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
          /*'access' => [
                'class' => AccessControl::className(),
                //'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','draft','create','view','investigation','investigation-data',
                        'conclusion','declaration-data','conclusion-data','data','download'],
                        'roles' => ['@'],
                    ],
                ],
            ],*/
      ];





  }
/*
  public function beforeAction($action)
  {



      $act= $this->action->id;
      $act=str_replace('-',' ',$act);
      $auth = \Yii::$app->authManager;
      $id=Yii::$app->user->getId();
      $allow= \Yii::$app->user->can($act);
      try {
          if (! $allow ){
            throw  new ForbiddenHttpException('You are not allowed to access this page');
          }
      } catch (ErrorException $e) {
           echo 'not allowed';
           print_r($e);
      }
      return true;


  }*/

  public function actionNotAllowed()
  {
    return $this->render('//site/_not_allowed');
  }


public function actionViewConclusion()
{
          /*$batches= DevBatches::find()->where(['id_deviation'=>'370'])->all();
          print_r($batches);*/
  if (Yii::$app->request->isAjax   ) //|| 1
  {
    /*$notyet=DevSignatories::findOne(['id_deviation'=>'380','state'=>null]);

   print_r(isset($notyet));exit();*/
  //echo "lala";
  $id=Yii::$app->request->post()['id'];
  //$id=380;
  $model=  DevConclusion::find()->where(['id_deviation' => $id])->one();
  $state= DevDeviation::findOne($id);
  $batches= DevBatches::find()->With('decision')->where(['id_deviation'=>$id])->all();
  $signatories= DevSignatories::find()->with('user','notice')->where(['id_deviation'=>$id]);//->all();
  $signProviders= new ActiveDataProvider(['query'=>$signatories,'pagination'=>false,'sort'=>false]);

  $capas=CapaDeclaration::find()->where(['deviation_id'=>$id])->all();
  $capaslinks=$capas?'<div class="well col-md-12">':'';
  foreach ($capas as $key => $value) {
    $capaslinks.=' <a class="label label-info" href="/deviation/capa/view-capa?id='.$value->id.'">'.$value->title.'</a>  ';
  }
  $capaslinks.=$capas?'</div>':'';
  //dd($capaslinks);
  if($model && $state->stateIn==CONCLUSION)
  {
    return $this->renderAjax('_conclusion',['model'=> $model, 'batches'=>$batches, 'signProviders'=>$signProviders ]);
  }
  elseif($model && ($state->stateIn==CLOSED || $state->stateIn==SIGNING) )
  {

    return $this->renderAjax('_view-conclusion',['model'=> $model, 'batches'=>$batches,'capas'=>$capaslinks, 'signProviders'=>$signProviders ]);
  }
  else
  {
    // $model= ( !$model )? new DevInvestigation() : $model ;
    // $model->id_deviation=$id;
    // return $this->renderAjax('_investigation',['model'=> $model ]);
    //print_r ($model3);
     return $this->runAction('close-deviation',['id'=>$id]);
  }


  }
}

public function actionCloseDeviation($id=null)
{
    //print_r(Yii::$app->request->post());exit();
  /*
  print_r($DevConclusion);
        print_r(Yii::$app->request->method);exit();*/
  if ($id && Yii::$app->request->isAjax)  //|| 1
  {
  //echo "lala";
  //$id=($id)?$id:Yii::$app->request->post()['id'];
  $model=  DevConclusion::find()->where(['id_deviation' => $id])->one();


   $model= ( !$model )? new DevConclusion()  : $model;
   $model->id_deviation=$id;
   $batches= DevBatches::find()->where(['id_deviation'=>$id])->all();
   $signatories= DevSignatories::find()->with('user','notice')->where(['id_deviation'=>$id]);//->all();
   $signProviders= new ActiveDataProvider(['query'=>$signatories,'pagination'=>false,'sort'=>false]);
   return $this->renderAjax('_conclusion',['model'=> $model, 'batches'=>$batches, 'signProviders'=>$signProviders ]);
  //print_r ($model3);
  }
  elseif(($post=Yii::$app->request->post()) && !isset($post['initCapa']) ) //
  {
    //print_r($post['save']);exit();
    $id=$post['DevConclusion']['id_deviation'];
    //$check= new DevConclusion();
    $check = DevConclusion::findOne(['id_deviation'=>$id]);

   //isset($post['create'])

        $model= ($chk=DevConclusion::findOne(['id_deviation'=>$id]))? $chk : new DevConclusion();
        !$chk?$model->id_deviation= $id:'';

      isset($post['save'])? $model->scenario=  DevConclusion::SCENARIO_DRAFT:'';
      if( $model->load(Yii::$app->request->post())  && $model->save() && DevConclusion::saveDecisions($post['decision']) )
        {


        #work here for empty decision if not draft
        //dd($id);
        if(!isset($post['save']))
        {
          $update_state= DevDeviation::find()->with('state')->where(['id_deviation'=>$model->id_deviation])->one();

           $notyet=DevSignatories::findOne(['id_deviation'=>$model->id_deviation,'state'=>null]);

          //print_r($notyet);exit();
          $update_state->stateIn=
          (!isset($notyet))?   CLOSED:SIGNING;  // 3= investigation state


          if($update_state->save())
          {

            (!isset($notyet))?
            Yii::info(\Yii::t('app','Conclusion finished for deviation N° ').$id.'.', 'user') // log launch deviation
            :Yii::info(\Yii::t('app','Conclusion in signing phase for deviation N° ').$id.'.', 'user'); // log launch deviation

            DevConclusion::setDeviationDuration($model->id_deviation);
            print_r($update_state->getErrors());
          }

        }
        elseif(isset($post['save'])) {
          Yii::info(\Yii::t('app','Conclusion saved for deviation N° ').$id.'.', 'user'); // log launch deviation

        }



        return $this->redirect(['deviation/view-deviation', 'id' => $id]);



        }else print_r($model->getErrors());

      //exit();


  }
  elseif(isset($post['initCapa']))
  {
    //echo "capa";exit();
    $capa=new CapaDeclaration();
    $capa->deviation_id=$id;
    /*return $this->render('createCapa',[
      'model'=>$capa,
    ]);*/
    $id=$post['DevConclusion']['id_deviation'];
    //return $this->runAction('CapaController/create-capa',['id'=>$id]);
    return $this->redirect(['/deviation/capa/create-capa','id'=>$id]);
  }





}

public function actionSignDeviation()
{
    if(Yii::$app->request->isAjax  ) //|| 1
    {
      $post= Yii::$app->request->post();

       /*$post['action']='sign'; //testing
      $post['id']='1';// testing
      $post['text']='blbla'; //testing
      $post['password']='1';
                $post['iddev']='379';
      $post['DevConclusion']['comment']='1';
      $post['DevConclusion']['final_decision']='1';*/

      if($post['action']=='load')
      {
        $signature= DevSignatories::findOne(['id_deviation'=>$post['iddev'], 'state'=>'1'  ]);

        if( (isset($signature->state) && $signature->state=='1') ||  (!empty($post['final']) && !empty($post['comment']) && isset($post['decision']) && ($post['decision']!='false') ) )
        return $this->renderAjax('_sign',['id'=>$post['id'],'user'=>$post['username'] ]);
        else return $this->renderAjax('_sign_error',['msg'=>Yii::t('app','Make sure all fields are filled !')]);
      }
      elseif($post['action']=='disapprove')
      {

        $user= User::findIdentity($post['id']);
        //print_r($user);
        $valid=$user->validatePassword($post['password']);

        if($valid)
        {
          $id=$post['iddev'];
          DevSignatories::updateAll(['state' => null,'id_notice'=>null,'comment'=>null], ['id_deviation'=>$id ]);
          $dev=DevDeviation::findOne(['id_deviation'=>$id ]);
          $dev->stateIn=CONCLUSION;
          $dev->save();

          Yii::info(\Yii::t('app','Conclusion disapproved for deviation N° ').$id.'.', 'user'); // log launch deviation


          return "disapproved";
        }
        else return \Yii::t('app','Wrong password !');


      }
      elseif($post['action']=='sign')
      {

        //$user= new User();
        $user= User::findIdentity($post['id']);
        //print_r($user);
        $valid=$user->validatePassword($post['password']);
        //echo $valid;
        if($valid)
        {
          $signature= DevSignatories::find()->where(['id_deviation'=>$post['iddev'], 'id_user'=>$post['id']  ])->one();

          /*$array=['DevConclusion'=>array('2' ,'6' )];
          $this->runAction('close-deviation',$array);
          */
          $signature->state=SIGNAPPROVED;
          $signature->comment=$post['text'];
          $signature->id_notice='1';
          $signature->save();

          Yii::info(\Yii::t('app','Conclusion approved for deviation N° ').$post['iddev'].'.', 'user'); // log launch deviation



          if(DevSignatories::isSigned($post['iddev'])) //return 'scc';
          {
             $state=CLOSED;
             DevDeviation::setState( $post['iddev'] , $state );
             DevConclusion::setDeviationDuration($post['iddev']);

             Yii::info(\Yii::t('app','Conclusion finished for deviation N° ').$post['iddev'].'.', 'user'); // log launch deviation


          }
          return 'success';
        }
        else return 'Wrong password !';//'id:'.$user->id_user;//

      }
    }
}

}
