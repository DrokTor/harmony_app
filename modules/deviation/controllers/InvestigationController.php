<?php

namespace app\modules\deviation\controllers;

use Yii;
use Psy\VarDumper\Dumper;
use app\models\User;
use app\modules\deviation\models\DevDeviation;
use app\modules\deviation\models\DevDraft;
use app\modules\deviation\models\DevDeclaration;
use app\modules\deviation\models\DevInvestigation;
use app\modules\deviation\models\DevConclusion;
use app\modules\deviation\models\DevDeviationSearch;
use app\modules\deviation\models\DevBatches;
use app\modules\deviation\models\DevSignatories;
use app\modules\deviation\models\DevInvestigationIshikawa;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use app\modules\deviation\models\Files;
use yii\web\UploadedFile;
use app\modules\deviation\models\DevFilters;
use app\modules\deviation\models\DevActions;
use app\modules\deviation\models\DevRpn;
use app\modules\deviation\models\DevPreRpn;
use app\modules\deviation\models\DevReasons;
use app\models\Profiles;
use app\models\Process;
use app\models\Assignments;
use yii\base\Event;
use yii\web\View;
use yii\helpers\ArrayHelper;
use app\modules\deviation\models\DevLevel;
use app\modules\deviation\models\DevSeverity;
use app\modules\deviation\models\DevOccurence;
use app\modules\deviation\models\DevDetectability;
use app\modules\deviation\models\DevEstimatedLvl;
use app\modules\deviation\models\CapaDeclaration;
use app\modules\deviation\models\CapaImplementation;
use app\modules\deviation\models\CapaEfficacity;
use app\modules\deviation\models\DevApprovals;
use kartik\mpdf\Pdf;
use yii\helpers\VarDumper;



Class InvestigationController extends Controller
{
  public $layout = 'deviation';
  public $url = '/deviation/investigation/';

  public function behaviors()
  {
      return [
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['post'],
              ],
          ],
          /*'access' => [
                'class' => AccessControl::className(),
                //'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','draft','create','view','investigation','investigation-data',
                        'conclusion','declaration-data','conclusion-data','data','download'],
                        'roles' => ['@'],
                    ],
                ],
            ],*/
      ];





  }
/*
  public function beforeAction($action)
  {

//dd($this);
/*
      $act= $this->action->id;
      $act=str_replace('-',' ',$act);
      $auth = \Yii::$app->authManager;
      $id=Yii::$app->user->getId();
      $allow= \Yii::$app->user->can($act);
      try {
          if (! $allow ){
            //throw  new ForbiddenHttpException('You are not allowed to access this page');
            return $this->render('//site/_not_allowed');

          }
      } catch (ErrorException $e) {
           echo 'not allowed';
           print_r($e);
      }
      return true;


  }*/
  public function actionNotAllowed()
  {
    return $this->render('//site/_not_allowed');
  }
  public function actionViewInvestigation()//$id
  {
    //echo "lala";
 //print_r(Yii::$app->request->get('id'));
 //exit();
    if (Yii::$app->request->isAjax )//|| 1
    {

    //$id=285;
    $id=Yii::$app->request->post()['id'];
    $state= DevDeviation::findOne($id);
    $model=  DevInvestigation::find()->With('method','ishikawa'/*,'level'*/)->where(['id_deviation' => $id])->one();
    $ishikawa=($model)? $model->selectedIshikawa(true):'';

    $reason= DevReasons::findOne(['id_deviation' => $id]);
    //$reason=$reason?$reason:'';

    $files=new files();
    //$files->files=UploadedFile::getInstances($files, 'files');//
    if($model)
    {
      $files->list= files::findFiles($model->id_investigation, 'investigation') ;
      $files->url=$this->url;
      $files->type='investigation';
      $files->id=$model->id_investigation;
    }


    if($reason && $reason->close_reason)
    {
      echo \Yii::t('app','The deviation was closed in Declaration state.');
    }
    elseif($model && $model->skipped=='true') // just for now, maybe create a view for this later
    {
      echo \Yii::t('app','The investigation step was not needed and therefore it was skipped.');
    }
    elseif($model && $state->stateIn>INVESTIGATION)
    {
      $elvl=DevPreRpn::findOne(['id_deviation'=>$id]);

      switch ($elvl->id_level)
      {
        case 1 : $elvl='eLvlMinor';break;
        case 2 : $elvl='eLvlMedium';break;
        case 3 : $elvl='eLvlMajor';break;
      }
      return $this->renderAjax('_view-investigation',['model'=> $model,'ishikawa'=>$ishikawa,'model_file'=>$files,'elvl'=>$elvl ]);
    }elseif($state->stateIn==DECLARATION)
    {
      //echo "decide to investigate";
      return $this->runAction('require-investigation',['id'=>$id]);
    }
    else
    {
      // $model= ( !$model )? new DevInvestigation() : $model ;
      // $model->id_deviation=$id;
      // return $this->renderAjax('_investigation',['model'=> $model ]);
      //print_r ($model3);
       if(\Yii::$app->user->can('investigate deviation'))
        return $this->runAction('investigate-deviation',['id'=>$id]);
       elseif($model)
        return $this->renderAjax('_view-investigation',['model'=> $model,'ishikawa'=>$ishikawa,'model_file'=>$files,'elvl'=>null ]);
       else {
        echo \yii\Alert::widget([
              'options' => [
                  'class' => 'alert-info',
              ],
              'body' => 'Say hello...',
          ]);
        }
    }


    }
  }

  public function actionRequireInvestigation($id)
  {
    if ($id && Yii::$app->request->isAjax)
    {
      $severity=ArrayHelper::map(DevSeverity::find()->asArray()->all(),'id_severity','nm_severity');
      $occurence=ArrayHelper::map(DevOccurence::find()->asArray()->all(),'id_occurence','nm_occurence');
      $detectability=ArrayHelper::map(DevDetectability::find()->asArray()->all(),'id_detectability','nm_detectability');
      return $this->renderAjax('_require-investigation',['severity'=>$severity,'occurence'=>$occurence,'detectability'=>$detectability]);
    }
    elseif ($id && Yii::$app->request->isPost)
    {
                $state= DevDeviation::find()->with('declaration')->where(['id_deviation'=>$id])->one();
                //print_r(Yii::$app->request->post());exit();
      if(Yii::$app->request->post('require'))
      {
        $state->stateIn=INVESTIGATION;
        $state->save();



        Yii::info(\Yii::t('app','Investigation opened for deviation N° ').$id.'.', 'user'); // log launch deviation


        $post=Yii::$app->request->post();


        $user=User::findIdentity($post['assign']);
        DevSignatories::add($id,$user->id_user);

        Yii::$app->mailer->compose()
        ->setFrom('deviation@harmony.com')
        ->setTo($user->email)
        ->setSubject('Deviation investigation N°'.$id)
        //->setTextBody('Plain text content')
        ->setHtmlBody('<h1>'.$state->declaration['deviation_title'].'</h1><p><a href="'.Yii::getAlias('@devUrl').'/view-deviation?id='.$id.'">'.\Yii::t('app',"The deviation {id}</a> is waiting for your investigation.",['id'=>$id]).'</p>')
        ->send();

        Yii::info(\Yii::t('app','Investigation assigned to [{username}] for deviation N° ',['username'=>$user->username]).$id.'.', 'user'); // log launch deviation


        if($post['preseverity'] && $post['preoccurence'] && $post['predetectability'])
        {
          $rpn=new DevPreRpn();
          $rpn->id_deviation=$id;
          $sev= $rpn->id_severity=$post['preseverity'];
          $occ= $rpn->id_occurence=$post['preoccurence'];
          $det= $rpn->id_detectability=$post['predetectability'];

          $rpnResult=$sev*$occ*$det;

           if($rpnResult>=25)
           {
             $rpnResult=3;
           }
           elseif($rpnResult>=5)
           {
             $rpnResult=2;
           }
           else
           {
             $rpnResult=1;
           }
           //print_r($rpn);
           $rpn->id_level=$rpnResult;
           $rpn->save();

           Yii::info(\Yii::t('app','Deviation N° {id} prelimenary rate [{lvl}].',['id'=>$id,'lvl'=>$rpn->lvl->nm_level]), 'user'); // log launch deviation

           /*
           if($rpn->save())
           {
             return $this->redirect(['view-deviation', 'id' =>$id]);
           }else{
             echo "error";
           }*/
        }
      }
      elseif(Yii::$app->request->post('skip'))
      {

        //$reason=  new DevReasons();
        ///print_r(Yii::$app->request->post());exit();
        $post=Yii::$app->request->post();
        if($post['severity'] && $post['occurence'] && $post['detectability'])
        {
          $rpn=new DevRpn();
          $rpn->id_deviation=$id;
          $sev= $rpn->id_severity=$post['severity'];
          $occ= $rpn->id_occurence=$post['occurence'];
          $det= $rpn->id_detectability=$post['detectability'];

          $rpnResult=$sev*$occ*$det;

           if($rpnResult>=12)
           {
             $rpnResult=3;
           }
           elseif($rpnResult>=6)
           {
             $rpnResult=2;
           }
           else
           {
             $rpnResult=1;
           }
           //print_r($rpn);
           $rpn->id_level=$rpnResult;
           $rpn->save();

           Yii::info(\Yii::t('app','Final rate for deviation N° ').$id.' ['.$rpn->lvl->nm_level.'].', 'user'); // log launch deviation


           /*
           if($rpn->save())
           {
             return $this->redirect(['view-deviation', 'id' =>$id]);
           }else{
             echo "error";
           }*/
        }
        $findreason=DevReasons::findOne(['id_deviation'=>$id]);
        $reason= $findreason?  $findreason:new DevReasons();
        $invest= new DevInvestigation();
        $invest->scenario= DevInvestigation::SCENARIO_SKIP ;
        $invest->id_deviation=$id;
        $invest->skipped='true';
        if(!$invest->save())
        {
        print_r($invest->getErrors());exit();
        }
        $state->stateIn=CONCLUSION;
        $state->save();
        $reason->id_deviation=$id;
        $reason->skip_investigation=Yii::$app->request->post('skip');
        $reason->id_user=  Yii::$app->user->identity->id_user;
        $reason->save();

        Yii::info(\Yii::t('app','Investigation skipped for deviation N° ').$id.' by [reason: '.$reason->skip_investigation.'].', 'user'); // log launch deviation

      }
      $approvals= DevApprovals::findOne(['deviation_id'=>$id]);
      $approvals->expert=Yii::$app->user->identity->id_user;
      $approvals->expertiseDate=date('Y-m-d H:i');
      $approvals->save();

      return $this->redirect(['/deviation/deviation/view-deviation', 'id' => $id]);
    }//else echo 'blabla';
  }
  public function actionInvestigateDeviation($id=null)
  {
    //dd(Yii::$app->request->post());
    //echo $id;exit();
    //print_r(Yii::$app->request->get('id'));exit();
    if ($id && Yii::$app->request->isAjax  )//
    {
      //print_r(Yii::$app->request);
      //echo $id;exit();
    //echo "lala";
    //$id=($id)?$id:Yii::$app->request->post()['id'];
    $model=  DevInvestigation::find()->With('method','ishikawa'/*,'level'*/)->where(['id_deviation' => $id])->one();
    $model_file = new Files();
    if($model)
    {
      $model_file->files=UploadedFile::getInstances($model_file, 'files');//
      $model_file->list=files::findFiles($model->id_investigation, 'investigation');
      $model_file->url=$this->url;
      $model_file->type='investigation';
      $model_file->id=$model->id_investigation;
      //dd($model_file);
    }

    $selected= ( $model )? $model->selectedIshikawa():'';

    $model= ( !$model )? new DevInvestigation() : $model ;
    $model->id_deviation=$id;

    $severity=ArrayHelper::map(DevSeverity::find()->asArray()->all(),'id_severity','nm_severity');
    $occurence=ArrayHelper::map(DevOccurence::find()->asArray()->all(),'id_occurence','nm_occurence');
    $detectability=ArrayHelper::map(DevDetectability::find()->asArray()->all(),'id_detectability','nm_detectability');



    return $this->renderAjax('_investigation',['model'=> $model,'model_file'=>$model_file,'severity'=>$severity,'detectability'=>$detectability,'occurence'=>$occurence, 'selected'=>$selected ]);
    //print_r ($model3);
    }
    else
    {

 //print_r($_FILES);exit();
      $post=Yii::$app->request->post();
      //print_r($post);exit();
      $id=$post['DevInvestigation']['id_deviation'];
      $check= new DevInvestigation();
      $check = DevInvestigation::findOne(['id_deviation'=>$id]);

    if($check === null)
    {
        $model= new DevInvestigation();
        $model->id_deviation= $id;
        $model_file = new Files();
        //print_r($check);
        //print_r($model);exit();
        (isset($post['save']))?$model->scenario=DevInvestigation::SCENARIO_DRAFT:'';

      if( $model->load($post)  && $model->save() )
        {//dd($model->getDirtyAttributes());
          isset($post['DevInvestigation']['id_ishikawa'])?
          $model->loadIshikawa($post['DevInvestigation']['id_ishikawa']):'';

          $model_file->files = UploadedFile::getInstances($model_file, 'files');
        //  print_r($model_file);exit();
        //  echo"blabla";exit();
          if(!$model_file->upload($model->id_investigation,'investigation'))
          { //           print_r($model_file);exit();
            //
            echo "error";
          }


        if(!isset($post['save']))
        {
          $update_state= DevDeviation::find()->with('state')->where(['id_deviation'=>$model->id_deviation])->one();
          $update_state->stateIn=CONCLUSION; // 2= investigation state

          $approvals= DevApprovals::findOne(['deviation_id'=>$id]);
          $approvals->investigator=Yii::$app->user->identity->id_user;
          $approvals->investigationDate=date('Y-m-d H:i');
          $approvals->save();

          if($update_state->save())
        {
          Yii::info(\Yii::t('app','Investigation finished for deviation N° ').$id.'.', 'user'); // log launch deviation

          //$post=Yii::$app->request->post();
          if($post['severity'] && $post['occurence'] && $post['detectability'])
          {
            $rpn=new DevRpn();
            $rpn->id_deviation=$id;
            $sev= $rpn->id_severity=$post['severity'];
            $occ= $rpn->id_occurence=$post['occurence'];
            $det= $rpn->id_detectability=$post['detectability'];

            $rpnResult=$sev*$occ*$det;

             if($rpnResult>=12)
             {
               $rpnResult=3;
             }
             elseif($rpnResult>=6)
             {
               $rpnResult=2;
             }
             else
             {
               $rpnResult=1;
             }
             //print_r($rpn);
             $rpn->id_level=$rpnResult;
             $rpn->save();

             Yii::info(\Yii::t('app','Final rate for deviation N° ').$id.' ['.$rpn->lvl->nm_level.'].', 'user'); // log launch deviation

             /*
             if($rpn->save())
             {
               return $this->redirect(['view-deviation', 'id' =>$id]);
             }else{
               echo "error";
             }*/
          }
          return $this->redirect(['/deviation/deviation/view-deviation', 'id' => $id]);
        }else{
            print_r($update_state->getErrors());
          //  print_r($update_state);

        }

      }else
      {
        Yii::info(\Yii::t('app','Investigation saved for deviation N° ').$id.'.', 'user'); // log launch deviation

        return $this->redirect(['/deviation/deviation/view-deviation', 'id' => $id]);
      }



      }else
      {
        print_r($model->getErrors());

      exit();
      }
    }else
    {
      (isset($post['save']))?$check->scenario=DevInvestigation::SCENARIO_DRAFT:'';
      $old= clone $check;
      $check->load($post);
      $dirty=$check->getDirtyAttributes();
      //dd($old);
      //dd($check);
      //dd($check->getDirtyAttributes());
      if(  $check->save())
      {
        $this->logChanges($check,$dirty,$old);
        //dd($post);
        isset($post['DevInvestigation']['id_ishikawa'])?
        $check->loadIshikawa($post['DevInvestigation']['id_ishikawa']):'';

        $model_file = new Files();
        $model_file->files = UploadedFile::getInstances($model_file, 'files');
        //  print_r($model_file);exit();
        //  echo"blabla";exit();
          if(!$model_file->upload($check->id_investigation,'investigation'))
          {
            echo "error file";
          }


        if(!isset($post['save']))
        {
          $update_state= DevDeviation::find()->with('state')->where(['id_deviation'=>$check->id_deviation])->one();
          $update_state->stateIn=CONCLUSION; // 2= investigation state



          if($update_state->save())
        {

          $approvals= DevApprovals::findOne(['deviation_id'=>$update_state->id_deviation]);
          $approvals->investigator=Yii::$app->user->identity->id_user;
          $approvals->investigationDate=date('Y-m-d H:i');
          $approvals->save();

          Yii::info(\Yii::t('app','Investigation finished for deviation N° ').$id.'.', 'user'); // log launch deviation

          if($post['severity'] && $post['occurence'] && $post['detectability'])
          {
            $rpn=new DevRpn();
            $rpn->id_deviation=$id;
            $sev= $rpn->id_severity=$post['severity'];
            $occ= $rpn->id_occurence=$post['occurence'];
            $det= $rpn->id_detectability=$post['detectability'];

            $rpnResult=$sev*$occ*$det;

             if($rpnResult>=12)
             {
               $rpnResult=3;
             }
             elseif($rpnResult>=6)
             {
               $rpnResult=2;
             }
             else
             {
               $rpnResult=1;
             }
             //print_r($rpn);
             $rpn->id_level=$rpnResult;
             $rpn->save();

             Yii::info(\Yii::t('app','Final rate for deviation N° ').$id.' ['.$rpn->lvl->nm_level.'].', 'user'); // log launch deviation

             /*
             if($rpn->save())
             {
               return $this->redirect(['view-deviation', 'id' =>$id]);
             }else{
               echo "error";
             }*/
          }

          return $this->redirect(['/deviation/deviation/view-deviation', 'id' => $id]);
        }else{
            print_r($update_state->getErrors());
          //  print_r($update_state);

        }
        }
        else{
          Yii::info(\Yii::t('app','Investigation saved for deviation N° ').$id.'.', 'user'); // log launch deviation

        }
      }
      return $this->redirect(['/deviation/deviation/view-deviation', 'id' => $id]);
    }
    }
  }

  protected function logChanges($model,$dirty,$oldModel)
  {

    //dd($oldModel->method->nm_method);
    $update='';
    $relations=DevInvestigation::getRelations();
    //d($relations);
    //dd($dirty);
    foreach ($dirty as $key => $value) {
      //d($key);
      //dd($value);
      //dd($relations);
    //  dd($oldModel[$relations[$key][0]][$relations[$key][1]]);
    // changed from object access to array access jully 2017
     $update.='('.$model->getAttributeLabel($key);
     $update.=' _old : '.(array_key_exists($key,$relations)?$oldModel[$relations[$key][0]][$relations[$key][1]]:$oldModel->$key).' - ';
     $update.=' _new : '.(array_key_exists($key,$relations)?$model[$relations[$key][0]][$relations[$key][1]]:$value).') - ';
    }
    //dd($update);
    Yii::info(\Yii::t('app','Investigation updated N° ').$model->id_deviation.' .'.$update, 'user'); // log launch deviation

  }
  public function actionDeleteFile($id,$name,$type)
  {
    //echo 'ba';
    files::DeleteFile($id,$name,$type);
    Yii::info(\Yii::t('app','File [{name}] deleted in {module} N° {id}.',['name'=>$name,'id'=>$id,'module'=>$module]), 'user'); // log launch deviation

    //return false;
    //echo findFiles(Yii::getAlias('@app').'/modules/deviation/uploads/'.$id.'/')->count();
  }

  public function actionDownloadFiles()
  {

    $name = $_GET['file'];
    $id = $_GET['id'];
    $type = $_GET['type'];
    //$upload_path = Yii::$app->params[Yii::getAlias('@web').'/deviation/deviation/download'];
    $upload_path =Yii::getAlias('@app').'/modules/deviation/uploads/'.$type.'/'.$id.'/';
    if( file_exists( $upload_path.$name ) ){
    //Yii::$app->getRequest()->sendFile( $name  );
    \Yii::$app->response->sendFile($upload_path.$name );
      //, file_get_contents( $upload_path.$name )
    }
    else{
       throw new NotFoundHttpException('The requested page does not exist.');
    }



  }


}
