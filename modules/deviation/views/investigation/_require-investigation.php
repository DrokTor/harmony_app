<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Assignments;
use yii\helpers\ArrayHelper;
 ?>

<div class="form-group text-center" style="margin-top:150px">

  <?php
        $id=Yii::$app->request->getBodyParam('id');
        $form = ActiveForm::begin(['action'=>'/deviation/investigation/require-investigation?id='.$id]);

        $Investigators=ArrayHelper::map(Assignments::find()->with('user')->where(['item_name'=>'Investigation manager'])->asArray()->all(),'user_id','user.username');

  ?>

    <?= Html::button( \Yii::t('app','Launch investigation process') ,['class' => 'moreInfo', 'name'=>'require', 'value'=>'true','data-toggle'=>'modal','data-target'=>'#launchModal' ]); ?>
    <?= Html::button(  \Yii::t('app','Skip investigation process'), ['class' =>  'discardDev', 'name'=>'skip', 'value'=>'true','data-toggle'=>'modal','data-target'=>'#skipModal']); ?>

    <!-- Modal -->
    <div id="launchModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><?= \Yii::t('app','Preliminary rating')?></h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="levels text-left">

                <div class="col-md-4 ">
                    <h3 class=""><?= \Yii::t('app','Severity')?></h3>
                  <?= Html::dropDownList( 'preseverity' ,null, $severity,   $options = [] ) ?>
                  <span id="sev-rate"></span>
                </div>

                <div class="col-md-4 ">
                  <h3><?= \Yii::t('app','Occurence')?></h3>

                  <?= Html::dropDownList( 'preoccurence' ,null, $occurence,   $options = [] ) ?>
                  <span id="occ-rate"></span>
                </div>

                <div class="col-md-4 ">
                    <h3><?= \Yii::t('app','Detectability')?></h3>
                  <?= Html::dropDownList( 'predetectability' ,null, $detectability,   $options = [] ) ?>
                  <span id="det-rate"></span>
                </div>

                <div class="col-md-12">
                  <h3><?= \Yii::t('app','Risk level')?></h3>
                  <span id="pre-risk-lvl"></span>
                </div>
              </div>
              <div class="col-md-12">
              <?= Html::dropDownList( 'assign' ,null, $Investigators,   $options = ['prompt'=>'Select Investigation manager'] ) ?>

              </div>
              <div class="levels-result"></div>
            </div><hr>

          </div>
          <div class="modal-footer">
          <input type="submit" name="require" value="<?= \Yii::t('app','Launch')?>" class="btn btn-default" />
            <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('app','Cancel')?></button>
          </div>
        </div>

      </div>
    </div>


  <!-- Modal -->
  <div id="skipModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= \Yii::t('app','Final rating & reason to skip investigation step') ?></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="levels text-left">

              <div class="col-md-4 ">
                  <h3 class=""><?= \Yii::t('app','Severity') ?></h3>
                <?= Html::dropDownList( 'severity' ,null, $severity,   $options = [] ) ?>
                <span id="sev-rate"></span>
              </div>

              <div class="col-md-4 ">
                <h3><?= \Yii::t('app','Occurence') ?></h3>

                <?= Html::dropDownList( 'occurence' ,null, $occurence,   $options = [] ) ?>
                <span id="occ-rate"></span>
              </div>

              <div class="col-md-4 ">
                  <h3><?= \Yii::t('app','Detectability') ?></h3>
                <?= Html::dropDownList( 'detectability' ,null, $detectability,   $options = [] ) ?>
                <span id="det-rate"></span>
              </div>

              <div class="col-md-12">
                <h3><?= \Yii::t('app','Risk level') ?></h3>
                <span id="pre-risk-lvl"></span>
              </div>
            </div>
            <div class="levels-result"></div>
          </div><hr>
          <textarea class="row" name="skip"></textarea>
        </div>
        <div class="modal-footer">
        <input type="submit" value="<?= \Yii::t('app','Skip') ?>" class="btn btn-default" />
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('app','Cancel') ?></button>
        </div>
      </div>

    </div>
  </div>


    <?php ActiveForm::end(); ?>


</div>
