<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;
use app\modules\deviation\models\DevInvestigation;
use app\models\Method;
use app\models\Ishikawa;
use yii\widgets\ActiveForm;




/* @var $form yii\widgets\ActiveForm */

  //$model2= new DevDeclaration();

/* @var $this yii\web\View */
/* @var $model app\modules\deviation\models\DevInvestigation */
/*
$this->title = $model->id_deviation;
$this->params['breadcrumbs'][] = ['label' => 'Dev Deviations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/



if(1 || !$model->isNewRecord)
{
  //$lvlDev='<div class="lvl-Vlow">'.$model->level['nm_level'].'</div>';

?>
<div class="dev-deviation-view">



    <?= DetailView::widget([
        'model' => $model,
      //'template'=> '<tr><th>{label}</th><td class="lvl-Vlow">{value}</td></tr>',
        'attributes' => [
          [
            'label'=>\Yii::t('app','Preliminary risk level'),
             'value'=>'<span class="'.$elvl.'"></span>',
             'format'=>'html',
          ],
             [
              'label'=>\Yii::t('app','Method'),
             'value'=>$model->method['nm_method'],
            ],
             [
            'label'=>\Yii::t('app','Ishikawa'),
             'value'=>$ishikawa,//$model->ishikawa['nm_ishikawa'],
            ],
            'origin_anomaly' ,
/*
            [
              'label'=>'is_history' ,
              'value'=>(($model->is_history == '1')?  'Yes':'No'),

            ],*/
            'history_anomaly' ,
            'process_review' ,
            'material_review' ,
            'batch_review' ,
            'equipment_review' ,
            'conformity' ,
            'review_actions' ,
            'impact_review',
            'gmp_documents_review',
            'product_quality_review',
            'additional_analysis',
            'conclusion' ,
            [
            'label'=>\Yii::t('app','Files'),
            'value'=>$model_file->list,
            'format'=>'html',
            ]
            //'deviation_lvl' ,
            /*[

            'label'=>'Deviation level',
             'value'=>$lvlDev,
              'format'=>'html'
            ],*/

        ],
    ]) ?>





</div>
<?php
}
