<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;
use app\modules\deviation\models\DevInvestigation;
use app\models\Method;
use app\models\Ishikawa;
use yii\widgets\ActiveForm;




/* @var $form yii\widgets\ActiveForm */

  //$model2= new DevDeclaration();

/* @var $this yii\web\View */
/* @var $model app\modules\deviation\models\DevInvestigation */
/*
$this->title = $model->id_deviation;
$this->params['breadcrumbs'][] = ['label' => 'Dev Deviations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/






?>
<div class="dev-deviation-form">

    <?php $form = ActiveForm::begin(['action'=>'/deviation/investigation/investigate-deviation','options' =>['enctype' => 'multipart/form-data']]); ?>

    <!-----<?= $form->field($model, 'id_deviation')->textInput() ?>--->
    <?= $form->field($model, 'id_deviation', ['options'=>[ 'class'=>'hidden']])->hiddenInput()->label(false)  ?>


    <h3 class="collapseLabel col-md-12" data-toggle="collapse" data-target="#identification">Identification</h3>
 <div id="identification" class="collapse">
      <div class="  row">
    <?= $form->field($model, 'id_method', ['options'=>['class'=>'col-md-6']])->dropdownList(
    Method::find()->select(['nm_method','id_method'])->indexBy('id_method')->column(),['prompt'=>'select method']
    ) ?>


    <div class="col-md-6 required">
      <label class="control-label">Ishikawa</label>
    <?= Html::listBox('DevInvestigation[id_ishikawa]',$selected,
    Ishikawa::find()->select(['nm_ishikawa','id_ishikawa'])->indexBy('id_ishikawa')->column(),
    [/*'prompt'=>'select method ishikawa',*/'multiple'=>true, 'class'=>'form-control' ]
    ) ?>
    </div>





      </div>
      <div class="  row">
    <?= $form->field($model, 'origin_anomaly', ['options'=>['class'=>'col-md-6']])->textarea(['maxlength' => true]) ?>

      <div class=" col-md-6">
    <!-----<?= $form->field($model, 'is_history')->Checkbox(['history' => 'is','id'=>'idHistory','onchange'=>'historyCheck()']) ?>
---->
    <!--  <label for="idHistory">History </label> <!--<input id="idHistory" type="checkbox" />-->
    <?= $form->field($model, 'history_anomaly')->textarea([/*'disabled'=>"",*/'id'=>'historyField'] ) ?>
      </div>

  </div>
      </div>

      <h3 class="collapseLabel col-md-12" data-toggle="collapse" data-target="#reviews"><?= \Yii::t('app','Reviews') ?></h3>
   <div id="reviews" class="collapse">
      <div class="  row">
    <?= $form->field($model, 'process_review', ['options'=>['class'=>'col-md-6']])->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'material_review', ['options'=>['class'=>'col-md-6']])->textarea(['maxlength' => true]) ?>
      </div>
      <div class="  row">
    <?= $form->field($model, 'batch_review', ['options'=>['class'=>'col-md-6']])->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'equipment_review', ['options'=>['class'=>'col-md-6']])->textarea(['maxlength' => true]) ?>
      </div>
      <div class="  row">
    <?= $form->field($model, 'conformity', ['options'=>['class'=>'col-md-6']])->textarea(['maxlength' => true]) ?>


    <?= $form->field($model, 'review_actions', ['options'=>['class'=>'col-md-6']])->textarea(['maxlength' => true]) ?>
      </div>

      <div class="  row">
        <?= $form->field($model, 'impact_review', ['options'=>['class'=>'col-md-6']])->textarea(['maxlength' => true]) ?>

      <?= $form->field($model, 'gmp_documents_review', ['options'=>['class'=>'col-md-6']])->textarea(['maxlength' => true]) ?>
    </div>
    <div class="  row">
  <?= $form->field($model, 'product_quality_review', ['options'=>['class'=>'col-md-6']])->textarea(['maxlength' => true]) ?>
  </div>
  </div>

  <h3 class="collapseLabel col-md-12" data-toggle="collapse" data-target="#finalanalysis"><?= \Yii::t('app','Final analysis') ?></h3>
<div id="finalanalysis" class="collapse">
    <div class="  row">
      <?= $form->field($model, 'additional_analysis', ['options'=>['class'=>'col-md-6']])->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'conclusion', ['options'=>['class'=>'col-md-6']])->textarea(['maxlength' => true]) ?>
  </div>
</div>
  <?php
  //dd($url);
  $arr=explode('<br/>',$model_file->list);
  unset($arr[count($arr)-1]);
  foreach ($arr as  $value) {

    echo '<span class="">'.$value;
    $value=explode('</a>',$value)[0];
    $value=explode('>',$value)[1];
    echo '<span class="del-file" data-type="'.$model_file->type.'" data-url="'.$model_file->url.'" data-name="'.$value.'" data-id="'.$model_file->id.'" >x</span></span> ';

  }
//data-id="'.Yii::$app->request->getBodyParam('id').'"
     ?>

  <div class="form_field row">
  <?= $form->field($model_file, 'files[]')->fileInput(['multiple'=>true]) ?>
  </div>
<!--
      <div class="col-md-6">
      <label>Deviation level</label>
        <div id="lvl_section" >
          <div id="riskLVL" class="lvl col-md-6">Very low</div>
          <button type="button" class="btn btn-danger btn-lg col-md-5 pull-right" data-toggle="modal" data-target="#ratingModal">Rate deviation</button>
        </div>
      </div>
     </div>
      <!-- Modal
    <div id="ratingModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Deviation risk level</h4>
          </div>
          <div class="modal-body">
            <h3>Severity</h3>
            <p>Some text in the modal.</p>
              <div class="rating">
                <label class="radio-inline"><input type="radio" value='1' name="severity"><div class="Vlow">Very low</div></label>
                <label class="radio-inline"><input type="radio" value='2' name="severity"><div class="low">Low</div></label>
                <label class="radio-inline"><input type="radio" value='3' name="severity"><div class="modr">Moderate</div></label>
                <label class="radio-inline"><input type="radio" value='4' name="severity"><div class="high">High</div></label>
                <label class="radio-inline"><input type="radio" value='5' name="severity"><div class="crit">Critical</div></label>
              </div>
            <h3>Occurence</h3>
            <p>Some text in the modal.</p>
              <div class="rating">
                <label class="radio-inline"><input type="radio" value='1' name="occurence"><div class="Vlow">Very low</div></label>
                <label class="radio-inline"><input type="radio" value='2' name="occurence"><div class="low">Low</div></label>
                <label class="radio-inline"><input type="radio" value='3' name="occurence"><div class="modr">Moderate</div></label>
                <label class="radio-inline"><input type="radio" value='4' name="occurence"><div class="high">High</div></label>
                <label class="radio-inline"><input type="radio" value='5' name="occurence"><div class="crit">Very high</div></label>
              </div>
            <div class="rating"></div>
            <h3>Detectability</h3>
            <p>Some text in the modal.</p>
              <div class="rating">
                <label class="radio-inline"><input type="radio" value='1' name="detectability"><div class="Vlow">Certain</div></label>
                <label class="radio-inline"><input type="radio" value='2' name="detectability"><div class="low">High</div></label>
                <label class="radio-inline"><input type="radio" value='3' name="detectability"><div class="modr">Moderate</div></label>
                <label class="radio-inline"><input type="radio" value='4' name="detectability"><div class="high">Low</div></label>
                <label class="radio-inline"><input type="radio" value='5' name="detectability"><div class="crit">Very low</div></label>
              </div>
            <div class="rating"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>-->


    <div class="form-group">

        <?php
         if(\Yii::$app->user->can('Finish investigation'))
         {
           echo  Html::button( \Yii::t('app','Finish investigation') , ['class' => 'btn btn-success','data-toggle'=>'modal','data-target'=>'#finishModal' ]);
         }
          ?>
        <?= Html::submitButton(  \Yii::t('app','Save'), ['class' =>  'save-draft btn btn-primary', 'name'=>"save", 'value'=>'true']) ?>

    </div>

    <?php
    if(\Yii::$app->user->can('Finish investigation'))
    {

     ?>
    <!-- Modal -->
    <div id="finishModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><?= \Yii::t('app','Final rating') ?></h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="levels text-left">

                <div class="col-md-4 ">
                    <h3 class=""><?= \Yii::t('app','Severity') ?></h3>
                  <?= Html::dropDownList( 'severity' ,null, $severity,   $options = [] ) ?>
                  <span id="sev-rate"></span>
                </div>

                <div class="col-md-4 ">
                  <h3><?= \Yii::t('app','Occurence') ?></h3>

                  <?= Html::dropDownList( 'occurence' ,null, $occurence,   $options = [] ) ?>
                  <span id="occ-rate"></span>
                </div>

                <div class="col-md-4 ">
                    <h3><?= \Yii::t('app','Detectability') ?></h3>
                  <?= Html::dropDownList( 'detectability' ,null, $detectability,   $options = [] ) ?>
                  <span id="det-rate"></span>
                </div>

                <div class="col-md-12">
                  <h3><?= \Yii::t('app','Risk level') ?></h3>
                  <span id="pre-risk-lvl"></span>
                </div>
              </div>
              <div class="levels-result"></div>
            </div><hr>

          </div>
          <div class="modal-footer">
          <input type="submit" name="finish" value="<?= \Yii::t('app','Launch') ?>" class="btn btn-default" />
            <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('app','Cancel') ?></button>
          </div>
        </div>

      </div>
    </div>

    <?php


      } ?>

    <?php ActiveForm::end(); ?>

</div>
