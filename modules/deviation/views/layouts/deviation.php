<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\modules\deviation\assets\DevAsset;
use kartik\icons\Icon;
$bundle=DevAsset::register($this);
// Initialize framework as per <code>icon-framework</code> param in Yii config
Icon::map($this);


/* @var $this \yii\web\View */
/* @var $content string */

//AppAsset::register($this);
?>
<?php $this->beginContent('@app/views/layouts/main.php') ?>

      <div class="row">

        <?php

            NavBar::begin([
                'brandLabel' => \Yii::t('app','Deviation & CAPA management'),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse deviation-bar' //navbar-fixed-top',
                ],
            ]);

            echo Nav::widget([
              'options' => ['class' => 'navbar-nav navbar-right'],
              'encodeLabels'=>false,
              'items' => [
              ['label' => ''.\Yii::t('app','').' <i class="fa fa-book"></i>','linkOptions'=>['class'=>'libraryTag'], 'url' => ['/deviation/library/guide']] ,



              ],
            ]);

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'encodeLabels'=>false,
                'items' => [
                isset($this->params['draft']) && $this->params['draft']>0 ?
                ['label' => ''.\Yii::t('app','Deviations Drafts').' <span>'.$this->params['draft'].'</span>','linkOptions'=>['class'=>'tag-deviation'], 'url' => ['/deviation/deviation/list-drafts']]:'',
                isset($this->params['investigate']) && $this->params['investigate']>0?
                ['label' => ''.\Yii::t('app','to be Investigated').' <span>'.$this->params['investigate'].'</span>','linkOptions'=>['class'=>'tag-deviation'], 'url' => ['/deviation/deviation/list-deviations?DevFilters[state]=3']]:'',
                isset($this->params['close']) && $this->params['close']>0?
                ['label' => ''.\Yii::t('app','to be Closed').' <span>'.$this->params['close'].'</span>','linkOptions'=>['class'=>'tag-deviation'], 'url' => ['/deviation/deviation/list-deviations?DevFilters[state]=5']]:'',
                isset($this->params['capadraft']) && $this->params['capadraft']>0?
                ['label' => ''.\Yii::t('app','Capa Drafts').' <span>'.$this->params['capadraft'].'</span>','linkOptions'=>['class'=>'tag-deviation'], 'url' => ['/deviation/capa/list-drafts']]:'',

                //['label' => ''.\Yii::t('app','| Library').' <i class="fa fa-book"></i>','linkOptions'=>['class'=>''], 'url' => ['/deviation/library/guide']] ,



                ],
            ]);


            NavBar::end();
        ?>
      </div>

      <div class="row">

          <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
      </div>

      <div class="row">
        <div class="col-md-2">



<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menside">
  <span class="sr-only">Toggle navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
</button>

</div>
  <?php //Create deviation List deviations Data analysis
           echo ' <div id="menside" class="collapse in col-md-12">
              <ul class="nav  left_menu">
                <li class="active col-xs-3 col-md-12"><a href="'.Yii::getAlias('@web').'/deviation/deviation/new-draft"><div  class="fa fa-3x  fa-file-text" id="menNew" ></div></a></li>

                <li  class="col-xs-3  col-md-12"><a href="'.Yii::getAlias('@web').'/deviation/deviation/list-deviations"><div class="fa fa-3x  fa-list-ul" id="menList" ></div></a></li>
                <li  class="col-xs-3  col-md-12"><a href="'.Yii::getAlias('@web').'/deviation/data/"><div class="fa fa-3x  fa-area-chart"  id="menData" ></div></a></li>
                <li  class="col-xs-3  col-md-12"><a href="'.Yii::getAlias('@web').'/deviation/capa/list-capas"><div class="fa fa-3x  fa-wrench"  id="menCapa" ></div></a></li>
              </ul>
            </div>';
           ?>
         </div>

        <div class="col-md-10">
          <div class="container-fluid">
        <?= $content ?>
          </div>
        </div>
      </div>

    </div>



<?php $this->endContent() ?>
