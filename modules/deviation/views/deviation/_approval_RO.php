<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;
use app\modules\deviation\models\DevDeclaration;
use app\modules\deviation\models\DevPlanned;
use yii\widgets\ActiveForm;
use app\models\Location;
use app\models\Process;
use app\models\Equipment;
use app\models\Product;
use app\models\Assignments;
use yii\helpers\ArrayHelper;

use app\modules\deviation\models\DevType;
use kartik\time\TimePicker;
use kartik\date\DatePicker;

  //$model2= new DevDeclaration();

/* @var $this yii\web\View */
/* @var $model app\modules\deviation\models\DevDeclaration */
/*
$this->title = $model->id_deviation;
$this->params['breadcrumbs'][] = ['label' => 'Dev Deviations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/

 /*echo $this->render('_form', [
    'model' => $model,'model_file'=>$model_file
]);*/

if($dev_model->stateIn ==APPROVAL /* && has permission */)
{
  $id=Yii::$app->request->getBodyParam('id');
//$form = ActiveForm::begin(['action' => ['deviation/new-deviation'],'options' => ['class'=>'declaration-form-class','enctype' => 'multipart/form-data']]);
?>

<div class="dev-deviation-form">

    <?php   $form = ActiveForm::begin(['id'=>'approvealForm','action'=>'approve-deviation?id='.$id]);  ?>

    <?= $this->render('_form', [
        'model' => $model,'model_file'=>$model_file,'form'=>$form,
    ]) ?>


<?php






  echo '<div class="row">';

  echo Html::button( \Yii::t('app','Approve deviation') ,['class' => ' approve' , 'name'=>'approve', 'value'=>'true','data-toggle'=>'modal','data-target'=>'#approveModal' ]);
  echo Html::button( \Yii::t('app','Require more information') ,['class' => ' moreInfo', 'name'=>'moreInfo', 'value'=>'true','data-toggle'=>'modal','data-target'=>'#moreinfoModal' ]);
  echo Html::button( \Yii::t('app','Close deviation') ,['class' => ' discardDev', 'name'=>'discardDev', 'value'=>'true','data-toggle'=>'modal','data-target'=>'#closeModal' ]);
    echo '</div>';
// problem here !!!!!!!!!
$QualityExpert=ArrayHelper::map(Assignments::find()->with('user')->where(['item_name'=>'Quality expert'])->asArray()->all(),'user_id','user.username');

//print_r ($QualityExpert);
  echo '
<!-- Modal -->
<div id="approveModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">'.\Yii::t("app","Estimated risk level").'</h4>
      </div>
      <div class="modal-body">
'.Html::dropDownList( 'approve' ,null, $levelData,   $options = ['prompt'=>'Select Level'] ).
'
      </div>
      <h4>'.\Yii::t('app','Assign to Quality expert').'</h4>
      <div class="modal-body">
      '.Html::dropDownList( 'assign' ,null, $QualityExpert,   $options = ['prompt'=>'Select Quality expert'] ).
'
      </div>
      <div class="modal-footer">
      <input type="submit" value="'.\Yii::t('app','Send').'" class="btn btn-default" />
        <button type="button" class="btn btn-default" data-dismiss="modal">'.\Yii::t('app','Cancel').'</button>
      </div>
    </div>

  </div>
</div>';

  echo '
<!-- Modal -->
<div id="moreinfoModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">'.\Yii::t('app','Reason').'</h4>
      </div>
      <div class="modal-body">
        <textarea name="moreInfo"></textarea>
      </div>
      <div class="modal-footer">
      <input type="submit" value="'.\Yii::t('app','Send').'" class="btn btn-default" />
        <button type="button" class="btn btn-default" data-dismiss="modal">'.\Yii::t('app','Cancel').'</button>
      </div>
    </div>

  </div>
</div>';

echo '

<div id="closeModal" class="modal fade" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">'.\Yii::t('app','Reason').'</h4>
    </div>
    <div class="modal-body">
      <textarea name="discardDev"></textarea>
    </div>
    <div class="modal-footer">
    <input type="submit" value="'.\Yii::t('app','Validate').'" class="btn btn-default" />
      <button type="button" class="btn btn-default" data-dismiss="modal">'.\Yii::t('app','Cancel').'</button>
    </div>
  </div>

</div>
</div>';

  ActiveForm::end();


 ?>
</div>
<?php
}
 ?>
