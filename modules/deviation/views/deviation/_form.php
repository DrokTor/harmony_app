<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Location;
use app\models\Process;
use app\models\Equipment;
use app\models\Product;
use app\modules\deviation\models\DevPlanned;
use app\models\Assignments;
use yii\helpers\ArrayHelper;
use app\modules\deviation\models\DevType;
//use yii\jui\DatePicker;
use kartik\time\TimePicker;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\DevDeviation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dev-deviation-form">

    <!-----<?= $form->field($model, 'id_deviation')->textInput() ?>--->

    <h3 class="collapseLabel col-md-12" data-toggle="collapse" data-target="#genInfo"><?= \Yii::t('app','General information') ?></h3>
 <div id="genInfo" class="collapse">
    <div class='form_field row'>
    <?= $form->field($model, 'deviation_title', ['options'=>['class'=>'col-sm-6']])->textInput(['maxlength' => 200]) ?>


   <!----- ---></div>

    <div class='form_field row'>
    <div class="form-group field-devdeclaration-detection_date col-sm-3">

    <!--<label class="control-label">Detection date</label>-->
    <?= DatePicker::widget([
      'model' => $model,
      'form'=>$form,
    'attribute' => 'detection_date',
    //'value' => date('d-M-Y', strtotime('+2 days')),
   // 'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [

        'format' => 'yyyy-m-dd',
        'todayHighlight' => true
    ]
    ]);
    ?>
     </div>

    <div class="form-group field-devdeclaration-detection_time col-sm-3">

    <label class="control-label"><?= Yii::t('app','Detection time') ?></label>
    <?= TimePicker::widget([
        'model' => $model,
        //'form'=>$form,
        'attribute' => 'detection_time',
        //'value' => '11:24 AM',

        'pluginOptions' => [
            //'showSeconds' => true,
            'defaultTime' =>false,
            'showMeridian' => false,
        ]
    ]);
   ?>
    </div>
    <!---</div>

    <div class="form_field row">--->

    <?= $form->field($model, 'id_type', ['options'=>['class'=>'col-sm-3']])->dropdownList(
    DevType::find()->select([ 'nm_type','id_type'])->indexBy('id_type')->column(),
  ['prompt'=> ''/*Yii::t('app','Select type')*/ ]
      );  ?>

    <?= $form->field($model, 'id_planned', ['options'=>['class'=>'col-sm-3']])->dropdownList(
    DevPlanned::find()->select([ 'planned','id_planned'])->indexBy('id_planned')->column(),
    ['prompt'=>'']
    );  ?>


    </div>

 </div>

    <h3 class="collapseLabel col-md-12" data-toggle="collapse" data-target="#targetInfo"><?= \Yii::t('app','Targetting') ?></h3>
  <div id="targetInfo" class="collapse">
    <div class="form_field row">

    <?= $form->field($model, 'process_step', ['options'=>['class'=>'col-sm-3']])->dropdownList(
    Process::find()->select([ 'nm_process','id_process'])->indexBy('id_process')->column(),
    ['prompt'=>''] //Select process
    );  ?>

    <!--</div>

    <div class="form_field row">-->
    <?= $form->field($model, 'locality', ['options'=>['class'=>'col-sm-3']])->dropdownList(
    Location::find()->select([ 'nm_locality','id_locality'])->indexBy('id_locality')->column(),
    ['prompt'=>''],['style' => 'color:red'] //Select locality
      );  ?>


    <?= $form->field($model, 'equipment_involved', ['options'=>['class'=>'col-sm-3']])->dropdownList(
    Equipment::find()->select([ 'nm_equipment','id_equipment'])->indexBy('id_equipment')->column(),
    ['prompt'=>''] //Select equipment
      );  ?>
    <!--</div>
    <div class="form_field row"> -->
    <?= $form->field($model, 'product_code', ['options'=>['class'=>'col-sm-3']])->dropdownList(
    Product::find()->select([ 'nm_product','id_product'])->indexBy('id_product')->column(),
    ['prompt'=>''] //Select product
      );  ?>


      </div>
    <div class="form_field row">
    <?= $form->field($model, 'batch_number', ['options'=>['class'=>'col-sm-6']])->textInput(['maxlength' => 50]) ?>
    </div>
  </div>

    <h3 class="collapseLabel col-md-12" data-toggle="collapse" data-target="#detailsInfo"><?= \Yii::t('app','Details') ?></h3>
  <div id="detailsInfo" class="collapse">
    <div class="form_field row">
    <?= $form->field($model, 'what', ['options'=>['class'=>'col-sm-6']])->textarea(['maxlength' => 500]) ?>

    <?= $form->field($model, 'how', ['options'=>['class'=>'col-sm-6']])->textarea(['maxlength' => 500]) ?>
    </div>
    <div class="form_field row">
    <?= $form->field($model, 'impact', ['options'=>['class'=>'col-sm-6']])->textarea(['maxlength' => 500]) ?>

    <?= $form->field($model, 'immediate_actions', ['options'=>['class'=>'col-sm-6']])->textarea(['maxlength' => 500]) ?>
    </div>
    <div class="form_field row">

    <?= $form->field($model, 'suspected_cause', ['options'=>['class'=>'col-sm-6']])->textarea(['maxlength' => 500,'rows'=>2]) ?>
    </div>
  </div>

  <div class="form_field row col-md-12">
    <?php
    //dd($url);
    $arr=explode('<br/>',$model_file->list);
    unset($arr[count($arr)-1]);
    foreach ($arr as  $value) {

      echo '<span class="">'.$value;
      $value=explode('</a>',$value)[0];
      $value=explode('>',$value)[1];
      echo '<span class="del-file" data-type="'.$model_file->type.'" data-url="'.$model_file->url.'" data-name="'.$value.'" data-id="'.($model_file->id? $model_file->id : $model->id_declaration).'" >x</span></span> ';

    }
  //data-id="'.Yii::$app->request->getBodyParam('id').'"
       ?>


    <?= $form->field($model_file, 'files[]')->fileInput(['multiple'=>true]) ?>
    </div>


</div>
