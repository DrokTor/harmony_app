<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;
use app\modules\deviation\models\DevDeclaration;
use yii\widgets\ActiveForm;
use app\models\Location;
use app\models\Process;
use app\models\Equipment;
use app\models\Product;
use app\modules\deviation\models\DevType;
use app\modules\deviation\models\DevPlanned;
use app\models\Assignments;
use yii\helpers\ArrayHelper;
use kartik\time\TimePicker;
use kartik\date\DatePicker;

  //$model2= new DevDeclaration();

/* @var $this yii\web\View */
/* @var $model app\modules\deviation\models\DevDeclaration */
/*
$this->title = $model->id_deviation;
$this->params['breadcrumbs'][] = ['label' => 'Dev Deviations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/




if($dev_model->stateIn ==DRAFT || $dev_model->stateIn ==MISSINGINFO)
{
?>

<div class="dev-deviation-form">

    <?php $form = ActiveForm::begin(['action' => ['deviation/'.(($dev_model->stateIn == MISSINGINFO)?'save-missing?id='.$dev_model->id_deviation:'new-draft')],'options' => ['class'=>'declaration-form-class','enctype' => 'multipart/form-data']]); ?>

     <?= $form->field($dev_model, 'id_deviation', ['options'=>['class'=>'hidden']])->textInput() ?>

     <?= $this->render('_form', [
         'model' => $model,'model_file'=>$model_file,'form'=>$form,
     ]) ?>

    <div class="form-group">
         <?= Html::Button( \Yii::t('app','Launch deviation process') ,['class' => 'launch-process btn btn-success', 'name'=>'launch', 'value'=>'true','data-toggle'=>'modal','data-target'=>'#assignManagerModal' ]) ?>


        <?= Html::submitButton(  \Yii::t('app','Save'), ['class' =>  'save-draft btn btn-primary', 'name'=>'draft' ,'value'=>'true']) ?>
    </div>

      <?php

      $SectorManagers=ArrayHelper::map(Assignments::find()->with('user')->where(['item_name'=>'Sector manager'])->asArray()->all(),'user_id','user.username');
      //print_r($SectorManagers);

    echo '
    <!-- Modal -->
    <div id="assignManagerModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">'.\Yii::t('app','Assign this declaration to sector manager').'</h4>
        </div>
        <div class="modal-body">
        '.Html::dropDownList( 'launch' ,null, $SectorManagers,   $options = ['prompt'=>\Yii::t('app','Select Sector manager')] ).
        '
        </div>
        <div class="modal-footer">
        <input type="submit" value="'.\Yii::t('app','Send').'" class="btn btn-default" />
          <button type="button" class="btn btn-default" data-dismiss="modal">'.\Yii::t('app','Cancel').'</button>
        </div>
      </div>

    </div>
    </div>';

    ?>
    <?php ActiveForm::end(); ?>

</div>
<?php



}
 ?>
