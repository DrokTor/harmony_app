<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;
use app\modules\deviation\models\DevDeclaration;
use yii\widgets\ActiveForm;
use app\models\Location;
use app\models\Process;
use app\models\Equipment;
use app\models\Product;
use app\modules\deviation\models\DevType;
use app\modules\deviation\models\DevPlanned;
use app\models\Assignments;
use yii\helpers\ArrayHelper;
use kartik\time\TimePicker;
use kartik\date\DatePicker;

  //$model2= new DevDeclaration();

/* @var $this yii\web\View */
/* @var $model app\modules\deviation\models\DevDeclaration */
/*
$this->title = $model->id_deviation;
$this->params['breadcrumbs'][] = ['label' => 'Dev Deviations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/




if($dev_model->stateIn ==DRAFT || $dev_model->stateIn ==MISSINGINFO)
{
?>

<div class="dev-deviation-form">

    <?php $form = ActiveForm::begin(['action' => ['deviation/new-draft'],'options' => ['class'=>'declaration-form-class','enctype' => 'multipart/form-data']]); ?>

     <?= $form->field($dev_model, 'id_deviation', ['options'=>['class'=>'hidden']])->textInput() ?>

    <div class='well row'>
    <?= $form->field($model, 'deviation_title', ['options'=>['class'=>'col-sm-6']])->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'id_type', ['options'=>['class'=>'col-sm-6']])->dropdownList(
    DevType::find()->select([ 'nm_type','id_type'])->indexBy('id_type')->column(),
    ['prompt'=>'Select type']
      );  ?>
   <!----- ---></div>

    <div class='well row'>
    <div class="form-group field-devdeclaration-detection_date col-sm-6">

    <label class="control-label">Detection date</label>
    <?= DatePicker::widget([
      'model' => $model,
    'attribute' => 'detection_date',
    //'value' => date('d-M-Y', strtotime('+2 days')),
   // 'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [

        'format' => 'yyyy-m-dd',
        'todayHighlight' => true
    ]
    ]);
    ?>
     </div>

    <div class="form-group field-devdeclaration-detection_time col-sm-6">

    <label class="control-label">Detection time</label>
    <?= TimePicker::widget([
        'model' => $model,
        'attribute' => 'detection_time',
        //'value' => '11:24 AM',

        'pluginOptions' => [
            //'showSeconds' => true,
            'defaultTime' =>false,
            'showMeridian' => false,
        ]
    ]);
   ?>
    </div>
    </div>

    <div class="well row">
    <?= $form->field($model, 'id_planned', ['options'=>['class'=>'col-sm-4']])->dropdownList(
    DevPlanned::find()->select([ 'planned','id_planned'])->indexBy('id_planned')->column(),
    ['prompt'=>'Select planification'],['style' => 'color:red']
      );  ?>

    <?= $form->field($model, 'locality', ['options'=>['class'=>'col-sm-4']])->dropdownList(
    Location::find()->select([ 'nm_locality','id_locality'])->indexBy('id_locality')->column(),
    ['prompt'=>'Select locality'],['style' => 'color:red']
      );  ?>
    </div>
    <div class="well row">
    <?= $form->field($model, 'process_step', ['options'=>['class'=>'col-sm-4']])->dropdownList(
    Process::find()->select([ 'nm_process','id_process'])->indexBy('id_process')->column(),
    ['prompt'=>'Select process']
      );  ?>

    <?= $form->field($model, 'equipment_involved', ['options'=>['class'=>'col-sm-4']])->dropdownList(
    Equipment::find()->select([ 'nm_equipment','id_equipment'])->indexBy('id_equipment')->column(),
    ['prompt'=>'Select equipment']
      );  ?>
    </div>
    <div class="well row">
    <?= $form->field($model, 'product_code', ['options'=>['class'=>'col-sm-6']])->dropdownList(
    Product::find()->select([ 'nm_product','id_product'])->indexBy('id_product')->column(),
    ['prompt'=>'Select product']
      );  ?>



    <?= $form->field($model, 'batch_number', ['options'=>['class'=>'col-sm-6']])->textInput(['maxlength' => 50]) ?>
    </div>

    <div class="well row">
    <?= $form->field($model, 'what', ['options'=>['class'=>'col-sm-6']])->textarea(['maxlength' => 500]) ?>

    <?= $form->field($model, 'how', ['options'=>['class'=>'col-sm-6']])->textarea(['maxlength' => 500]) ?>
    </div>
    <div class="well row">
    <?= $form->field($model, 'impact', ['options'=>['class'=>'col-sm-6']])->textarea(['maxlength' => 500]) ?>

    <?= $form->field($model, 'immediate_actions', ['options'=>['class'=>'col-sm-6']])->textarea(['maxlength' => 500]) ?>
    </div>
    <div class="well row">

    <?= $form->field($model, 'suspected_cause', ['options'=>['class'=>'col-sm-6']])->textarea(['maxlength' => 500]) ?>
    </div>

    <?php
    //dd($url);
    $arr=explode('<br/>',$model_file->list);
    unset($arr[count($arr)-1]);
    foreach ($arr as  $value) {

      echo '<span class="">'.$value;
      $value=explode('</a>',$value)[0];
      $value=explode('>',$value)[1];
      echo '<span class="del-file" data-type="'.$model_file->type.'" data-url="'.$model_file->url.'" data-name="'.$value.'" data-id="'.$model_file->id.'" >x</span></span> ';

    }
  //data-id="'.Yii::$app->request->getBodyParam('id').'"
       ?>

    <div class="well row">
    <?=   $form->field($model_file, 'files[]')->fileInput(['multiple'=>true])   ?>
    </div>

    <div class="form-group">
         <?= Html::Button( \Yii::t('app','Launch deviation process') ,['class' => 'launch-process btn btn-success', 'name'=>'launch', 'value'=>'true','data-toggle'=>'modal','data-target'=>'#assignManagerModal' ]) ?>


        <?= Html::submitButton(  \Yii::t('app','Save'), ['class' =>  'save-draft btn btn-primary', 'name'=>'draft' ,'value'=>'true']) ?>
    </div>

      <?php

      $SectorManagers=ArrayHelper::map(Assignments::find()->with('user')->where(['item_name'=>'Sector manager'])->asArray()->all(),'user_id','user.username');
      //print_r($SectorManagers);

    echo '
    <!-- Modal -->
    <div id="assignManagerModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">'.\Yii::t('app','Assign this declaration to sector manager').'</h4>
        </div>
        <div class="modal-body">
        '.Html::dropDownList( 'launch' ,null, $SectorManagers,   $options = ['prompt'=>\Yii::t('app','Select Sector manager')] ).
        '
        </div>
        <div class="modal-footer">
        <input type="submit" value="'.\Yii::t('app','Send').'" class="btn btn-default" />
          <button type="button" class="btn btn-default" data-dismiss="modal">'.\Yii::t('app','Cancel').'</button>
        </div>
      </div>

    </div>
    </div>';

    ?>
    <?php ActiveForm::end(); ?>

</div>
<?php



}
 ?>
