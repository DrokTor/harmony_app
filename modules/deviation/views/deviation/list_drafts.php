<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use app\models\user;
use app\models\State;
use app\models\Location;
use app\models\Process;
use app\models\Product;
use app\models\Equipment;
use app\models\Method;
use app\models\Ishikawa;
use app\models\Decision;
use app\modules\deviation\models\DevLevel;
use app\modules\deviation\models\DevRpn;
use app\modules\deviation\models\DevPreRpn;
use app\modules\deviation\models\DevEstimatedLvl;
use app\modules\deviation\models\DevType;
use app\modules\deviation\models\DevDeviation;
use kartik\select2\Select2;
use dosamigos\multiselect\MultiSelect;
use kartik\export\ExportMenu;
//use kartik\grid\GridView;

use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DevDeviationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app','Deviations list');
$this->params['breadcrumbs'][] = $this->title;
//$multiUsers['0']='clear';
//$multiUsers= ArrayHelper::merge($multiUsers,ArrayHelper::map(User::find()->asArray()->all(), 'id_user', 'username'));
//$multiUsers['0']='clear';
//print_r($dataProvider->getModels()['0']);exit();
?>
<div class="dev-deviation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-----<p>
        <?= Html::a('Create Dev Deviation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>----->








    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'rowOptions'=>['class'=>'list-table'],


        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id_declaration',
            'deviation_title',
            //'creator',
            'user.username',





            [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{view}',
              'buttons'=>[
                  'view' => function ($url, $model, $key) {
                      return  Html::a('<span class="glyphicon glyphicon-search view-color"></span>', Url::to(['new-draft', 'id' => $model->id_declaration])) ;
                  },
               ],
               //'contentOptions'=>['class'=>'text-center'],

            ],
        ],
    ]); ?>

</div>
