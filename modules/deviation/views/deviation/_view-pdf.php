<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\VarDumper;
 //VarDumper::dump($rpnProvider->getModels()[0],50,true); die();

?>
<div class="dev-deviation-view">


    <h1> <?= \Yii::t('app','Deviation management rapport N°') ?> <?= $dataProvider->getModels()[0]->id_deviation ?></h1>
    <h2 class="title">- <?= $dataProvider->getModels()[0]->declaration->deviation_title ?> -</h2>
    <?=
      GridView::widget([
    'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{sorter}",

    'columns'=>
        [

          [

            'label'=>\Yii::t('app','Type'),
            'value'=>function($model)
              {
              return $model->declaration->type->nm_type;
              } ,
            'contentOptions' => ['class' => 'text-center'],

          ],
          'creation_date:date',
          'creation_time:time',
          /*
          [
            'attribute'=>'creation_date',
            //'label'=>'Creation date',
            'format' => ['date', 'php:d-m-Y']
          ],
          [
            'attribute'=>'creation_time',
            //'label'=>'Creation time',
            'format' => ['time', 'H:mm']
          ],*/

          [
            'attribute'=>'creator',
            'label'=>\Yii::t('app','Creator'),
            'value' => function($model)
              {
              return $model->user['username'];
              } ,
          ],


        ]
      ]);
  ?>
<h2><?= \Yii::t('app','Risk') ?>:</h2>
  <?=
  GridView::widget([
    'dataProvider'=>$rpnProvider,
    'summary'=>false,
    'columns'=>
    [
      [
        'label'=>\Yii::t('app','Risk'),
        'value'=>'lvl.nm_level',
        'contentOptions'=>function($model)
        {
          return ['class'=>$model->lvl['nm_class']];
        }

      ],
      [
        'label'=>\Yii::t('app','Severity'),
        'value'=>'sev.nm_severity',
        'contentOptions'=>function($model)
        {
          return ['class'=>$model->sev['nm_class']];
        }

      ],
      [
        'label'=>\Yii::t('app','Occurence'),
        'value'=>'occ.nm_occurence',
        'contentOptions'=>function($model)
        {
          return ['class'=>$model->occ['nm_class']];
        }

      ],
      [
        'label'=>\Yii::t('app','Detectablity'),
        'value'=>'det.nm_detectability',
        'contentOptions'=>function($model)
        {
          return ['class'=>$model->det['nm_class']];
        }

      ]
    ]
  ]);
  ?>
  <h2> <?= \Yii::t('app','Declaration') ?> :</h2>
  <?= DetailView::widget([
      'model' => $dec,
      'attributes' => [

          [
            'label'=>\Yii::t('app','Estimated risk level'),
             'value'=>$elvl?'<div class="elvl '.$elvl->lvl->nm_class.'">'.$elvl->lvl->nm_level.'</div>':'',
             'format'=>'html',

          ],

          [
            'attribute'=>'id_planned',
            'value'=>$dec->plan['planned'],
          ],
          'detection_date:date',
          'detection_time:time',
          /*[
          'attribute'=>'detection_date',
           //'value'=>$model->type['nm_type'],
            'format'=> ['date','Y-M-dd'],
          ],
          [
          'attribute'=>'detection_time',
           //'value'=>$model->type['nm_type'],
            'format'=> ['date','H:mm'],
          ],*/
          [
          'label'=>\Yii::t('app','Locality'),
           'value'=>$dec->location['nm_locality'],
          ],
          [
          'label'=>\Yii::t('app','Process'),
           'value'=>$dec->process['nm_process'],
          ],
          [
          'label'=>\Yii::t('app','Equipment'),
           'value'=>$dec->equipment['nm_equipment'],
          ],
          [
          'label'=>\Yii::t('app','Product code'),
           'value'=>$dec->product['nm_product'],
          ],

          'batch_number' ,
          'what' ,
          'how' ,
          'impact' ,
          'immediate_actions' ,
          'suspected_cause' ,



      ],
  ]) ?>

<?php
  if(isset($reason) && $reason->close_reason)
  {

    echo \Yii::t('app','Closed in declaration by ').$reason->user->username.\Yii::t('app',' on ').$approvals->approvalDate.'.<br>';
    echo \Yii::t('app','Reason').': '.$reason->close_reason ;

  }else{

    echo \Yii::t('app','Approved by ').$approvals->managerName->username.\Yii::t('app',' on ').$approvals->approvalDate;
?>

  <h2><?= \Yii::t('app','Investigation') ?>:</h2>

<?php
    if(isset($reason) && $reason->skip_investigation)
    {
      echo \Yii::t('app','Investigation skipped by ').$reason->user->username.\Yii::t('app',' on ').$approvals->expertiseDate.'.<br>';
      echo \Yii::t('app','Reason').': '.$reason->skip_investigation;

      }
      else{

      echo \Yii::t('app','Launched by ').$approvals->expertName->username.\Yii::t('app',' on ').$approvals->expertiseDate;



 ?>

  <?= DetailView::widget([
      'model' => $inv,
    //'template'=> '<tr><th>{label}</th><td class="lvl-Vlow">{value}</td></tr>',
      'attributes' => [
        [
          'label'=>\Yii::t('app','Preliminary risk level'),
           'value'=>'<div class="'.$plvl->lvl->nm_class.'">'.$plvl->lvl->nm_level.'</div>',
           'format'=>'html',
        ],
           [
            'label'=>\Yii::t('app','Method'),
           'value'=>$inv->method['nm_method'],
          ],
           [
          'label'=>\Yii::t('app','Ishikawa'),
           'value'=>$ishikawa,//$inv->ishikawa['nm_ishikawa'],
          ],
          'origin_anomaly' ,
/*
          [
            'label'=>'is_history' ,
            'value'=>(($inv->is_history == '1')?  'Yes':'No'),

          ],*/
          'history_anomaly' ,
          'process_review' ,
          'material_review' ,
          'batch_review' ,
          'equipment_review' ,
          'conformity' ,
          'review_actions' ,
          'impact_review',
          'gmp_documents_review',
          'product_quality_review',
          'additional_analysis',
          'conclusion' ,
          [
          'label'=>\Yii::t('app','Files'),
          'value'=>$model_file->list,
          'format'=>'html',
          ]
          //'deviation_lvl' ,
          /*[

          'label'=>'Deviation level',
           'value'=>$lvlDev,
            'format'=>'html'
          ],*/

      ],
  ]) ?>
<?php

      echo \Yii::t('app','Investigated by ').$approvals->investigatorName->username.\Yii::t('app',' on ').$approvals->investigationDate;

 }
 ?>
    <h2><?= \Yii::t('app','Conclusion') ?>:</h2>
    <?php
    $attributes=[];
      foreach ($batches as $batch)
      {
        $attributes[]=['label'=>\Yii::t('app','Decision for batch: ').$batch['id_batch'], 'value'=>$batch->decision['nm_decision']];
      }


    $attributes[]='comment';
    $attributes[]='final_decision';
    $attributes[]='closing_date:date';
?>
    <?= DetailView::widget([
        'model' => $conc,

        'attributes' => $attributes,
    ]) ?>

    <h2><?= \Yii::t('app','Signatories') ?>:</h2>
    <?= $this->render('../conclusion/_signForm',['model'=>$conc, 'signProviders'=>$signProviders])  ?>



</div>
<?php


} ?>
