<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use app\models\user;
use app\models\State;
use app\models\Location;
use app\models\Process;
use app\models\Product;
use app\models\Equipment;
use app\models\Method;
use app\models\Ishikawa;
use app\models\Decision;
use app\modules\deviation\models\DevLevel;
use app\modules\deviation\models\DevRpn;
use app\modules\deviation\models\DevPreRpn;
use app\modules\deviation\models\DevEstimatedLvl;
use app\modules\deviation\models\DevType;
use app\modules\deviation\models\DevDeviation;
use kartik\select2\Select2;
use dosamigos\multiselect\MultiSelect;
use kartik\export\ExportMenu;
//use kartik\grid\GridView;

use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DevDeviationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Deviations list');
$this->params['breadcrumbs'][] = $this->title;
//$multiUsers['0']='clear';
//$multiUsers= ArrayHelper::merge($multiUsers,ArrayHelper::map(User::find()->asArray()->all(), 'id_user', 'username'));
//$multiUsers['0']='clear';
//print_r($dataProvider->getModels()['0']);exit();
?>
<div class="dev-deviation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-----<p>
        <?= Html::a('Create Dev Deviation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>----->



<?php

if(\Yii::$app->user->can('deviations filter')) // filter permission check starts here
{

$searchForm= ActiveForm::begin(['method'=>'get','options' => ['enctype' => 'multipart/form-data']]);

/*
echo '
<button class="list-buttons glyphicon glyphicon-share-alt pull-right" value="test">
</button>';*/




 ?>
<?php

    Modal::begin([
    'header' => '<h2>Filters</h2>',
    'toggleButton' => ['label' => '', 'class'=>'list-buttons glyphicon glyphicon-filter  pull-right'],
    'footer'=>'<div class="form-group ">
      <input type="submit" value="Filter" />
    </div>',
]);


$textQuery=(new \yii\db\Query())
  ->select(['COLUMN_NAME'])
  ->from('INFORMATION_SCHEMA.COLUMNS')
  ->where(['and',['TABLE_SCHEMA' => 'harmony'],['or',['TABLE_NAME' => 'dev_declaration'],['TABLE_NAME' => 'dev_investigation'],['TABLE_NAME' => 'dev_conclusion']],['DATA_TYPE'=>'VARCHAR']])
  ->all();
  //;
  //  print_r($textQuery); exit();
  // echo $textQuery->createCommand()->getRawSql(); exit();

?>
<div class="container-fluid">
  <?= $searchForm->field($devFilters, 'idDev', ['options'=>['class'=>'col-md-6']] )->textInput();  ?>
  <?= $searchForm->field($devFilters, 'creationDate', ['options'=>['class'=>'col-md-6']] )->textInput();  ?>
  <?= $searchForm->field($devFilters, 'creationTime', ['options'=>['class'=>'col-md-6']] )->textInput();  ?>
  <?= $searchForm->field($devFilters, 'creator', ['options'=>['class'=>'col-md-6']] )->widget(Select2::className(),
  [
  'model' => $devFilters,
  'attribute' => 'creator',
  'data' =>ArrayHelper::map(User::find()->asArray()->all(), 'id_user', 'username'),
  'options' => ['placeholder' => 'Select a user' ],
  'pluginOptions' => [
  'allowClear' => true,
  'multiple' => true
  ],
  ])  ?>
  <?= $searchForm->field($devFilters, 'state', ['options'=>['class'=>'col-md-6']] )->widget(Select2::className(),
  [
  'model' => $devFilters,
  'attribute' => 'state',
  'data' =>ArrayHelper::map(State::find()->asArray()->all(), 'id_state', 'nm_state'),
  'options' => ['placeholder' => 'Select a state' ],
  'pluginOptions' => [
      'allowClear' => true,
      'multiple' => true
  ],
  ]) ?>
  <?= $searchForm->field($devFilters, 'type', ['options'=>['class'=>'col-md-6']] )->widget(Select2::className(),
  [
  'model' => $devFilters,
  'attribute' => 'type',
  'data' =>ArrayHelper::map(DevType::find()->asArray()->all(), 'id_type', 'nm_type'),
  'options' => ['placeholder' => 'Select a type' ],
  'pluginOptions' => [
  'allowClear' => true,
  'multiple' => true
  ],
  ])  ?>

  <?= $searchForm->field($devFilters, 'locality', ['options'=>['class'=>'col-md-6']] )->widget(Select2::className(),
  [
  'model' => $devFilters,
  'attribute' => 'locality',
  'data' =>ArrayHelper::map(Location::find()->asArray()->all(), 'id_locality', 'nm_locality'),
  'options' => ['placeholder' => 'Select a locality' ],
  'pluginOptions' => [
  'allowClear' => true,
  'multiple' => true
  ],
  ])  ?>
  <?= $searchForm->field($devFilters, 'process', ['options'=>['class'=>'col-md-6']] )->widget(Select2::className(),
  [
  'model' => $devFilters,
  'attribute' => 'process',
  'data' =>ArrayHelper::map(Process::find()->asArray()->all(), 'id_process', 'nm_process'),
  'options' => ['placeholder' => 'Select a process' ],
  'pluginOptions' => [
  'allowClear' => true,
  'multiple' => true
  ],
  ])  ?>
  <?= $searchForm->field($devFilters, 'equipment', ['options'=>['class'=>'col-md-6']] )->widget(Select2::className(),
  [
  'model' => $devFilters,
  'attribute' => 'equipment',
  'data' =>ArrayHelper::map(Equipment::find()->asArray()->all(), 'id_equipment', 'nm_equipment'),
  'options' => ['placeholder' => 'Select a equipment' ],
  'pluginOptions' => [
  'allowClear' => true,
  'multiple' => true
  ],
  ])  ?>
  <?= $searchForm->field($devFilters, 'product', ['options'=>['class'=>'col-md-6']] )->widget(Select2::className(),
  [
  'model' => $devFilters,
  'attribute' => 'product',
  'data' =>ArrayHelper::map(Product::find()->asArray()->all(), 'id_product', 'nm_product'),
  'options' => ['placeholder' => 'Select a product' ],
  'pluginOptions' => [
  'allowClear' => true,
  'multiple' => true
  ],
  ])  ?>
  <?= $searchForm->field($devFilters, 'ishistory', ['options'=>['class'=>'col-md-6']] )->dropdownList(
  [ 'Yes','No'],
  ['prompt'=>'Select if has history']
    ) ?>
  <?= $searchForm->field($devFilters, 'method', ['options'=>['class'=>'col-md-6']] )->widget(Select2::className(),
  [
  'model' => $devFilters,
  'attribute' => 'method',
  'data' =>ArrayHelper::map(Method::find()->asArray()->all(), 'id_method', 'nm_method'),
  'options' => ['placeholder' => 'Select a method' ],
  'pluginOptions' => [
  'allowClear' => true,
  'multiple' => true
  ],
  ])  ?>
  <?= $searchForm->field($devFilters, 'ishikawa', ['options'=>['class'=>'col-md-6']] )->widget(Select2::className(),
  [
  'model' => $devFilters,
  'attribute' => 'ishikawa',
  'data' =>ArrayHelper::map(Ishikawa::find()->asArray()->all(), 'id_ishikawa', 'nm_ishikawa'),
  'options' => ['placeholder' => 'Select a ishikawa' ],
  'pluginOptions' => [
  'allowClear' => true,
  'multiple' => true
  ],
  ])  ?>
  <?= $searchForm->field($devFilters, 'devlevel', ['options'=>['class'=>'col-md-6']] )->widget(Select2::className(),
  [
  'model' => $devFilters,
  'attribute' => 'level',
  'data' =>ArrayHelper::map(DevLevel::find()->asArray()->all(), 'id_level', 'nm_level'),
  'options' => ['placeholder' => 'Select a level' ],
  'pluginOptions' => [
  'allowClear' => true,
  'multiple' => true
  ],
  ])  ?>
  <?= $searchForm->field($devFilters, 'decision', ['options'=>['class'=>'col-md-6']] )->widget(Select2::className(),
  [
  'model' => $devFilters,
  'attribute' => 'decision',
  'data' =>ArrayHelper::map(Decision::find()->asArray()->all(), 'id_decision', 'nm_decision'),
  'options' => ['placeholder' => 'Select a decision' ],
  'pluginOptions' => [
  'allowClear' => true,
  'multiple' => true
  ],
  ])  ?>
<div class="col-md-12">
  <?= $searchForm->field($devFilters, 'section', ['options'=>['class'=>'col-md-6']] )->widget(Select2::className(),
  [
  'model' => $devFilters,
  'attribute' => 'section',
  'data' =>ArrayHelper::map($textQuery, 'COLUMN_NAME', 'COLUMN_NAME'),
  'options' => ['placeholder' => 'Select a section' ],
  'pluginOptions' => [
  'allowClear' => true,
  'multiple' => true
  ],
  ])  ?>
  <?= $searchForm->field($devFilters, 'keyword', ['options'=>['class'=>'col-md-6']] )->textInput();  ?>
</div>





</div>

<?php Modal::end();
//print_r($dataProvider->models);exit();


 ?>
    <?php ActiveForm::end();

  }//filter permission check end here
//dd($dataProvider->getModels());

    if(\Yii::$app->user->can('deviations excel'))//export to excel permission starts here
    {
    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
        'id_deviation',
        'state.nm_state',
        'user.username',
        'creation_time',
        'creation_date:date',
        'declaration.deviation_title',
        'declaration.type.nm_type',
        'declaration.plan.planned',
        'declaration.detection_date:date',
        'declaration.detection_time',
        'declaration.location.nm_locality',
        'declaration.process.nm_process',
        'declaration.equipment.nm_equipment',
        'declaration.product_code',
        'declaration.batch_number',
        'declaration.what',
        'declaration.how',
        'declaration.impact',
        'declaration.immediate_actions',
        'declaration.suspected_cause',
        //investigation
        'investigation.method.nm_method',
        'investigation.ishikawa.nm_ishikawa',
        'investigation.origin_anomaly',
        'investigation.history_anomaly',
        'investigation.process_review',
        'investigation.material_review',
        'investigation.batch_review',
        'investigation.conformity',
        'investigation.review_actions',
        'investigation.gmp_documents_review',
        'investigation.product_quality_review',
        'investigation.impact_review',
        'investigation.additional_analysis',
        'investigation.conclusion',
        //conclusion
        'conclusion.comment',
        'conclusion.final_decision',
        'conclusion.closing_date:date',
        'conclusion.deviation_duration',
        [
          'label'=>'batches',
          'value'=>function($model){
            $val='';
            foreach ($model->decision as $key => $value) {
              $val.='['.$value['id_batch'].':'.$value->decision['nm_decision'].'] , ';
            }
            return $val;
          }
        ],
        [
          'label'=>'signatories',
          'value'=>function($model){
            $val='';
            foreach ($model->signatories as $key => $value) {
              $val.='['.$value->user['username'].':'.$value->statenm['nm_sign_state'].'] , ';
            }
            return $val;
          }
        ],

    ];


    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'fontAwesome' => true,
        'exportConfig' => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_PDF=> false,
            ExportMenu::FORMAT_CSV=> false,
            ExportMenu::FORMAT_HTML=> false
        ],
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-default'
        ]
    ]) . "<hr>\n";
  }// export to excel permission ends here
     ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'rowOptions'=>['class'=>'list-table'],


        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
              'attribute'=>'id_deviation',
              //'filter'=>false,
              /*'contentOptions'=>
              function($model,$key,$index,$column)
              {//print_r($model);exit();

              $dev= new DevDeviation();
              $dev = DevDeviation::find()->with('state')->where(['id_deviation'=>$key])->one();
              return ['class'=>$dev->state['nm_state']];

              } ,*/

            ] ,
            [
              'value'=>'declaration.deviation_title',
              'label'=>'Deviation title',
            ],
            'creation_date:date',
            'creation_time:time',
            /*[
            'attribute'=>'creation_date:date',
            'label'=>'Creation date',
            'format'=> ['date', 'dd-MM-Y'],
             //'filter'=>false,



            ],
            [
            'attribute'=> 'creation_time:time',
            /*'label'=>'Creation time',
            'format'=> ['time', 'HH:mm'],
            //'filter'=>false,
            ],*/

            [
            'attribute' => 'creator', 'value' => 'user.username' ,
            /*'filter'=> false,/* Html::activeDropDownList($searchModel, 'creator', ArrayHelper::map(User::find()
            ->asArray()->all(), 'id_user', 'username'),
            ['class'=>'form-control','prompt' => 'Select Creator','multiple'=>''])
              MultiSelect::widget([
                    "model"=>$searchModel,
                    'attribute'=>'creator',
                    "options" => ['multiple'=>"multiple"], // for the actual multiselect
                    'data' => $multiUsers, // data as array
                    //'value' => [ 0], // if preselected
                    //'name' => 'DevDeviationSearch', // name for the form
                    "clientOptions" =>
                        [
                            "includeSelectAllOption" => true,
                            //'numberDisplayed' => 2
                        ],
                ])*/
            ],

            [
              'contentOptions'=>
              function($model,$key,$index,$column)
              {//print_r($model);exit();

              $dev= new DevDeviation();
              $dev = DevDeviation::find()->with('state')->where(['id_deviation'=>$key])->one();
              return ['class'=>$dev->state['nm_state']];

              } ,
              'attribute' => 'stateIn', 'value' => 'state.nm_state',
            /*'filter'=> false,/*Html::activeDropDownList($searchModel, 'stateIn', ArrayHelper::map(State::find()
            ->asArray()->all(), 'id_state', 'nm_state'),
            ['class'=>'form-control','prompt' => 'Select State'])

              Select2::widget([
                'model' => $searchModel,
                'attribute' => 'stateIn',
                'data' =>ArrayHelper::map(State::find()->asArray()->all(), 'id_state', 'nm_state'),
                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => true
                ],
            ])*/
              /* MultiSelect::widget([
                    "model"=>$searchModel,
                    'attribute'=>'stateIn',
                    //'id'=>"multiState",
                    "options" => ['multiple'=>"multiple"], // for the actual multiselect
                    'data' => ArrayHelper::map(State::find()->asArray()->all(), 'id_state', 'nm_state'), // data as array
                    //'value' => [ 0], // if preselected
                    //'name' => 'DevDeviationSearch', // name for the form
                    "clientOptions" =>
                        [
                            "includeSelectAllOption" => true,
                            'numberDisplayed' => 2
                        ],
                ])*/


            ],/*
            [
              //'attribute'=>'estimatedLvl.id_level',
              'contentOptions'=>
              function($model,$key,$index,$column)
              {//print_r($model);exit();
              $lev=    isset($model->estimatedLvl->attributes['id_level'])?$model->estimatedLvl->attributes['id_level']:null;

              switch($lev)
              {
                case 1 : return ['class'=>'eLvlMinor'];
                case 2 : return ['class'=>'eLvlMedium'];
                case 3 : return ['class'=>'eLvlMajor'];
                default: return ['class'=>''];
              }

            } ,
              'label'=>'Risk',
              //'value'=>'estimatedLvl.id_level',
            ],*/
            [
              //'attribute'=>'estimatedLvl.id_level',
              'contentOptions'=>
              function($model,$key,$index,$column)
              {//print_r($model->estimatedLvl);exit();
              //$lev=$elvl;    //isset($model->estimatedLvl->attributes['id_level'])?$model->estimatedLvl->attributes['id_level']:null;

              if($model->stateIn>INVESTIGATION)
              {
                $riskLVL= DevRpn::findOne(['id_deviation'=>$model->id_deviation]);
              }
              elseif($model->stateIn==INVESTIGATION)
              {
                $riskLVL= DevPreRpn::findOne(['id_deviation'=>$model->id_deviation]);
              }
              elseif($model->stateIn<INVESTIGATION)
              {
                $riskLVL= DevEstimatedLvl::findOne(['id_deviation'=>$model->id_deviation]);
              }

              if(isset($riskLVL->id_level))
              {
                switch($riskLVL->id_level)
                {
                  case 1 : return ['class'=>'eLvlMinor'];
                  case 2 : return ['class'=>'eLvlMedium'];
                  case 3 : return ['class'=>'eLvlMajor'];

                }
              }
              else
              {
                    return ['class'=>'eNorate'];
              }


            } ,
              'label'=>\Yii::t('app','Level'),
              //'value'=>'estimatedLvl.id_level',
            ],

            //'stateIn',

            [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{view}',
              'buttons'=>[
                  'view' => function ($url, $model, $key) {
                      return  Html::a('<span class="glyphicon glyphicon-search view-color"></span>', Url::to(['view-deviation', 'id' => $model->id_deviation])) ;
                  },
               ],
               //'contentOptions'=>['class'=>'text-center'],

            ],
        ],
    ]); ?>

</div>
