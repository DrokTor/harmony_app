<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Assignments;




/* @var $this yii\web\View */
/* @var $model app\models\DevDeviation */

$this->title = \Yii::t('app','Create deviation');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app','Deviations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$flash=\Yii::$app->session->allFlashes;

if(isset($flash['draftDiscarded']))
{
  $msg=Yii::$app->session->getFlash('draftDiscarded');
  $class='alert-success';
}
elseif(isset($flash['draftSaved']))
{
  $msg=Yii::$app->session->getFlash('draftSaved');
  $class='alert-success';
}
if(isset($class))
{
  echo \yii\bootstrap\Alert::widget([
   'options' => ['class' => $class.'  text-center'],
   'body' => $msg,
]);
}

?>
<div class="dev-deviation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(['action'=>((isset($state) && $state ==MISSINGINFO)?'save-missing':'new-draft').(isset($id)?'?id='.$id:''),'options' => ['class'=>'declaration-form-class','enctype' => 'multipart/form-data']]); ?>

    <input hidden="hidden"  name="newDraftID" value= <?= $id ?> />

    <?= $this->render('_form', [
        'model' => $model,'model_file'=>$model_file,'form'=>$form,
    ]) ?>

    <div class="form-group col-md-12">
         <?= Html::Button( \Yii::t('app','Launch deviation process') ,['class' => 'launch-process btn btn-success', 'name'=>'launch', 'value'=>'true','data-toggle'=>'modal','data-target'=>'#assignManagerModal' ]) ?>

        <?= Html::submitButton(  \Yii::t('app','Save'), ['class' =>  'save-draft btn btn-primary', 'name'=>'draft' ,'value'=>'true']) ?>

        <?php if(isset($id))
              echo Html::submitButton(  \Yii::t('app','Discard'), ['class' =>  'discard-draft btn btn-danger', 'name'=>'discard' ,'value'=>'true', 'data' => [
                  'confirm' => Yii::t('app', 'Are you sure you want to delete this draft?'),
                  'method' => 'post',
              ]])
        ?>
    </div>

      <?php

      $SectorManagers=ArrayHelper::map(Assignments::find()->with('user')->where(['item_name'=>'Sector manager'])->asArray()->all(),'user_id','user.username');
      //print_r($SectorManagers);

    echo '
    <!-- Modal -->
    <div id="assignManagerModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">'.\Yii::t('app',"Assign this declaration to sector manager").'</h4>
        </div>
        <div class="modal-body">
        '.Html::dropDownList( 'launch' ,null, $SectorManagers,   $options = ['prompt'=>\Yii::t('app','Select Sector manager')] ).
        '
        </div>
        <div class="modal-footer">
        <input type="submit" value="'.\Yii::t('app','Send').'" class="btn btn-default" />
          <button type="button" class="btn btn-default" data-dismiss="modal">'.\Yii::t('app','Cancel').'</button>
        </div>
      </div>

    </div>
    </div>';

    ?>
    <?php ActiveForm::end(); ?>

</div>
