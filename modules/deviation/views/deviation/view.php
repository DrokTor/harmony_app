<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\Tabs;
use app\modules\deviation\models\DevDeviation;
use app\modules\deviation\models\DevDeclaration;
use app\modules\deviation\models\DevInvestigation;
use app\modules\deviation\models\DevConclusion;
use app\modules\deviation\models\DevRpn;
use app\modules\deviation\models\DevPreRpn;
use app\modules\deviation\models\DevEstimatedLvl;
use yii\web\Session;
use yii\widgets\Pjax;




  /*$session = Yii::$app->session;
  $session->open();
  $session->set('id_deviation',$model->id_deviation);
*/
/* @var $this yii\web\View */
/* @var $model app\modules\deviation\models\DevDeviation */

$this->title = $model->id_deviation;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app','Deviations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dev-deviation-view">

    <!----<h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_deviation], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_deviation], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

  ---->

    <?=
      GridView::widget([
    'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{sorter}",

    'columns'=>
        [
          [
            'attribute'=>'id_deviation',
            //'label'=>'Id deviation',
          ],
          'creation_date:date',
          'creation_time:time',
          /*[
            'attribute'=>'creation_date',
            //'label'=>'Creation date',
            'format' => ['date', 'php:d-m-Y']
          ],
          [
            'attribute'=>'creation_time',
            //'label'=>'Creation time',
            'format' => ['time', 'H:mm']
          ],*/

          [
            'attribute'=>'creator',
            'label'=>\Yii::t('app','Creator'),
            'value' => function($model)
              {
              return $model->user['username'];
              } ,
          ],
          [
            'attribute'=>'stateIn',
            'label'=>\Yii::t('app','State'),
            'value' => function($model)
              {
              return $model->state['nm_state'];
              } ,
              'contentOptions'=>
              function($model,$key,$index,$column)
              {//print_r($model);exit();

              $dev= new DevDeviation();
              $dev = DevDeviation::find()->with('state')->where(['id_deviation'=>$key])->one();
              return ['class'=>$dev->state['nm_state']];

              } ,
          ],
          [
            //'attribute'=>'estimatedLvl.id_level',
            'contentOptions'=>
            function($model,$key,$index,$column)
            {//print_r($model->estimatedLvl);exit();
            //$lev=$elvl;    //isset($model->estimatedLvl->attributes['id_level'])?$model->estimatedLvl->attributes['id_level']:null;

            if($model->stateIn>INVESTIGATION)
            {
              $riskLVL= DevRpn::findOne(['id_deviation'=>$model->id_deviation]);
            }
            elseif($model->stateIn==INVESTIGATION)
            {
              $riskLVL= DevPreRpn::findOne(['id_deviation'=>$model->id_deviation]);
            }
            elseif($model->stateIn<INVESTIGATION)
            {
              $riskLVL= DevEstimatedLvl::findOne(['id_deviation'=>$model->id_deviation]);
            }

            if(isset($riskLVL->id_level))
            {
              switch($riskLVL->id_level)
              {
                case 1 : return ['class'=>'eLvlMinor'];
                case 2 : return ['class'=>'eLvlMedium'];
                case 3 : return ['class'=>'eLvlMajor'];

              }
            }
            else
            {
                  return ['class'=>'eNorate'];
            }


          } ,
            'label'=>\Yii::t('app','Level'),
            //'value'=>'estimatedLvl.id_level',
          ],

        ]
      ]);
//This deviation was closed in declaration state.<br>
//print_r(Yii::$app->request);exit();
if($decClosed)
{
  echo '
  <div class="alert alert-info">
  <strong>'.\Yii::t('app',"Closed in declaration by ").$by.': </strong>'.$decClosed.'
  </div>';
}elseif($investSkipped)
{
  echo '
  <div class="alert alert-info">
  <strong>'.\Yii::t('app',"Investigation skipped by ").$by.': </strong>'.$investSkipped.'
  </div>';
}elseif($requireInfo)
{
  echo '
  <div class="alert alert-info">
  <strong>'.\Yii::t('app',"More information requested by ").$by.': </strong>'.$requireInfo.'
  </div>';
}


      ?>




    <?php

    if(!isset($tabClass))
    {
      $tabClass='lvl-tab';
    }
    if($levelModel)
    switch($levelModel->id_level)
    {
      case null:$tabClass='lvl-tab';$class='lvl';$desc='undefined';break;
      case 1:$tabClass='lvl-tab lvl-tab-minor';$class='low';$desc='Minor';break;
      case 2:$tabClass='lvl-tab lvl-tab-major';$class='high';$desc='Major';break;
      case 3:$tabClass='lvl-tab lvl-tab-catastrophic';$class='crit';$desc='Catastrophic';break;
    }

    $tabs=[
        [
        'label' => \Yii::t('app','Declaration'),
        //'content' => $this->render('_declaration',['model'=> $model2,'files'=>$files ]),
         'options' => ['id' => 'declaration-tab','class' => 'fade  in'],
         'headerOptions' => ['id'=>'decLinkTab', 'class' => 'my-class col-md-3 row' ],

        ]
      ];
    if(!$draft && $model->stateIn>APPROVAL && !$decClosed)
    {
        $investSkipped?'':$tabs[]=[
            'label' => \Yii::t('app','Investigation'),
            //'content' => $this->render('_investigation',['model'=> $model3 ]),
            'options' => ['id' => 'investigation-tab','class' => 'fade in'],
            'headerOptions' => ['id'=>'investLinkTab', 'class' => 'my-class col-md-3 row ' ],//.(($model->stateIn<CONCLUSION)?'active':'').''
            //'visible'=>true,
            'active'=>($model->stateIn<CONCLUSION)?true:false,
        ];

      ($model->stateIn>=CONCLUSION)?
      $tabs[]=  [
            'label' => \Yii::t('app','Conclusion'),
            //'content' => $this->render('_conclusion',['model'=> $model4 ]),
            'options' => ['id' => 'conclusion-tab','class' => 'fade  in'],
            'headerOptions' => ['id'=>'concLinkTab', 'class' => 'my-class col-md-3 row ' ],
            'active'=>true,
        ]:'';



      /*$tabs[]=  [
            'label' => 'Risk level<span class="'.$tabClass.'"></span>',

            'content' => $this->render('_rating',['lvlModel'=>$levelModel/*'sev'=>$sev,'occ'=>$occ,'det'=>$det,'rpn'=>$level,'class'=>$class,'desc'=>$desc*///]), //,['model'=> $model4 ]
          /*  'options' => ['id' => 'risk-tab','class' => 'fade'],
            'headerOptions' => [ 'id'=>'riskLinkTab', 'class' => 'my-class col-md-3  row pull-right' ],
        ];*/
    }
    ($model->stateIn==CLOSED && \Yii::$app->user->can('deviation pdf'))?
/*
    $tabs[]=  [
          'label' => '<a target="_blank" href="/deviation/deviation/deviation-pdf?id='.$model->id_deviation.'" >Export to PDF</a>',
          //'content' => $this->render('_conclusion',['model'=> $model4 ]),
          'options' => ['id' => 'export-tab','class' => 'fade'],
          'headerOptions' => ['id'=>'exportLinkTab', 'class' => 'my-class col-md-3 row' ],
      ]:'';*/
    $tabs[]=  [
          'label' => \Yii::t('app',' Export to PDF'),//<a target="_blank" href="/deviation/capa/capa-pdf?id='.$dataProvider->getModels()[0]->id.'" >Export to PDF</a>',
          'url' => '/deviation/deviation/deviation-pdf?id='.$model->id_deviation.'',
          'linkOptions'=>['target'=>"_blank", 'class'=>'fa fa-file-pdf-o'],
          'options' => ['id' => 'export-tab','class' => ' fade'],
          'headerOptions' => ['id'=>'exportLinkTab', 'class' => '  col-md-3 row'],
      ]:'';
    //print_r($tabs);die();

     echo Tabs::widget([

       'encodeLabels' => false,
    'items' => $tabs/*[
        [
            'label' => 'Declaration',
            //'content' => $this->render('_declaration',['model'=> $model2,'files'=>$files ]),
             'options' => ['id' => 'declaration-tab','class' => 'fade in'],
             'headerOptions' => ['id'=>'decLinkTab', 'class' => 'my-class' ],

        ],
        [
            'label' => 'Investigation',
            //'content' => $this->render('_investigation',['model'=> $model3 ]),
            'options' => ['id' => 'investigation-tab','class' => 'fade'],
            'headerOptions' => ['id'=>'investLinkTab', 'class' => 'my-class' ],
            'visible'=>false,
        ],
        [
            'label' => 'Conclusion',
            //'content' => $this->render('_conclusion',['model'=> $model4 ]),
            'options' => ['id' => 'conclusion-tab','class' => 'fade'],
            'headerOptions' => ['id'=>'concLinkTab', 'class' => 'my-class' ],
        ],

    ]*/,

     ]); ?>


</div>
