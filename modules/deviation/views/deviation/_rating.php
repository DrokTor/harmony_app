<?php
use yii\helpers\html;
use yii\widgets\ActiveForm;
use yii\jui\SliderInput;



$id= Yii::$app->request->get('id');
//echo 'blbl'.$id; exit();
?>

<div id="ratingModal" >
  <?php     $form = ActiveForm::begin(['action'=>'rate-deviation?id='.$id]); ?>

  <div class="col-md-4" >



      <div  >
        <h3>Severity</h3><div id="sevTag" class=" <?= $lvlModel? ' lvl '.$lvlModel->sev['nm_class']:'lvl'  ; ?>" ><?= $lvlModel?$lvlModel->sev['nm_severity']:'undefined'  ; ?></div>

          <div class="rating">
              <?= SliderInput::widget([
                'id' => 'severity-slider',
                'name'=>'severity',
                'value'=>$lvlModel?$lvlModel->id_severity:1,
                'clientOptions' => [
                  'animate'=>'fast',
                    'min' => 1,
                    'max' => 5,
                ],
            ]);?>
          </div>
        </div>
      <div>
        <h3>Occurence</h3><div id="occTag" class=" <?= $lvlModel? ' lvl '.$lvlModel->occ['nm_class'] :'lvl'  ; ?>" ><?= $lvlModel?$lvlModel->occ['nm_occurence']:'undefined'  ; ?></div>

          <div class="rating">
            <?= SliderInput::widget([
              'id' => 'occurence-slider',
              'name'=>'occurence',
              'value'=>$lvlModel?$lvlModel->id_occurence:1,
              'clientOptions' => [
                'animate'=>'fast',
                  'min' => 1,
                  'max' => 5,
              ],
          ]);?>
            </div>
      </div>
      <div>
        <h3>Detectability</h3><div id="detTag" class=" <?= $lvlModel? ' lvl '.$lvlModel->det['nm_class'] :'lvl'  ; ?>" ><?= $lvlModel?$lvlModel->det['nm_detectability']:'undefined'  ; ?> </div>

          <div class="rating">
            <?= SliderInput::widget([
              'id' => 'detectability-slider',
              'name'=>'detectability',
              'value'=>$lvlModel?$lvlModel->id_detectability:1,
              'clientOptions' => [
                'animate'=>'fast',
                  'min' => 1,
                  'max' => 5,
              ],
          ]);?>
              </div>

      </div>



  </div>
  <div class="col-xs-12 col-md-8">
    <div class="col-xs-12 col-md-7 pull-right">
      <h3>Risk level</h3>
      <div id="lvl-screen" class=" <?= $lvlModel? ' lvl '.$lvlModel->lvl['nm_class'] :'lvl'  ; ?>"  ><div><?= $lvlModel?$lvlModel->lvl['nm_level']:'undefined'  ; ?></div></div>
    </div>
    <div class="col-xs-12 col-md-10 pull-right">
    <!---  <button id="rateButton" class="pull-right" >Rate</button>-->
      <?= Html::submitButton( 'Rate' ,['id'=>"rateButton",'class' => 'pull-right approve'     ]); ?>
    </div>
  </div>
    <?php   ActiveForm::end(); ?>
</div><!---
<label class="radio-inline"><input type="radio" value='1' name="severity"><div class="Vlow">Very low</div></label>
<label class="radio-inline"><input type="radio" value='2' name="severity"><div class="low">Low</div></label>
<label class="radio-inline"><input type="radio" value='3' name="severity"><div class="modr">Moderate</div></label>
<label class="radio-inline"><input type="radio" value='4' name="severity"><div class="high">High</div></label>
<label class="radio-inline"><input type="radio" value='5' name="severity"><div class="crit">Critical</div></label>
<label class="radio-inline"><input type="radio" value='1' name="occurence"><div class="Vlow">Very low</div></label>
<label class="radio-inline"><input type="radio" value='2' name="occurence"><div class="low">Low</div></label>
<label class="radio-inline"><input type="radio" value='3' name="occurence"><div class="modr">Moderate</div></label>
<label class="radio-inline"><input type="radio" value='4' name="occurence"><div class="high">High</div></label>
<label class="radio-inline"><input type="radio" value='5' name="occurence"><div class="crit">Very high</div></label>
<label class="radio-inline"><input type="radio" value='1' name="detectability"><div class="Vlow">Certain</div></label>
<label class="radio-inline"><input type="radio" value='2' name="detectability"><div class="low">High</div></label>
<label class="radio-inline"><input type="radio" value='3' name="detectability"><div class="modr">Moderate</div></label>
<label class="radio-inline"><input type="radio" value='4' name="detectability"><div class="high">Low</div></label>
<label class="radio-inline"><input type="radio" value='5' name="detectability"><div class="crit">Very low</div></label>
---->
