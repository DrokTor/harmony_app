<?php
//use dosamigos\chartjs\ChartJs;

use yii\widgets\Pjax;
use yii\helpers\Html;



/* @var $this yii\web\View */
/* @var $model app\models\DevDeviation */

$this->title =   \Yii::t('app','Data analysis') ;
$this->params['breadcrumbs'][] = ['label' => 'Deviation', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;



//echo $locality;exit();
/*print_r($methodHash);echo '<br>';
print_r($decision);exit();

<a id="monthly-button" href="data-analysis" class="btn btn-large btn-default" > <?= \Yii::t('app','Indicators') ?></a>
<a id="monthly-button" href="data-analysis?t=m" class="btn btn-large btn-default" > <?= \Yii::t('app','Monthly analysis') ?></a>
<a class="btn btn-large  btn-default" href="data-analysis?t=y"> <?= \Yii::t('app','Yearly analysis') ?></a>
<a class="btn btn-large  btn-default" href="data-analysis?t=g"> <?= \Yii::t('app','Global analysis') ?></a>
<a class="btn btn-large  btn-default" href="global-analysis"> <?= \Yii::t('app','Global analysis') ?></a>

*/

Pjax::begin(['timeout' => '500000' ]);

$monthlist=['Select a month','Jan','Feb','Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];
 ?>

<!--<div id="iconload" class="hidden">Im an icon..</div>-->
<a id="monthly-button" href="indicators" class="btn btn-large btn-default" > <?= \Yii::t('app','Indicators') ?></a>
<a id="monthly-button" href="monthly-analysis" class="btn btn-large btn-default" > <?= \Yii::t('app','Monthly analysis') ?></a>
<a class="btn btn-large  btn-default" href="yearly-analysis"> <?= \Yii::t('app','Yearly analysis') ?></a>
<button id="toPDF" class="btn btn-large  btn-info fa fa-file-pdf-o"> <?= \Yii::t('app',' Export to PDF') ?></button>
<button id="resize-data" class="fa fa-expand btn btn-large  btn-default pull-right"></button><br>

<?php
if(isset($selectedMonth))
{
?>
<div id="" class="col-md-12">
<?= Html::dropDownList( 'monthlist' ,$selectedMonth, $monthlist,   $options = [/*'prompt'=>'Select a month',*/ 'id'=>'monthDropDown','options'=>['0'=>['disabled'=>true]], 'class'=>'pull-right'] ) ?>
</div>

<?php
}
 if(isset($monthnum))
{
?>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Number of deviations per month') ?></h3>
  <div id="monthnum-data" data-title="<?=\Yii::t('app','Number of deviations per month') ?>" class='chartByNbr' data-hash=<?= $monthnum ?> > <canvas id="monthnum"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Incremental number of deviations per month') ?></h3>
  <div id="monthadd-data" data-title="<?=\Yii::t('app','Incremental number of deviations per month') ?>" class='chartByNbr' data-hash=<?= $monthadd ?> > <canvas id="monthadd"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Number of CAPAs per month') ?></h3>
  <div id="capaAnum-data" data-title="<?=\Yii::t('app','Number of CAPAs per month') ?>" class='chartByNbr' data-hash=<?= $capaAnum ?> > <canvas id="capaAnum"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Process per month') ?></h3>
  <div id="processAnum-data" data-title="<?=\Yii::t('app','Process per month') ?>" class='chartByAnum' data-hash="<?= $processAnum ?>" > <canvas id="processAnumCanv"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Severity per month') ?></h3>
  <div id="severityAnum-data" data-title="<?=\Yii::t('app','Severity per month') ?>" class='chartByAnum' data-hash="<?= $severityAnum ?>" > <canvas id="severityAnumCanv"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Method per month') ?></h3>
  <div id="methodAnum-data" data-title="<?=\Yii::t('app','Method per month') ?>" class='chartByAnum' data-hash="<?= $methodAnum ?>" > <canvas id="5MAnumCanv"  width="1250" height="700" ></canvas> </div>
</div>

<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','5M per month') ?></h3>
  <div id="ishikawaAnum-data" data-title="<?=\Yii::t('app','5M per month') ?>" class='chartByAnum' data-hash="<?= $ishikawa ?>" > <canvas id="5MAnumCanv"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Locality per month') ?></h3>
  <div id="locality-data" data-title="<?=\Yii::t('app','Locality per month') ?>" class='chartByAnum' data-hash="<?= $localityAnum ?>" > <canvas id="localityCanv"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Equipment per month') ?></h3>
  <div id="equipment-data"  data-title="<?=\Yii::t('app','Equipment per month') ?>" class='chartByAnum' data-hash="<?= $equipmentAnum ?>" > <canvas id="equipmentCanv"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Product per month') ?></h3>
  <div id="product-data" data-title="<?=\Yii::t('app','Product per month') ?>" class='chartByAnum' data-hash="<?= $productAnum ?>" > <canvas id="productCanv"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Decision per month') ?></h3>
  <div id="decision-data" data-title="<?=\Yii::t('app','Decision per month') ?>" class='chartByAnum' data-hash="<?= $decisionAnum ?>" > <canvas id="decisionCanv"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Category per month') ?></h3>
  <div id="categoryAnum-data" data-title="<?=\Yii::t('app','Category per month') ?>" class='chartByAnum' data-hash="<?= $categoryAnum ?>" > <canvas id="categoryCanv"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Closing rate per month') ?></h3>
  <div id="closingAnum-data" data-title="<?=\Yii::t('app','Closing rate per month') ?>" class='chartByNbr' data-hash="<?= $closAnum ?>" > <canvas id="closingAnum"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Average closing time per month') ?></h3>
  <div id="avgAnum-data" data-title="<?=\Yii::t('app','Average closing time per month') ?>" class='chartByNbr' data-hash="<?= $avgAnum ?>" > <canvas id="avgCanv"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Avg process per month') ?></h3>
  <div id="processAnum-data" data-title="<?=\Yii::t('app','Avg process per month') ?>" class='chartByAnum' data-hash="<?= $processAVGAnum ?>" > <canvas id="processCanv"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Avg product per month') ?></h3>
  <div id="productAnum-data" data-title="<?=\Yii::t('app','Avg product per month') ?>" class='chartByAnum' data-hash="<?= $productAVGAnum ?>" > <canvas id="productCanv"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Avg equipment per month') ?></h3>
  <div id="equipmentAnum-data" data-title="<?=\Yii::t('app','Avg equipment per month') ?>" class='chartByAnum' data-hash="<?= $equipmentAVGAnum ?>" > <canvas id="equipmentCanv"  width="1250" height="700" ></canvas> </div>
</div>
<div class="tb tbs col-md-6">
  <h3> <?=\Yii::t('app','Avg locality per month') ?></h3>
  <div id="localityAnum-data" data-title="<?=\Yii::t('app','Avg locality per month') ?>" class='chartByAnum' data-hash="<?= $localityAVGAnum ?>" > <canvas id="localityCanv"  width="1250" height="700" ></canvas> </div>
</div>
<?php
 }/*
elseif(isset($avgByProduct))
{
?>
<div class="tb tbs col-md-6">
  <h3>Average closing per month</h3>
  <div id="avgAnum-data" class='chartByNbr' data-hash="<?= $avgAnum ?>" > <canvas id="avgCanv"  width="1250" height="700" ></canvas> </div>
</div>



<?php
}*/
elseif(isset($DevNbr) && !isset($process)) {

?>
  <div class="tb tbi col-md-6">
    <div class="objectives col-md-10">
      <h3> <?= \Yii::t('app',"Number of deviations") ?></h3>
      <div> <?= $DevNbr ?> </div>
    </div>
    <div class="col-md-2 <?php if($DevNbr>$objectives[0]->objective) echo 'Obad'; else echo 'Ogood' ?>" > <div class="good"></div> <div class="bad"></div> </div>
  </div>
  <div class="tb tbi col-md-6">
    <div class="objectives col-md-10">
      <h3><?= \Yii::t('app',"Closing rate") ?></h3>
      <div> <?= $Clsd ?> %</div>
    </div>
    <div class="col-md-2 <?php if($Clsd<$objectives[1]->objective) echo 'Obad'; else echo 'Ogood' ?>" > <div class="good"></div> <div class="bad"></div> </div>
  </div>

  <div class="tb tbi col-md-6">
    <div class="objectives col-md-10">
      <h3> <?= \Yii::t('app',"Average closing time") ?></h3>
      <div> <?= $avg ?> days </div>
    </div>
    <div class="col-md-2 <?php if($avg>$objectives[2]->objective) echo 'Obad'; else echo 'Ogood' ?>" > <div class="good"></div> <div class="bad"></div> </div>
  </div>

  <div class="tb tbi col-md-6">
    <div class="objectives col-md-10">
      <h3> <?= \Yii::t('app',"Number of CAPAs") ?></h3>
      <div> <?= $capaNbr ?>  </div>
    </div>
    <div class="col-md-2 <?php if($capaNbr>$objectives[3]->objective) echo 'Obad'; else echo 'Ogood' ?>" > <div class="good"></div> <div class="bad"></div> </div>
  </div>

  <div class="tb tbi col-md-6">
    <div class="objectives col-md-10">
      <h3> <?= \Yii::t('app',"Deviations overdue") ?></h3>
      <div> <?= $devover ?>  </div>
    </div>
    <div class="col-md-2 <?php if($devover>$objectives[4]->objective) echo 'Obad'; else echo 'Ogood' ?>" > <div class="good"></div> <div class="bad"></div> </div>
  </div>

  <div class="tb tbi col-md-6">
    <div class="objectives col-md-10">
      <h3> <?= \Yii::t('app',"CAPAs overdue") ?> </h3>
      <div> <?= $capaover ?>  </div>
    </div>
    <div class="col-md-2 <?php if($capaover>$objectives[5]->objective) echo 'Obad'; else echo 'Ogood' ?>" > <div class="good"></div> <div class="bad"></div> </div>
  </div>

  <div class="tb tbi col-md-6">
    <div class="objectives col-md-10">
      <h3> <?= \Yii::t('app',"CAPAs Implemented") ?></h3>
      <div> <?= $capaimp ?> % </div>
    </div>
    <div class="col-md-2 <?php if($capaimp<$objectives[6]->objective) echo 'Obad'; else echo 'Ogood' ?>" > <div class="good"></div> <div class="bad"></div> </div>
  </div>

  <div class="tb tbi col-md-6">
    <div class="objectives col-md-10">
      <h3> <?= \Yii::t('app',"CAPAs efficacity review") ?> </h3>
      <div> <?= $capaeff ?> % </div>
    </div>
    <div class="col-md-2 <?php if($capaeff<$objectives[7]->objective) echo 'Obad'; else echo 'Ogood' ?>" > <div class="good"></div> <div class="bad"></div> </div>
  </div>

  <div class="tb tbi col-md-6">
    <div class="objectives col-md-10">
      <h3> <?= \Yii::t('app',"CAPAs efficacity overdue") ?></h3>
      <div> <?= $efficover ?>  </div>
    </div>
    <div class="col-md-2 <?php if($efficover>$objectives[8]->objective) echo 'Obad'; else echo 'Ogood' ?>" > <div class="good"></div> <div class="bad"></div> </div>
  </div>

  <?php
}
else
{



?>
<!---
<div class="tb tbs col-md-6">
  <h3>5M classification</h3>
  <div id="radar-data" data-material=  $material ?> data-milieu=$milieu  ?> data-manpower= $manpower ?> data-machine= $machine ?> data-methode= $method ?> ><canvas id="radar"  width="1250" height="700" ></canvas>
</div>
</div>--->


<div class="tb tbs col-md-6">
  <h3> <?= \Yii::t('app','Severity') ?></h3>
  <div id="severity-data" data-title="<?= \Yii::t('app','Severity') ?>"   data-low=<?= $low  ?>   data-hi=<?= $hight  ?> data-crit=<?= $critical  ?>><canvas id="severity" width="1250" height="700"  ></canvas></div>
</div>

<div class="tb tbs col-md-6">
  <h3> <?= \Yii::t('app','Category classification') ?></h3>
  <div id="category-data" data-title="<?= \Yii::t('app','Category classification') ?>" data-quality=<?= $qlt ?> data-hse=<?= $hse ?> ><canvas id="category" width="1250" height="700"  ></canvas></div>
</div>

<div class="tb tbs col-md-6">
  <h3> <?= \Yii::t('app','Process classification') ?></h3>
  <div id="process-data"  class='chartByHash' data-title="<?= \Yii::t('app','Process classification') ?>" data-hash="<?= $process ?>"   ><canvas id="process" width="1250" height="700"  ></canvas></div>
</div>

<div class="tb tbs col-md-6">
  <h3> <?= \Yii::t('app','Locality classification') ?></h3>
  <div id="locality-data" class='chartByHash' data-title="<?= \Yii::t('app','Locality classification') ?>"  data-hash="<?= $locality ?>"   ><canvas id="locality" width="1250" height="700"  ></canvas></div>
</div>


<div class="tb tbs col-md-6">
  <h3> <?= \Yii::t('app','Equipment classification') ?></h3>
  <div id="equipment-data" class='chartByHash' data-title="<?= \Yii::t('app','Equipment classification') ?>"  data-hash="<?= $equipment ?>"   ><canvas id="equipment"  width="1250" height="700" ></canvas></div>
</div>


<div class="tb tbs col-md-6">
  <h3> <?= \Yii::t('app','Product classification') ?></h3>
  <div id="product-data" class='chartByHash' data-title="<?= \Yii::t('app','Product classification') ?>"  data-hash="<?= $product ?>"   ><canvas id="product" width="1250" height="700"  ></canvas></div>
</div>


<div class="tb tbs col-md-6">
  <h3> <?= \Yii::t('app','5M classification') ?></h3>
  <div id="ishikawa-data" class='chartByHash' data-title="<?= \Yii::t('app','5M classification') ?>"  data-hash="<?= $ishikawa ?>"   ><canvas id="ishikawa"  width="1250" height="700" ></canvas></div>
</div>

<div class="tb tbs col-md-6">
  <h3> <?= \Yii::t('app','Method classification') ?></h3>
  <div id="method-data" class='chartByHash' data-title="<?= \Yii::t('app','Method classification') ?>"  data-hash="<?= $methodHash ?>"   ><canvas id="method"  width="1250" height="700" ></canvas></div>
</div>

<div class="tb tbs col-md-6">
  <h3> <?= \Yii::t('app','Decision classification') ?></h3>
  <div id="decision-data" class='chartByHash' data-title="<?= \Yii::t('app','Decision classification') ?>"  data-hash="<?= $decision ?>"   ><canvas id="decisionChart"  width="1250" height="700" ></canvas></div>
</div>


<div class="tb tbs col-md-6">
  <h3> <?= \Yii::t('app','Average closing per process') ?></h3>
  <div id="avgProcess-data" class='chartByHash' data-title="<?= \Yii::t('app','Average closing per process') ?>" data-hash="<?= $avgByProcess ?>" > <canvas id="avgProcessCanv"  width="1250" height="700" ></canvas> </div>
</div>

<div class="tb tbs col-md-6">
  <h3> <?= \Yii::t('app','Average closing per product') ?></h3>
  <div id="avgProduct-data" class='chartByHash' data-title="<?= \Yii::t('app','Average closing per product') ?>" data-hash="<?= $avgByProduct ?>" > <canvas id="avgProductCanv"  width="1250" height="700" ></canvas> </div>
</div>

<div class="tb tbs col-md-6">
  <h3> <?= \Yii::t('app','Average closing per locality') ?></h3>
  <div id="avgLocality-data" class='chartByHash' data-title="<?= \Yii::t('app','Average closing per locality') ?>" data-hash="<?= $avgByLocality ?>" > <canvas id="avgLocalityCanv"  width="1250" height="700" ></canvas> </div>
</div>

<div class="tb tbs col-md-6">
  <h3> <?= \Yii::t('app','Average closing per equipment') ?></h3>
  <div id="avgLocality-data" class='chartByHash' data-title="<?= \Yii::t('app','Average closing per equipment') ?>"   data-hash="<?= $avgByEquipment ?>" > <canvas id="avgLocalityCanv"  width="1250" height="700" ></canvas> </div>
</div>


<?php
}
Pjax::end();

?>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.5/Chart.js"></script>
 <!--script src="/js/chartsjs/Chart.js"></script-->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.debug.js"></script>
 <!--script src="/js/jspdf/jspdf.debug.js"></script-->
