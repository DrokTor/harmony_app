<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DevDeviation */

$this->title = 'Data analysis';
$this->params['breadcrumbs'][] = ['label' => 'Deviation', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dev-deviation-create">

    <!---<h1><?= Html::encode($this->title) ?></h1>--->

   <?= $this->render('_tb',['DevNbr'=>$DevNbr,'monthnum'=>$monthnum,'selectedMonth'=>$selectedMonth,'Clsd'=>$Clsd,'qlt'=>$qlt,'hse'=>$hse ,
   'avg'=>$avg/*,'manpower'=>$manpower, 'material'=>$material, 'method'=>$method, 'milieu'=>$milieu, 'machine'=>$machine*/
   ,'process'=>$process,'locality'=>$locality,'product'=>$product,'equipment'=>$equipment,'ishikawa'=>$ishikawa,'methodHash'=>$methodHash,
   'decision'=>$decision,'processAnum'=>$processAnum,'severityAnum'=>$severityAnum,'_5MAnum'=>$_5MAnum,'localityAnum'=>$localityAnum,
   'equipmentAnum'=>$equipmentAnum,'productAnum'=>$productAnum,'decisionAnum'=>$decisionAnum,'categoryAnum'=>$categoryAnum,'avgAnum'=>$avgAnum,
    'avgByProcess'=>$avgByProcess,'avgByProduct'=>$avgByProduct,'avgByLocality'=>$avgByLocality,'avgByEquipment'=>$avgByEquipment,
    'processAVGAnum'=>$processAVGAnum,'productAVGAnum'=>$productAVGAnum,'equipmentAVGAnum'=>$equipmentAVGAnum,'localityAVGAnum'=>$localityAVGAnum,
     'low'=>$low,   'hight'=>$hight, 'critical'=>$critical
 ]) ?>

</div>
