<?php
use dosamigos\chartjs\ChartJs;






?>
 <!---- <div id="legends" ></div>
  <canvas style="width: 300px; height: 200px;" id="myCanvas" width="300" height="200"></canvas>
  --><?= ChartJs::widget([
    'type' => 'Doughnut',
   'options' => [
        'height' => 200,
        'width' => 300,



    ],
    'data' => [

    [
        'value'=>  $model->where(['stateIn'=>'1'])->count(),
        'color'=> "#FFC081",
        'highlight'=> "#FFC081",
        'label'=> "Declaration",
          'labelColor' => 'white',
          'labelFontSize' => '16'
    ],
    [
        'value'=> $model->where(['stateIn'=>'2'])->count(),
        'color'=> "#F7F98F",
        'highlight' => "#F7F98F",
        'label'=> "Investigation"
    ],
    [
        'value'=> $model->where(['stateIn'=>'3'])->count(),
        'color'=> "#7EE4E9",
        'highlight'=> "#7EE4E9",
        'label' => "Conclusion"
    ]
]
          ]);
          ?>

   <?= ChartJs::widget([
    'type' => 'Bar',
    'options' => [
        'height' => 200,
        'width' => 300,
    ],
    'data' => [
        'labels' => ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        'datasets' => [
            [
                'fillColor' => "#FFC081",
                'strokeColor' => "#FFC081",
                'pointColor' => "rgba(220,220,220,1)",
                'pointStrokeColor' => "#fff",
                'data' => [(($cnt=$model->where(['Month(creation_date)'=>'1'])->count())==='0')?null:$cnt,
                           (($cnt=$model->where(['Month(creation_date)'=>'2'])->count())==='0')?null:$cnt,
                           (($cnt=$model->where(['Month(creation_date)'=>'3'])->count())==='0')?null:$cnt,
                           (($cnt=$model->where(['Month(creation_date)'=>'4'])->count())==='0')?null:$cnt,
                           (($cnt=$model->where(['Month(creation_date)'=>'5'])->count())==='0')?null:$cnt,
                           (($cnt=$model->where(['Month(creation_date)'=>'6'])->count())==='0')?null:$cnt,
                           (($cnt=$model->where(['Month(creation_date)'=>'7'])->count())==='0')?null:$cnt,
                           (($cnt=$model->where(['Month(creation_date)'=>'8'])->count())==='0')?null:$cnt,
                           (($cnt=$model->where(['Month(creation_date)'=>'9'])->count())==='0')?null:$cnt,
                           (($cnt=$model->where(['Month(creation_date)'=>'10'])->count())==='0')?null:$cnt,
                           (($cntt=$model->where(['Month(creation_date)'=>'11'])->count())==='0')?null:$cntt,
                           (($cnt=$model->where(['Month(creation_date)'=>'12'])->count())==='0')?null:$cnt,]
            ],
          ],
      ]
          ]);
          ?>



  <?= ChartJs::widget([
    'type'=>'Radar',
     'options' => [
        'height' => 200,
        'width' => 300,
    ],
  'data' => [
    'labels'=> ["Manpower", "Material", "Method", "Milieu", "Machine", ],
    'datasets'=> [
        [

            'label' => "My First dataset",
            'fillColor' => "rgba(220,220,220,0.2)",
            'strokeColor' => "#FFC081",
            'pointColor' => "rgba(220,220,220,1)",
            'pointStrokeColor' => "#fff",
            'pointHighlightFill' => "#fff",
            'pointHighlightStroke' => "rgba(220,220,220,1)",
            'data' => [$model2->where(['id_ishikawa'=>'1'])->count(),
                       $model2->where(['id_ishikawa'=>'2'])->count(),
                       $model2->where(['id_ishikawa'=>'3'])->count(),
                       $model2->where(['id_ishikawa'=>'4'])->count(),
                       $model2->where(['id_ishikawa'=>'5'])->count(),]
        ],

    ],
    ]


    ]);

?>
