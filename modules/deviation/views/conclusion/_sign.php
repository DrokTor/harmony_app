<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title" data-idconc="">Signature</h4>
  </div>


  <div class="modal-body">
  <h4><?= \Yii::t('app','Username') ?></h4>
  <label id="signatory" data-iduser= <?= $id ?> > <?= $user ?></label>

  <h4><?= \Yii::t('app','Password') ?></h4>

  <input id="signPassword" type="password" name="signPassword" />
  <h4><?= \Yii::t('app','Comment') ?></h4>
  <textarea id="signText"></textarea>
  </div>
  <div class="modal-footer">
    <!---<input type="submit" value="Send" class="btn btn-default" />--->
    <button id="signApprove" type="button" class="btn btn-success" name="signApprove" ><?= \Yii::t('app','Approve') ?></button>
    <button id="signDisapprove" type="button" class="btn btn-danger"  name="signDisapprove" ><?= \Yii::t('app','Disapprove') ?></button>
    <button type="button" class="btn btn-default" data-dismiss="modal" ><?= \Yii::t('app','Cancel') ?></button>
  </div>
</div>
