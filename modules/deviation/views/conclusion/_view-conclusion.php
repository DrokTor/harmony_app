<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use yii\bootstrap\Tabs;
use app\modules\deviation\models\DevConclusion;
use app\modules\deviation\models\DevInvestigation;
use app\models\Decision;

use yii\widgets\ActiveForm;



/* @var $form yii\widgets\ActiveForm */

  //$model2= new DevDeclaration();

/* @var $this yii\web\View */
/* @var $model app\modules\deviation\models\DevConclusion */
/*
$this->title = $model->id_deviation;
$this->params['breadcrumbs'][] = ['label' => 'Dev Deviations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/


if(DevInvestigation::findOne(['id_deviation'=>$model->id_deviation]))
{

  //print_r($model);
$attributes=[];
  foreach ($batches as $batch)
  {
    $attributes[]=['label'=>\Yii::t('app','Decision for batch: ').$batch['id_batch'], 'value'=>$batch->decision['nm_decision']];
  }


$attributes[]='comment';
$attributes[]='final_decision';

    /*[
    'label'=>'decision',
    'value'=>$model->decision['nm_decision'],


  ],*/




//print_r($attributes);exit();
?>
<div class="dev-deviation-view">



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

    <?= ($capas)?'<h3>CAPAS:</h3>':''; ?> 
    <?= $capas ?>
    <?= $this->render('_signForm',['model'=>$model, 'signProviders'=>$signProviders])  ?>


</div>
<?php
}
//


else{

echo \Yii::t('app',"the investigation step must be done first.");

}
?>
