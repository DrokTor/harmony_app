<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\Tabs;
use app\modules\deviation\models\DevConclusion;
use app\modules\deviation\models\DevInvestigation;
use app\models\Decision;

use yii\widgets\ActiveForm;



/* @var $form yii\widgets\ActiveForm */

  //$model2= new DevDeclaration();

/* @var $this yii\web\View */
/* @var $model app\modules\deviation\models\DevConclusion */
/*
$this->title = $model->id_deviation;
$this->params['breadcrumbs'][] = ['label' => 'Dev Deviations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/

//print_r($batches);exit();
if(DevInvestigation::findOne(['id_deviation'=>$model->id_deviation]))
{
//print_r($signProviders->getTotalCount());exit();
?>
<div class="dev-deviation-form">

    <?php $form = ActiveForm::begin(['id'=>'conclusionForm', 'action'=>'/deviation/conclusion/close-deviation']); ?>

    <?= $form->field($model, 'id_deviation', ['options'=>[ 'class'=>'hidden']])->hiddenInput()->label(false)  ?>

    <?php
    if(\Yii::$app->user->can('deviation decision'))
    {


     ?>

    <!-- $form->field($model, 'id_decision')->dropdownList(
    Decision::find()->select([ 'nm_decision','id_decision'])->indexBy('id_decision')->column(),
    ['prompt'=>'Select decision']
      );  -->
      <h3 class="collapseLabel col-md-12" data-toggle="collapse" data-target="#decision"><?= Yii::t('app','Decision') ?></h3>
    <div id="decision" class="collapse">
    <?php

    foreach ($batches as $batch)
    {
      echo "<div class='form-group' ><label>".\Yii::t('app',"Decision for batch N°: ").$batch['id_batch']."</label><br>";
      echo html::dropDownList('decision['.$batch['id_relation'].']',$batch['id_decision'],    Decision::find()->select([ 'nm_decision','id_decision'])->indexBy('id_decision')->column(),
          ['prompt'=>'Select decision','class'=>'batches-select']).'<br></div>';
    }

     ?>
    <?= $form->field($model, 'comment')->textInput() ?>

    <?= $form->field($model, 'final_decision')->textInput(['maxlength' => true]) ?>

  </div>

  <div class="form-group ">
      <br>
      <?= Html::submitButton(  \Yii::t('app','close')  , ['class' =>  'btn btn-success hidden' , 'name'=>"close", 'value'=>'true'])  ?>
      <?= Html::submitButton(  \Yii::t('app','Create CAPA')  , ['class' =>  'btn btn-success' , 'name'=>"initCapa", 'value'=>'true']) ?>
      <?= Html::submitButton(  \Yii::t('app','Save'), ['class' =>  'save-draft btn btn-primary', 'name'=>"save", 'value'=>'true']) ?>

  </div>
  <?php

    }elseif($model)
    {
      $attributes=[];
        foreach ($batches as $batch)
        {
          $attributes[]=['label'=>\Yii::t('app','Decision for batch: ').$batch['id_batch'], 'value'=>$batch->decision['nm_decision']];
        }


      $attributes[]='comment';
      $attributes[]='final_decision';

      echo  DetailView::widget([
          'model' => $model,
          'attributes' => $attributes,
      ]);

    }else{
      echo \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Conclusion not started yet.'),
        ]);
    } ?>





    <?= $this->render('_signForm',['model'=>$model ,'signProviders'=>$signProviders]) ?>





    <?php ActiveForm::end(); ?>

</div>




 <?php



}
else{
    //<?= $this->render('_signForm',['signProviders'=>$signProviders])
echo "the investigation step must be done first.";

}
?>
