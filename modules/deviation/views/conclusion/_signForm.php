<?php
use yii\grid\GridView;

//echo 'blablab'; exit();
 ?>


 <?= GridView::widget([
 'dataProvider' => $signProviders,
 'summary'=>false,
 'columns' => [


     [
         'value'=>'user.username',
         'label'=>\Yii::t('app','Username'),
     ],

     [
       'value'=>'notice.nm_notice',
       'label'=>\Yii::t('app','Notice'),
     ],
     'comment',
     [
       'class' => 'yii\grid\ActionColumn',
       'template' => '{view}',
       'buttons'=>[
           'view' => function ($url, $model, $key) {
               //return  Html::a('<span class="glyphicon glyphicon-search view-color"></span>', Url::to(['view-deviation', 'id' => $model->id_deviation])) ;
               return ($model->state!=null)?
                "<span class='signApproved'>".\Yii::t('app','Approved')."</span>":
                "<button type='button' class='btn btn-success col-xs-10'  name ='approve' value= 'true' data-username=".$model->user['username']." data-user=$model->id_user data-toggle ='modal' data-target= '#signModal'>".\Yii::t('app','Sign')."</button>";
           },
        ],
        //'contentOptions'=>['class'=>'text-center'],

     ],

     //'created_at:datetime',
     // ...
 ],
 ]) ?>

 <div id="signModal" class="modal fade" role="dialog">
  <div class="modal-dialog" data-iddev= <?= $model->id_deviation ?> >



 </div>
 </div>
