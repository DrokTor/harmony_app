<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title" data-idconc="">Signature</h4>
  </div>


  <div class="modal-body">

  <label id="signatory"  >  <?= $msg ?> </label>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal" > <?= \Yii::t('app','Cancel') ?></button>
  </div>
</div>
