<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\Tabs;
use app\modules\deviation\models\DevConclusion;
use app\modules\deviation\models\DevInvestigation;
use app\models\Decision;
use kartik\date\DatePicker;
use app\modules\deviation\models\CapaReviewType;
use yii\widgets\ActiveForm;




//print_r($batches);exit();
if(1)
{
 ?>



<div class="dev-deviation-form">
    <?php $form = ActiveForm::begin([ 'action'=>'capa-efficacity','options' => ['id'=>'capa-implementation','class'=>'implementation-form-class']]); // ,'enctype' => 'multipart/form-data''id'=>'capa-implementation', ?>
    <?php //$form = ActiveForm::begin([ 'action'=>'create-capa','options' => ['class'=>'declaration-form-class','enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'capa_id', ['options'=>[ 'class'=>'hidden']])->hiddenInput()->label(false)  ?>





    <?= $form->field($model, 'efficacity_review')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'review_type', ['options'=>['class'=>'']])->dropdownList(
    CapaReviewType::find()->select([ 'name','id'])->indexBy('id')->column(),
    ['prompt'=>'Select type']
      );  ?>

    <?= $form->field($model, 'detail')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'efficacity_conclusion')->textarea(['maxlength' => true]) ?>


    <?php
    //dd($url);
    $arr=explode('<br/>',$model_file->list);
    unset($arr[count($arr)-1]);
    foreach ($arr as  $value) {

      echo '<span class="">'.$value;
      $value=explode('</a>',$value)[0];
      $value=explode('>',$value)[1];
      echo '<span class="del-file" data-type="'.$model_file->type.'" data-url="'.$model_file->url.'" data-name="'.$value.'" data-id="'.$model_file->id.'" >x</span></span> ';

    }
  //data-id="'.Yii::$app->request->getBodyParam('id').'"
       ?>
    <div class="form_field row">
    <?= $form->field($model_file, 'files[]')->fileInput(['multiple'=>true]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(  \Yii::t('app','Close efficacity review')  , ['class' =>  'btn btn-success' , 'name'=>"closeEfficacity", 'value'=>'true']) ?>
        <?= Html::submitButton(  \Yii::t('app','Save'), ['class' =>  'save-draft btn btn-primary', 'name'=>"save", 'value'=>'true']) ?>

    </div>



    <?php //echo $this->render('_signForm',['model'=>$model ,'signProviders'=>$signProviders]) ?>





    <?php ActiveForm::end(); ?>

</div>




 <?php



}
else{
    //<?= $this->render('_signForm',['signProviders'=>$signProviders])
echo "the investigation step must be done first.";

}
?>
