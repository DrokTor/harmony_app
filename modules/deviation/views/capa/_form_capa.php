<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Assignments;
use yii\helpers\ArrayHelper;
use kartik\time\TimePicker;
use kartik\date\DatePicker;
use app\modules\deviation\models\CapaType;

/* @var $this yii\web\View */
/* @var $model app\models\DevDeviation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="capa-create-form">


    <?= $form->field($model, 'deviation_id', ['options'=>[ 'class'=>'hidden']])->hiddenInput()->label(false)  ?>

    <div class='form_field row'>
    <?= $form->field($model, 'title', ['options'=>['class'=>'col-sm-6']])->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'type_id', ['options'=>['class'=>'col-sm-6']])->dropdownList(
    CapaType::find()->select([ 'name','id'])->indexBy('id')->column(),
    ['prompt'=>'Select type']
      );  ?>
    </div>

    <div class='form_field row'>
    <div class="form-group field-capadraft-detection_date col-sm-6">

     <?= DatePicker::widget([
      'model' => $model,
    'attribute' => 'implementation_date',
     'form' => $form,
    //'value' => date('d-M-Y', strtotime('+2 days')),

    'pluginOptions' => [

        'format' => 'yyyy-m-dd',
        'todayHighlight' => true
    ]
    ]);
    ?>
     </div>
     <?= $form->field($model, 'description', ['options'=>['class'=>'col-sm-6']])->textarea(['maxlength' => 500]) ?>

     </div>

     <?php
     //dd($url);
     $arr=explode('<br/>',$model_file->list);
     unset($arr[count($arr)-1]);
     foreach ($arr as  $value) {

       echo '<span class="">'.$value;
       $value=explode('</a>',$value)[0];
       $value=explode('>',$value)[1];
       echo '<span class="del-file" data-type="'.$model_file->type.'" data-url="'.$model_file->url.'" data-name="'.$value.'" data-id="'.$model_file->id.'" >x</span></span> ';

     }
   //data-id="'.Yii::$app->request->getBodyParam('id').'"
        ?>
     <div class="form_field row">
     <?= $form->field($model_file, 'files[]')->fileInput(['multiple'=>true]) ?>
     </div>

    <div class="form-group">


        <?= Html::Button(  \Yii::t('app','Create CAPA'), ['class' =>  'create-capa btn btn-success'/*, 'name'=>'capa' ,'value'=>'true'*/,'data-toggle'=>'modal','data-target'=>'#assignManagerModal']) ?>
        <?= Html::submitButton(  \Yii::t('app','Save'), ['class' =>  'save-draft btn btn-primary', 'name'=>'draft' ,'value'=>'true']) ?>
    </div>

      <?php

      //$SectorManagers=ArrayHelper::map(Assignments::find()->with('user')->where(['item_name'=>'Sector manager'])->asArray()->all(),'user_id','user.username');
      //print_r($SectorManagers);
/*

<?= Html::Button( 'Launch CAPA process' ,['class' => 'launch-process btn btn-success', 'name'=>'launch', 'value'=>'true','data-toggle'=>'modal','data-target'=>'#assignManagerModal' ]) ?>
*/
    echo '
    <!-- Modal -->
    <div id="assignManagerModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">'.\Yii::t('app','Assign this CAPA to sector manager').'</h4>
        </div>
        <div class="modal-body">
        '.Html::dropDownList( 'capa' ,null, $SectorManagers,   $options = ['prompt'=>'Select Sector manager'] ).
        '
        </div>
        <div class="modal-footer">
        <input type="submit" value="'.\Yii::t('app','Send').'" class="btn btn-default" />
          <button type="button" class="btn btn-default" data-dismiss="modal">'.\Yii::t('app','Cancel').'</button>
        </div>
      </div>

    </div>
    </div>';

    ?>


</div>
