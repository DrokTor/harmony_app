<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use app\models\user;
use app\models\State;
use app\models\Location;
use app\models\Process;
use app\models\Product;
use app\models\Equipment;
use app\models\Method;
use app\models\Ishikawa;
use app\models\Decision;
use app\modules\deviation\models\DevLevel;
use app\modules\deviation\models\DevRpn;
use app\modules\deviation\models\DevPreRpn;
use app\modules\deviation\models\DevEstimatedLvl;
use app\modules\deviation\models\DevType;
use app\modules\deviation\models\DevDeviation;
use kartik\select2\Select2;
use dosamigos\multiselect\MultiSelect;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DevDeviationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app','Capas list');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="dev-deviation-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'rowOptions'=>['class'=>'list-table'],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
              'attribute'=>'id',
            ] ,
            [
              'attribute'=>'title',
            ],

            [
            'attribute'=>'implementation_date',
            //'label'=>'Creation date',
            'format'=> ['date', 'dd-MM-Y'],
            ],
            [
            'label' => \Yii::t('app','Creator'),
            'value' => 'creator.username' ,
            ],
            [
            'label' => \Yii::t('app','State'),
            'value' => 'capaState.name' ,
            ],
            [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{view}',
              'buttons'=>[
                  'view' => function ($url, $model, $key) {
                      return  Html::a('<span class="glyphicon glyphicon-search view-color"></span>', Url::to(['view-capa', 'id' => $model->id])) ;
                  },
               ],
               //'contentOptions'=>['class'=>'text-center'],

            ],
        ],
    ]); ?>

</div>
