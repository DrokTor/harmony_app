<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\Tabs;
use app\modules\deviation\models\DevConclusion;
use app\modules\deviation\models\DevInvestigation;
use app\models\Decision;
use kartik\date\DatePicker;

use yii\widgets\ActiveForm;




//print_r($batches);exit();
if(1)
{
 ?>
<div class="dev-deviation-form">
    <?php $form = ActiveForm::begin([ 'action'=>'capa-implementation','options' => ['class'=>'implementation-form-class','enctype' => 'multipart/form-data']]); //'id'=>'capa-implementation', ?>
    <?php //$form = ActiveForm::begin([ 'action'=>'create-capa','options' => ['class'=>'declaration-form-class','enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'capa_id', ['options'=>[ 'class'=>'hidden']])->hiddenInput()->label(false)  ?>





    <?= DatePicker::widget([
      'model' => $model,
      'form' => $form,
    'attribute' => 'implementation_date',
    //'value' => date('d-M-Y', strtotime('+2 days')),
   // 'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [

        'format' => 'yyyy-m-dd',
        'todayHighlight' => true
    ]
    ]);
    ?>

    <?= DatePicker::widget([
      'model' => $model,
      'form' => $form,
    'attribute' => 'efficacity_review_date',
    //'value' => date('d-M-Y', strtotime('+2 days')),
   // 'options' => ['placeholder' => 'Select date ...'],
    'pluginOptions' => [

        'format' => 'yyyy-m-dd',
        'todayHighlight' => true
    ]
    ]);
    ?>

    <?= $form->field($model, 'implementation')->textarea(['maxlength' => true]) ?>


    <?php
    //dd($url);
    $arr=explode('<br/>',$model_file->list);
    unset($arr[count($arr)-1]);
    foreach ($arr as  $value) {

      echo '<span class="">'.$value;
      $value=explode('</a>',$value)[0];
      $value=explode('>',$value)[1];
      echo '<span class="del-file" data-type="'.$model_file->type.'" data-url="'.$model_file->url.'" data-name="'.$value.'" data-id="'.$model_file->id.'" >x</span></span> ';

    }
  //data-id="'.Yii::$app->request->getBodyParam('id').'"
       ?>
    <div class="form_field row">
    <?= $form->field($model_file, 'files[]')->fileInput(['multiple'=>true]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(  \Yii::t('app','Close CAPA')  , ['class' =>  'btn btn-success' , 'name'=>"closeCapa", 'value'=>'true']) ?>
        <?= Html::submitButton(  \Yii::t('app','Save'), ['class' =>  'save-draft btn btn-primary', 'name'=>"save", 'value'=>'true']) ?>

    </div>



    <?php //echo $this->render('_signForm',['model'=>$model ,'signProviders'=>$signProviders]) ?>





    <?php ActiveForm::end(); ?>

</div>




 <?php



}
else{
    //<?= $this->render('_signForm',['signProviders'=>$signProviders])
echo "the investigation step must be done first.";

}
?>
