<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;
use app\modules\deviation\models\DevDeclaration;
use yii\widgets\ActiveForm;
use app\models\Location;
use app\models\Process;
use app\models\Equipment;
use app\models\Product;
use app\modules\deviation\models\DevType;
use kartik\time\TimePicker;
use kartik\date\DatePicker;


  //$model2= new DevDeclaration();

/* @var $this yii\web\View */
/* @var $model app\modules\deviation\models\DevDeclaration */
/*
$this->title = $model->id_deviation;
$this->params['breadcrumbs'][] = ['label' => 'Dev Deviations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/




   ?>
<div class="dev-deviation-view">



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [


            [
              'attribute'=>'reviewType.name',
              'label'=>\Yii::t('app','Review type'),
            ],
            'detail',
            'efficacity_review',

            'efficacity_conclusion',
            [
              'label'=>\Yii::t('app','files'),
              'value'=>$model_file->list,
              'format'=>'html',
            ],




        ],
    ]) ?>

</div>
<?php


?>
