<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;
use app\modules\deviation\models\DevDeclaration;
use yii\widgets\ActiveForm;
use app\models\Location;
use app\models\Process;
use app\models\Equipment;
use app\models\Product;
use app\modules\deviation\models\DevType;
use kartik\time\TimePicker;
use kartik\date\DatePicker;


  //$model2= new DevDeclaration();

/* @var $this yii\web\View */
/* @var $model app\modules\deviation\models\DevDeclaration */
/*
$this->title = $model->id_deviation;
$this->params['breadcrumbs'][] = ['label' => 'Dev Deviations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/




   ?>
<div class="dev-deviation-view">



    <?= DetailView::widget([
        'model' => $modelPlan,
        'attributes' => [


            'title',
            [
              'attribute'=>'capaType.name',
              'label'=> 'Type'

            ],
            'description',

            'implementation_date:date',
            [
              'label'=>\Yii::t('app','Files'),
              'value'=>$model_file->list,
              'format'=>'html',
            ],




        ],
    ]) ?>
</div>
<?php

/*
if($dev_model->stateIn==APPROVAL)
{
  $id=Yii::$app->request->getBodyParam('id');
  $form = ActiveForm::begin(['action'=>'approve-deviation?id='.$id]);
  echo Html::button( 'Approve deviation' ,['class' => ' approve' , 'name'=>'approve', 'value'=>'true','data-toggle'=>'modal','data-target'=>'#approveModal' ]);
  echo Html::button( 'Require more information' ,['class' => ' moreInfo', 'name'=>'moreInfo', 'value'=>'true','data-toggle'=>'modal','data-target'=>'#moreinfoModal' ]);
  echo Html::button( 'Close deviation' ,['class' => ' discardDev', 'name'=>'discardDev', 'value'=>'true','data-toggle'=>'modal','data-target'=>'#closeModal' ]);
// problem here !!!!!!!!!

//print_r ($levelData);
  echo '
<!-- Modal -->
<div id="approveModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Estimated risk level</h4>
      </div>
      <div class="modal-body">
'.Html::dropDownList( 'approve' ,null, $levelData,   $options = ['prompt'=>'Select Level'] ).
'
      </div>
      <div class="modal-footer">
      <input type="submit" value="Send" class="btn btn-default" />
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>';

  echo '
<!-- Modal -->
<div id="moreinfoModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <textarea name="moreInfo"></textarea>
      </div>
      <div class="modal-footer">
      <input type="submit" value="Send" class="btn btn-default" />
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>';

echo '

<div id="closeModal" class="modal fade" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Modal Header</h4>
    </div>
    <div class="modal-body">
      <textarea name="discardDev"></textarea>
    </div>
    <div class="modal-footer">
    <input type="submit" value="valiate" class="btn btn-default" />
      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    </div>
  </div>

</div>
</div>';

  ActiveForm::end();
}
*/
?>
