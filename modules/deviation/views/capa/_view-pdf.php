<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\VarDumper;
 //VarDumper::dump($rpnProvider->getModels()[0],50,true); die();

?>
<div class="dev-deviation-view">


    <h1> <?=\Yii::t('app','Capa management rapport N° ')?> <?= $dataProvider->getModels()[0]->id ?></h1>
    <h2 class="title">- <?= $dataProvider->getModels()[0]->title ?> -</h2>
    <?=
      GridView::widget([
    'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{sorter}",

    'columns'=>
        [

          'deviation_id',
          'creation_date:date',
          /*[
            'attribute'=>'creation_date:date',
            //'format' => ['date', 'php:d-m-Y']
          ],*/
          [
            'attribute'=>'creator',
            'value' => function($model)
              {
              return $model->creator->username;
              } ,
          ],


        ]
      ]);
  ?>

  <h2><?= \Yii::t('app','Planning') ?>:</h2>
  <?= DetailView::widget([
      'model' => $dec,
      'attributes' => [

          [
            'label'=>'Type',
             'value'=>$dec->capaType->name,

          ],


          'description' ,
          'implementation_date:date' ,



      ],
  ]) ?>

  <h2><?= Yii::t('app','Implementation') ?>:</h2>


  <?= DetailView::widget([
      'model' => $imp,
    //'template'=> '<tr><th>{label}</th><td class="lvl-Vlow">{value}</td></tr>',
      'attributes' => [

          'implementation_date:date' ,
          'implementation' ,
          'efficacity_review_date:date',


      ],
  ]) ?>



<?php
if($eff)
{

?>
    <h2><?= Yii::t('app','Efficacity review') ?>:</h2>

    <?= DetailView::widget([
        'model' => $eff,

        'attributes' =>
        [
          [
            'attribute'=>'review_type',
            'value'=>$eff->reviewType->name,
          ],
          'detail',
          'efficacity_review',
          'efficacity_conclusion'
        ]
    ]) ?>

<?php

}
elseif(!$eff && !$imp->efficacity_review_date) {

  echo \Yii::t('app','Efficacity review not planned.');
}
elseif(!$eff && $imp->efficacity_review_date) {

  echo \Yii::t('app','Efficacity review not done yet.');
}
?>

</div>
