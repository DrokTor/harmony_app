<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\Tabs;
use app\modules\deviation\models\DevDeviation;
use app\modules\deviation\models\DevDeclaration;
use app\modules\deviation\models\DevInvestigation;
use app\modules\deviation\models\DevConclusion;
use app\modules\deviation\models\DevRpn;
use app\modules\deviation\models\DevPreRpn;
use app\modules\deviation\models\DevEstimatedLvl;
use yii\web\Session;
use yii\widgets\Pjax;




  /*$session = Yii::$app->session;
  $session->open();
  $session->set('id_deviation',$model->id_deviation);
*/
/* @var $this yii\web\View */
/* @var $model app\modules\deviation\models\DevDeviation */

//$this->title = $model->id_deviation;   <h1><?= Html::encode($this->title)  </h1>
//print_r($dataProvider->getModels()[0]['creator']->username);exit();
//print_r($dataProvider->getModels()[0]['capaState']);exit();
$this->params['breadcrumbs'][] = ['label' => 'CAPA', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="capa-view">




    <?=
      GridView::widget([
    'dataProvider' => $dataProvider,
        //'layout'=>"{items}\n{sorter}",
    //'sorter'=>[],
    'layout'=>"{items}\n{sorter}",
    //'sorter'=>[],


    'columns'=>
        [
          [
            'attribute'=>'id',
          ],
          [
            'attribute'=>'deviation_id',
          ],
          [
            'attribute'=>'creator.username',
            'label'=>\Yii::t('app','Creator'),

          ],
          [
            'attribute'=>'creation_date',
            'format'=>'date',

          ],
          [
            'attribute'=>'capaState.name',
            'label'=>\Yii::t('app','state'),

          ],
          /*[
            'attribute'=>'creation_date',
            'label'=>'Creation date',
            'format' => ['date', 'php:d-m-Y']
          ],*/


        /*  [
            'attribute'=>'stateIn',
            'label'=>'state',
            'value' => function($model)
              {
              return $model->state['nm_state'];
              } ,
              'contentOptions'=>
              function($model,$key,$index,$column)
              {//print_r($model);exit();

              $dev= new DevDeviation();
              $dev = DevDeviation::find()->with('state')->where(['id_deviation'=>$key])->one();
              return ['class'=>$dev->state['nm_state']];

              } ,
          ],*/


        ]
      ]);
//This deviation was closed in declaration state.<br>
//print_r(Yii::$app->request);exit();


      ?>




    <?php



    $tabs=[
        [
        'label' => \Yii::t('app','Planning'),
      'content' => $modelPlan,
         'options' => ['id' => 'capa-plan-tab','class' => 'fade '],
         'headerOptions' => ['id'=>'capaPlanTab', 'class' => 'tab  col-md-3 row' ],

        ]
      ];
    if(1) //!$draft && $model->stateIn>APPROVAL && !$decClosed
    {
        (0)?'':$tabs[]=[
            'label' => \Yii::t('app','Implementation'),
            'content' =>  $modelImplement,//$this->render('_investigation',['model'=> $model3 ]),
            'options' => ['id' => 'capa-implement-tab','class' => ' fade in'],
            'headerOptions' => ['id'=>'capaImplementTab', 'class' => 'tab  col-md-3 row' ],
            //'visible'=>false,
            'active'=>true,
        ];

      (1)?
      $tabs[]=  [
            'label' => \Yii::t('app','Efficacity review'),
            'content' => $modelEffic,//'fff',// $this->render('_conclusion',['model'=> $model4 ]),
            'options' => ['id' => 'capa-efficacity-tab','class' => ' fade'],
            'headerOptions' => ['id'=>'capaEfficacityTab', 'class' => 'tab col-md-3 row' ],
        ]:'';

        ($dataProvider->getModels()[0]->state>=CAPAIMPLEMENTATION)?

      $tabs[]=  [
            'label' => \Yii::t('app',' Export to PDF'),//<a target="_blank" href="/deviation/capa/capa-pdf?id='.$dataProvider->getModels()[0]->id.'" >Export to PDF</a>',
            'url' => '/deviation/capa/capa-pdf?id='.$dataProvider->getModels()[0]->id.'',
            'linkOptions'=>['target'=>"_blank", 'class'=>'fa fa-file-pdf-o'],
            'options' => ['id' => 'export-tab','class' => ' fade'],
            'headerOptions' => ['id'=>'exportLinkTab', 'class' => '  col-md-3 row'],
        ]:'';

    }
     echo Tabs::widget([

       'encodeLabels' => false,
    'items' => $tabs

     ]); ?>


</div>
