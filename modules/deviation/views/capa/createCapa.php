<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\DevDeviation */

$this->title = \Yii::t('app','Create CAPA');
$this->params['breadcrumbs'][] = ['label' => 'Deviations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="capa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([ 'action'=>'create-capa','options' => ['class'=>'declaration-form-class','enctype' => 'multipart/form-data']]); ?>

    <?= $this->render('_form_capa', [
        'model' => $model,
        'model_file'=>$model_file,
        'form'=>$form,
        'url'=>isset($url)?$url:'',
        'SectorManagers'=>$SectorManagers,
    ]) ?>

    <?php ActiveForm::end(); ?>
</div>
