<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;
use app\modules\deviation\models\DevDeclaration;
use yii\widgets\ActiveForm;
use app\models\Location;
use app\models\Process;
use app\models\Equipment;
use app\models\Product;
use app\modules\deviation\models\DevType;
use kartik\time\TimePicker;
use kartik\date\DatePicker;


  //$model2= new DevDeclaration();

/* @var $this yii\web\View */
/* @var $model app\modules\deviation\models\DevDeclaration */
/*
$this->title = $model->id_deviation;
$this->params['breadcrumbs'][] = ['label' => 'Dev Deviations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/



//dd($model);
   ?>
<div class="dev-deviation-view">



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [



            'implementation_date:date',
            'implementation',
            [
              'attribute'=>'efficacity_review_date',
              'value'=> $model->efficacity_review_date?$model->efficacity_review_date:\Yii::t('app','not applicable'),
              'format'=>$model->efficacity_review_date?'date':null,
            ],
            [
              'label'=>\Yii::t('app','Files'),
              'value'=>$model_file->list,
              'format'=>'html',
            ],




        ],
    ]) ?>
</div>
<?php


?>
