<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DevDeviationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dev-deviation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_deviation') ?>

    <?= $form->field($model, 'stateIn') ?>

    <?= $form->field($model, 'creator') ?>

    <?= $form->field($model, 'creation_timestamp') ?>

    <?= $form->field($model, 'creation_timestamp') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
