<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DevDeviationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dev Deviations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dev-deviation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dev Deviation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_deviation',
            'stateIn',
            'creator',
            'creation_date',
            'creation_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
