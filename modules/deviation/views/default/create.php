<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DevDeviation */

$this->title = 'Create Dev Deviation';
$this->params['breadcrumbs'][] = ['label' => 'Dev Deviations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dev-deviation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
