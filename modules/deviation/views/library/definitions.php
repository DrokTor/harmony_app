<h1>Definitions</h1>
<h3 data-toggle="collapse" data-target="#deviation" ><button type="button" class="btn btn-default">Deviation</button></h3>
<p id="deviation" class="collapse">
Departure from an approved instruction or established standard. [ICH Q7].
</p>


<h2 data-toggle="collapse" data-target="#devgen"><button type="button" class="btn btn-default">Deviation information</button></h2>
<ul id="devgen" class="collapse">
    <li><h3>Deviation title:</h3> As best practice, the title should include a key word associated to the deviation and the product/batch number(s).</li>
    <li><h3>Detection date:</h3> The date in which the deviation has been detected.</li>
    <li><h3>Detection time:</h3> The approximative time in which the deviation has been detected.</li>
    <li><h3>Type:</h3> Could be a Quality or an EHS related deviation.</li>
    <li><h3>Planning:</h3>  Has the deviation been planned?</li>
    <li><h3>Process:</h3>  The process in which the deviation occured.</li>
    <li><h3>Locality:</h3>  The process in which the deviation occured.</li>
    <li><h3>Equipment:</h3>  The equipment involved in the deviation.</li>
    <li><h3>Product Code:</h3>  The product involved in the deviation.</li>
    <li><h3>Batch number:</h3>  The batches impacted by the deviation.</li>
    <li><h3>What:</h3>  What happened exactly in this deviation ?</li>
    <li><h3>How:</h3>  How did it happen ?</li>
    <li><h3>Impact:</h3>  What is the impact of this deviation ?</li>
    <li><h3>Immediate actions:</h3>  What are the immediate actions taken after the deviation has occured ?</li>
    <li><h3>Suspected root cause:</h3>  What are the suspected root causes ?</li>
</ul>

<h2 data-toggle="collapse" data-target="#devinvest"><button type="button" class="btn btn-default">Deviation investigation</button></h2>
<ul id="devinvest" class="collapse">
    <li><h3>Method:</h3> </li>
    <li><h3>Ishikawa:</h3> </li>
    <li><h3>Identified root causes:</h3>  </li>
    <li><h3>History:</h3> </li>
    <li><h3>Process review:</h3>  </li>
    <li><h3>Material review:</h3>  </li>
    <li><h3>Batch review:</h3>  </li>
    <li><h3>Equipment review:</h3> </li>
    <li><h3>Regulatory compliance:</h3> </li>
    <li><h3>Actions review:</h3>  </li>
    <li><h3>Impact review:</h3>  </li>
    <li><h3>GMP document review:</h3>  </li>
    <li><h3>Product quality review:</h3>  </li>
    <li><h3>Additional analysis and tests:</h3>  </li>
    <li><h3>Conclusion:</h3>  </li>
</ul>

<h2 data-toggle="collapse" data-target="#devconc"><button type="button" class="btn btn-default">Deviation conclusion</button></h2>
<ul id="devconc" class="collapse">
    <li><h3>Decision for batch:</h3> </li>
    <li><h3>Comment:</h3> </li>
    <li><h3>Final decision:</h3>  </li>
    <li><h3>Sign:</h3> </li>
</ul>
