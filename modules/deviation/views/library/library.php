<?php

use yii\widgets\Pjax;
use yii\helpers\Html;



/* @var $this yii\web\View */
/* @var $model app\models\DevDeviation */

$this->title =   \Yii::t('app','Data analysis') ;
$this->params['breadcrumbs'][] = ['label' => 'Deviation', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;



Pjax::begin();

  ?>

<!--<div id="iconload" class="hidden">Im an icon..</div>-->
<a id="" href="guide" class="btn btn-large btn-default" > <?= \Yii::t('app','User guide') ?></a>
<a id="" href="flowchart" class="btn btn-large btn-default" > <?= \Yii::t('app','Flowchart') ?></a>
<a id="" href="definitions" class="btn btn-large btn-default" > <?= \Yii::t('app','Definitions') ?></a>
<a class="btn btn-large  btn-default" href="refrences"> <?= \Yii::t('app','References') ?></a>


<?php

echo $page;

Pjax::end();

?>
