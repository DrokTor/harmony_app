<h3>Scope:</h3>
<p>This is a functional guide intended to help the user to get familiar with the Deviation & CAPA management module and guide him through each step with a quick and consice explanation.</p>
<p>The HARMONY-qms Deviatons & CAPA management system allows the thorough tracking of the Deviation and CAPA process, from the Deviation/CAPA creation to the closing. </p>
<h3>Deviation management:</h3>
<label>Definition:</label>
<p>< definition of deviation ></p>
<label>Deviation creation:</label>
<p>In order to create a deviation, please click on the first menu in the menu bar on the left.</p>
<p>This action will show a page allowing you to document the deviation with numerous information, in this page you can save the document as a draft and comeback later to finish the deviation declaration. When the declaration is done you can click on the "Launch deviation process" button, by clicking on the button you can choose from a list of "Managers" to inform them the declaration is waiting for their approval. </p>
<label>Deviation approval:</label>
<p>After the declaration was declared, the selected Manager will receive an email notifiying him about the deviation waiting for his approval.</p>
<p>The Manager should read through the declaration document and decide to approve the declaration, close the deviation or require more information from the creator of the deviation.</p>
<p>If the Manager decides to close it, it will pass to a "Closed" state.</p>
<p>The manager may decide to ask for more information if he thinks necessary, otherwise he can "Approve" the deviation and take it to the next step by selecting a "Quality expert" and send it to him giving an "Estimated rate" in the process.</p>
<p>If the deviation is sent back to the creator, he can add the information required and launch the process again.</p>
<label>Deviation investigation:</label>
<p>When the deviation is in "Declaration" state, the "Quality expert" intervenes to launch the investigation or might also skip this step should he decide to.</p>
<p>When the quality expert decides to launch the investigation, he selects an "Invistigator" to notify him and performs the "Preliminary rating".</p>
<p>The "Invistigator" receives an email informing him about the deviation to be investigated.</p>
<p>The trained "Investigator" finshes the investigation using his expertise.</p>
<p>At the end of the investigation, the "Quality expert" close the investigation step and performs the "Final rating".</p>
<label>Deviation conclusion:</label>
<p>When the "Quality expert" close the investigation, the deviation goes to its final stage "Conclusion", in this step, the "Quality expert" decides on what to do with the involved batches and put the deviation in the "Signing" state.</p>
<p>The deviation then is ready to be signed by the three actors "Manager", "Quality expert" and the "Investigator".</p>
<p>The "Quality expert" may decides to launch a CAPA for this deviation before closing it.</p>
<p>If one of the three actors decides to disapprove when signing the conclusion will reopen for modification.</p>
<p>When all of the actors have signed and approved the deviation the deviation process is finaly finished and goes to the "Closed" state.</p>
<p>At the end of the Deviation process, the complete process can be downloaded as a PDF.</p>
<label>Deviations list:</label>
<p>The "Deviations list" menu (second on the left menu), allows you to view all the deviations availables on the system, you can have a general view of the evolution of each deviation or you can select one of the deviations to access more details.</p>
<p>In this menu you have powerful tools such as "The filter" which allows you to filter throw the deviations list according to a specific criteria present in the desired deviation.</p>
<p>This menu offers also the possibility of extracting the deviation list (or the filtered deviations list) to spreadsheet if the should the need arise.</p>
<h3>CAPA management:</h3>
<label>CAPA planification:</label>
<p>After the "Quality expert" launches a CAPA, he can save or launch the CAPA planification.</p>
<label>CAPA implementation:</label>
<p>In this step the CAPA should be accomplished and the implementation document can be saved and closed when finished.</p>
<label>CAPA efficacity review:</label>
<p>After the implementation step is done, the CAPA can be evaluated with the "Efficacity review".</p>
<p>At the end of the CAPA process, the complete process can be downloaded as a PDF.</p>
<h3>Data analysis:</h3>
<p>The "Data analysis" section (the third menu on the left) is powerful tool intended for managers which offers you a complete set of indicators and charts in an instant reflecting the situation of the system.</p>
<p>The Data is separated into different subsections</p>
<label>Indicators:</label>
<p>In this section the Manager/Analyst has a quick view on the situation of this month. The section also provide an efficient view with color coded lights system displaying the system health situation.</p>
<label>Monthly analysis:</label>
<p>This section displays a multitude of detailed charts covering a wide range of the system data.</p>
<p>This section is monthly based, by default it displays the current monthly data but also allows you to select another month to view its related data.</p>
<p>The charts can be displayed in small size or bigger size using the switch on the top right corner.</p>
<p>All the charts can be exported to PDF with the "export pdf" button.</p>
<label>Yearly analysis:</label>
<p>This section displays a multitude of detailed charts showing the yearly evolution of a wide range of the system data.</p>
<p>The charts can be displayed in small size or bigger size using the switch on the top right corner.</p>
<p>All the charts can be exported to PDF with the "export pdf" button.</p>
