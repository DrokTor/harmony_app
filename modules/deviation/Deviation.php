<?php

  namespace app\modules\deviation;

class Deviation extends \yii\base\Module
{
    public function init()
    {
        define('DRAFT','1');
        define('APPROVAL','2');
        define('DECLARATION','3');
        define('INVESTIGATION','4');
        define('CONCLUSION','5');
        define('CLOSED','6');
        define('MISSINGINFO','7');
        define('SIGNING','8');
        define('MAXOPEN','30');
        define('SIGNAPPROVED','1');
        define('SIGNREJECTED','2');

        define('CAPADECLARATION','1');
        define('CAPAIMPLEMENTATION','2');
        define('CAPAEFFICACITYREVIEW','3');
        define('CAPADRAFT','4');

        parent::init();

        //\Yii::configure($this, require(__DIR__ . '/config.php'));
        $this->setAliases([
            '@dev' =>  __DIR__ ,  ///@webdeviation/deviation
            '@devUrl' => \Yii::$app->request->hostInfo.\Yii::getAlias('@web').'/deviation/deviation',
            '@capaUrl' => \Yii::$app->request->hostInfo.\Yii::getAlias('@web').'/deviation/capa',
        ]);
    }
}
