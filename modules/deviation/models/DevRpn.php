<?php
namespace app\modules\deviation\models;


use yii\db\ActiveRecord;



class DevRpn extends ActiveRecord
{

  public static function tableName()
  {
    return 'dev_rpn';
  }
  public function getDeviation()
  {
    return $this->hasOne(DevDeviation::className(),['id_deviation'=>'id_deviation']);
  }
  public function getSev()
  {
    return $this->hasOne(DevSeverity::className(),['id_severity'=>'id_severity']);
  }
  public function getOcc()
  {
    return $this->hasOne(DevOccurence::className(),['id_occurence'=>'id_occurence']);
  }
  public function getDet()
  {
    return $this->hasOne(DevDetectability::className(),['id_detectability'=>'id_detectability']);
  }
  public function getLvl()
  {
    return $this->hasOne(DevLevel::className(),['id_level'=>'id_level']);
  }

}




 ?>
