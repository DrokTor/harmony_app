<?php
namespace app\modules\deviation\models;

use yii\db\ActiveRecord;
use app\models\User;

class DevReasons extends ActiveRecord
{

public static function tableName()
{
  return "dev_reasons";
}

public function getUser()
{
  return $this->hasOne(User::className(),['id_user'=>'id_user']);
}


}


?>
