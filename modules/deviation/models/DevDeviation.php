<?php

namespace app\modules\deviation\models;

use Yii;
use app\models\User;
use app\models\State;
use app\modules\deviation\models\DevBatches;
use app\modules\deviation\models\DevSignatories;
use app\modules\deviation\models\DevApprovals;
/**
 * This is the model class for table "dev_deviation".
 *
 * @property integer $id_deviation
 * @property integer $stateIn
 * @property integer $creator
 * @property time    $creation_time
 * @property date      $creation_date
 */
class DevDeviation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dev_deviation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stateIn', 'creator'], 'required'],
            [['stateIn', 'creator'], 'integer'],
            [['creation_date'], 'date', 'format' =>'y-M-d'  ],
            //[['creation_time'], 'date', 'format' =>'H:mm' ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_deviation' => \Yii::t('app','Deviation ID'),
            'stateIn' => \Yii::t('app','State'),
            'creator' => \Yii::t('app','Creator'),
            'creation_date' => \Yii::t('app','Creation date'),
            'creation_time' => \Yii::t('app','Creation time'),
        ];
    }

    public function setState( $id, $state )
    {
          $dev=DevDeviation::findOne($id);
          $dev->stateIn=$state;
          $dev->save();
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'creator']);
    }
    public function getState()
    {
        return $this->hasOne(State::className(), ['id_state' => 'stateIn']);
    }
    public function getDeclaration()
    {
      return $this->hasOne(DevDeclaration::className(),['id_deviation'=>'id_deviation']);
    }
    public function getInvestigation()
    {
      return $this->hasOne(DevInvestigation::className(),['id_deviation'=>'id_deviation']);
    }
    public function getConclusion()
    {
      return $this->hasOne(DevConclusion::className(),['id_deviation'=>'id_deviation']);
    }
    public function getEstimatedLvl()
    {
      return $this->hasOne(DevEstimatedLvl::className(),['id_deviation'=>'id_deviation']);
    }
    public function getDecision()
    {
       return $this->hasMany(DevBatches::className(), ['id_deviation' => 'id_deviation']);
    }
    public function getSignatories()
    {
      return $this->hasMany(DevSignatories::className(), ['id_deviation' => 'id_deviation']);
    }

}
