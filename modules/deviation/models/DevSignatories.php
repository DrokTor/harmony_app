<?php

namespace app\modules\deviation\models;

use yii\db\ActiveRecord;
use app\models\User;
use app\modules\deviation\models\DevNotice;
use app\modules\deviation\models\DevSignState;





class DevSignatories extends ActiveRecord
{


    public static function tableName()
    {
        return 'dev_signatories';
    }
    /**
     * @inheritdoc
     */

     public function rules()
     {
         return [
             [['id_deviation', 'id_user', 'id_notice'], 'integer'],

         ];
     }

     public function attributeLabels()
     {
       return [

         'comment'=> \Yii::t('app','Comment')

        ];
     }

     public function add($id,$user)
     {

       $signatory= new DevSignatories();
       $signatory->id_deviation=$id;
       $signatory->id_user=$user;
       $signatory->save();


     }

     public function isSigned($id)
     {

       $check=DevSignatories::findOne(['id_deviation'=>$id,'state'=>null]);

       return !isset($check);
     }

     public function getUser()
     {
       return $this->hasOne(User::className(),['id_user'=>'id_user']);
     }
     public function getNotice()
     {
       return $this->hasOne(DevNotice::className(),['id_notice'=>'id_notice']);
     }
    public function getStatenm()
    {
        return $this->hasOne(DevSignState::className(), ['id_sign_state' => 'state']);
    }
    /*public function getDecision()
   {
       return $this->hasOne(Decision::className(), ['id_decision' => 'id_decision']);
   }*/
}
