<?php

namespace app\modules\deviation\models;

use Yii;

class CapaType extends yii\db\ActiveRecord
{
  public static function tableName()
  {
    return "capa_type";
  }


  public function rules()
  {
    return [
            [['name'],'required'],
            [['name'],'string']
              ];
  }


}
