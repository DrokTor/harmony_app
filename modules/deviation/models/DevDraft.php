<?php
namespace app\modules\deviation\models;

use yii\db\ActiveRecord;
use app\modules\deviation\models\DevDeviation;
use app\models\Location;
use app\models\Process;
use app\models\Equipment;
use app\models\Product;
use app\models\User;
use app\modules\deviation\models\DevType;


class DevDraft extends ActiveRecord
{
  public static function tableName()
  {
    return "dev_declaration_draft";
  }

  public function scenarios()
  {
      $scenarios = parent::scenarios();
      //$scenarios[self::SCENARIO_DRAFT] = [ 'deviation_title','id_type','what','detection_date','detection_time', 'how', 'locality', 'impact', 'immediate_actions', 'process_step', 'equipment_involved','suspected_cause', 'product_code', 'batch_number'];
      $scenarios[self::SCENARIO_DEFAULT] = [ 'deviation_title','id_type','id_planned','what','detection_date','detection_time', 'how', 'locality', 'impact', 'immediate_actions', 'process_step', 'equipment_involved','suspected_cause', 'product_code', 'batch_number'];

      return $scenarios;
  }

  public function rules()
  {
      return [
          /*[[ 'deviation_title','id_type','what','detection_date','detection_time', 'how', 'locality', 'impact', 'immediate_actions', 'process_step', 'equipment_involved','suspected_cause', 'product_code', 'batch_number'],
           'required', 'whenClient'=>"function(attribute,value){return false;}",'on' => self::SCENARIO_DEFAULT],*/
          [['id_deviation','id_type','id_planned', 'locality', 'process_step', 'equipment_involved', 'product_code'], 'integer'],
          [['deviation_title','what', 'how', 'impact', 'immediate_actions', 'batch_number' , 'suspected_cause'], 'string'],
          //[['detection_date'], 'date', 'format'=>'Y-M-dd'],
          //[['detection_time'],  'date','format'=>'H:mm'],
      ];
  }

  public function attributeLabels()
  {
      return [
          'id_declaration' => 'ID Draft',
          'id_deviation' => 'Id Deviation',
          'deviation_title'=>\Yii::t('app','Deviation title'),
          'id_type'=>\Yii::t('app','Type'),
          'id_planned'=>\Yii::t('app','Planification'),
          'detection_date'=> \Yii::t('app','Detection date'),
          'detection_time'=> \Yii::t('app','Detection time'),
          'what' => \Yii::t('app','What'),
          'how' => \Yii::t('app','How'),
          'locality' => \Yii::t('app','Locality'),
          'impact' => \Yii::t('app','Impact'),
          'immediate_actions' => \Yii::t('app','Immediate Actions'),
          'process_step' => \Yii::t('app','Process'),
          'equipment_involved' => \Yii::t('app','Equipment'),
          'suspected_cause'=>\Yii::t('app','Suspected root cause'),
          'product_code' => \Yii::t('app','Product code'),
          'batch_number' => \Yii::t('app','Batch Number'),
      ];
  }

  public function getUser()
 {
     return $this->hasOne(User::className(), ['id_user' => 'creator']);
 }
  public function getLocation()
 {
     return $this->hasOne(Location::className(), ['id_locality' => 'locality']);
 }

  public function getProcess()
 {
     return $this->hasOne(Process::className(), ['id_process' => 'process_step']);
 }

  public function getEquipment()
 {
     return $this->hasOne(Equipment::className(), ['id_equipment' => 'equipment_involved']);
 }

  public function getType()
  {
    return $this->hasOne(DevType::className(), ['id_type'=>'id_type']);
  }
 public function getProduct()
 {
    return $this->hasOne(Product::className(),['id_product'=>'product_code']);
 }


}






 ?>
