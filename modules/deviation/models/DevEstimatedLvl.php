<?php

namespace app\modules\deviation\models;

use yii\db\ActiveRecord;



class DevEstimatedLvl extends ActiveRecord
{


    public static function tableName()
    {
        return 'dev_estimated_level';
    }
    /**
     * @inheritdoc
     */

    public function getLvl()
    {
      return $this->hasOne(DevLevel::className(), ['id_level' => 'id_level']);

    }
    /*public function getDevDeviation()
    {
        return $this->hasOne(getDevDeviation::className(), ['state' => 'id_state']);
    }*/
}
