<?php

  namespace app\modules\deviation\models;

use yii\base\Model;

class DevFilters extends Model
{
   public $idDev;
   public $creationDate;
   public $creationTime;
   public $creator;
   public $state;
   public $type;
   public $locality;
   public $process;
   public $equipment;
   public $product;
   public $ishistory;
   public $method;
   public $ishikawa;
   public $devlevel;
   public $decision;
   public $section;
   public $keyword;

public function attributeLabels()
{
  return [
          'idDev'=>"Id deviation",
          'creationDate'=>"Creation date",
          'creationTime'=>"Creation time",
          'creator'=>'Creator',
           'state'=>'State',
           'type'=>'Type',
           'locality'=>'Locality',
           'process'=>'Process',
           'equipment'=>'Equipment',
           'product'=>'Product',
           'ishistory'=>'Is history',
           'method'=>'Method',
           'ishikawa'=>'Ishikawa',
           'devlevel'=>'Dev level',
           'decision'=>'Decision',
           'section'=>'Section',
           'keyword'=>'Keyword',


  ];

}

}
