<?php

namespace app\modules\deviation\models;

use Yii;
use app\modules\deviation\models\CapaReviewType;

class CapaEfficacity extends \yii\db\ActiveRecord
{

  const SAVE = 'save';
  const EFFICACITY = 'efficacity';

  public function scenarios()
  {
      $scenarios = parent::scenarios();
      $scenarios[self::SAVE] = ['capa_id','efficacity_review','review_type','detail','efficacity_conclusion'];
      $scenarios[self::EFFICACITY] = ['capa_id','efficacity_review','review_type','detail','efficacity_conclusion'];
      return $scenarios;
  }

  public static function tableName()
  {
    return "capa_efficacity";
  }


  public function rules()
  {
    return [
            [['capa_id','efficacity_review','review_type','detail','efficacity_conclusion'],'required', 'on' => self::EFFICACITY],
            [['capa_id','review_type' ],'integer'],
            [['efficacity_review' ,'efficacity_conclusion','detail'],'string']
              ];
  }

  public function attributeLabels()
  {
    return [
      'capa_id'=>\Yii::t('app','CAPA ID'),
      'efficacity_review'=>\Yii::t('app','Efficacity review'),
      'review_type'=>\Yii::t('app','Review type'),
      'detail'=>\Yii::t('app','Details'),
      'efficacity_conclusion'=>\Yii::t('app','Efficacity review conclusion')
    ];
  }

  public function getReviewType()
  {
    return $this->hasOne(CapaReviewType::className(),['id'=>'review_type']);
  }
}
