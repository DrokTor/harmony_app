<?php

namespace app\modules\deviation\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

class Files extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $files;
    public $id;
    public $list;
    public $url;
    public $type;

    public function rules()
    {
        return [
            [['files'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg,jpeg,doc,docx,odt,wpd,pdf,xls,xlsx,ods', 'maxFiles'=>4],
        ];
    }

    public function attributeLabels()
    {
      return [
        'files'=>Yii::t('app','Files'),
      ];
    }

    public function upload($id,$type)
    {
        //print_r($this);exit();
        if(isset($this->files[0]))
        {
          if ($this->validate()) {

              //$id=$mod->id_deviation;
              \yii\helpers\FileHelper::createDirectory(Yii::getAlias('@app').'/modules/deviation/uploads/'.$type.'/'.$id);
              foreach ($this->files as $file)
              {
              $file->saveAs( Yii::getAlias('@app').'/modules/deviation/uploads/'.$type.'/'.$id.'/' . $file->baseName . '.' . $file->extension);
              }
            return true;
          } else {
              return false;
          }
        }else {  return true;}
    }

    public function findFiles($id,$type,$url=null)
    {
      if(is_dir(Yii::getAlias('@app').'/modules/deviation/uploads/'.$type.'/'.$id.'/'))
      $dir=\yii\helpers\FileHelper::findFiles(Yii::getAlias('@app').'/modules/deviation/uploads/'.$type.'/'.$id.'/');
      $list='';
      if (isset($dir[0])) {
              foreach ($dir as $index => $file) {
                  $nameFile = substr($file, strrpos($file, '/') + 1);
                  $list.= \yii\helpers\Html::a($nameFile, Yii::getAlias('@web').'/deviation/'.($url?$url:$type).'/download-files?id='.$id.'&file='.$nameFile.'&type='.$type) . "<br/>" ; // render do ficheiro no browser
              }
          } else {
              $list= Yii::t('app',"There are no joined files available.");
          }
          return $list;
    }
    public function DeleteFile($id,$name,$type)
    {
      unlink(Yii::getAlias('@app').'/modules/deviation/uploads/'.$type.'/'.$id.'/' .$name);
      if(count(\yii\helpers\FileHelper::findFiles(Yii::getAlias('@app').'/modules/deviation/uploads/'.$type.'/'.$id.'/'))==0)
      {
        \yii\helpers\FileHelper::removeDirectory(Yii::getAlias('@app').'/modules/deviation/uploads/'.$type.'/'.$id);
      }
    }

    public function approveFiles($id,$draft=null,$type,$oldtype)
    {


      $dir=Yii::getAlias('@app').'/modules/deviation/uploads/'.$oldtype.'/'.$draft.'/';

      if(is_dir($dir))
      {


      $files=\yii\helpers\FileHelper::findFiles($dir);
      $dr=Yii::getAlias('@app').'/modules/deviation/uploads/'.$type.'/'.$id.'/';

      if(!empty($files))
      {
        if(!is_dir($dr))
        \yii\helpers\FileHelper::createDirectory($dr);

      foreach ($files as $file) {
        copy($file, Yii::getAlias('@app').'/modules/deviation/uploads/'.$type.'/'.$id.'/'.basename($file) );
        unlink($file);
        }
        rmdir($dir);
      }
      }
    }
}
