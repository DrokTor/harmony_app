<?php

namespace app\modules\deviation\models;

use yii\db\ActiveRecord;
use app\models\User;


 class DevApprovals extends ActiveRecord
{
/*
    public static function tableName()
    {
      return 'dev_approvals';
    }*/
    public function getManagerName()
    {
        return $this->hasOne(User::className(), ['id_user' => 'manager']);
    }
    public function getExpertName()
    {
        return $this->hasOne(User::className(), ['id_user' => 'expert']);
    }
    public function getInvestigatorName()
    {
        return $this->hasOne(User::className(), ['id_user' => 'investigator']);
    }
}
