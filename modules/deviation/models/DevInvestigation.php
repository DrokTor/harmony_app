<?php


namespace app\modules\deviation\models;

use Yii;
use app\modules\deviation\models\DevInvistigation;
use app\modules\deviation\models\DevInvestigationIshikawa;
use app\models\Method;
use app\models\Ishikawa;
use yii\helpers\ArrayHelper;
//use app\modules\deviation\models\DevLevel;
/**
 * This is the model class for table "dev_investigation".
 *
 * @property integer $id_investigation
 * @property integer $id_deviation
 * @property integer $id_method
 * @property integer $id_ishikawa
 * @property string $origin_anomaly
 * @property integer $is_history
 * @property string $history_anomaly
 * @property string $batch_review
 * @property string $equipment_review
 * @property string $conformity
 * @property string $review_actions
 * @property string $conclusion
 * @property integer $deviation_lvl
 */
class DevInvestigation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     public $id_ishikawa;

     const SCENARIO_SKIP= 'skip';
    const SCENARIO_DRAFT= 'draft';
    public static function tableName()
    {
        return 'dev_investigation';
    }

    /**
     * @inheritdoc
     */
     public function scenarios()
     {
         $scenarios = parent::scenarios();
         $scenarios[self::SCENARIO_SKIP] = [ 'id_deviation','skipped','skip_reason'];
         $scenarios[self::SCENARIO_DRAFT] = [  'id_method', 'id_ishikawa', 'origin_anomaly', 'history_anomaly','process_review', 'material_review', 'batch_review', 'equipment_review', 'conformity', 'review_actions','impact_review','gmp_documents_review','product_quality_review','additional_analysis', 'conclusion'];


         return $scenarios;
     }
    public function rules()
    {
        return [
            [['id_deviation', 'id_method', 'id_ishikawa', 'origin_anomaly', 'history_anomaly','process_review', 'material_review', 'batch_review', 'equipment_review', 'conformity', 'review_actions','impact_review','gmp_documents_review','product_quality_review','additional_analysis', 'conclusion'], 'required', 'whenClient'=>"function(attribute,value){return false;}",'on' => self::SCENARIO_DEFAULT],
            [['id_deviation', 'id_method', 'is_history'], 'integer'],
            [[ 'id_method'],  'filter', 'filter' => 'intval' ],
            ['id_ishikawa', 'each', 'rule' => ['integer']],
            [['origin_anomaly', 'history_anomaly','process_review', 'material_review', 'batch_review', 'equipment_review', 'conformity', 'review_actions','impact_review','gmp_documents_review','product_quality_review','additional_analysis', 'conclusion'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_investigation' => 'Id Investigation',
            'id_deviation' => 'Id Deviation',
            'id_method' => \Yii::t('app','Method'),
            'id_ishikawa' => \Yii::t('app','Ishikawa'),
            'origin_anomaly' => \Yii::t('app','Identified root causes'),
            'is_history' => 'Is History',
            'history_anomaly' => \Yii::t('app','History Anomaly'),
            'process_review' => \Yii::t('app','Process Review'),
            'material_review' => \Yii::t('app','Material Review'),
            'batch_review' => \Yii::t('app','Batch Review'),
            'equipment_review' => \Yii::t('app','Equipment Review'),
            'conformity' => \Yii::t('app','Regulatory compliance'),
            'review_actions' => \Yii::t('app','Review Actions'),
            'impact_review'=>\Yii::t('app','Impact Review'),
            'gmp_documents_review'=>\Yii::t('app','GMP documents Review'),
            'product_quality_review'=>\Yii::t('app','Product Quality Review'),
            'additional_analysis'=>\Yii::t('app','Addtional analysis and tests'),
            'conclusion' => \Yii::t('app','Conclusion'),
            //'deviation_lvl' => 'Deviation Lvl',
        ];
    }


  public function loadIshikawa($ishi)
  {
    //dd($this);
    $rows=[];
    DevInvestigationIshikawa::deleteAll('id_investigation = '.$this->id_investigation);
    foreach ($ishi as $key => $value) {
      $rows[]=['id_investigation'=>$this->id_investigation,$this->id_deviation,'id_ishikawa'=>$value];
     }
    Yii::$app->db->createCommand()->batchInsert('dev_investigation_ishikawa', ['id_investigation','id_deviation','id_ishikawa'], $rows)->execute();
  }

  public function selectedIshikawa($string=false)
  {
    $sel=DevInvestigationIshikawa::find()
      ->where(['id_investigation'=>$this->id_investigation])
      ->select('id_ishikawa')
      ->all();

      //dd($sel);

    if($string)
    {
      foreach ($sel as $key => $value) {

          $sel[$key]=$value->ishikawa->nm_ishikawa;
      }

       return implode($sel, ', ');
    }
    else
    {
      $sel=ArrayHelper::getColumn($sel, 'id_ishikawa');
      return $sel;

    }
  }

  public function getRelations()
  {
    return [
            //'id_deviation'=>['deviation','deviation'],
            'id_method'=>['method','nm_method'],
            'id_ishikawa'=>['ishikawa','nm_ishikawa'],
          ];
  }

  public function getMethod()
  {

      return $this->hasOne(Method::className(),['id_method'=>'id_method']);



  }

  public function getIshikawa()
  {

      return $this->hasMany(Ishikawa::className(),['id_ishikawa'=>'id_ishikawa'])
      ->viaTable('dev_investigation_ishikawa', ['id_investigation' => 'id_investigation']);

  }

/*  public function getLevel()
  {
      return $this->hasOne(DevLevel::className(),['id_level'=>'deviation_lvl']);

  }*/
}
