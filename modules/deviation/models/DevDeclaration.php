<?php

namespace app\modules\deviation\models;

use Yii;
use app\modules\deviation\models\DevDeviation;
use app\models\Location;
use app\models\Process;
use app\models\Equipment;
use app\models\Product;
use app\modules\deviation\models\DevType;
/**
 * This is the model class for table "dev_declaration".
 *
 * @property integer $id_declaration
 * @property integer $id_deviation
 * @property integer $id_type
 * @property time    $detected_time
 * @property date    $detected_date
 * @property string  $deviation_title
 * @property string  $what
 * @property string  $how
 * @property integer $locality
 * @property string  $impact
 * @property string  $immediate_actions
 * @property integer $process_step
 * @property integer $equipment_involved
 * @property integer $product_code
 * @property integer $batch_number
 */
class DevDeclaration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     public $typedev;
     const SCENARIO_DRAFT = 'draft';
    public static function tableName()
    {
        return 'dev_declaration';
    }


    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DRAFT] = [ 'deviation_title','id_type','id_planned','what','detection_date','detection_time', 'how', 'locality', 'impact', 'immediate_actions', 'process_step', 'equipment_involved','suspected_cause', 'product_code', 'batch_number'];

        return $scenarios;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'deviation_title','id_type','id_planned','what','detection_date','detection_time', 'how', 'locality', 'impact', 'immediate_actions', 'process_step', 'equipment_involved','suspected_cause', 'product_code', 'batch_number'],
             'required', 'whenClient'=>"function(attribute,value){return false;}",'on' => self::SCENARIO_DEFAULT],
             [['id_deviation','id_type','id_planned', 'locality', 'process_step', 'equipment_involved', 'product_code'],  'filter', 'filter' => 'intval'],
            [['id_deviation','id_type','id_planned', 'locality', 'process_step', 'equipment_involved', 'product_code'], 'integer'],
            [['deviation_title','what', 'how', 'impact', 'immediate_actions', 'batch_number'], 'string'],
            //[['detection_date'], 'date', 'format'=>'Y-M-dd'],
            //[['detection_time'],  'date','format'=>'H:mm'],
          //[['detection_time'],  'filter','filter'=>function($att){return $att.':00'; }*/],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_declaration' => 'Id Declaration',
            'id_deviation' => \Yii::t('app','Deviation ID'),
            'deviation_title'=>\Yii::t('app','Deviation title'),
            'id_type'=>'Type',
            'id_planned'=>\Yii::t('app','Planification'),
            'detection_date'=> \Yii::t('app','Detection date'),
            'detection_time'=> \Yii::t('app','Detection time'),
            'what' => \Yii::t('app','What'),
            'how' => \Yii::t('app','How'),
            'locality' => \Yii::t('app','Locality'),
            'impact' => \Yii::t('app','Impact'),
            'immediate_actions' => \Yii::t('app','Immediate Actions'),
            'process_step' => \Yii::t('app','Process'),
            'equipment_involved' => \Yii::t('app','Equipment'),
            'suspected_cause'=>\Yii::t('app','Suspected root cause'),
            'product_code' => \Yii::t('app','Product Code'),
            'batch_number' => \Yii::t('app','Batch Number'),
        ];
    }


    public function init_dev() //$draft=false $id='false'
    {


      if($this->validate())
        {
          /*$model_init=DevDeviation::findOne(['id_deviation'=>$this->id_deviation]);
          if(!$model_init)
          {
            $model_init = new DevDeviation();
          }*/
          $model_init = new DevDeviation();
          /*
          if($draft)
          {
            $model_init->stateIn=DRAFT;

            //print_r($this->scenario);exit();
          }   // 5 draft state
          else
          {
            $model_init->stateIn=APPROVAL; // 1= declaration state
          }*/

          $model_init->stateIn=APPROVAL;
          $model_init->creator=  Yii::$app->user->id;;
          $model_init->creation_time= date('h:m');  //\Yii::t('app', '{0, time, HH:mm}', time());
          $model_init->creation_date= date('Y-m-d');//\Yii::t('app', '{0, date, dd-MM-yyyy}', date());
          //print_r($model_init);exit();
          if( $model_init->save() )
          {

          $idlast = Yii::$app->db->getLastInsertID();
//print_r($id);exit();
           //if($id=='false')
           $this->id_deviation = $idlast  ;
  //        print_r($this->id_deviation);exit();

            return "success";
          }else  return ($model_init->errors);

        }else return ($this->errors);
    /**/


    }
    /*public function fields()
    {
      $fields= parent::fields();
      $fields['tt']=function(){ return $this->type->nm_type;};
      return $fields;
    }*/
    public function edit($id,$post)
    {
      $declaration= DevDeclaration::findOne(['id_deviation'=>$id]);
      $old=clone $declaration;
      //dd($old);
      $declaration->load($post);
      $declaration->validate();
      //$old=$declaration;
      $dirtyAt=$declaration->getDirtyAttributes();
      //dd($dirtyAt);
      //dd($declaration->toArray());
      $declaration->save();
      //dd($declaration);

      return ['dirty'=>$dirtyAt,'oldModel'=>$old];

    }

    public function getRelations()
    {
      return [
              //'id_deviation'=>['deviation','deviation'],
              'locality'=>['location','nm_locality'],
              'process_step'=>['process','nm_process'],
              'equipment_involved'=>['equipment','nm_equipment'],
              'id_type'=>['type','nm_type'],
              'id_planned'=>['plan','planned'],
              'product_code'=>['product','nm_product'],


            ];
    }

    public function getDeviation()
    {
      return $this->hasOne(DevDeviation::classNAme(),['id_deviation'=>'id_deviation']);
    }
     public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id_locality' => 'locality']);
    }

     public function getProcess()
    {
        return $this->hasOne(Process::className(), ['id_process' => 'process_step']);
    }

     public function getEquipment()
    {
        return $this->hasOne(Equipment::className(), ['id_equipment' => 'equipment_involved']);
    }

     public function getType()
     {
       return $this->hasOne(DevType::className(), ['id_type'=>'id_type']);
     }
     public function getPlan()
     {
       return $this->hasOne(DevPlanned::className(), ['id_planned'=>'id_planned']);
     }

    public function getProduct()
    {
       return $this->hasOne(Product::className(),['id_product'=>'product_code']);
    }

}
