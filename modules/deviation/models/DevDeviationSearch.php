<?php


namespace app\modules\deviation\models;


use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\deviation\models\DevDeviation;

//use app\models\User;

/**
 * DevDeviationSearch represents the model behind the search form about `app\models\DevDeviation`.
 */
class DevDeviationSearch extends DevDeviation
{
    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['id_deviation', 'stateIn', 'creator'], 'integer'],
            [[ 'creation_time','creation_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DevDeviation::find()->joinWith('declaration')->joinWith('investigation')->joinWith('conclusion')->orderBy('id_deviation DESC'); //->joinWith('estimatedLvl')

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $auth= \Yii::$app->authManager;
      //print_r( $params);
        /*$getIds=null;
        if(isset($params['DevDeviationSearch']['id_deviation']))
        {
        $getIds=$params['DevDeviationSearch']['id_deviation'];
        $params['DevDeviationSearch']['id_deviation']='';
        }
        $getCreators=null;
        if(isset($params['DevDeviationSearch']['creator']))
        {
          //print_r( $params['DevDeviationSearch']['creator']); exit();
        $getCreators=$params['DevDeviationSearch']['creator'];
        $params['DevDeviationSearch']['creator']='';
        }
        $getStates=null;
        if(isset($params['DevDeviationSearch']['stateIn']))
        {
        $getStates=$params['DevDeviationSearch']['stateIn'];
        $params['DevDeviationSearch']['stateIn']='';
        }*/

        if(\Yii::$app->user->can('deviations filter'))
        {

        /* check if a particular filter is present, get the hash and proccess it */
        $getIds=null;
        if(isset($params['DevFilters']['idDev']))
        {
        $getIds=$params['DevFilters']['idDev'];
        $params['DevFilters']['idDev']='';
        }
        $getCreators=null;
        if(isset($params['DevFilters']['creator']))
        {
          //print_r( $params['DevDeviationSearch']['creator']); exit();
        $getCreators=$params['DevFilters']['creator'];
        $params['DevFilters']['creator']='';
        }
        $getStates=null;
        if(isset($params['DevFilters']['state']))
        {
        $getStates=$params['DevFilters']['state'];
        $params['DevFilters']['state']='';
        }
        $getTypes=null;
        if(isset($params['DevFilters']['type']))
        {
        $getTypes=$params['DevFilters']['type'];
        $params['DevFilters']['type']='';
        }
        $getLocality=null;
        if(isset($params['DevFilters']['locality']))
        {
        $getLocality=$params['DevFilters']['locality'];
        $params['DevFilters']['locality']='';
        }
        $getProcess=null;
        if(isset($params['DevFilters']['process']))
        {
        $getProcess=$params['DevFilters']['process'];
        $params['DevFilters']['process']='';
        }
        $getEquipment=null;
        if(isset($params['DevFilters']['equipment']))
        {
        $getEquipment=$params['DevFilters']['equipment'];
        $params['DevFilters']['equipment']='';
        }
        $getProduct=null;
        if(isset($params['DevFilters']['product']))
        {
        $getProduct=$params['DevFilters']['product'];
        $params['DevFilters']['product']='';
        }
        $getMethod=null;
        if(isset($params['DevFilters']['method']))
        {
        $getMethod=$params['DevFilters']['method'];
        $params['DevFilters']['method']='';
        }
        $getIshikawa=null;
        if(isset($params['DevFilters']['ishikawa']))
        {
        $getIshikawa=$params['DevFilters']['ishikawa'];
        $params['DevFilters']['ishikawa']='';
        }
        $getDevlvl=null;
        if(isset($params['DevFilters']['devlevel']))
        {
        $getDevlvl=$params['DevFilters']['devlevel'];
        $params['DevFilters']['devlevel']='';
        }
        $getHistory=null;
        if(isset($params['DevFilters']['ishistory']))
        {
        $getHistory=$params['DevFilters']['ishistory'];
        $params['DevFilters']['ishistory']='';
        }
        $getDecision=null;
        if(isset($params['DevFilters']['decision']))
        {
        $getDecision=$params['DevFilters']['decision'];
        $params['DevFilters']['decision']='';
        }
        $getSection=null;
        if(isset($params['DevFilters']['section']))
        {
        $getSection=$params['DevFilters']['section'];
        $params['DevFilters']['section']='';
        }
        $getKeyword=null;
        if(isset($params['DevFilters']['keyword']))
        {
        $getKeyword=$params['DevFilters']['keyword'];
        $params['DevFilters']['keyword']='';
        }
     //print_r( $getStates); //exit();

        $this->load($params);
//print_r( $this->creator); exit();

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            //print_r( $this->errors); exit();
            return $dataProvider;
        }

        /*$query->andFilterWhere([
            'id_deviation' => $this->id_deviation,
            'stateIn' => $this->state['id_state'],
            'username' => $this->user['username'],
        ]);*/




        //print_r( $timeArray); echo count($timeArray); exit();
       //print_r( $DateArray); echo count($DateArray); exit();

        $query->andFilterWhere([
            //'id_deviation' => $this->id_deviation,
            'stateIn' => $this->stateIn,
            //'creator' => $this->creator,
        ]);



       // $query->andFilterWhere(['like', 'creation_time', $this->creation_time])
         //   ->andFilterWhere(['like', 'creation_date', $this->creation_date]);
          //->andFilterWhere(['like', 'state', $this->state['id_state']]);

      if($getIds)
      {
        $idArray=\yii\helpers\StringHelper::explode($getIds,',');
        $val_id=[];
        $ids=[];
        $idsBetween=['or'];
        for($i=0;$i<count($idArray);++$i)
        {
         if(!stristr($idArray[$i],'..'))  $ids[$i]= $idArray[$i];
          else $idsBetween[]=['between','dev_deviation.id_deviation', explode('..',$idArray[$i])[0],explode('..',$idArray[$i])[1] ];
        }
        $val_id['dev_deviation.id_deviation']=$ids;


        //print_r( $idsBetween);  exit();
        $query->andFilterWhere( ['or',$val_id,$idsBetween]  );
      }

      if($getCreators)
      {
        $creatorArray=$getCreators;//\yii\helpers\StringHelper::explode($this->creator,',');
        $val_creator=[];
        for($i=0;$i<count($creatorArray);++$i)
        {
         $val_creator[$i]= $creatorArray[$i];
        }
        //print_r( $val_creator);  exit();
        $query->andFilterWhere( ['creator'=>$val_creator]  );

      }

      if($getStates)
      {
        $stateArray=$getStates;//\yii\helpers\StringHelper::explode($this->creator,',');
        $val_state=[];
        for($i=0;$i<count($stateArray);++$i)
        {
         $val_state[$i]= $stateArray[$i] ;
        }
        //print_r( $val_state); // exit();
        $query->andFilterWhere( ['stateIn'=>$val_state]  );

      }
      if($getTypes)
      {
        $typeArray=$getTypes;//\yii\helpers\StringHelper::explode($this->creator,',');
        $val_type=[];
        for($i=0;$i<count($typeArray);++$i)
        {
         $val_type[$i]=  $typeArray[$i];
        }
        //print_r( $val_state); // exit();
        $query->andFilterWhere( ['id_type'=>$val_type]  );

      }
      if($getLocality)
      {
        $LocalityArray=$getLocality;//\yii\helpers\StringHelper::explode($this->creator,',');
        $val_locality=[];
        for($i=0;$i<count($LocalityArray);++$i)
        {
         $val_locality[$i]= $LocalityArray[$i] ;
        }
        //print_r( $LocalityArray ); // exit();
        $query->andFilterWhere( ['locality'=>$val_locality ]  );

      }
      if($getProcess)
      {
        $ProcessArray=$getProcess;//\yii\helpers\StringHelper::explode($this->creator,',');
        $val_process=[];
        for($i=0;$i<count($ProcessArray);++$i)
        {
         $val_process[$i]= $ProcessArray[$i] ;
        }
        //print_r( $LocalityArray ); // exit();
        $query->andFilterWhere( ['process_step'=>$val_process ]  );

      }
      if($getProduct)
      {
        $ProductArray=$getProduct;//\yii\helpers\StringHelper::explode($this->creator,',');
        $val_product=[];
        for($i=0;$i<count($ProductArray);++$i)
        {
         $val_product[$i]= $ProductArray[$i] ;
        }
        //print_r( $LocalityArray ); // exit();
        $query->andFilterWhere( ['product_code'=>$val_product ]  );

      }
      if($getEquipment)
      {
        $EquipmentArray=$getEquipment;//\yii\helpers\StringHelper::explode($this->creator,',');
        $val_equipment=[];
        for($i=0;$i<count($EquipmentArray);++$i)
        {
         $val_equipment[$i]= $EquipmentArray[$i] ;
        }
        //print_r( $LocalityArray ); // exit();
        $query->andFilterWhere( ['equipment_involved'=>$val_equipment ]  );

      }
      if($getMethod)
      {
        $methodArray=$getMethod;//\yii\helpers\StringHelper::explode($this->creator,',');
        $val_method=[];
        for($i=0;$i<count($methodArray);++$i)
        {
         $val_method[$i]= $methodArray[$i] ;
        }
        //print_r( $LocalityArray ); // exit();
        $query->andFilterWhere( ['id_method'=>$val_method ]  );

      }
      if($getIshikawa)
      {
        /*$EquipmentArray=$getEquipment;//\yii\helpers\StringHelper::explode($this->creator,',');
        $val_equipment=[];
        for($i=0;$i<count($EquipmentArray);++$i)
        {
         $val_equipment[$i]= $EquipmentArray[$i] ;
       }*/
        //print_r( $LocalityArray ); // exit();
        $query->andFilterWhere( ['id_ishikawa'=>$getIshikawa ]  );

      }
      if($getHistory=='1')
      {
        /*$EquipmentArray=$getEquipment;//\yii\helpers\StringHelper::explode($this->creator,',');
        $val_equipment=[];
        for($i=0;$i<count($EquipmentArray);++$i)
        {
         $val_equipment[$i]= $EquipmentArray[$i] ;
       }*/
        //print_r( $LocalityArray ); // exit();
        $query->andWhere( ['history_anomaly'=>'' ]  );

      }
      if($getDevlvl)
      {
        /*$EquipmentArray=$getEquipment;//\yii\helpers\StringHelper::explode($this->creator,',');
        $val_equipment=[];
        for($i=0;$i<count($EquipmentArray);++$i)
        {
         $val_equipment[$i]= $EquipmentArray[$i] ;
       }*/
        //print_r( $LocalityArray ); // exit();
        $query->andFilterWhere( ['deviation_lvl'=>$getDevlvl ]  );

      }
      if($getDecision)
      {
        /*$EquipmentArray=$getEquipment;//\yii\helpers\StringHelper::explode($this->creator,',');
        $val_equipment=[];
        for($i=0;$i<count($EquipmentArray);++$i)
        {
         $val_equipment[$i]= $EquipmentArray[$i] ;
       }*/
        //print_r( $LocalityArray ); // exit();
        $query->andFilterWhere( ['id_decision'=>$getDecision ]  );

      }
      if($getSection && $getKeyword)
      {
        /*$EquipmentArray=$getEquipment;//\yii\helpers\StringHelper::explode($this->creator,',');
        $val_equipment=[];
        for($i=0;$i<count($EquipmentArray);++$i)
        {
         $val_equipment[$i]= $EquipmentArray[$i] ;
       }*/
        //print_r( $getSection ); echo '<br>'.$getKeyword;  exit();

        for($i=0;$i<count($getSection);++$i)
        {
          $query->orFilterWhere( ['like',$getSection[$i],$getKeyword ]  );
        }


      }
      if($this->creation_time)
      {/*
        $idArray=\yii\helpers\StringHelper::explode($getIds,',');
        $val_id=[];
        $ids=[];
        $idsBetween=['or'];
        for($i=0;$i<count($idArray);++$i)
        {
         if(!stristr($idArray[$i],'...'))  $ids[$i]= $idArray[$i];
          else $idsBetween[]=['between','id_deviation', explode('...',$idArray[$i])[0],explode('...',$idArray[$i])[1] ];
        }
        $val_id['id_deviation']=$ids;


        //print_r( $idsBetween);  exit();
        $query->andFilterWhere( ['or',$val_id,$idsBetween]  );
        _____*/
        $timeArray=\yii\helpers\StringHelper::explode($this->creation_time,',');
        $val_time=['or'];
        $timesBetween=['or'];
        for($i=0;$i<count($timeArray);++$i)
        {
         if(!stristr($timeArray[$i],'..')) $val_time[$i+1]=['like', 'creation_time', $timeArray[$i].'%:%', false];
          else $timesBetween[]=['between','creation_time', explode('..',$timeArray[$i])[0],explode('..',$timeArray[$i])[1] ];
        }
        //print_r( $timesBetween);  exit();
        $query->andFilterWhere( ['or',$val_time,$timesBetween]  );
      }

      if($this->creation_date)
      {
        $DateArray=\yii\helpers\StringHelper::explode($this->creation_date,',');
        $val_date=['or'];
        $datesBetween=['or'];
        for($i=0;$i<count($DateArray);++$i)
        {
         //$val_date[$i+1]=['like', 'creation_date', $DateArray[$i]];
         if(!stristr($DateArray[$i],'..')) $val_date[$i+1]=['like', 'creation_date', $DateArray[$i],false];
          else $datesBetween[]=['between','creation_date', explode('..',$DateArray[$i])[0],explode('..',$DateArray[$i])[1] ];

        }
        $query->andFilterWhere( ['or',$val_date,$datesBetween]  );
      }
    }else {
      $query->andFilterWhere( ['creator'=>\Yii::$app->user->identity->id_user]  );
    } // check if he can filter ends here
      //print_r($query);exit();
       //echo $query->createCommand()->getRawSql();// exit();
      return $dataProvider;
    }



}
