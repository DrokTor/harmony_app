<?php

namespace app\modules\deviation\models;

use Yii;

class CapaReviewType extends yii\db\ActiveRecord
{
  public static function tableName()
  {
    return "capa_review_type";
  }


  public function rules()
  {
    return [
            [['name'],'required'],
            [['name'],'string']
              ];
  }


}
