<?php

namespace app\modules\deviation\models;

use Yii;
use app\modules\deviation\models\DevBatches;




/**
 * This is the model class for table "dev_conclusion".
 *
 * @property integer $id_conclusion
 * @property integer $id_deviation
 * @property integer $id_decision
 * @property string $comment
 * @property string $final_decision
 * @property integer $id_signature
 */
class DevConclusion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dev_conclusion';
    }

    const SCENARIO_DRAFT='Draft';
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DRAFT] = ['id_deviation', 'comment', 'final_decision'];//, 'id_decision'

        return $scenarios;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_deviation', 'comment', 'final_decision'], 'required', 'whenClient'=>"function(attribute,value){return false;}",'on' => self::SCENARIO_DEFAULT], //, 'id_decision'
            [['id_deviation'], 'integer'], //, 'id_decision'
            [['comment', 'final_decision'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_conclusion' => \Yii::t('app','Conclusion'),
            'id_deviation' => \Yii::t('app','Deviation'),

            'comment' => \Yii::t('app','Comment'),
            'final_decision' => \Yii::t('app','Final Decision'),

        ];
    }

    public function saveDecisions($decisions)
    {
      foreach ($decisions as $key => $id_decision)
      {
        $batch=DevBatches::findOne(['id_relation'=>$key]);
        $batch->id_decision=$id_decision;
        if(!$batch->save())
        {
          return false;
        }
      }
      return true;
    }

    public function setDeviationDuration($id)
    {
      $dev= DevDeviation::findOne($id);
      $conc=  DevConclusion::findOne(['id_deviation'=>$id]);
      $d= new  \DateTime($conc->closing_date);
      $creation= new  \DateTime($dev->creation_date);
      $duration=date_diff($creation,$d)->format('%a');
      $conc->deviation_duration=$duration;
      $conc->save();
    }

/*
     public function getDecision()
    {
        return $this->hasOne(Decision::className(), ['id_decision' => 'id_decision']);
    }*/


}
