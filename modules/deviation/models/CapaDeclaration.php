<?php

namespace app\modules\deviation\models;

use Yii;
use app\models\User;
use app\modules\deviation\models\CapaState;
use app\modules\deviation\models\CapaType;
use app\modules\deviation\models\CapaImplementation;
use app\modules\deviation\models\CapaEfficacity;

class CapaDeclaration extends \yii\db\ActiveRecord
{
  public static function tableName()
  {
    return "capa_declaration";
  }


  public function rules()
  {
    return [
            [['deviation_id','user_id','title','description','type_id','implementation_date'],'required'],
            [['deviation_id','user_id','type_id'],'integer'],
            [['title','description',],'string']
              ];
  }

  public function attributeLabels()
  {
    return
    [
    'id'=>'ID',
    'deviation_id'=>\Yii::t('app','Deviation ID'),
    'user_id'=>\Yii::t('app','Creator'),
    'title'=>\Yii::t('app','Title'),
    'description'=>\Yii::t('app','Description'),
    'type_id'=>\Yii::t('app','Type'),
    'implementation_date'=>\Yii::t('app','Implementation date'),
    'creation_date'=>\Yii::t('app','Creation date')
    ];
  }

  public function getCreator()
  {
    return $this->hasOne(User::className(),['id_user'=>'user_id']);
  }
  public function getCapaState()
  {
    return $this->hasOne(CapaState::className(),['id'=>'state']);
  }
  public function getCapaType()
  {
    return $this->hasOne(CapaType::className(),['id'=>'type_id']);
  }
  public function getImp()
  {
    return $this->hasOne(CapaImplementation::className(),['capa_id'=>'id']);
  }
  public function getEff()
  {
    return $this->hasOne(CapaEfficacity::className(),['capa_id'=>'id']);
  }

}
