<?php

namespace app\modules\deviation\models;

use yii\db\ActiveRecord;
use app\models\User;
use app\models\Ishikawa;



 class DevInvestigationIshikawa extends ActiveRecord
{

  public function getIshikawa()
  {
    return $this->hasOne(Ishikawa::className(),['id_ishikawa'=>'id_ishikawa']);
  }
}
