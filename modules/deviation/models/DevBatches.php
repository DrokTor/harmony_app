<?php

namespace app\modules\deviation\models;

use yii\db\ActiveRecord;
use app\modules\deviation\models\DevBatches;
use app\models\Decision;



class DevBatches extends ActiveRecord
{


    public static function tableName()
    {
        return 'dev_batches';
    }
    /**
     * @inheritdoc
     */

    /*public function getDevDeviation()
    {
        return $this->hasOne(getDevDeviation::className(), ['state' => 'id_state']);
    }*/
    public function getDecision()
   {
       return $this->hasOne(Decision::className(), ['id_decision' => 'id_decision']);
   }
}
