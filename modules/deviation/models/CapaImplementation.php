<?php

namespace app\modules\deviation\models;

use Yii;

class CapaImplementation extends \yii\db\ActiveRecord
{

  const SAVE = 'save';
  const IMPLEMENT = 'implement';

  public function scenarios()
  {
      $scenarios = parent::scenarios();
      $scenarios[self::SAVE] = ['capa_id','implementation','implementation_date','efficacity_review_date'];
      $scenarios[self::IMPLEMENT] = ['capa_id','implementation','implementation_date','efficacity_review_date'];
      return $scenarios;
  }

  public static function tableName()
  {
    return "capa_implementation";
  }


  public function rules()
  {
    return [
            [['capa_id','implementation','implementation_date'],'required', 'on' => self::IMPLEMENT],
            [['capa_id' ],'integer'],
            [['implementation'],'string']
              ];
  }

  public function attributeLabels()
  {
    return [
      'capa_id'=>\Yii::t('app','CAPA ID'),
      'implementation'=>\Yii::t('app','Implementation'),
      'implementation_date'=>\Yii::t('app','Implementation date'),
      'efficacity_review_date'=>\Yii::t('app','Efficacity review date')

    ];
  }

}
