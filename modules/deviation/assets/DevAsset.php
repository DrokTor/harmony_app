<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\deviation\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DevAsset extends AssetBundle
{
    public $sourcePath = '@dev/assets';

    public $css = [
        'css/dev.css',
    ];
    public $js = [
         'js/dev.js',
    ];
    public $image = [
         'images/dev_menu.png',
         'images/menu-lg.png',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $publishOptions = [
    'forceCopy' => true,
];
}
