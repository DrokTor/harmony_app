function draft()
{
  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
      id = (sPageURL.split('='))[1],action=$('.declaration-form-class').prop('action'),
      url=action.split('?')[0]+'?draft=true'+'&id='+(id?id:'false');//decodeURIComponent(window.location)+'?draft=true';
      //url=((url.split('create'))[0])+'draft?draft=true';
        //alert(url);
        $('.dev-deviation-form form').prop('action',url);
        //return false;
}
function loadCharts()
{
/*
  Chart.defaults.global = {
    /*segmentStrokeWidth : null,
    segmentStrokeColor : "#000",
    responsive: true,
    options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                    }
                }]
            }
        }


  };*/
Chart.defaults.global.defaultFontSize =18;
Chart.defaults.global.defaultFontStyle ='bold';
Goptions= {
         scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                }
            }]
        }
    }
/*
 var devMonthly = function(){
    if($("#monthnum").length)
  {


  var devMonthly = $("#monthnum").get(0).getContext("2d"), monthnumData=$('#monthnum-data').data('monthnum');
  //alert(monthnumData.split(','));
  var data = {

      labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug","Sep","Oct" , "Nov", "Dec" ],//["January", "February", "March", "April", "May", "June", "July","August","September","October" , "November", "December"],
      datasets: [
          {

              label: "Monthly deviation",
              backgroundColor: "rgb(43, 173, 208)",
              //strokeColor: "rgba(220,220,220,0.8)",
              //highlightFill: "rgba(220,220,220,0.75)",
              //highlightStroke: "rgba(220,220,220,1)",
              data: monthnumData.split(','),
          },

      ]
  };
  var options={
    barValueSpacing : 0,

    //Number - Spacing between data sets within X values

  };
  //var monthnumChart = new Chart(devMonthly).Bar(data,options);
  var myBarChart = new Chart(devMonthly,{
    type: 'bar',
    data: data,
    options: options
  });

}
}();*/

var chartByNbr= function()
{

  if($('.chartByNbr').length)
  {
    //console.log($('.chartByNbr'));
  $('.chartByNbr').each(function(index,value){

    //alert( $(value).children()[0] );

  var param = $(value).children().get(0).getContext("2d"), datp=$(value).data('hash'),
       el= ((datp.length)? datp.split(',') : []), labelsP=[],dataP=[];
       //console.log(datp);
/*
       $.each(el , function(index,value){

         var split=value.split('-');
         //alert(split[1]);
         labelsP[index]=split[0];
         dataP[index]=parseInt(split[1]);

       } );*/
 //alert(pairs['process2']);
  // alert(dataP);




  var data = {
       labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug","Sep","Oct" , "Nov", "Dec" ],//["January", "February", "March", "April", "May", "June", "July","August","September","October" , "November", "December"],
//labelsP,//['bla','bla'] ,//labels,
       datasets: [
         {
             label: $(value).data('title'),
             backgroundColor: "#52b0cf",
             data: el,//dataP,//['20','45'],//data,
         },


       ],

   };


            //var paramChart = new Chart(param).Bar(data);

            /*options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true,
                            }
                        }]
                    }
                };*/

            var paramChart = new Chart(param, {
              type: 'bar',
              data: data,
              options: Goptions
              });








  } );

  }
}();
/*
var avgAnum = function(){
   if($("#avgAnum-data").length)
 {


 var devMonthly = $("#avgCanv").get(0).getContext("2d"), monthnumData=$('#avgAnum-data').data('hash');
 //alert(monthnumData.split(','));
 var data = {

     labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug","Sep","Oct" , "Nov", "Dec" ],//["January", "February", "March", "April", "May", "June", "July","August","September","October" , "November", "December"],
     datasets: [
         {

             label: "Monthly average deviation ",
             backgroundColor: "rgb(43, 173, 208)",
             //strokeColor: "rgba(220,220,220,0.8)",
             //highlightFill: "rgba(220,220,220,0.75)",
             //highlightStroke: "rgba(220,220,220,1)",
             data: monthnumData.split(','),
         },

     ]
 };
 var options={
   barValueSpacing : 0,

   //Number - Spacing between data sets within X values

 };
 //var monthnumChart = new Chart(devMonthly).Bar(data,options);

 var myBarChart = new Chart(devMonthly,{
   type: 'bar',
   data: data,
   options: options
 });

}
}();*/
/*
var ishikawa= function()
{
  var ishikawa = $("#radar").get(0).getContext("2d"), dataIshikawa=$('#radar-data').data();
  //alert(data.milieu);
  var data = {
      labels: ["Material", "Manpower", "Milieu", "Machine", "Method"],
      datasets: [
          {

              fillColor: "rgba(220,220,220,0.2)",
              strokeColor: " rgb(0, 0, 0)",
              pointColor: "rgba(220,220,220,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: [dataIshikawa.material, dataIshikawa.manpower, dataIshikawa.milieu,dataIshikawa.machine, dataIshikawa.methode]
          },

      ]
  };

  var myNewChart = new Chart(ishikawa).Radar(data);
}();
*/
var severity= function()
{
  if($("#severity").length)
  {
    var sev = $("#severity").get(0).getContext("2d"), dat=$('#severity-data').data();
  //alert(dat.crit);
 var data = {
      labels:["Low","High","Catastrophic"],
      datasets: [
        {
            label: "Severity",
            backgroundColor: "#52b0cf",
            data: [dat.low,dat.hi,dat.crit ],
        },


      ],

  };

  var options={
    barValueSpacing : 10,

    //Number - Spacing between data sets within X values

  };




  //var myNewChart = new Chart(sev).Bar(data,options);

  var myBarChart = new Chart(sev, {
    type: 'bar',
    data: data,
    options: Goptions
    });

/*
    myNewChart.datasets[0].bars[1].fillColor ="#f9fc5c";
    myNewChart.datasets[0].bars[2].fillColor ="#ff3939";
    myNewChart.datasets[0].bars[1].highlightFill ="#f9fc5c";
    myNewChart.datasets[0].bars[2].highlightFill ="#ff3939";


  // Would update the first dataset's value of 'March' to be 50
  myNewChart.update();*/
}
}();

var chartByHash= function()
{

  if($('.chartByHash').length)
  {

  $('.chartByHash').each(function(index,value){

    //alert( $(value).children()[0] );

  var param = $(value).children().get(0).getContext("2d"), datp=$(value).data('hash'),
       el= datp.split('#'), labelsP=[],dataP=[];

       $.each(el , function(index,value){

         var split=value.split('-');
         //alert(split[1]);
         labelsP[index]=split[0];
         dataP[index]=parseInt(split[1]);

       } );
 //alert(pairs['process2']);
  // alert(dataP);




  var data = {
       labels: labelsP,//['bla','bla'] ,//labels,
       datasets: [
         {
             label: $(value).data('title'),
             backgroundColor: "#52b0cf",
             data: dataP,//['20','45'],//data,
         },


       ],

   };


            //var paramChart = new Chart(param).Bar(data);

            var paramChart = new Chart(param, {
              type: 'bar',
              data: data,
              options: Goptions
              });








  } );

  }
}();

var chartByAnum= function()
{

  if($('.chartByAnum').length)
  {

  $('.chartByAnum').each(function(index,value){

    //alert( $(value).children()[0] );

  var param = $(value).children().get(0).getContext("2d"), datp=$(value).data('hash'),
       el= datp.split('|'), dataSets=[], labelsP=[],dataP=[], colorRed=80, colorGreen=70, colorBlue=100;
       //rgb(82, 176, 207)
       for(i=0;i<4;i++)
       {
         dataP[i]=0;
       }
       $.each(el , function(ind,value){

         //console.log(value);
         var getMonth=(value.length)?value.split('=>'):[] , process=((getMonth.length)? getMonth[1].split('#'):[]);
         //alert(process);
         //console.log(getMonth);
         $.each(process,function(key,value){
//alert(ind);
           var split= value.split('-'), proc=key;

            if(typeof(dataSets[proc])=='undefined')
            {
              dataSets[proc]={label:split[0],data:[0,0,0,0,0,0,0,0,0,0,0,0],
              backgroundColor: "rgb("+colorRed+", "+colorGreen+", "+colorBlue+")"};
              colorGreen+=40;
              colorBlue+=40;
            }
           dataSets[proc].data[getMonth[0]-1]=parseInt(split[1]);
           //console.log(proc,getMonth[0]-1,split[1]);
            //console.log(dataSets);
           //alert(dataSets);
           //alert(dataSets[0].data[0]);

           //alert(key);
         });


       } );

 //console.log(dataSets);


  var data = {
       labels:  ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug","Sep","Oct" , "Nov", "Dec"  ],//labelsP,//['bla','bla'] ,//labels,
       datasets: dataSets//test//dataSets

   };

   var option={
     barDatasetSpacing : 0,//-10,
         //legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
    multiTooltipTemplate: "<%= datasetLabel %> : <%= value %>"
   }
            //var anumChart = new Chart(param).Bar(data,option);
            var anumChart = new Chart(param, {
              type: 'bar',
              data: data,
              options: Goptions
              });

  } );

  }
}();


var category= function()
{


  if($("#category").length)
  {

  var cat = $("#category").get(0).getContext("2d"), dataCategory=$('#category-data').data();
  //alert(data.milieu);
  /*var data = [
      {
          value: dataCategory.quality,
          color:"#46f7f7",
          segmentStrokeColor : "rgba(#700c0c, 1)",
          segmentStrokeWidth : 10,
          //highlight: "#FF5A5E",
          label: "Quality"
      },
      {
          value: dataCategory.hse,
          color: "#46bf56",
          segmentStrokeColor : "rgba(#700c0c, 1)",
          segmentStrokeWidth : 10,
          //highlight: "#5AD3D1",
          label: "HSE"
      },
  ];*/

  var data = {
    labels: ["Quality", "HSE"],
    datasets: [
        {
            label: "Category",
            backgroundColor: "#52b0cf",
            data: [dataCategory.quality, dataCategory.hse],
        }
    ]
    };

//  var myNewChart = new Chart(cat).Pie(data);

  var myBarChart = new Chart(cat,{
    type: 'bar',
    data: data,
    options: Goptions
  });



  }
}();


}

function toPdf()
{

  var doc = new jsPDF('landscape');

  if($('.objectives').length)
  {
    $('.objectives').each(function(){

      objectives= $(this).children();
      //console.log(objectives[0]);
      title= objectives[0].textContent;
      val= objectives[1].textContent;
      doc.setFontSize(50);
      doc.text(35, 25, title);
      doc.setFontSize(90);
      doc.text(120, 120, val);
      doc.addPage();

    });
    console.log('o');
    doc.save();

  }
  else {
    $('canvas').each(function(){

      //im= ($(this)[0])
      //alert($(this).parent().data('title'));
      title= $(this).parent().data('title');
      im= this.toDataURL();
      doc.setFontSize(40);
      doc.text(35, 25, title);
      doc.addImage(im, 'JPEG', 15, 40, 280, 150);
      doc.addPage();

    });
    doc.save();

  }

}

function resizeData()
{
    if(  $('.tb').hasClass('col-md-6')  )
    {
      $('.tb').removeClass('col-md-6');
      $('.tb').addClass('col-md-12');


      //$('.tb').removeClass('tbs');


      $('#resize-data').removeClass('fa-expand');
      $('#resize-data').addClass('fa-compress');
    }
    else
    {
      $('.tb').removeClass('col-md-12');
      $('.tb').addClass('col-md-6');

      //$('.tb').addClass('tbs');

      $('#resize-data').removeClass('fa-compress');
      $('#resize-data').addClass('fa-expand');
    }

}

function delfile()
{
  $(".del-file").on('click',function(event ){
    baseUrl=window.location.protocol + "//" + window.location.host ;
    //url=decodeURIComponent(window.location);
    url=$(event.target).data('url');// (url.split('view'))[0];
  //alert($(event.target).data('id'));
 //alert(url+"delete-file");
 var file = confirm("are you sure you want to delete this file ?");
//alert(baseUrl+url+"delete-file");
 if (file ) {
  $.get( baseUrl+url+"delete-file",
  {
      id:$(event.target).data('id'),
      name:$(event.target).data('name'),
      type:$(event.target).data('type'),
  },
  function(data, status){
      //alert("Data: " + data + "\nStatus: " + status);

          if(status=='success')$(event.target).parent().remove();
        //alert(file);


  });
  }
    $(".del-file").off('click');
  });
}

$(document).ready(function(){

//$(window).resize(function(){

  /*$('.tb').each(function(index,elem){
    //alert($(elem).find('canvas').css('height'));
    //$(elem 'canvas').css('height');
    //$(elem).find('canvas').

      canvasHeight= $(elem).find('canvas').css('height');
      //canvasHeight= $(this).css('height');
      //alert(  canvasHeight );
      $(elem).attr("style", "height: "+canvasHeight+" !important;");
      //document.getElementById("myP").style.cssText =
      //$(elem).css("height", canvasHeight);
      //$(elem).css('height',(parseInt(canvasHeight)+100));


    //alert(parseInt(canvasHeight)+100);

  });*/
//});

delfile();

$('#toPDF').on('click',function(){
    toPdf();
});

$('#resize-data').on('click',function(){
  resizeData();
});

if( $('#monthDropDown').length )
{

  $('#monthDropDown').on('change',function(){

    var url=decodeURIComponent(window.location.href), month=$('#monthDropDown').val(),
    split=url.split('?'),
    goUrl= ( split[1] && (split[1]).split('&')[0]=='t=m')? (split[0])+'?t=m&month='+month:(split[0])+'?month='+month;
    //alert(goUrl);

      //alert($('#monthDropDown').val());
      window.location.href = goUrl;
  });

}

if( $("#monthly-button").length)
loadCharts();

$(".save-draft").click(draft);
/* View tabs jquery */
$("#decLinkTab").ready(function(){

  if($("#concLinkTab").length) $("#concLinkTab").click();
  else if($("#investLinkTab").length) $("#investLinkTab").click();
  else $("#decLinkTab").click();
});
$("#decLinkTab").click(function(){
  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
      id = (sPageURL.split('='))[1],url=decodeURIComponent(window.location);
      url=(url.split('view'))[0];
      //alert(url);
      //window.location.replace("http://stackoverflow.com");
    $.post( url+"view-declaration",
    {
        id: id,

    },
    function(data, status){
        //alert("Data: " + data + "\nStatus: " + status);
        $('#declaration-tab').html(data);

    }).fail(function(data){alert(data.responseText)});
});

$("#investLinkTab").click(function(){
  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
      id = (sPageURL.split('='))[1],url=decodeURIComponent(window.location);
      url=(url.split('deviation/view'))[0];
      //alert(url);
      //window.location.replace("http://stackoverflow.com");
    $.post( url+"investigation/view-investigation",
    {
        id: id,

    },
    function(data, status){
        //alert("Data: " + data + "\nStatus: " + status);
        $('#investigation-tab').html(data);
    }).fail(function(data){
      alert(data.responseText)
      //$('#investigation-tab').html(data.responseText);
    });
});

$("#concLinkTab").click(function(){
  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
      id = (sPageURL.split('='))[1],url=decodeURIComponent(window.location);
      url=(url.split('deviation/view'))[0];
      //alert(url);
      //window.location.replace("http://stackoverflow.com");
    $.post( url+"conclusion/view-conclusion",
    {
        id: id,

    },
    function(data, status){
        //alert("Data: " + data + "\nStatus: " + status);
        $('#conclusion-tab').html(data);
    }).fail(function(msg){alert(msg.responseText);});;//
});
/**/


/**/
});
/*
$(document).on('ready pjax:beforeSend', function(){
    // ...
    $('#iconload').show();
});
$(document).on('ready pjax:complete', function(){
    // ...
    $('#iconload').hide();
});*/
$(document).on('pjax:success', function(){
    // ...
    if( $("#monthly-button").length)
    loadCharts();
    $('#toPDF').on('click',function(){
        toPdf();
    });

    $('#resize-data').on('click',function(){
      resizeData();
    });

    if( $('#monthDropDown').length )
    {

      $('#monthDropDown').on('change',function(){

        var url=decodeURIComponent(window.location.href), month=$('#monthDropDown').val(),
        split=url.split('?'),
        goUrl= ( split[1] && (split[1]).split('&')[0]=='t=m')? (split[0])+'?t=m&month='+month:(split[0])+'?month='+month;
        //alert(goUrl);

          //alert($('#monthDropDown').val());
          window.location.href = goUrl;
      });

    }

});
/* Deviation rating jquery */
$(document).ajaxComplete(function(){
  /* history check jquery */


  if( $('#monthDropDown').length )
  {

    $('#monthDropDown').on('change',function(){

      var url=decodeURIComponent(window.location.href), month=$('#monthDropDown').val(),
      split=url.split('?'),
      goUrl= ( split[1] && (split[1]).split('&')[0]=='t=m')? (split[0])+'?t=m&month='+month:(split[0])+'?month='+month;
      //alert(goUrl);

        //alert($('#monthDropDown').val());
        window.location.href = goUrl;
    });

  }

  var elem=$('#signModal'), ev = elem.length? $._data(elem[0], 'events'):null;
  if(!(ev && ev.click))
  {
    $('#signModal').on('show.bs.modal', function(e) {

           /*var $modal = $(this),
               esseyId = e.relatedTarget.id;*/
         var username = $(e.relatedTarget).data('username'),
              id=$(e.relatedTarget).data('user'),
              iddev=$('.modal-dialog').data('iddev'),
              comment=($('#devconclusion-comment').length)?$('#devconclusion-comment').val():null,
              final=($('#devconclusion-final_decision').length)?$('#devconclusion-final_decision').val():null;
              decision='true';
              $('.batches-select').each(function(index,value){
                if($(value).val() =='') decision='false';
                //alert($(value).val());
              });
              //alert(decision);
              //$(".modal-body #signatory").text( username );
              //$(".modal-body #signatory").data('iduser',id);
        var  url=decodeURIComponent(window.location);
            url=(url.split('deviation/view'))[0];
            //alert(url+"sign-deviation");
  //alert(id);
              $.post( url+"conclusion/sign-deviation",{ action:'load' ,id:id, username:username,iddev, comment:comment, final:final, decision:decision },function( data ) {

                /*if(data=='false')
                {

                  $('#signModal').modal('hide');
                  alert('Make sure all fields are filled !');

                }
                else*/

                $( ".modal-dialog" ).html( data );

              });//.fail(function(data){alert(data.responseText);});

       });

  }




  var elem=$('#signDisapprove'), ev =  elem.length? $._data(elem[0], 'events'):null;
  if(!(ev && ev.click))
  {
    $('#signDisapprove').on('click',function(){



      var  url=decodeURIComponent(window.location);
          url=(url.split('deviation/view'))[0],
          iddev=$('.modal-dialog').data('iddev'),
          id=$('#signatory').data('iduser'),
          password=$('#signPassword').val();

          $.post(url+'conclusion/sign-deviation',{ action:'disapprove',iddev, id, password },function(data){


            if(data=='disapproved')
            {
            $('#signModal').on('hidden.bs.modal',function(){
                          $("#concLinkTab").click();
                          $('#signModal').off('hidden.bs.modal');
                      });
            $('#signModal').modal('hide');

            //$('#signModal').off('hidden.bs.modal');

            }
            else alert(data);


          } );
      $('#signDisapprove').off('click');
    });
  }


  var elem=$('#signApprove'), ev =   elem.length? $._data(elem[0], 'events'):null;
  if(!(ev && ev.click))
  {
    $('#signApprove').on('click', function(){

      var  url=decodeURIComponent(window.location);
          url=(url.split('deviation/view'))[0];
      var  id=$('#signatory').data('iduser'),
           password=$('#signPassword').val(),
           text=$('#signText').val();
           iddev=$('.modal-dialog').data('iddev'),
           comment=$('#devconclusion-comment').val(),
           final=$('#devconclusion-final_decision').val();

      $.post( url+'conclusion/sign-deviation', { action:'sign' , iddev:iddev, id:id , password:password, text:text, DevConclusion:{comment,final} },
      function(data){
          //(data!='success')? alert(data):'';
              //$("")
          if(data=='success')
          {
            $('#conclusionForm').submit();

            $('#signModal').on('hidden.bs.modal',function(){
                          $("#concLinkTab").click();
                          $('#signModal').off('hidden.bs.modal');
                      });
            $('#signModal').modal('hide');





          }
          else   alert(data) ;

          /*$('#signModal').on('hidden.bs.modal',function(data){

            alert(data);
            if(data=='success')
          } );*/

      } ).fail(function(data){alert(data.responseText);});
      $('#signApprove').off('click');
    });

  }



  $("#sevFirstRate").hover(function( )
  {
  $("#sevFirstRate").addClass("minor-rate");
},function(){  $("#sevFirstRate").removeClass("minor-rate");}
  );

  $("#sevFirstRate").click(function( )
  {
  $("#sevFirstRate").removeClass();
  $("#sevFirstRate").addClass("minor-rated");
  }
  );

  $("#sevSecondRate").hover(function( )
  {
  $("#sevFirstRate").addClass("medium-rate");
    $("#sevSecondRate").addClass("medium-rate");
  },function(){
    $("#sevFirstRate").removeClass("medium-rate");
    $("#sevSecondRate").removeClass("medium-rate");
  }
  );

  $(".secondRate").click(function( )
  {
  $("#sevFirstRate").addClass("medium-rated");
  $(".secondRate").addClass("medium-rated");
  },function(){
    $("#sevFirstRate").removeClass("minor-rated");

  }
  );


    $(".thirdRate").hover(function( )
    {
    $("#sevFirstRate").addClass("major-rate");
    $(".secondRate").addClass("major-rate");
    $(".thirdRate").addClass("major-rate");
    },function(){
      $("#sevFirstRate").removeClass("major-rate");
      $(".secondRate").removeClass("major-rate");
      $(".thirdRate").removeClass("major-rate");
    }
    );


    $(".thirdRate").click(function( )
    {
    $("#sevFirstRate").addClass("major-rated");
    $(".secondRate").addClass("major-rated");
    $(".thirdRate").addClass("major-rated");
    }
    );

/*
  $('#idHistory').on('click', function ()
  {
    if( $("#historyField").prop('disabled')  ) $("#historyField").removeAttr('disabled');
    else $("#historyField").prop('disabled','disabled');

    $("#historyField").focus();

  });*/

  $(".save-draft").click(draft);

delfile();

$(".launch-process").click(function(){
  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
      id = (sPageURL.split('='))[1],
      url=$('.declaration-form-class').prop('action')+'?id='+id;//decodeURIComponent(window.location)+'?draft=true';
      //url=((url.split('create'))[0])+'draft?draft=true';
        //alert(url);
        $('.dev-deviation-form form').prop('action',url);
        //return false;
});
$('#ratingModal').on('hidden.bs.modal', function () {
       var lvl,
    lvl= parseInt( $('input[name=severity]:checked').val() )
    + parseInt( $('input[name=occurence]:checked').val() )
    + parseInt( $('input[name=detectability]:checked').val() );

    lvl/=3;
    lvl=Math.round(lvl);

     $('#LVLdev').prop('value',lvl);

    switch(lvl)
    {
      case 1: {
        $('#riskLVL').text('Very low');
        $('#riskLVL').removeClass();
        $('#riskLVL').addClass('lvl lvl-Vlow col-md-6');
        break;
      }
      case 2: {
        $('#riskLVL').text('Low');
        $('#riskLVL').removeClass();
        $('#riskLVL').addClass('lvl lvl-low col-md-6');
        break;
      }
      case 3: {
        $('#riskLVL').text('Moderate');
        $('#riskLVL').removeClass();
        $('#riskLVL').addClass('lvl lvl-modr col-md-6');
        break;
      }
      case 4: {
        $('#riskLVL').text('High');
        $('#riskLVL').removeClass();
        $('#riskLVL').addClass('lvl lvl-high col-md-6');
        break;
      }
      case 5: {
        $('#riskLVL').text('Critical');
        $('#riskLVL').removeClass();
        $('#riskLVL').addClass('lvl lvl-crit col-md-6');
        break;
      }

    }

});

$( "#severity-slider-container" ).on( "slidechange", function( event, ui ) {

  var valueDet=$( "#detectability-slider-container" ).slider( "option", "value" ),
  valueSev=$( "#severity-slider-container" ).slider( "option", "value" ),
  valueOcc=$( "#occurence-slider-container" ).slider( "option", "value" ),value;

  value=valueDet*valueSev*valueOcc;

   switch(valueSev)
   {
     case 1: {
       $('#sevTag').text('Very low');
       $('#sevTag').removeClass();
       $('#sevTag').addClass('lvl lvl-Vlow ');
       break;
     }
     case 2: {
       $('#sevTag').text('Low');
       $('#sevTag').removeClass();
       $('#sevTag').addClass('lvl lvl-low ');
       break;
     }
     case 3: {
       $('#sevTag').text('Moderate');
       $('#sevTag').removeClass();
       $('#sevTag').addClass('lvl lvl-modr ');
       break;
     }
     case 4: {
       $('#sevTag').text('High');
       $('#sevTag').removeClass();
       $('#sevTag').addClass('lvl lvl-high ');
       break;
     }
     case 5: {
       $('#sevTag').text('Critical');
       $('#sevTag').removeClass();
       $('#sevTag').addClass('lvl lvl-crit ');
       break;
     }

   }

   if(value<5) {
     $('#lvl-screen').text('Minor');
     $('#lvl-screen').removeClass();
     $('#lvl-screen').addClass('  lvl-low ');

   }else if(value>4 && value<25)
     {
     $('#lvl-screen').text('Major');
     $('#lvl-screen').removeClass();
     $('#lvl-screen').addClass('  lvl-high ');

   }
   else{
     $('#lvl-screen').text('Catastrophic');
     $('#lvl-screen').removeClass();
     $('#lvl-screen').addClass('  lvl-crit ');

   }


} );

$( "#occurence-slider-container" ).on( "slidechange", function( event, ui ) {

  var valueDet=$( "#detectability-slider-container" ).slider( "option", "value" ),
  valueSev=$( "#severity-slider-container" ).slider( "option", "value" ),
  valueOcc=$( "#occurence-slider-container" ).slider( "option", "value" ),value;

  value=valueDet*valueSev*valueOcc;
   switch(valueOcc)
   {
     case 1: {
       $('#occTag').text('Remote');
       $('#occTag').removeClass();
       $('#occTag').addClass('lvl lvl-Vlow ');
       break;
     }
     case 2: {
       $('#occTag').text('Low');
       $('#occTag').removeClass();
       $('#occTag').addClass('lvl lvl-low ');
       break;
     }
     case 3: {
       $('#occTag').text('Moderate');
       $('#occTag').removeClass();
       $('#occTag').addClass('lvl lvl-modr ');
       break;
     }
     case 4: {
       $('#occTag').text('High');
       $('#occTag').removeClass();
       $('#occTag').addClass('lvl lvl-high ');
       break;
     }
     case 5: {
       $('#occTag').text('Very high');
       $('#occTag').removeClass();
       $('#occTag').addClass('lvl lvl-crit ');
       break;
     }

   }

   if(value<5) {
     $('#lvl-screen').text('Minor');
     $('#lvl-screen').removeClass();
     $('#lvl-screen').addClass('  lvl-low ');

   }else if(value>4 && value<25)
     {
     $('#lvl-screen').text('Major');
     $('#lvl-screen').removeClass();
     $('#lvl-screen').addClass('  lvl-high ');

   }
   else{
     $('#lvl-screen').text('Catastrophic');
     $('#lvl-screen').removeClass();
     $('#lvl-screen').addClass('  lvl-crit ');

   }


} );

$( "#detectability-slider-container" ).on( "slidechange", function( event, ui ) {

   var valueDet=$( "#detectability-slider-container" ).slider( "option", "value" ),
   valueSev=$( "#severity-slider-container" ).slider( "option", "value" ),
   valueOcc=$( "#occurence-slider-container" ).slider( "option", "value" ),value;

   value=valueDet*valueSev*valueOcc;

   switch(valueDet)
   {
     case 1: {
       $('#detTag').text('Almost certain');
       $('#detTag').removeClass();
       $('#detTag').addClass('lvl lvl-Vlow ');
       break;
     }
     case 2: {
       $('#detTag').text('High');
       $('#detTag').removeClass();
       $('#detTag').addClass('lvl lvl-low ');
       break;
     }
     case 3: {
       $('#detTag').text('Low');
       $('#detTag').removeClass();
       $('#detTag').addClass('lvl lvl-modr ');
       break;
     }
     case 4: {
       $('#detTag').text('Remote');
       $('#detTag').removeClass();
       $('#detTag').addClass('lvl lvl-high ');
       break;
     }
     case 5: {
       $('#detTag').text('Absolute uncertainty');
       $('#detTag').removeClass();
       $('#detTag').addClass('lvl lvl-crit ');
       break;
     }

   }

//alert(valueSev);
     if(value<5) {
       $('#lvl-screen').text('Minor');
       $('#lvl-screen').removeClass();
       $('#lvl-screen').addClass('  lvl-low ');

     }else if(value>4 && value<25)
       {
       $('#lvl-screen').text('Major');
       $('#lvl-screen').removeClass();
       $('#lvl-screen').addClass('  lvl-high ');

     }
     else{
       $('#lvl-screen').text('Catastrophic');
       $('#lvl-screen').removeClass();
       $('#lvl-screen').addClass('  lvl-crit ');

     }




} );



});
/**/
