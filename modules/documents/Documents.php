<?php

  namespace app\modules\documents;

class Documents extends \yii\base\Module
{

  public $controllerNamespace = 'app\modules\documents\controllers';

    public function init()
    {

       define('rootpath', "../modules/documents/Entreprise/");

        parent::init();

        //\Yii::configure($this, require(__DIR__ . '/config.php'));
        $this->setAliases([
            '@docs' =>  __DIR__ ,
            '@trUrl' => \Yii::$app->request->hostInfo.\Yii::getAlias('@web').'/documents/documents',
            //'@capaUrl' => \Yii::$app->request->hostInfo.\Yii::getAlias('@web').'/deviation/capa',
        ]);
    }
}
