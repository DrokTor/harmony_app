<?php

namespace app\modules\documents\models;

use Yii;
use app\models\User;

/**
 * This is the model class for table "docs_locked".
 *
 * @property integer $id
 * @property integer $type
 * @property string $name
 * @property string $path
 */
class docLocked extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'docs_locked';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type','user_id'], 'integer'],
            [['name', 'path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'name' => Yii::t('app', 'Name'),
            'path' => Yii::t('app', 'Path'),
        ];
    }

    public function getUser()
    {
      return $this->hasOne(User::className(),['id_user'=>'user_id']);
    }
}
