<?php

namespace app\modules\documents\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "docs_profiles_documents".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $path
 */
class ProfilesDocuments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'docs_profiles_documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profile_id', 'path'], 'required'],
            [['profile_id', 'path'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'profile_id' => Yii::t('app', 'Profile ID'),
            'path' => Yii::t('app', 'Path'),
        ];
    }

    public function saveAssignments($request)
    {
      //$proTr = Profilesdocuments::find()->where(['id'=>$training])->all() ;
      //$perms=$auth->getChildren($id);
      //$auth->revokeAll($user);
      ProfilesDocuments::deleteAll(['path' => $request['path']]);
      $assignTab=[];
      //dd($request);
      $path=$request['path'];
      unset($request['path']);
      unset($request['save']);
       array_shift($request);
      foreach ($request as $key => $value)
      {
        $key=str_replace('_',' ',$key);
        $assignTab[]=[$key,$path];
      }
      //dd($assignTab);
    /*  $auth = \Yii::$app->authManager;
      $usersTr=[];
       $assignTabmapped= ArrayHelper::map($assignTab, '0', '1') ;

      foreach ($assignTabmapped as $key => $value) {
        $users[]=$auth->getUserIdsByRole($key);


      }

      //$users= ArrayHelper::map($users, '0', '1' ) ;
      $useval=[];
      foreach ($users as $key => $value) {
        foreach ($value as $key => $nvalue) {
            $useval[]=$value[$key];
        }

      }
      $useval=array_unique($useval);
      foreach ($useval as $key => $value) {
        $useval[$key]=[$value,$training];
      }*/
      //dd($useval);
      $count=count($assignTab);
      if($count>0)
      Yii::$app->db->createCommand()->batchInsert('docs_profiles_documents', ['profile_id', 'path'],
          $assignTab
       )->execute();
/*
      Yii::$app->db->createCommand()->batchInsert('docs_documents_users', ['user_id', 'path'],
          $useval
       )->execute();*/
      //print_r($perms);

      return $count;
    }

    //function getProfiles()
}
