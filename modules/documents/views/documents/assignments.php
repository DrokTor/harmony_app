<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('admin', 'Roles assignments');
$this->params['breadcrumbs'][] = $this->title;

 ?>

 <div class="auth-item-index">

      <div class="assignments-header">
        <h1 ><?= Html::encode($this->title) ?> </h1><label > - <?= $path ?></label>
      </div>
     <div class="container-fluid row">
 <?php
     $form = ActiveForm::begin(['options' => ['id'=>'form-assignments']]);
     $c=count($roles)/3;
     //echo $c;
     for($i=0;$i<$c;++$i)
     {
       echo '<table class="col-xs-12 col-sm-6 col-md-6 permissions-table">
         <thead>
         <tr>
           <th>Roles</th>
           <th class="col-xs-2 col-md-2 ">'.\Yii::t('admin','Assignments').'</th>
         </tr>
       </thead>';


     $j=0;
     foreach ($roles as  $key=>$value)
     {  //print_r($value);exit();
       # code...
       echo '<tr>
             <td>'.$value->name.'</td>
            <td  class="permission '.(in_array($value->name,$assigns)? 'perm-checked':'').' "><input type="checkbox" name="'.$value->name.'"'.(in_array($value->name,$assigns)? 'checked':'').'/></td>
            </tr>';
       unset($roles[$key]);
       $j++;
       if($j==3) break;
     }
     //print_r($model->allPermissions);

    echo'</table>';
    }

    echo'<div class="assignments-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">'.Yii::t('admin','Close').'</button>
      '.Html::submitButton( Yii::t('admin', 'Save'), ['id'=>'save-assignments','class' => ' btn btn-success btn-lg ' ])
      .'</div>';
    $form = ActiveForm::end();
     ?>
 </div>
 </div>
