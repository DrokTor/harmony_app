<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin(['options' => ['id'=>'form-upload','enctype' => 'multipart/form-data']]) ?>


      <div class="row upload-header">
        <h1 class="col-md-5">File upload</h1>
        <div class=" col-md-7">
          <div class="progress">
         <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
         </div>
         <div class="col-md-12  progress-status"></div>
       </div>
     </div>
      </div>
    <?= $form->field($modelFile, 'file')->fileInput() ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'category')->dropDownList($categories,['prompt'=>Yii::t('app',"Select category")]) ?>

    <?php // $form->field($model, 'workflow') ?>
    <?= $form->field($model, 'description')->textArea() ?>

     <div class="upload-footer">

      <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('admin','Close')?></button>
      <button id="submitButton" class="btn btn-primary" >Submit</button>

       </div>

<?php ActiveForm::end() ?>
