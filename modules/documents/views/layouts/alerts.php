
<div id="alert-div" class="col-md-4 col-md-offset-3">
  <div class="alert alert-success alert-dismissible  hidden" role="alert">
  <button type="button" class="close" ><span aria-hidden="true">&times;</span></button>
  <strong>Success!</strong> <p></p>
  </div>
  <div class="alert alert-warning alert-dismissible  hidden" role="alert">
  <button type="button" class="close" ><span aria-hidden="true">&times;</span></button>
  <strong>Warning!</strong> <p></p>
  </div>
  <div class="alert alert-danger alert-dismissible  hidden" role="alert">
  <button type="button" class="close" ><span aria-hidden="true">&times;</span></button>
  <strong>Danger!</strong> <p></p>
  </div>
</div>
