<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\modules\documents\assets\DocsAsset;
use kartik\icons\Icon;
$bundle=DocsAsset::register($this);
// Initialize framework as per <code>icon-framework</code> param in Yii config
Icon::map($this);


/* @var $this \yii\web\View */
/* @var $content string */

//AppAsset::register($this);
?>
<?php $this->beginContent('@app/views/layouts/main.php') ?>

      <div class="row">

        <?php

            NavBar::begin([
                'brandLabel' => \Yii::t('app','Documents management'),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse documents-bar' //navbar-fixed-top',
                ],
            ]);

            echo Nav::widget([
              'options' => ['class' => 'navbar-nav navbar-right'],
              'encodeLabels'=>false,
              'items' => [
              ['label' => ''.\Yii::t('app','').' <i class="fa fa-book"></i>','linkOptions'=>['class'=>'libraryTag'], 'url' => ['/documents/library/guide']] ,



              ],
            ]);

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'encodeLabels'=>false,
                'items' => [
                isset($this->params['draft']) && $this->params['draft']>0 ?
                ['label' => ''.\Yii::t('app','documents Drafts').' <span>'.$this->params['draft'].'</span>','linkOptions'=>['class'=>'tag-documents'], 'url' => ['/documents/documents/list-drafts']]:'',
                isset($this->params['investigate']) && $this->params['investigate']>0?
                ['label' => ''.\Yii::t('app','to be Investigated').' <span>'.$this->params['investigate'].'</span>','linkOptions'=>['class'=>'tag-documents'], 'url' => ['/documents/documents/list-documents?DevFilters[state]=3']]:'',
                isset($this->params['close']) && $this->params['close']>0?
                ['label' => ''.\Yii::t('app','to be Closed').' <span>'.$this->params['close'].'</span>','linkOptions'=>['class'=>'tag-documents'], 'url' => ['/documents/documents/list-documents?DevFilters[state]=5']]:'',
                isset($this->params['capadraft']) && $this->params['capadraft']>0?
                ['label' => ''.\Yii::t('app','Capa Drafts').' <span>'.$this->params['capadraft'].'</span>','linkOptions'=>['class'=>'tag-documents'], 'url' => ['/documents/capa/list-drafts']]:'',

                //['label' => ''.\Yii::t('app','| Library').' <i class="fa fa-book"></i>','linkOptions'=>['class'=>''], 'url' => ['/documents/library/guide']] ,



                ],
            ]);


            NavBar::end();
        ?>
      </div>

      <div class="row">

          <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
      </div>

      <div class="row">
        <div class="col-md-3">



<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menside">
  <span class="sr-only">Toggle navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
</button>

</div>
  <?php //Create documents List documents Data analysis
           echo ' <div id="menside" class="collapse in col-md-12">
              <div id="tree">

              </div>
              <div id="tree-actions">
              <button id="delete-folder" class="col-md-4 fa fa-minus"></button><button id="rename-folder" class="col-md-4 fa fa-pencil"></button><button id="create-folder" class="col-md-4 fa fa-plus"></button>
              </div>
            </div>';
           ?>
         </div>

        <div class="col-md-9">
          <div class="container-fluid">
            <?= $this->render('alerts.php'); ?>
            <?= $this->render('modal.php'); ?>
            <div id="modal-div">
            </div>
            <div class="row" id="searchdoc-parent">
              <input placeholder="Search" name="search" id="searchdoc" class="col-md-12 "/><i class="fa fa-search fa-2x" id="searchdoc-icon"></i>
            </div>
            <ol class="breadcrumb">
              <li class="active"><a href="#">Entreprise</a></li>
            </ol>
            <div class="row">
            <div id="dirContent" class="col-md-10">
            </div>
            <div id="dirActions" class="col-md-2 ">
              <button id="upload-button" class="btn btn-default btn-lg col-md-6"><i class="fa fa-upload"></i></button>
              <button id="permissions-button" class="btn btn-default btn-lg col-md-6 disabled enable-dependent"><i class="fa fa-key"></i></button>
              <button id="lock-button" class="btn btn-default btn-lg col-md-6 disabled enable-dependent"><i class="fa fa-lock"></i></button>
              <button id="delete-button" class="btn btn-default btn-lg col-md-6 disabled enable-dependent"><i class="fa fa-trash"></i></button>
              <button id="copy-button" class="btn btn-default btn-lg col-md-6 disabled enable-dependent"><i class="fa fa-clone"></i></button>
              <button id="cut-button" class="btn btn-default btn-lg col-md-6 disabled enable-dependent"><i class="fa fa-scissors"></i></button>

            </div>
            <?php // $this->render("../documents/upload",['model'=>$modelUpload] )?>
          </div>
          <div class="row">
            <div id="detail-div" class="col-md-12 ">
            </div>
          </div>

        <?=  $content ?>
          </div>
        </div>
      </div>

    </div>



<?php $this->endContent() ?>
