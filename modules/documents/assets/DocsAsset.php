<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\documents\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DocsAsset extends AssetBundle
{
    public $sourcePath = '@docs/assets';

    public $css = [
        'css/docs.css',
    ];
    public $js = [
         'js/docs.js',
    ];
    public $image = [
         'images/docs_menu.png',
         'images/menu-lg.png',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $publishOptions = [
    'forceCopy' => true,
];
}
