$(document).ready(function(){


    $(document).on('click',".tree-folder",function(event,create){


      $('#tree .selected').removeClass('selected');
      $(event.target).addClass('selected');
      var foldername=$(event.target).data('path');

      if(foldername=='/') $("#tree ul").collapse('hide');

      if(create!="undefined")
      loadTree(foldername,event,create);
      else
      loadTree(foldername,event);

      //updating breadcrumb paths
      $('.breadcrumb').children().slice(1).remove();
      $('.breadcrumb').append($(event.target).data('path').split('/').join('</li><li>'));
      });

    $(document).on('click',".dirContent-folder",function(event){


      $("#tree .selected").next('ul.collapse').collapse('show');

      $('#dirContent .selected').removeClass('selected');
      $(event.target).addClass('selected');
      enableActions();

    });
    $(document).on('click',".dirContent-file",function(event){


      $("#tree .selected").next('ul.collapse').collapse('show');

      $('#dirContent .selected').removeClass('selected');
      $(event.target).addClass('selected');
      enableActions();
      //console.log($(event.target).data('path'));
      path=$(event.target).data('path');
      loadDocumentDetails(path);

    });

    $(document).on('dblclick',".dirContent-folder",function(event){


      //alert($(event.target).text());
      //console.log($("#tree .selected").html());
      path=$(event.target).data('path');
      loadTreeItem(path);



    });



    $.post("tree", function(data, status){
          $("#tree").html(data);
          data=data.replace(/tree-folder/g, "dirContent-folder");
          data=data.replace(/tree-file/g, "dirContent-file");
          //console.log($($.parseHTML(data)[0]).children());
          $('#dirContent').html($($.parseHTML(data)[0]).children().slice(1));
      });

    $("#create-folder").on('click',function(){
       var folderName = prompt("Please enter the folder name");
       path= $("#tree .selected").data('path');

       $.post("create-folder",{name:folderName,path:path}, function(data, status){
             //if(status=='success') alert('created');
             showResult(".alert-success","Directory successfully created.");
             loadTreeItem(path,folderName);
             /*if(path=='/')
             {

             }*/


         });

    });
    $("#rename-folder").on('click',function(event){
      if($("#tree .selected").data('path')=='/')
      // alert("can't rename or delete Entreprise folder.");
      showResult(".alert-warning","can't rename or delete Entreprise folder.");

      else folderName = prompt("Please enter the new folder name");
      path= $("#tree .selected").data('path');

      $.post("rename-folder",{name:folderName,path:path}, function(data, status){
          if(data==true)
          {
            //alert("Directory successfully renamed.");
            showResult(".alert-success","Directory successfully renamed.");

            $("#tree .selected").contents().last()[0].textContent=folderName;
          }
          else //alert("Something went wrong when renaming the directory.");
          showResult(".alert-danger","Something went wrong when renaming the directory.");

        });
    });
    $("#delete-folder").on('click',function(event){
      if($("#tree .selected").data('path')=='/')
      // alert("can't rename or delete Entreprise folder.");
      showResult(".alert-danger","can't rename or delete Entreprise folder.");

      path= $("#tree .selected").data('path');

      $.post("delete-folder",{path:path}, function(data, status){
            if(data==true)
            {
              //alert('Directory successfully deleted');
              showResult(".alert-success",'Directory deleted successfully');
              $("#tree .selected").remove();
            }
            else if(data!=false)
            //alert(data);
            showResult(".alert-warning",data);
            else //alert("Something went wrong when deleting the directory.");
            showResult(".alert-danger","Something went wrong when deleting the directory.");

        });
    });

    $('#upload-button').on('click',function(){

      //if($('#form-upload-div').length)
      $.post("load-upload", function(data, status){
        //$('#form-upload-div').html(data);
        //$('#form-upload-div').collapse('toggle');
        object=$.parseHTML(data);
        header=$(object).find('.upload-header').remove();
        footer=$(object).find('.upload-footer').remove();
        //console.log(footer);
        $('#myModal .modal-header').html(header);
        $('#myModal .modal-body').html(object);
        $('#myModal .modal-footer').html(footer);
        $("#myModal").modal('show');

    });

    $(document).on('click',".upload-footer #submitButton",function(e){
      //alert("dddd");
      e.preventDefault();
      path= $("#tree .selected").data('path');
      //
      //alert( $("#form-upload").serialize());
      $("#form-upload").ajaxSubmit({
        //async:false,
        xhr: function()
        {
          //var xhr = new window.XMLHttpRequest();

          var xhr = $.ajaxSettings.xhr();
        xhr.upload.onprogress = function(e) {
          var progress=Math.floor(e.loaded / e.total *100) + '%';
            console.log(progress);
            //if($('.upload-header .progress-bar').css('width')<=progress)
            $('.upload-header .progress-bar').css('width',progress);
            $('.progress-status').html(progress);
            //if($('.upload-footer .progress-bar').css('width')=='100%') $('.progress-status').html("done");
        };
        //xhr.upload.onload = function(){ $('.progress-status').html("done"); } ;
        return xhr;


        },

        complete:function(data,status){
            //alert(status);
            if(status=="error") //alert(data);
           $('.progress-status').html(data.responseText);
           else
           $('.progress-status').html(data.responseText);
           /*{

              var file=$('input[type=file]').val().split('\\').pop();

              val=checkFile(file);
              if(val)
              {alert(val);
                setTimeout(function(){
                  $('.progress-status').html(data.responseText);

                },150);
              }
              */
             /*$(":animated").promise().done(function() {
               //$('.upload-footer .progress-bar').css('width','100%');
               $('.progress-status').html(data.responseText);
             });

           }*/
        },

        url: '/documents/documents/upload', type: 'post', data:{path:path}});

      });
    });

    $(document).on('click','#save-assignments',function(e) {
      e.preventDefault();
      item=$("#dirContent .selected");
      path= item.data('path');
      treeItem=$("#tree").find("[data-path='" + item.data('path') + "']");

      $("#form-assignments").ajaxSubmit({url: '/documents/documents/permissions', type: 'post', data:{path:path,save:true},success:function(data,status){

        $("#myModal").modal('hide');
        if(data==='set')
        {
          if(!$("#dirContent .selected").find('.permissions-key').length)
          {
            $("#dirContent .selected").append('<i class="fa fa-key permissions-key"></i>');
            treeItem.append('<i class="fa fa-key permissions-key"></i>');
          }
          showResult('.alert-success','Permissions set successfully.');

        }
        else if(data==='cleared') {
          $("#dirContent .selected").find('.permissions-key').remove();
          treeItem.find('.permissions-key').remove();
          showResult('.alert-success','Permissions cleared successfully.');
          showResult('.alert-warning','Be carefull this Items has no permissions !.');

        }

      }});

    })

    $('#permissions-button').on('click',function(event) {
      item=$('#dirContent .selected');
      //if(item)
      path=item.data('path');
      //name=item.contents().slice(1).text();
      //type=(item.hasClass('dirContent-folder'))? '1':'0';
      //console.log(item.contents().slice(1).text());
      $.post("permissions",{path:path,load:true/*,name:name,type:type*/}, function(data, status){
      //  $('#form-upload-div').html(data);
      //  $('#form-upload-div').collapse('toggle');
        object=$.parseHTML(data);
        header=$(object).find('.assignments-header').remove();
        footer=$(object).find('.assignments-footer').remove();
        //console.log(footer);
        $('#myModal .modal-header').html(header);
        $('#myModal .modal-body').html(object);
        $('#myModal .modal-footer').html(footer);
        $("#myModal").modal('show');

         /*if(data=='locked')
         {
           alert("item locked.");
           item.append('<i class="fa fa-lock"></i>');
           treeItem.append('<i class="fa fa-lock"></i>');
           $('#lock-button').children().first().removeClass('fa-lock').addClass('fa-unlock');
         }
         else if(data=='unlocked')
         {
           alert("item unlocked.");
           item.children().last().remove();
           treeItem.children().last().remove();

           $('#lock-button').children().first().removeClass('fa-unlock').addClass('fa-lock');

         }
         else
         {
           alert(data);

         }*/

      });
    });

    $('#lock-button').on('click',function(event){

      //if($('#form-upload-div').length)
      item=$('#dirContent .selected');
      treeItem=$("#tree").find("[data-path='" + item.data('path') + "']");
      //if(item)
      path=item.data('path');
      name=item.contents().slice(1).text();
      type=(item.hasClass('dirContent-folder'))? '1':'0';
      //console.log(item.contents().slice(1).text());
      $.post("lock",{path:path,name:name,type:type}, function(data, status){
         if(data=='locked')
         {
           //alert("item locked.");
           showResult(".alert-success","item locked successfully.");
           item.append('<i class="fa fa-lock"></i>');
           treeItem.append('<i class="fa fa-lock"></i>');
           $('#lock-button').children().first().removeClass('fa-lock').addClass('fa-unlock');
         }
         else if(data=='unlocked')
         {
           //alert("item unlocked.");
           showResult(".alert-success","item unlocked successfully.");
           showResult(".alert-warning","this item can now be accessed.");

           item.children().last().remove();
           treeItem.children().last().remove();

           $('#lock-button').children().first().removeClass('fa-unlock').addClass('fa-lock');

         }
         else
         {
           //alert(data);
           showResult(".alert-danger",data);


         }

      });
  });

  $(document).on('click','#post-comment',function(){
    $.post('new-comment',{comment:$('#new-comment').val(), path:$('.dirContent-file.selected').data('path')},function(data,status){
      if(status=='success' && data=='success')
      {
        //comment='<div class="comment row"><p class="col-md-9">'.$('#new-comment').val().'</p><p class="col-md-3">'.$value->timestamp.'<br> by <strong>'.$value->user->username.'</strong></p> </div>';
        $('.dirContent-file.selected').click();
        setTimeout(function () {
          $('#detail-div li:eq(1) a').tab('show');
        }, 100);

        $("html, body").animate({ scrollTop: $(document).height() }, "slow");

      }
    });
  });

  function loadTree(foldername,event,name)
  {
    $.post("tree",{path:foldername},function(data,status){

          //console.log($(event.target).children('ul.collapse'));
          var obj=$(event.target).next('ul.collapse')[0];
          name= name || false;

          //console.log(name);
          if(!obj)
          {
            $("#tree-collection").html(data);
          }
          $(obj).html(data);
          $(obj).collapse((name)?'show':'toggle');
          if(name)
          {
            elem=$("#tree").find("[data-path='" + foldername+'/'+name.create + "']");
            //$('#tree .selected').removeClass('selected');
            //elem.addClass('selected');
            elem.click();
            }
          // load to content div
          data=data.replace(/tree-folder/g, "dirContent-folder");
          data=data.replace(/tree-file/g, "dirContent-file");
          $('#dirContent').html(data);

    });
  }

  function loadTreeItem(path,create)
  {
    create= create || false;
    alem=$("#tree").find("[data-path='" + path + "']");
    if(create) alem.trigger('click', { create: create });
    else alem.click();
    //loadTree(path,alem);
  }

  function showResult(type,text)
  {
    $(type).children('p').html(text);
    $(type).removeClass('hidden');
    $(type).fadeIn('250');

    //$(type).toggleClass('in');
  }

  function enableActions()
  {
      $("#dirActions .btn.enable-dependent").removeClass('disabled');
  }

  /*$('#dirContent').on('focusout',function(){
    $("#dirActions .btn.enable-dependent").addClass('disabled');
  });*/

  function breadToPath()
  {
    a=[];
      ar=$(".breadcrumb").children().slice(1).each(function(index, el) {
        //return(el.innerHtml);
        a.push( el.innerHTML );
      });
      return(a.join("/"));
  }
  function search()
  {
    $.post('search', {path:breadToPath(), val:$('#searchdoc').val()}, function(data, textStatus, xhr) {
      $("#dirContent").html(data);
    });
  }
  function basename(path)
  {
    return path.split(/[\\/]/).pop() ;
  }
  function pathname(path)
  {
    path=path.split(/[\\/]/);
    path.pop();
    return path.join('/');
  }
  function loadDocumentDetails(path)
  {
    $.post('document-details',{path:path},function(data){

      $('#detail-div').html(data);
    });
  }
  function checkFile(file)
  {
      path='/';//alert(file);
      check=false;
      $('.breadcrumb').children().slice(1).each(function(i,e){path+=e.textContent+'/' });
      $.post('search',{path:path, val:file,checkFile:true},function(data){

        check=data;
      });
      setTimeout(function(){
        //do what you need here
        //alert(check);


    }, 100);
    alert(check);
    return check;

  }

  function loadPath(path,load,recursive)
  {
    if(!recursive)
    path= path.split('/');
    path.shift();
    //console.log(path);
    var load= load || '';

    load+='/'+path[0];

    //console.log(load);
    loadTreeItem(load,true);
    if(path.length>1)
    {
      setTimeout(function(){
        //do what you need here
      loadPath(path,load,true);

    }, 250);
    }
    /*
    $.each(path,function(index,elem){
      //console.log(elem);
      load+='/'+elem;
      //console.log(load);
      loadTreeItem(load);
      //alert();
    });*/
  }

  $(document).on('click','.alert  .close', function (e) {

    $(e.target).parent().fadeOut('250');

  });
  $(document).on('focusout','#dirContent', function (e) {

    $("#dirActions .btn.enable-dependent").addClass('disabled');
    alert("dd");
  });


  $('#searchdoc').on('keypress',function (e) {
    if(e.which == 13) {
      search();
    }
  });
  $('#searchdoc-icon').on("click",function() {


    search();

  });

  $(document).on('click','.search-result',function(event){
    //alert($(event.target).data('path'));
    path=$(event.target).data('path');
    //console.log(pathname(path));
    loadPath(path);

  });





});
