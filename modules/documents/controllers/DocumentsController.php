<?php

namespace app\modules\documents\controllers;

use Yii;
//use app\modules\documents\models\;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use app\modules\documents\models\uploadForm;
use app\modules\documents\models\Document;
use app\modules\documents\models\Comment;
use app\modules\documents\models\Workflow;
use app\modules\documents\models\Process;
use app\modules\documents\models\Category;
use app\modules\documents\models\docLocked;
use app\modules\documents\models\ProfilesDocuments;
use app\modules\documents\models\DocumentStatus;

/**
 * documentsController implements the CRUD actions for documents model.
 */
class DocumentsController extends Controller
{
    public $layout = 'documents';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all documents models.
     * @return mixed
     */
    public function actionIndex()
    {
      //dd('ddd');
      $model = new UploadForm();

        return $this->render('index', [
            //'model' => $model,
        ]);
    }


    /**
     * Displays a single documents model.
     * @param integer $id
     * @return mixed
     */
    public function actionTree()
    {

        $auth = \Yii::$app->authManager;
        $roles=$auth->getRolesByUser(Yii::$app->user->id);
        //$files=\yii\helpers\FileHelper::findFiles('/path/to');
        $locked=docLocked::find()->with('user')->select(['path','user_id'])->asArray()->all();//->asArray()->All();
        //dd(\yii\helpers\ArrayHelper::map($locked, 'name', 'path'));
        //dd(in_array('/projects', $locked ));
        //dd($roles);

        $locked=\yii\helpers\ArrayHelper::map($locked, /*'name',*/ 'path','user.username');
        //$roles=\yii\helpers\ArrayHelper::map($roles, /*'name',*/ 'name');
        //dd($locked);

        $permissions=ProfilesDocuments::find()/*->with('profiles')*/;
        $permissionsTab=\yii\helpers\ArrayHelper::map($permissions->all(), /*'name',*/ 'path','profile_id');
        $path=rootpath;
        $post=\Yii::$app->request->post();
        if( $post)
        {
          $permissions=$permissions->where(['path'=>$post['path']])->asArray()->all();
          $permissions=\yii\helpers\ArrayHelper::map($permissions, /*'name',*/ 'profile_id','path');
          //dd(array_intersect(array_keys($permissions), array_keys($roles)));

          //dd(array_keys($roles));
          //dd(array_keys($roles));
          if( array_key_exists($post['path'], $locked ) && ($locked[$post['path']]!= Yii::$app->user->identity->username) )
          {
            return 'cannot access item. locked by : '.$locked[$post['path']];
          }
          elseif(in_array($post['path'],$permissions) && empty(array_intersect(array_keys($permissions), array_keys($roles))))
          {
            return 'cannot access item. you do not have the required access level.';
          }
          $treepath=($post['path']!='/')? $post['path'].'/':$post['path'];
        }
        else
        $treepath='/';
        //dd($path);
        $dirs =  glob($path.$treepath.'/*') ;
        //$dirs = scandir('../modules/documents/Entreprise/');
        //dd($dirs);
        $treeHtml='<ul id="tree-collection">';
        $treeFiles='';
        $cid=0;//data-path="'.$treepath.'/'.$value'"
        foreach ($dirs as $key => $value) {

          if(is_dir($value)) //data-toggle="collapse"
          $treeHtml.='<li class="tree-folder"  data-target="#'.basename($treepath).$cid.'" data-path="'.$treepath.basename($value).'" ><i class=" fa fa-folder"></i>'.basename($value).(array_key_exists($treepath.basename($value), $locked )?'<i class="fa fa-lock"></i>':'').(array_key_exists($treepath.basename($value), $permissionsTab )?'<i class="fa fa-key"></i>':'').'</li><ul id="'.basename($treepath).$cid.'" class="collapse"></ul>';
          else
          $treeFiles.='<li class="tree-file" data-path="'.$treepath.basename($value).'" ><i class="fa fa-file-o"></i>'.basename($value).'</li>';
          $cid++;
          //$dirs[$key]=basename($value);
        }
        $treeHtml.=$treeFiles;
        $treeHtml.='</ul></ul>';
        if(!$post) $treeHtml='<ul><li class="selected tree-folder" data-path="/"><i class=" fa fa-folder"></i>Entreprise</li>'.$treeHtml.'</ul>';
        //dd($treeHtml);
        return $treeHtml;
    }

    /**
     * Creates a new documents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateFolder()
    {
        if(\Yii::$app->request->isAjax)
        {
          $post=\Yii::$app->request->post();
          mkdir(rootpath.$post['path'].'/'.$post['name']);
        }

    }

    /**
     * Updates an existing documents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionRenameFolder()
    {
      if(\Yii::$app->request->isAjax)
      {
        $post=\Yii::$app->request->post();
        if(rename(rootpath.$post['path'],pathinfo(rootpath.$post['path'])['dirname'].'/'.$post['name']))
        return true;
        else return false;
      }
    }

    /**
     * Deletes an existing documents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteFolder()
    {
      if(\Yii::$app->request->isAjax)
      {
        $post=\Yii::$app->request->post();
        if($count=count(glob(rootpath.$post['path'].'/*')) != 0) return "Directory not empty !";
        elseif(rmdir(rootpath.$post['path']))
        {
            return true;
        }else return false;
      }
    }


    public function actionUpload()
    {
      if(\Yii::$app->request->isAjax)
      {


        //dd(\Yii::$app->request->post());
        $model= new Document();
        $modelFile = new UploadForm();
        //dd('here');
        if (Yii::$app->request->isPost) {
            $post=\Yii::$app->request->post();
            //$post['Document']['last_update']=date("Y-m-d");
            $modelFile->file = UploadedFile::getInstance($modelFile, 'file');
            //dd($modelFile->file->name);
            try {
            if ($modelFile->upload(rootpath.Yii::$app->request->post('path').'/')) {
                // file is uploaded successfully
                $post['Document']['path']=$post['path'].'/'.$modelFile->file->name;

                if($model->load($post) && $model->save())
                {
                  //dd($model);
                }
                else "error saving data";

                return 'File uploaded successfully.';
            }
              else return 'file error.';//dd($model->errors);
            } catch (Exception $e) {
                return 'error when uploading.';
            }



        }

      }
        //return $this->render('upload', ['model' => $model]);
    }

    public function actionLoadUpload()
    {
      if(\Yii::$app->request->isAjax)
      {
      $modelFile = new UploadForm();
      $model = new Document();
      $categories = ArrayHelper::map(Category::find()->all(), 'id', 'name');
      return $this->renderAjax('upload', ['modelFile' => $modelFile,'model'=>$model, 'categories'=>$categories]);
      }
    }

    public function actionDocumentDetails()
    {
      if(Yii::$app->request->isAjax)
      {
        $post=Yii::$app->request->post();
        $model =  Document::find()->with('workflow0')->where(['path'=>$post['path']])->one();
        //dd($model->workflow0->process);
        $docStatus= DocumentStatus:: find()->where(['document_id'=>$model->id])->all();
        //dd($docStatus);
        $comments= Comment::find()->where(['document_id'=>$model->id])->all();
        $process=$model->workflow0->process;
        //dd($process);
        return $this->renderAjax('doc_detail',['model'=>$model,'comments'=>$comments, 'process'=>$process,'processStatus'=>$docStatus]);
      }
    }

    public function actionNewComment()
    {
      if(Yii::$app->request->isAjax)
      {
          $post=Yii::$app->request->post();
          $doc= Document::find()->where(['path'=>$post['path']])->one();
          $model= new Comment();
          $model->comment=$post['comment'];
          $model->document_id=$doc->id;
          if($model->save())
          {
            return 'success';
          }
          else return 'error';


      }
    }
    public function actionPermissions()
    {

      if(Yii::$app->request->isAjax)
      {

      $request = Yii::$app->request->post();
//dd($request);
      if(isset($request['save']))
      {
         $count=ProfilesDocuments::saveAssignments($request);
        //return $this->redirect(['profiles','training'=>$training]);
        if($count==0) return 'cleared';
        else return 'set';
      }
      else
      {
        //$training=Training::findOne($training);
        $auth = \Yii::$app->authManager;
        $roles=$auth->getRoles();
        $match= ProfilesDocuments::find()->where(['path'=>$request['path']])->all();
        $assigns=[];
        foreach ($match as $key => $value)
        {
          $value->toArray();
          $assigns[]=$value['profile_id'];
        }

         return $this->renderAjax('assignments',['assigns'=>$assigns, 'roles'=>$roles,'path'=>$request['path']]);

      }

      }
    }

    public function actionLock()
    {
      if(\Yii::$app->request->isAjax)
      {

      if (Yii::$app->request->isPost) {
          $post=Yii::$app->request->post();
          $model=docLocked::find()->where(['path'=>$post['path']])->one();
          if($model && Yii::$app->user->id==$model->user_id ){
            $model->delete();
            return 'unlocked';
          }
          elseif($model && Yii::$app->user->id!=$model->user_id)
          {
            return 'You cannot unlock this item as it was locked by : '.$model->user->username;
          }
          else{
            $model = new docLocked();
            $model->type=$post['type'];
            $model->name=$post['name'];
            $model->path=$post['path'];
            $model->user_id=\Yii::$app->user->id;
            if($model->save())
            return 'locked';
          }
      }
      }
    }




    public function actionSearch()
    {
      if(Yii::$app->request->isAjax)
      {
        $post=Yii::$app->request->post();//dd($post);
        //$files=\yii\helpers\FileHelper::findFiles(rootpath.$post['path'],['only'=>['*'.$post['val']."*"], 'caseSensitive'=>false]);
        //$result=glob(rootpath.$post['path'].'*'.$post['val']."*");
        $files=$this->glob_recursive(\yii\helpers\FileHelper::normalizePath(rootpath.$post['path'].'/*'.$post['val']."*"));
        //dd(rootpath.$post['path'].'*'.$post['val']."*");
        //dd($files);
        if($post['checkFile']) return count($files);

        $content='';
        foreach ($files as $key => $value) {
          $type= is_dir($value)? 'folder':'file-o';
          $file= explode('/Entreprise',$value);
          //dd($file);
        $content.='<div class="row"><div class="search-result col-md-4" data-path="'.$file[1].'"><i class="fa fa-'.$type.'"></i> '.basename($file[1]).'</div><div class="col-md-8"><i class="fa fa-angle-double-right "></i> '.$file[1].'</div></div>';

        }
        return $content;
      }
    }



    protected function glob_recursive($pattern, $flags = 0)
    {
      $files = glob($pattern, $flags);
      foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir)
      {
        $files = array_merge($files, $this->glob_recursive($dir.'/'.basename($pattern), $flags));
      }
      return $files;
    }
    /**
     * Finds the documents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return documents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = documents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
