<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\training\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TrAsset extends AssetBundle
{
    public $sourcePath = '@tr/assets';

    public $css = [
        'css/tr.css',
    ];
    public $js = [
         'js/tr.js',
    ];
    public $image = [
         'images/tr_menu.png',
         'images/menu-lg.png',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $publishOptions = [
    'forceCopy' => true,
];
}
