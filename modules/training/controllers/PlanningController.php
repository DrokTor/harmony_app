<?php

namespace app\modules\training\controllers;

use Yii;
use app\modules\training\models\Planning;
use app\modules\training\models\PlanProfiles;
use app\modules\training\models\Training;
use app\modules\training\models\ProfilesTrainings;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * TrainingController implements the CRUD actions for Training model.
 */
class PlanningController extends Controller
{
    public $layout = 'training';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Training models.
     * @return mixed
     */
    public function actionIndex()
    {

        /*$dataProvider = new ActiveDataProvider([
            'query' => Planning::find(),
        ]);*/
        $model=Planning::find()->with('training','profiles')->orderBy('start_date')->all();

        //dd($model);
        return $this->render('index', [
            'model' => $model,//$dataProvider
        ]);
    }




    /**
     * Displays a single Training model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $profiles=Planning::find()->with('profiles')->where(['id'=>$id])->one();

        $profiles = ArrayHelper::getColumn($profiles->profiles, function ($element) {
            return $element['profile_id'];
        });


        $profiles='<p><span class="label label-info">'.implode('</span> <span class="label label-info">',$profiles).'</span></p>';

        return $this->render('view', [
            'model' => $this->findModel($id),
            'profiles' => $profiles,

        ]);
    }

    /**
     * Creates a new Training model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionNew()
    {
        $post=Yii::$app->request->post();
        $model = new Planning();
        $trainings= Training::find();
        $auth = \Yii::$app->authManager;
        $roles=$auth->getRoles();
        $roles=ArrayHelper::map($roles, 'name', 'name');
        //dd($post);
        if ($model->load($post) && $model->save()) {

          $profileTab=[];
          foreach ($post['roles'] as $key => $value) {
            $profileTab[]=[$model->id,$value];
          }

          Yii::$app->db->createCommand()->batchInsert(PlanProfiles::tableName(), ['plan_id','profile_id'], $profileTab)->execute();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('new', [
                'model' => $model,
                'trainings'=> $trainings,
                'roles'=>$roles,
            ]);
        }
    }

    /**
     * Updates an existing Training model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Training model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Training model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Training the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Planning::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
