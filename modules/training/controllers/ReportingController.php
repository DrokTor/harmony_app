<?php

namespace app\modules\training\controllers;

use Yii;
use app\models\DevDeviation;
use app\models\DevDeviationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DeviationController implements the CRUD actions for DevDeviation model.
 */
class ReportingController extends Controller
{


    public $layout = 'training';
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DevDeviation models.
     * @return mixed
     */
  public function actionIndex()
    {

      if(! \yii::$app->user->isGuest)
        {
          return $this->render('home');
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }


    }

    public function actionNew()
    {

      if(! \yii::$app->user->isGuest)
        {
          return $this->render('new');
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }


    }
    public function actionList()
    {

      if(! \yii::$app->user->isGuest)
        {
          return $this->render('list');
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }


    }
    public function actionData()
    {

      if(! \yii::$app->user->isGuest)
        {
          return $this->render('data');
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }


    }
    public function actionRef()
    {

      if(! \yii::$app->user->isGuest)
        {
          return $this->render('ref');
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }


    }
    public function actionView()
    {

      if(! \yii::$app->user->isGuest)
        {
          return $this->render('view');
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }


    }

}
