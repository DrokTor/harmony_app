<?php

namespace app\modules\training\controllers;

use Yii;
use app\models\User;
use app\modules\training\models\TrainingsUsers;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * UsersController implements the CRUD actions for TrainingsUsers model.
 */
class UsersController extends Controller
{

    public $layout = "training";
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TrainingsUsers models.
     * @return mixed
     */
    public function actionIndex()
    {

        //$query= User::find()->leftJoin('auth_assignment','id_user=user_id')->select(['id_user','item_name'])->all() ;
        $query = (new \yii\db\Query())
        ->select(['adm_users.id_user','username', 'COUNT(DISTINCT tr_trained_users.training_id) as trained'
        ,'COUNT(DISTINCT tr_profiles_trainings.id) as total_training'
      ])
        ->from('adm_users')
        ->leftJoin('auth_assignment','id_user=auth_assignment.user_id')
        ->leftJoin('tr_profiles_trainings','item_name=profile_id')
        ->leftJoin('tr_training','training_id=tr_training.id')
        ->leftJoin('tr_trained_users','id_user=tr_trained_users.user_id')
        ->groupBy('username');
        //dd($query->createCommand()->rawSql);
        //->where(['last_name' => 'Smith'])
        //->limit(10)
        //->all();

        //dd($query);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TrainingsUsers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $user= TrainingsUsers::find()->with('user','training')->where(['user_id' => $id])->asArray()->all();
        //$user=ArrayHelper::map($user, 'training_id', 'user_id');
        //dd($user);
        return $this->render('view', [
            'model' => $user,//$this->findModel($id),
        ]);
    }

    /**
     * Creates a new TrainingsUsers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TrainingsUsers();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TrainingsUsers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TrainingsUsers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrainingsUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrainingsUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TrainingsUsers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
