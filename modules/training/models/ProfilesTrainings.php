<?php

namespace app\modules\training\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tr_profiles_trainings".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $training_id
 */
class ProfilesTrainings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tr_profiles_trainings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profile_id', 'training_id'], 'required'],
            [['profile_id', 'training_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'profile_id' => Yii::t('app', 'Profile ID'),
            'training_id' => Yii::t('app', 'Training ID'),
        ];
    }

    public function saveAssignments($request,$training)
    {
      //$proTr = ProfilesTrainings::find()->where(['id'=>$training])->all() ;
      //$perms=$auth->getChildren($id);
      //$auth->revokeAll($user);
      ProfilesTrainings::deleteAll(['training_id' => $training]);
      $assignTab=[];
       array_shift($request);
      foreach ($request as $key => $value)
      {
        $key=str_replace('_',' ',$key);
        $assignTab[]=[$key,$training];
      }

      $auth = \Yii::$app->authManager;
      $usersTr=[];
       $assignTabmapped= ArrayHelper::map($assignTab, '0', '1') ;

      foreach ($assignTabmapped as $key => $value) {
        $users[]=$auth->getUserIdsByRole($key);


      }

      //$users= ArrayHelper::map($users, '0', '1' ) ;
      $useval=[];
      foreach ($users as $key => $value) {
        foreach ($value as $key => $nvalue) {
            $useval[]=$value[$key];
        }

      }
      $useval=array_unique($useval);
      foreach ($useval as $key => $value) {
        $useval[$key]=[$value,$training];
      }
      //dd($useval);

      Yii::$app->db->createCommand()->batchInsert('tr_profiles_trainings', ['profile_id', 'training_id'],
          $assignTab
       )->execute();

      Yii::$app->db->createCommand()->batchInsert('tr_trainings_users', ['user_id', 'training_id'],
          $useval
       )->execute();
      //print_r($perms);
    }
}
