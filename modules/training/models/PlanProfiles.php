<?php

namespace app\modules\training\models;

use Yii;

/**
 * This is the model class for table "tr_plan_profiles".
 *
 * @property integer $id
 * @property integer $plan_id
 * @property string $profile_id
 */
class PlanProfiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tr_plan_profiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_id'], 'integer'],
            [['profile_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'plan_id' => Yii::t('app', 'Plan ID'),
            'profile_id' => Yii::t('app', 'Profile ID'),
        ];
    }
}
