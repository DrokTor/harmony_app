<?php

namespace app\modules\training\models;

use Yii;
use app\modules\training\models\Training;
use app\models\User;


/**
 * This is the model class for table "tr_trainings_users".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $training_id
 */
class TrainingsUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tr_trained_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'training_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'training_id' => Yii::t('app', 'Training ID'),
        ];
    }
    public function getUser()
    {
      return $this->hasOne(User::className(),['id_user'=>'user_id']);
    }

    public function getTraining()
    {
      return $this->hasOne(Training::className(),['id'=>'training_id']);
    }
}
