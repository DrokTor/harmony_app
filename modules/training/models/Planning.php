<?php

namespace app\modules\training\models;

use Yii;
use app\modules\training\models\Training;
use app\modules\training\models\PlanProfiles;

/**
 * This is the model class for table "tr_planning".
 *
 * @property integer $id
 * @property string $start_date
 * @property string $end_date
 * @property string $description
 */
class Planning extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tr_planning';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_date', 'end_date','training_id'],'required'],
            [['start_date', 'end_date','training_id'], 'safe'],
            [['description'], 'string'],
            //[['start_date'], 'date', 'format'=>'Y-M-dd'],
            //[['end_date'], 'date', 'format'=>'Y-M-dd'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    public function getTraining()
    {
      return $this->hasOne(Training::className(),['id'=>'training_id']);
    }
    public function getProfiles()
    {
      return $this->hasMany(PlanProfiles::className(),['plan_id' => 'id']);
    }
}
