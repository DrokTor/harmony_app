<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\training\models\TrainingsUsers */

$this->title =  $model[0]['user']['username'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Trainings Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainings-users-view">

    <h1><?= Html::encode(' Training history : '. $this->title) ?></h1><hr>



    <?php
        foreach ($model as $key => $value) {
          echo '<div class="row" ><table class="table-striped col-md-12">';
          echo '<tr><td class="col-md-4"><label>Training</label><td><td  class="col-md-8">'.$value['training']['title'].'</td></tr>';
          echo '<tr><td class="col-md-4"><label>Start date</label><td><td  class="col-md-8">'.$value['start_date'].'</td></tr>';
          echo '<tr><td class="col-md-4"><label>End date</label><td><td  class="col-md-8">'.$value['end_date'].'</td></tr>';
          echo '<tr><td class="col-md-4"><label>Evaluation</label><td><td  class="col-md-8">'.$value['evaluation'].'</td></tr>';
          echo '</table></div></br></br>';
        }

     ?>

</div>
