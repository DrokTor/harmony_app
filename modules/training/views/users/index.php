<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Trainings Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainings-users-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            [
              'label'=>'Skills',
              'value'=>function($model,$index){ return '<div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: '.(($model['total_training']>0)?$model['trained']/$model['total_training']*100:0).'%;">
                        </div>
                      </div>';},
              'format'=>'html',
            ],
            'trained',
            'total_training',
            //'user_id',
            //'training_id',

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'buttons'=>[
                'view' => function ($url, $model, $key) {
                    return  Html::a('<span class="glyphicon glyphicon-eye-open"></span>', yii\helpers\Url::to(['/training/users/view', 'id' => $model['id_user']]) ) ;
                },
              ],
             'contentOptions'=>['class'=>'text-center'],
           ],
        ],
    ]); ?>

</div>
