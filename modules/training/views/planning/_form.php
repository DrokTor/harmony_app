<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
//use kartik\date\DatePicker;



/* @var $this yii\web\View */
/* @var $model app\modules\training\models\Training */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="training-form">

    <?php $form = ActiveForm::begin();


      echo '<label class="control-label">Select date range</label>';
    echo DatePicker::widget([
        'model' => $model,
        'attribute' => 'start_date',
        'attribute2' => 'end_date',
        'options' => ['placeholder' => 'Start date'],
        'options2' => ['placeholder' => 'End date'],
        //'language' => 'fr',
        'type' => DatePicker::TYPE_RANGE,
        'form' => $form,
        'pluginOptions' => [
            'format' => 'yyyy-m-dd',
            'autoclose' => true,
        ]
    ]);






    $tr=ArrayHelper::map($trainings, 'id', 'title');
    //dd($tr);
    ?>


    <?= $form->field($model, 'training_id', ['options'=>['class'=>'']])->dropdownList(
    $trainings->select([ 'title','id'])->indexBy('id')->column(),
    ['prompt'=>'Select training']
    );  ?>



    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="col-sm-6">
      <label><?= \Yii::t('app','Profiles')?></label>
    <?= Html::checkboxList('roles', null, $roles, ['separator'=>'<br>' ]) ?>
    </div>

    <div class="col-sm-6">
      <label><?= \Yii::t('app','Users')?></label>

    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Plan') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
