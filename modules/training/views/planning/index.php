<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$formatter = \Yii::$app->formatter;
$this->title = Yii::t('app', 'Trainings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Plan Training'), ['new'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="">
      <?php

      foreach ($model as $key => $value) {


        echo '<a href="view?id='.$value->id.'"><div class="training-plan col-sm-3"><label>'.$value->training->title.'</label><h5>'.$formatter->asDate($value->start_date).' - '.$formatter->asDate($value->end_date).'</h5>
            <p>'.\yii\helpers\StringHelper::truncate($value->description,80).'</p>

            </div></a>';
      }
       ?>
    </div>



</div>
