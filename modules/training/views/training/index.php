<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Trainings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Training'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description:ntext',

          //  ['class' => 'yii\grid\ActionColumn'],

          [
            'class' => 'yii\grid\ActionColumn',
             'template' => '{view}{update}{delete} {roles}',
             'buttons'=>[
                 'roles' => function ($url, $model, $key) {
                     return  Html::a('<span class="glyphicon glyphicon-user"></span>', yii\helpers\Url::to(['/training/training/profiles', 'training' => $model->id]) ) ;
                 },

              ],
              'contentOptions'=>['class'=>'text-center'],
          ],
        ],
    ]); ?>

</div>
