<?php

  namespace app\modules\training;

class Training extends \yii\base\Module
{

  public $controllerNamespace = 'app\modules\training\controllers';

    public function init()
    {


        parent::init();

        //\Yii::configure($this, require(__DIR__ . '/config.php'));
        $this->setAliases([
            '@tr' =>  __DIR__ ,
            '@trUrl' => \Yii::$app->request->hostInfo.\Yii::getAlias('@web').'/training/training',
            //'@capaUrl' => \Yii::$app->request->hostInfo.\Yii::getAlias('@web').'/deviation/capa',
        ]);
    }
}
