<?php
namespace app\modules\administration\models;


class Permissions extends \yii\base\Model
{
  //public $permission;
  public $allPermissions;
  //public $isSet_permission[];

/*  public function rules()
  {
    return [
            [['name'],'required'],
            [['name'],'string']
          ];

  }
*/
  public function getPermissions($id=null)
  {
    $auth = \Yii::$app->authManager;
    $allPermissions=AuthItem::find()->where(['type'=>'2'])->all();
    $perms=$auth->getChildren($id);

    $i=0;
    foreach ($allPermissions as $perm ) {
      $all[$i]['name']=$perm->name;
      if($auth->hasChild($auth->getRole($id),$auth->getPermission($perm->name)))
      {
        $all[$i]['isSet']='true';
      }
      else
      {
        $all[$i]['isSet']='false';
      }
      ++$i;
    }
    $this->allPermissions=$all;

    /*foreach ($perms as $perm ) {
      # code...
      //print_r($perm->name);exit();
      $this->permission['name'][]=$perm->name;

    }
*/
    //return( $this );
  }

  public function savePermissions($id,$post)
  {
    $auth = \Yii::$app->authManager;
    //$perms=$auth->getChildren($id);
    $auth->removeChildren($auth->getRole($id));
    foreach ($post as $key => $value) {
      # code...
      $key=str_replace('_',' ',$key);
       
      if($auth->getPermission($key))
      {
          $auth->addChild($auth->getRole($id),$auth->getPermission($key));
      }

    }
    //print_r($perms);
  }
}


 ?>
