<?php

namespace app\modules\administration\models;

use Yii;

/**
 * This is the model class for table "adm_countries_table".
 *
 * @property integer $id
 * @property string $code
 * @property string $nom
 */
class AdmCountries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adm_countries_table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code', 'nom'], 'string', 'max' => 255],
            [['code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'nom' => Yii::t('app', 'Name'),
        ];
    }
}
