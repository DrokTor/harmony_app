<?php

namespace app\modules\administration\models;

use Yii;
use app\modules\administration\models\AdmLanguages;
use app\modules\administration\models\AdmCountries;

/**
 * This is the model class for table "adm_locales_table".
 *
 * @property integer $id
 * @property string $language
 * @property string $timezone
 */
class AdmLocale extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     //public  $timezone;

    public static function tableName()
    {
        return 'adm_locales_table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'timezone'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'timezone' => 'Timezone',
        ];
    }

    public function getRLanguage()
    {
      return $this->hasOne(AdmLanguages::className(),['id'=>'language']);
    }
    /*public function getRCountry()
    {
      return $this->hasOne(AdmCountries::className(),['id'=>'country']);
    }*/
}
