<?php

namespace app\modules\administration\models;

use yii\db\ActiveRecord;



class Position extends ActiveRecord
{

    public static function tableName()
    {
        return 'adm_positions';
    }
    /**
     * @inheritdoc
     */

     public function rules()
     {
         return [
             [['name'], 'required'],
             [['name'], 'string', 'max' => 200]
         ];
     }
     public function attributeLabels()
     {
       return [
          'name'=>\Yii::t('admin','Name'),
       ];
     }
}
