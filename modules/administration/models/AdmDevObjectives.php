<?php

namespace app\modules\administration\models;

use Yii;

/**
 * This is the model class for table "adm_dev_objectives".
 *
 * @property integer $id
 * @property string $indicator
 * @property integer $objective
 */
class AdmDevObjectives extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adm_dev_objectives';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['objective'], 'integer'],
            [['indicator'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'indicator' => Yii::t('app', 'Indicator'),
            'objective' => Yii::t('app', 'Objective'),
        ];
    }
}
