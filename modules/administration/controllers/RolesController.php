<?php

namespace app\modules\administration\controllers;

use Yii;
use app\modules\administration\models\AuthItem;
use app\modules\administration\models\Permissions;
use app\modules\administration\models\Assignments;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use app\models\User;

/**
 * RolesController implements the CRUD actions for AuthItem model.
 */
class RolesController extends Controller
{
    public $layout = 'administration';
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => AuthItem::find()->where(['type'=>'1']),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AuthItem();
        $auth = \Yii::$app->authManager;

        if ($model->load(Yii::$app->request->post()) /*&& $model->save()*/)
        {
          $role = $auth->createRole($model->name);
          $role->description=$model->description;
          $auth->add($role);
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPermissions($id)
    {
      $request = Yii::$app->request->post();
      if($request)
      {
        print_r($request) ;
        permissions::savePermissions($id,$request);
        return $this->redirect(['permissions','id'=>$id]);
      }
      else
      {
        # code...
        $model= new permissions();
        $model->getPermissions($id);
        //$data= new ArrayDataProvider(['allModels'=>$model]);
        //$model->permission['name']='draft';
        //print_r(count($model));
        return $this->render('permissions',['model'=>$model]);
      }


    }

    public function actionAssignments($user)
    {
      $request = Yii::$app->request->post();

      if($request)
      {
        assignments::saveAssignments($request,$user);
        return $this->redirect(['assignments','user'=>$user]);
      }
      else
      {
        $username=User::findOne($user);
        $auth = \Yii::$app->authManager;
        $roles=$auth->getRoles();
        $match= Assignments::find()->where(['user_id'=>$user])->all();
        $assigns=[];
        foreach ($match as $key => $value)
        {
          $value->toArray();
          $assigns[]=$value['item_name'];
        }
        return $this->render('assignments',['assigns'=>$assigns, 'roles'=>$roles,'username'=>$username->username]);
      }
    }

    public function actionUsers()
    {

      $users=new ActiveDataProvider([
          'query' => User::find(),
      ]);

      return $this->render('users',['users'=>$users]);
    }
    protected function findModel($id)
    {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
