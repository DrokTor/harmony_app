<?php

namespace app\modules\administration\controllers;

use Yii;
use app\modules\administration\models\AuthItem;
use app\modules\administration\models\Permissions;
use app\modules\administration\models\Assignments;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use app\models\User;

/**
 * RolesController implements the CRUD actions for AuthItem model.
 */
class LogsController extends Controller
{
    public $layout = 'administration';
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dir=Yii::getAlias('@app').'/harmony/logs/';

        $files=scandir($dir);

        $files = array_diff($files, array('..', '.'));

        $file=file_get_contents( Yii::getAlias('@app').'/harmony/logs/harmony.log' );

        //dd($files);
        //echo '<pre>'.$file.'</pre>';
        //pr( $file);
        //date ("F d Y H:i:s ",filectime($dir.$file)).
        foreach ($files as $file)
        {
          echo ' <a href="logs/file?file='.$file.'" >'.$file.'</a><br>';
        }
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */

     public function actionFile($file)
     {
       $file=file_get_contents( Yii::getAlias('@app').'/harmony/logs/'.$file );
       echo '<pre>'.$file.'</pre>';
     }

}
