<?php

namespace app\modules\administration\controllers;

use Yii;
use app\models\DevDeviation;
use app\models\DevDeviationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DeviationController implements the CRUD actions for DevDeviation model.
 */
class DefaultController extends Controller
{


    public $layout = 'administration';
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DevDeviation models.
     * @return mixed
     */
  public function actionIndex()
    {

      if(! \yii::$app->user->isGuest)
        {
          return $this->render('home');
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }


    }
}
?>
