<?php

namespace app\modules\administration\controllers;

use Yii;
use app\modules\administration\models\AdmLocale;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\administration\models\AdmLanguages;
use app\modules\administration\models\AdmCountries;

/**
 * LocaleController implements the CRUD actions for AdmLocale model.
 */
class LocaleController extends Controller
{

    public $layout="administration";

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AdmLocale models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*$dataProvider = new ActiveDataProvider([
            'query' => AdmLocale::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);*/
        return $this->runAction('view',['id'=>'1']);
    }

    /**
     * Displays a single AdmLocale model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AdmLocale model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     /*
    public function actionCreate()
    {
        $model = new AdmLocale();
        $langs= AdmLanguages::find()->all();
        $crts= AdmCountries::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
              'model' => $model,
              'langs'=>$langs,
              'ctrs'=>$crts,
            ]);
        }
    }*/

    /**
     * Updates an existing AdmLocale model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $langs= AdmLanguages::find()->all();
        $crts= AdmCountries::find()->all();
        $list=\DateTimeZone::listIdentifiers();
        foreach ($list as $key => $value) {
          $timezone[$key]['id']=$key;
          $timezone[$key]['nom']=$value;
        }
        //dd($timezone);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
              'model' => $model,
              'langs'=>$langs,
              'ctrs'=>$timezone,//$crts,
              //'timezone'=>$timezone,
            ]);
        }
    }

    /**
     * Deletes an existing AdmLocale model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
     /*
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/

    /**
     * Finds the AdmLocale model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdmLocale the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AdmLocale::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
