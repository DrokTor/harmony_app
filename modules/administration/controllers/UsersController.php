<?php

namespace app\modules\administration\controllers;

use Yii;
use app\models\User;
use app\models\Assignments;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Account;
use app\modules\administration\models\Department;
use app\modules\administration\models\Position;


/**
 * UserController implements the CRUD actions for User model.
 */
class UsersController extends Controller
{
    public $layout='administration';



    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */



    public function actionIndex()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => User::find()->with('account'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $auth= \Yii::$app->authManager;
        $profiles= $auth->getAssignments($id);
        //dd(\yii\helpers\ArrayHelper::toArray($profile,['roleName']));
        $profile='';
        foreach ($profiles as $key => $value)
        {
          $profile.= '<span class="label label-info">'.$key.'</span> ';
        }

        $state=Account::findOne(['user_id'=>$id]);
        //dd($state->rState->name);
        $state=$state->rState->name;
        switch ($state) {
          case 'Active':$state='<span class=" label label-success">'.$state.'</span>';
            # code...
            break;
          case 'Inactive':$state='<span class=" label label-danger">'.$state.'</span>';
            # code...
            break;
          case 'On hold':$state='<span class=" label label-primary">'.$state.'</span>';
            # code...
            break;

          default:
            # code...
            break;
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'profile'=>$profile,
            'state'=>$state,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $account=new Account();
          $account->user_id=$model->id;
          $account->state=Account::ACCOUNT_ON_HOLD;
          $account->token=Yii::$app->getSecurity()->generateRandomString();
          $account->created_at=date('Y-m-d');

          if($account->save())
          { ///dd($model->email);
            Yii::$app->mailer->compose()
            ->setFrom('admin@harmony.com')
            ->setTo($model->email)
            ->setSubject(\Yii::t('app','Harmony-QMS <Account activation> '))
            //->setTextBody('Plain text content')
            ->setHtmlBody('<h2>Username: '.$model->username.'</h2><h2>Name: '.$model->first_name.' '.$model->last_name.'</h2><p> <a href="'.Yii::$app->params['baseurl'].'/account/activate?id='.$account->token.'">'.\Yii::t('app','Your account is waiting for your activation.</a> ').'</p>')
            ->send();
          }

            return $this->redirect(['view', 'id' => $model->id_user]);
        } else {

            $departments= Department::find()->all();
            $positions= Position::find()->all();

            $departments=\yii\helpers\ArrayHelper::map($departments,'id','name');
            $positions=\yii\helpers\ArrayHelper::map($positions,'id','name');

            return $this->render('create', [
                'model' => $model,
                'departments'=>$departments,
                'positions'=>$positions,

            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_user]);
        } else {
          $departments= Department::find()->all();
          $positions= Position::find()->all();

          $departments=\yii\helpers\ArrayHelper::map($departments,'id','name');
          $positions=\yii\helpers\ArrayHelper::map($positions,'id','name');


            return $this->render('update', [
                'model' => $model,
                'departments'=>$departments,
                'positions'=>$positions,
            ]);
        }
    }

    public function actionReset($id)
    {


        $account= Account::findOne(['user_id'=>$id]);
        //dd($account);
        $account->state=Account::ACCOUNT_ON_HOLD;
        $account->token=Yii::$app->getSecurity()->generateRandomString();
        $account->save();

        $user= User::findOne($id);
        $user->password=null;
        $user->save();

        Yii::$app->mailer->compose()
        ->setFrom('admin@harmony.com')
        ->setTo($user->email)
        ->setSubject(\Yii::t('app','Harmony-QMS <Account reset> '))
        //->setTextBody('Plain text content')
        ->setHtmlBody('<h2>Username: '.$user->username.'</h2><h2>Name: '.$user->first_name.' '.$user->last_name.'</h2><p> <a href="'.Yii::$app->params['baseurl'].'/account/activate?id='.$account->token.'">'.\Yii::t('app','Your account has been reset, click to activate it.</a> ').'</p>')
        ->send();

        $session = Yii::$app->session;
        $session->setFlash('accountReset', '<strong>Account successfully reset !</strong> an email has been sent to inform him.');


        return $this->redirect(['index']);
    }

    public function actionDeactivate($id)
    {

        $account= Account::findOne(['user_id'=>$id]);
        $account->state=Account::ACCOUNT_INACTIVE;
        $account->save();

        $user= User::findOne($id);
        $user->password=null;
        $user->save();

        Yii::$app->mailer->compose()
        ->setFrom('admin@harmony.com')
        ->setTo($user->email)
        ->setSubject(\Yii::t('app','Harmony-QMS <Account deactivation> '))
        //->setTextBody('Plain text content')
        ->setHtmlBody('<h2>Username: '.$user->username.'</h2><h2>Name: '.$user->first_name.' '.$user->last_name.'</h2><p> <p >'.\Yii::t('app','Your account has been deactivated, you can contact your HARMONY-qms administrator for more information.</p> ').'</p>')
        ->send();

        $session = Yii::$app->session;
        $session->setFlash('accountDeactivated', '<strong>Account successfully deactivated !</strong> an email has been sent to inform him.');

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */

    public function actionDelete($id)
    {
        if($this->findModel($id)->delete()){
        Assignments::deleteAll(['user_id'=>$id]);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
