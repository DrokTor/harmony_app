<?php

namespace app\modules\administration;

class Administration extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\administration\controllers';

    public function init()
    {
        //dd(\yii\web\Session::getTimeout());
        define('ACCOUNT_ACTIVE','1');
        define('ACCOUNT_INACTIVE','2');
        define('ACCOUNT_ON_HOLD','3');

        parent::init();

        // custom initialization code goes here
        $this->setAliases([
            '@adm' => __DIR__
        ]);
    }
}
