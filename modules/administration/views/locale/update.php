<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\administration\models\AdmLocale */

$this->title = Yii::t('admin', 'Update {modelClass}: ', [
    'modelClass' => '',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Adm Locales'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update');
?>
<div class="adm-locale-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,

        'langs'=>$langs,
        'ctrs'=>$ctrs,
    ]) ?>

</div>
