<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\administration\models\AdmLocale */

$this->title = Yii::t('admin', 'Create Adm Locale');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Adm Locales'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adm-locale-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'langs'=>$langs,
        'ctrs'=>$ctrs,
    ]) ?>

</div>
