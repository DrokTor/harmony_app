<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Adm Locales');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adm-locale-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Adm Locale'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
              'value'=>'rLanguage.name',
              'attribute'=>'language',
            ],
            'timezone',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
