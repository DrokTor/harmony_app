<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\administration\models\AdmLocale */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adm-locale-form">

    <?php

    $languages=ArrayHelper::map($langs, 'id', 'name');
    array_unshift($languages,'Select language');
    $timezones=ArrayHelper::map($ctrs, 'nom', 'nom');
    array_unshift($timezones,'Select a timezone');


    $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'language', ['options'=>['class'=>'col-sm-3']])->dropdownList(
    $languages,
    [ 'options'=>['0'=>['disabled'=>true]]]
      );  ?>


    <?= $form->field($model, 'timezone', ['options'=>['class'=>'col-sm-3']])->dropdownList(
    $timezones,
    [ 'options'=>['0'=>['disabled'=>true]]]
      );  ?>


    <div class="form-group col-sm-12 ">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
