<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Profiles');
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="auth-item-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('admin', 'Create Profile'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,

        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'name',
            // 'type',

            'description:ntext',

            // 'rule_name',
            // 'data:ntext',
            // 'created_at',
            // 'updated_at',

            [
              'class' => 'yii\grid\ActionColumn',
               'template' => '{view} {update} {delete} {permissions}',
               'buttons'=>[
                   'permissions' => function ($url, $model, $key) {
                       return  Html::a('<span class="glyphicon glyphicon-lock"></span>', Url::to(['permissions', 'id' => $model->name])) ;
                   },
                ],
                'contentOptions'=>['class'=>'text-center'],

             ],
        ],
    ]); ?>

</div>
