<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
$this->title = Yii::t('admin', 'Users');
$this->params['breadcrumbs'][] = $this->title;


 ?>

<?= GridView::widget([

        'dataProvider'=>$users,
        'columns'=>
        [
          'id_user',
          'username',
          [
            'class' => 'yii\grid\ActionColumn',
             'template' => ' {roles}',
             'buttons'=>[
                 'roles' => function ($url, $model, $key) {
                     return  Html::a('<span class="glyphicon glyphicon-lock"></span>', Url::to(['assignments', 'user' => $model->id_user]) ) ;
                 },

              ],
              'contentOptions'=>['class'=>'text-center'],
          ]
        ]
  ]);

?>
