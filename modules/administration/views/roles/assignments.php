<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('admin', 'Roles assignments');
$this->params['breadcrumbs'][] = $this->title;

 ?>

 <div class="auth-item-index">

     <h1 ><?= Html::encode($this->title) ?> <label > - <?= $username?></label></h1>
     <div class="container-fluid row">
 <?php
     $form = ActiveForm::begin();
     echo '</div><div class="form-group container-fluid row">';
        echo  Html::submitButton( Yii::t('admin', 'Save'), ['class' => ' btn btn-success btn-lg ' ]);
     echo'</div>';
     $c=count($roles)/3;
     //echo $c;
     for($i=0;$i<$c;++$i)
     {
       echo '<table class="col-xs-12 col-sm-6 col-md-6 permissions-table">
         <thead>
         <tr>
           <th>Roles</th>
           <th class="col-xs-2 col-md-2 ">'.\Yii::t('admin','Assignments').'</th>
         </tr>
       </thead>';


     $j=0;
     foreach ($roles as  $key=>$value)
     {  //print_r($value);exit();
       # code...
       echo '<tr>
             <td>'.$value->name.'</td>
            <td  class="permission '.(in_array($value->name,$assigns)? 'perm-checked':'').' "><input type="checkbox" name="'.$value->name.'"'.(in_array($value->name,$assigns)? 'checked':'').'/></td>
            </tr>';
       unset($roles[$key]);
       $j++;
       if($j==3) break;
     }
     //print_r($model->allPermissions);

    echo'</table>';
    }


    $form = ActiveForm::end();
     ?>
 </div>
