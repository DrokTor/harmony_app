<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('admin', 'Permissions');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="auth-item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="container-fluid row">
<?php
    $form = ActiveForm::begin();
    echo '</div><div class="form-group container-fluid row">';
       echo  Html::submitButton( Yii::t('admin', 'Save'), ['class' => ' btn btn-success btn-lg ' ]);
    echo'</div>';
    $c=count($model->allPermissions)/3;
    //echo $c;
    for($i=0;$i<$c;++$i)
    {
      echo '<table class="col-xs-12 col-sm-6 col-md-6 permissions-table">
        <thead>
        <tr>
          <th>Action</th>
          <th class="col-xs-2 col-md-2 ">'.\Yii::t('admin','Permissions').'</th>
        </tr>
      </thead>';


    $j=0;
    foreach ($model->allPermissions as  $key=>$value)
    {
      # code...
      echo '<tr>
            <td>'.$value['name'].'</td>
            <td  class="permission '.(($value['isSet']=='true')? 'perm-checked':'').' "><input type="checkbox" name="'.$value['name'].'"'.(($value['isSet']=='true')? 'checked':'').'/></td>
            </tr>';
      unset($model->allPermissions[$key]);
      $j++;
      if($j==3) break;
    }
    //print_r($model->allPermissions);

   echo'</table>';
   }


   $form = ActiveForm::end();
    ?>
</div>
