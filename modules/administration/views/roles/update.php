<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\administration\models\AuthItem */

$this->title = Yii::t('admin', 'Update {modelClass}: ', [
    'modelClass' => '',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update');
?>
<div class="auth-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
