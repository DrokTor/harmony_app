<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\modules\administration\assets\AdminAsset;
use kartik\icons\Icon;

$bundle=AdminAsset::register($this);
Icon::map($this);

/* @var $this \yii\web\View */
/* @var $content string */

//AppAsset::register($this);
?>
<?php $this->beginContent('@app/views/layouts/main.php') ?>

      <div class="row">

        <?php
            NavBar::begin([
                'brandLabel' => 'Administration',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse deviation-bar' //navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'admin_navbar navbar-nav navbar-right'],

                'items' => [
                    [
                      'label'=>\Yii::t('admin','Account management'),
                      'items'=>[

                        ['label' => \Yii::t('admin','Roles'),'url' => ['/administration/roles']],
                        ['label' => \Yii::t('admin','Users'), 'url' => ['/administration/users']],
                        ['label' => \Yii::t('admin','Users profiles'),'url' => ['/administration/roles/users']],

                        ],
                      ],
                      [
                      'label'=>\Yii::t('admin','Organization'),
                      'items'=>[

                        ['label' => \Yii::t('admin','Departments'),'url' => ['/administration/departments']],
                        ['label' => \Yii::t('admin','Positions'),'url' => ['/administration/positions']],


                        ],
                      ],
                      [
                      'label'=>\Yii::t('admin','Tracking'),
                      'items'=>[

                        ['label' => \Yii::t('admin','Logs'),'url' => ['/administration/logs']],
                        ['label' => \Yii::t('admin','Objectives'),'url' => ['/administration/objectives']],


                        ],
                      ],
                      [
                      'label'=>\Yii::t('admin','Localization'),
                      'items'=>[

                        ['label' => \Yii::t('admin','Localization'),'url' => ['/administration/locale']],

                        ],
                    ],
                    [
                      'label'=>\Yii::t('admin','Global config'),
                      'items' => [

                        ['label' => \Yii::t('admin','Products'), 'url' => ['/administration/product']],
                        ['label' => \Yii::t('admin','Equipments'), 'url' => ['/administration/equipment']],
                        ['label' => \Yii::t('admin','Localities'), 'url' => ['/administration/locality']],
                        ['label' => \Yii::t('admin','Processes'), 'url' => ['/administration/process']],
                        ['label' => \Yii::t('admin','Methods'),'url' => ['/administration/methods']],
                      ],
                    ],

                    /*[
                        'label' => 'Modules',
                        'items' => [
                          [
                            'label' => 'Deviation',
                            //'url' => ['/administration/roles']

                          ],
                        ],
                    ],*/



                ],
            ]);
            NavBar::end();
        ?>
      </div>

      <div class="row">

          <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
      </div>

      <div class="row">
        <div class="col-md-2">



<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menside">
  <span class="sr-only">Toggle navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
</button>

</div>
  <?php
          //dd($this->context->id);
          if($this->context->id!='default' && $this->context->id!='locale')
          {

           echo ' <div id="menside" class="collapse in col-md-12">
              <ul class="nav  left_menu">
                <li class="active col-xs-3 col-md-12"><a  class="" href="'.\yii\helpers\BaseStringHelper::dirname(\yii\helpers\Url::canonical()).'/create"><div  class="fa fa-plus" id="menNew" ></div> </a></li>

                <li  class="col-xs-3  col-md-12"><a class=""  href="'.\yii\helpers\BaseStringHelper::dirname(\yii\helpers\Url::canonical()).'/index"><div class="fa fa-list" id="menList" ></div> </a></li>
                </ul>
            </div>';

          }
           ?>
         </div>

        <div class="col-md-10">
          <div class="container-fluid">
        <?= $content ?>
          </div>
        </div>
      </div>

    </div>



<?php $this->endContent() ?>
