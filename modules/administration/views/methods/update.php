<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Method */

$this->title = Yii::t('admin', 'Update {modelClass}: ', [
    'modelClass' => '',
]) . ' ' . $model->nm_method;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Methods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_method, 'url' => ['view', 'id' => $model->id_method]];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update');
?>
<div class="method-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
