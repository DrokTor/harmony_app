<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Method */

$this->title = Yii::t('admin', 'Create Method');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Methods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="method-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
