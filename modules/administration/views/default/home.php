<?php
/* @var $this yii\web\View */
$this->title = \Yii::t('admin','Administration home');
?>
<div class="container-fluid">

    <div class="container-fluid">

      <?= \yii\bootstrap\Alert::widget([
        'options'=>[
        'class'=>'alert-info col-md-10 text-center',
        'style'=>'margin-top:150px',
          ],
        'body'=>\Yii::t('admin','<strong>Welcome to your administration panel,</strong><br> you can choose from the menu above to proceed to the configuration.')
      ]); ?>

    </div>


</div>
