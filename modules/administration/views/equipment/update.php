<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Equipment */

$this->title = Yii::t('admin', 'Update {modelClass}: ', [
    'modelClass' => '',
]) . ' ' . $model->nm_equipment;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Equipments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_equipment, 'url' => ['view', 'id' => $model->id_equipment]];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update');
?>
<div class="equipment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
