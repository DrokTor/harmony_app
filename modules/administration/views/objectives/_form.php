<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\administration\models\AdmDevObjectives */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adm-dev-objectives-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php // $form->field($model, 'indicator')->textInput(['maxlength' => true]) ?>


    <h2><?= $model->indicator ?></h2>

    <?= $form->field($model, 'objective')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
