<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Objectives');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adm-dev-objectives-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php // Html::a(Yii::t('app', 'Create Adm Dev Objectives'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'indicator',
            'objective',

            ['class' => 'yii\grid\ActionColumn',
             'template' => '{update}'],
        ],
    ]); ?>

</div>
