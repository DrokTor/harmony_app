<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\administration\models\AdmDevObjectives */

$this->title = Yii::t('admin', 'Create objectives');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Objectives'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adm-dev-objectives-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
