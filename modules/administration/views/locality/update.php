<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Location */

$this->title = Yii::t('admin', 'Update {modelClass}: ', [
    'modelClass' => '',
]) . ' ' . $model->nm_locality;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Locations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_locality, 'url' => ['view', 'id' => $model->id_locality]];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update');
?>
<div class="location-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
