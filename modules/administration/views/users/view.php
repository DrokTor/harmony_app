<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->id_user;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('admin', 'Update'), ['update', 'id' => $model->id_user], ['class' => 'btn btn-primary']) ?>

        <?= Html::a('Profile Update', yii\helpers\Url::to('/administration/roles/assignments?user='.$model->id_user, true), ['class' => 'btn btn-success']) ?>

        <?= Html::a(Yii::t('admin', 'Reset'), ['reset', 'id' => $model->id_user], [
            'class' => 'btn btn-warning',
            'data' => [
                'confirm' => Yii::t('admin', 'Are you sure you want to reset this account?'),
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a(Yii::t('admin', 'Deactivate'), ['deactivate', 'id' => $model->id_user], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('admin', 'Are you sure you want to deactivate this account?'),
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a(Yii::t('admin', 'Delete'), ['delete', 'id' => $model->id_user], [
            'class' => 'btn btn-danger disabled',
            'data' => [
                'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_user',
            'username',
            'first_name',
            'last_name',
            'email:email',
            //'country',
            //'unit',
            'department.name',
            'position.name',
            [
              'label'=>'Profile',
              'value'=>$profile,
              'format'=>'html',
            ],
            [
              'label'=>'State',
              'value'=>$state,
              'format'=>'html',
            ]
        ],
    ]) ?>

</div>
