<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>


    <?php if($model->isNewRecord){ ?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
  <?php }else{ ?>

    <div class="row"><label class="col-md-3"><?= \Yii::t('admin','Username') ?>:</label><h4 class="col-md-6"><?= $model->username ?></h4></div>
    <div class="row"><label class="col-md-3"><?= \Yii::t('admin','First name') ?>:</label><h4 class="col-md-6"><?= $model->first_name ?></h4></div>
    <div class="row"><label class="col-md-3"><?= \Yii::t('admin','Last name') ?>:</label><h4 class="col-md-6"><?= $model->last_name ?></h4></div>


    <?php } ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'department_id')->dropDownList($departments,[  'prompt'=>'select department']) ?>

    <?= $form->field($model, 'position_id')->dropDownList($positions,[ 'prompt'=>'Select position']) ?>

    <?php /* $form->field($model, 'country')->textInput(['maxlength' => true])

      $form->field($model, 'unit')->textInput(['maxlength' => true]) */ ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
