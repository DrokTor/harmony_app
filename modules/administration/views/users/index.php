<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Users');
$this->params['breadcrumbs'][] = $this->title;

//dd(\Yii::$app->session->allFlashes);

$flash=\Yii::$app->session->allFlashes;

if(isset($flash['accountReset']))
{
  $msg=Yii::$app->session->getFlash('accountReset');
  $class='alert-warning';
}
elseif(isset($flash['accountDeactivated']))
{
  $msg=Yii::$app->session->getFlash('accountDeactivated');
  $class='alert-danger';
}
if(isset($class))
{
  echo \yii\bootstrap\Alert::widget([
   'options' => ['class' => $class],
   'body' => $msg,
]);
}

?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('admin', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id_user',
            'username',
            'first_name',
            'last_name',
            // 'email:email',
            // 'country',
            // 'unit',
            // 'department',
            // 'position',
            [
              'label'=>'Account state',
              'value'=>//'account.rState.name',
              /*rowOptions*/
              function($model)
              {
                //dd($model);
                $state=$model->account->rState->name;

                switch ($state) {
                  case 'Active':return '<span class="label label-success" >'.$state.'</span>';
                    # code...
                    break;
                  case 'Inactive':return '<span class="label label-danger" >'.$state.'</span>';return '<span class="label label-info" >'.$state.'</span>';
                    # code...
                    break;
                  case 'On hold':return '<span class="label label-primary" >'.$state.'</span>';
                    # code...
                    break;

                  default:
                    # code...
                    break;
                }
                return '<span class="label label-info" >'.$state.'</span>';
              },
              'format'=>'html',
            ],

            //['class' => 'yii\grid\ActionColumn'],
            [
              'class' => 'yii\grid\ActionColumn',
               'template' => '{view}{update}{delete} {roles}',
               'buttons'=>[
                   'roles' => function ($url, $model, $key) {
                       return  Html::a('<span class="glyphicon glyphicon-lock"></span>', yii\helpers\Url::to(['/administration/roles/assignments', 'user' => $model->id_user]) ) ;
                   },

                ],
                'contentOptions'=>['class'=>'text-center'],
            ]
        ],
    ]); ?>

</div>
