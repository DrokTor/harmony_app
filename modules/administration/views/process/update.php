<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Process */

$this->title = Yii::t('admin', 'Update {modelClass}: ', [
    'modelClass' => '',
]) . ' ' . $model->nm_process;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Processes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_process, 'url' => ['view', 'id' => $model->id_process]];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update');
?>
<div class="process-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
