<?php

use yii\db\Migration;

/**
 * Handles the creation for table `adm_departments`.
 */
class m160803_131042_create_adm_departments extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('adm_departments', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('adm_departments');
    }
}
