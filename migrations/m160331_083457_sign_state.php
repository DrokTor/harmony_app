<?php

use yii\db\Schema;
use yii\db\Migration;

class m160331_083457_sign_state extends Migration
{
    public function up()
    {
      $this->createTable('dev_sign_state',
        [
          'id_sign_state'=>$this->primaryKey(),
          'nm_sign_state'=>$this->string(10),
        ]);
    }

    public function down()
    {
        //echo "m160331_083457_sign_state cannot be reverted.\n";
        $this->dropTable('dev_sign_state');
        //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
