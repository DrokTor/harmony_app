<?php

use yii\db\Migration;

class m160815_113624_insert_data_analysis_to_auth_items extends Migration
{
    public function up()
    {
      $this->batchInsert('auth_item', ['name','type','description'],
       [['Indicators','2','indicators'],
        ['Monthly analysis','2','Monthly analysis'],
        ['Yearly analysis','2','Yearly analysis'],

      ]);


    }

    public function down()
    {
      $this->delete('auth_item', ['name' => 'Indicators']);
      $this->delete('auth_item', ['name' => 'Monthly analysis']);
      $this->delete('auth_item', ['name' => 'Yearly analysis']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
