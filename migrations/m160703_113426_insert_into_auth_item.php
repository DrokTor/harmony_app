<?php

use yii\db\Migration;

class m160703_113426_insert_into_auth_item extends Migration
{
    public function up()
    {
      $this->insert('auth_item', [
            'name' => 'capa pdf',
            'type' => '2',
            'description' => 'capa pdf',
        ]);
    }

    public function down()
    {
      $this->delete('auth_item', ['name' => 'capa pdf']);

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
