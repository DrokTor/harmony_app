<?php

use yii\db\Schema;
use yii\db\Migration;

class m160414_085627_add_closing_time extends Migration
{
    public function up()
    {
      $this->addColumn('dev_conclusion','deviation_duration',$this->smallInteger());
    }

    public function down()
    {
        echo "m160414_085627_add_closing_time cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
