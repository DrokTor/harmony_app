<?php

use yii\db\Schema;
use yii\db\Migration;

class m160602_094757_new_action_export_deviation_pdf extends Migration
{

      public function up()
      {

        $col_items=['name','type','description'];
        $items=[

          ['Deviation pdf','2','export deviation into pdf']
          ];

        $this->batchInsert('auth_item', $col_items,$items );

        $col_children=['parent','child'];
        $children=[
                    ['Administrator','Deviation pdf']
                  ];

         $this->batchInsert('auth_item_child', $col_children, $children);
      }

      public function down()
      {

          $this->delete('auth_item_child', ['parent' => 'Administrator','child' => 'Deviation pdf']);


          $this->delete('auth_item', ['name' => 'Deviation pdf']);


      }


}
