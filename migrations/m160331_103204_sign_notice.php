<?php

use yii\db\Schema;
use yii\db\Migration;

class m160331_103204_sign_notice extends Migration
{
    public function up()
    {
      $this->createTable('dev_sign_notice',[
        'id_notice'=>$this->primaryKey(),
        'nm_notice'=>$this->string(200),
      ]);

      $this->insert('dev_sign_notice',[
        'id_notice'=>'1',
        'nm_notice'=>'I have fully read all of this deviation details of every step in the process.',
      ]);
    }

    public function down()
    {
        //echo "m160331_103204_sign_notice cannot be reverted.\n";
        $this->delete('dev_sign_notice',['id_notice'=>'1']);
        $this->dropTable('dev_sign_notice');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
