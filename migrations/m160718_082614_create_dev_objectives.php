<?php

use yii\db\Migration;

/**
 * Handles the creation for table `dev_objectives`.
 */
class m160718_082614_create_dev_objectives extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('adm_dev_objectives', [
            'id' => $this->primaryKey(),
            'indicator' => $this->string(),
            'objective' => $this->integer(),
        ]);

        $this->batchInsert('adm_dev_objectives',['indicator'], [
            [ 'Number of deviations'],
            [ 'Closing rate'],
            [ 'Average closing time'],
            [ 'Number of capas'],
            [ 'Deviations overdues'],
            [ 'CAPAs overdues'],
            [ 'CAPAs implemented'],
            [ 'CAPAs efficacity review'],
            [ 'CAPAs efficacity review overdues'],

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->delete('adm_dev_objectives');
        $this->dropTable('adm_dev_objectives');
    }
}
