<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tr_plan_profiles`.
 */
class m170220_084746_create_tr_plan_profiles extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tr_plan_profiles', [
            'id' => $this->primaryKey(),
            'plan_id' => $this->integer(),
            'profile_id' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tr_plan_profiles');
    }
}
