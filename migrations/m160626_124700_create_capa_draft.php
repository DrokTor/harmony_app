<?php

use yii\db\Migration;

/**
 * Handles the creation for table `capa_draft`.
 */
class m160626_124700_create_capa_draft extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('capa_draft', [
            'id' => $this->primaryKey(),
            'deviation_id' => $this->integer(),
            'title' => $this->string(),
            'description' => $this->string(),
            'type_id' => $this->integer(),
            'implementation_date' => $this->date(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('capa_draft');
    }
}
