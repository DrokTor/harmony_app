<?php

use yii\db\Schema;
use yii\db\Migration;

class m160502_092830_insert_capa_type_table extends Migration
{
    public function up()
    {
      $this->createTable('capa_type',[
        'id'=>$this->primaryKey(),
        'name'=>$this->string()
      ]);

      $col=['name'];
      $data=[['Corrective action'],['Preventive action']];

      $this->batchInsert('capa_type',$col,$data );
    }

    public function down()
    {
      $this->dropTable('capa_type');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
