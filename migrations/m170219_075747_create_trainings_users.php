<?php

use yii\db\Migration;

/**
 * Handles the creation for table `trainings_users`.
 */
class m170219_075747_create_trainings_users extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tr_trained_users', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'training_id' => $this->integer(),
            'start_date'=> $this->date(),
            'end_date'=> $this->date(),
            'evaluation'=> $this->string(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tr_trained_users');
    }
}
