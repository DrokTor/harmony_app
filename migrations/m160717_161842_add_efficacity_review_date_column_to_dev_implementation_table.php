<?php

use yii\db\Migration;

/**
 * Handles adding efficacity_review_date_column to table `dev_implementation_table`.
 */
class m160717_161842_add_efficacity_review_date_column_to_dev_implementation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('capa_implementation', 'efficacity_review_date', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('capa_implementation', 'efficacity_review_date');
    }
}
