<?php

use yii\db\Schema;
use yii\db\Migration;

class m160426_095326_create_capa_table extends Migration
{
    public function up()
    {
      $this->createTable('capa_declaration',[
        'id'=>$this->primaryKey(),
        'deviation_id'=>$this->integer(),
        'title'=>$this->string(200),
        'description'=>$this->text(),
        'type_id'=>$this->integer(),
        'user_id'=>$this->integer(),
        'implementation_date'=>$this->date(),
      ]);
    }

    public function down()
    {
        /*echo "m160426_095326_create_capa_table cannot be reverted.\n";

        return false;*/
        $this->dropTable('capa_declaration');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
