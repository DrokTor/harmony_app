<?php

use yii\db\Migration;

class m160628_102530_insert_into_auth_item extends Migration
{
    public function up()
    {
      $this->insert('auth_item', [
            'name' => 'capa draft',
            'type' => '2',
            'description' => 'capa draft',
        ]);
    }

    public function down()
    {
      $this->delete('auth_item', ['name' => 'capa draft']);
    }


}
