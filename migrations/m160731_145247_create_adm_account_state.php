<?php

use yii\db\Migration;

/**
 * Handles the creation for table `adm_account_state`.
 */
class m160731_145247_create_adm_account_state extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('adm_account_state', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);

        $this->batchInsert('adm_account_state',['name'],

          [
            ['name'=>'Active'],
            ['name'=>'Inactive'],
            ['name'=>'On hold'],
          ]
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('adm_account_state');
    }
}
