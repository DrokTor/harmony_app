<?php

use yii\db\Migration;

/**
 * Handles the creation for table `training_table`.
 */
class m170216_101236_create_training_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tr_training', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tr_training');
    }
}
