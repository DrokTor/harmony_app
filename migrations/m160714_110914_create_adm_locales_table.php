<?php

use yii\db\Migration;

/**
 * Handles the creation for table `adm_locales_table`.
 * Has foreign keys to the tables:
 *
 * - `adm_languages`
 * - `adm_countries`
 */
class m160714_110914_create_adm_locales_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('adm_locales_table', [
            'id' => $this->primaryKey(),
            'language' => $this->string(),
            'country' => $this->string(),
        ]);


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('adm_locales_table');
    }
}
