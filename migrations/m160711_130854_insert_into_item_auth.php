<?php

use yii\db\Migration;

class m160711_130854_insert_into_item_auth extends Migration
{
    public function up()
    {
      $this->insert('auth_item', [
            'name' => 'list drafts',
            'type' => '2',
            'description' => 'list drafts',
        ]);
    }

    public function down()
    {
      $this->delete('auth_item', ['name' => 'list drafts']);

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
