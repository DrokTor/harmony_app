<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tr_profiles_trainings_table`.
 */
class m170216_130058_create_tr_profiles_trainings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tr_profiles_trainings', [
            'id' => $this->primaryKey(),
            'profile_id' => $this->integer()->notNull(),
            'training_id' => $this->integer()->notNull(),
        ]);

      }
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tr_profiles_trainings_table');
    }
}
