<?php

use yii\db\Schema;
use yii\db\Migration;

class m160505_091806_add_number_to_capa_efficacity_table extends Migration
{
    public function up()
    {
      $this->addColumn('capa_efficacity','detail',$this->string());

    }

    public function down()
    {
      $this->dropColumn('capa_efficacity','number');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
