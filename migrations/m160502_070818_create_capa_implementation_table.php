<?php

use yii\db\Schema;
use yii\db\Migration;

class m160502_070818_create_capa_implementation_table extends Migration
{
    public function up()
    {
      $this->createTable('capa_implementation',[
        'id'=>$this->primaryKey(),
        'capa_id'=>$this->integer(),
        'implementation'=>$this->string(),
        'implementation_date'=>$this->date()
      ]);
    }

    public function down()
    {
        /*echo "m160502_070818_create_capa_implementation_table cannot be reverted.\n";

        return false;*/
        $this->dropTable('capa_implementation');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
