<?php

use yii\db\Schema;
use yii\db\Migration;

class m160503_120546_insert_auth_item_table extends Migration
{
    public function up()
    {

      $col_items=['name','type','description'];
      $items=[
        //['Create capa','2','Create a Capa'],
        //['List capas','2','List of Capas'],
        //['View capa','2','View a Capa'],
        ['Capa efficacity','2','Capa efficacity'],
        ['Capa implementation','2','Capa implementation']
        ];

      $this->batchInsert('auth_item', $col_items,$items );

      $col_children=['parent','child'];
      $children=[//['Administrator','Create capa'],
                  //['Administrator','List capas'],
                  //['Administrator','View capa'],
                  ['Administrator','Capa efficacity'],
                  ['Administrator','Capa implementation']
                ];

       $this->batchInsert('auth_item_child', $col_children, $children);
    }

    public function down()
    {
        /*echo "m160502_084442_insert_auth_item_table cannot be reverted.\n";

        return false;*/

        //$this->delete('auth_item_child', ['parent' => 'Administrator','child' => 'Create capa']);
        //$this->delete('auth_item_child', ['parent' => 'Administrator','child' => 'List capas']);
        //$this->delete('auth_item_child', ['parent' => 'Administrator','child' => 'View capa']);
        $this->delete('auth_item_child', ['parent' => 'Administrator','child' => 'Capa implementation']);
        $this->delete('auth_item_child', ['parent' => 'Administrator','child' => 'Capa efficacity']);

        //$this->delete('auth_item', ['name' => 'Create capa']);
        //$this->delete('auth_item', ['name' => 'List capas']);
        //$this->delete('auth_item', ['name' => 'View capa']);
        $this->delete('auth_item', ['name' => 'Capa implementation']);
        $this->delete('auth_item', ['name' => 'Capa efficacity']);


    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
