<?php

use yii\db\Schema;
use yii\db\Migration;

class m160328_141357_dev_signatories extends Migration
{
    public function up()
    {
      $this->createTable('dev_signatories',
              ['id_signature'=>$this->primaryKey(),
                'id_conclusion'=>$this->smallInteger(),
                'id_user'=>$this->smallInteger(),
                'id_notice'=>$this->smallInteger(),
                'comment'=>$this->string(500),

              ]);
    }

    public function down()
    {
        //echo "m160328_141357_dev_signatories cannot be reverted.\n";
        $this->dropTable('dev_signatories');
        //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
