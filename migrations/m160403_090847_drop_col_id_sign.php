<?php

use yii\db\Schema;
use yii\db\Migration;

class m160403_090847_drop_col_id_sign extends Migration
{
    public function up()
    {
      $this->dropColumn('dev_conclusion','id_signature');
    }

    public function down()
    {
        echo "m160403_090847_drop_col_id_sign cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
