<?php

use yii\db\Migration;

/**
 * Handles the creation for table `languages_table`.
 */
class m160714_105523_create_languages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('adm_languages_table', [
            'id' => $this->primaryKey(),
            'code'=> $this->string()->notNull()->unique(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('adm_languages_table');
    }
}
