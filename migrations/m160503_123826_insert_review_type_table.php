<?php

use yii\db\Schema;
use yii\db\Migration;

class m160503_123826_insert_review_type_table extends Migration
{
    public function up()
    {
      $this->batchInsert('capa_review_type', ['name'], [['Time period'],['Number of batches']]);
    }

    public function down()
    {
      $this->delete('capa_review_type', ['name' => 'Time period']);
      $this->delete('capa_review_type', ['name' => 'Number of batches']);

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
