<?php

use yii\db\Schema;
use yii\db\Migration;

class m160328_154130_signatory_id_deviation extends Migration
{
    public function up()
    {
      $this->addColumn('dev_signatories','id_deviation',$this->smallInteger()); //->after('id_signture')
    }

    public function down()
    {
        echo "m160328_154130_signatory_id_deviation cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
