<?php

use yii\db\Migration;

/**
 * Handles the creation for table `dev_investigation_ishikawa`.
 */
class m160623_100826_create_dev_investigation_ishikawa extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('dev_investigation_ishikawa', [
            'id' => $this->primaryKey(),
            'id_investigation' => $this->integer(),
            'id_ishikawa' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('dev_investigation_ishikawa');
    }
}
