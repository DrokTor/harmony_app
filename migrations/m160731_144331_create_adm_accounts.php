<?php

use yii\db\Migration;

/**
 * Handles the creation for table `adm_accounts`.
 */
class m160731_144331_create_adm_accounts extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('adm_accounts', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'state' => $this->integer(),
            'token' => $this->string(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('adm_accounts');
    }
}
