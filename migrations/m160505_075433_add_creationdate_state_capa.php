<?php

use yii\db\Schema;
use yii\db\Migration;

class m160505_075433_add_creationdate_state_capa extends Migration
{
    public function up()
    {
      $this->addColumn('capa_declaration','creation_date',$this->date());
      $this->addColumn('capa_declaration','state',$this->integer());
    }

    public function down()
    {
        /*echo "m160505_075433_add_creationdate_state_capa cannot be reverted.\n";

        return false;*/
        $this->dropColumn('capa_declaration','creation_date');
        $this->dropColumn('capa_declaration','state');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
