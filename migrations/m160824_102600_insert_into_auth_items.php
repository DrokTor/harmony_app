<?php

use yii\db\Migration;

class m160824_102600_insert_into_auth_items extends Migration
{
    public function up()
    {
      $this->insert('auth_item',['name'=>'Finish investigation','type'=>'2','description'=>'Can finish investigation']);
    }

    public function down()
    {
      $this->delete('auth_item', ['name'=>'Finish investigation']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
