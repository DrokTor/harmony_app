<?php

use yii\db\Migration;

/**
 * Handles the creation for table `docs_locked`.
 */
class m170312_122959_create_docs_locked extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('docs_locked', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'name' => $this->string(),
            'path' => $this->string(),
            'user_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('docs_locked');
    }
}
