<?php

use yii\db\Schema;
use yii\db\Migration;

class m160404_064324_add_newstate extends Migration
{
    public function up()
    {
      $this->insert('dev_state', ['nm_state'=>'Signing']);
    }

    public function down()
    {
        echo "m160404_064324_add_newstate cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
