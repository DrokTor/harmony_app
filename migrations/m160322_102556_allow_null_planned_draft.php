<?php

use yii\db\Schema;
use yii\db\Migration;

class m160322_102556_allow_null_planned_draft extends Migration
{
  public function up()
  {
    $this->alterColumn('dev_declaration_draft','id_planned',$this->smallInteger(2));
  }

  public function down()
  {
      //echo "m160322_101034_allow_null_planned cannot be reverted.\n";
      $this->alterColumn('dev_declaration_draft','id_planned',$this->notNull());
      //return false;
  }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
