<?php

use yii\db\Schema;
use yii\db\Migration;

class m160329_150242_sign_state extends Migration
{
    public function up()
    {
      $this->addColumn('dev_signatories','state', $this->smallInteger(1) );
    }

    public function down()
    {
        echo "m160329_150242_sign_state cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
