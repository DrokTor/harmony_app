<?php

use yii\db\Migration;

/**
 * Handles dropping id_ishikawa from table `dev_investigation`.
 */
class m160623_155507_drop_id_ishikawa_from_dev_investigation extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('dev_investigation', 'id_ishikawa');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('dev_investigation', 'id_ishikawa', $this->integer());
    }
}
