<?php

use yii\db\Migration;

/**
 * Handles adding id_deviation to table `dev_investigation_ishikawa`.
 */
class m160626_163557_add_id_deviation_to_dev_investigation_ishikawa extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('dev_investigation_ishikawa', 'id_deviation', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('dev_investigation_ishikawa', 'id_deviation');
    }
}
