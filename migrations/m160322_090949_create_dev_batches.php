<?php

use yii\db\Schema;
use yii\db\Migration;

class m160322_090949_create_dev_batches extends Migration
{
    public function up()
    {
      $this->createTable('dev_batches',
      ['id_relation'=>$this->primaryKey(),
        'id_deviation'=>$this->integer(),
        'id_batch'=>$this->string(100)
        ]);
      //$this->dropColumn('dev_declaration','batch_number');
    }

    public function down()
    {
        //echo "m160322_090949_create_dev_batches cannot be reverted.\n";
        $this->dropTable('dev_batches');
        //$this->addColumn('dev_declaration','batch_number',$this->varchar(50));
        //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
