<?php

use yii\db\Migration;

/**
 * Handles the creation for table `adm_countries_table`.
 */
class m160714_105913_create_adm_countries_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('adm_countries_table', [
            'id' => $this->primaryKey(),
            'code'=> $this->string()->notNull()->unique(),
            'nom' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('adm_countries_table');
    }
}
