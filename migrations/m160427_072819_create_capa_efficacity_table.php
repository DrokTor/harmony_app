<?php

use yii\db\Schema;
use yii\db\Migration;

class m160427_072819_create_capa_efficacity_table extends Migration
{
    public function up()
    {

      $this->createTable('capa_efficacity',
        [
          'id'=>$this->primaryKey(),
          'capa_id'=>$this->integer(),
          'efficacity_review'=>$this->text(),
          'review_type'=>$this->integer(),
          'efficacity_conclusion'=>$this->text(),

        ]);
    }

    public function down()
    {
        /*echo "m160427_072819_create_capa_efficacity_table cannot be reverted.\n";

        return false;*/
        $this->dropTable('capa_efficacity');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
