<?php

use yii\db\Schema;
use yii\db\Migration;

class m160323_114331_add_decision_batches extends Migration
{
    public function up()
    {
      $this->addColumn('dev_batches','id_decision',$this->smallInteger(4));
    }

    public function down()
    {
        echo "m160323_114331_add_decision_batches cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
