<?php

use yii\db\Migration;

/**
 * Handles the creation for table `adm_positions`.
 */
class m160803_131058_create_adm_positions extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('adm_positions', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('adm_positions');
    }
}
