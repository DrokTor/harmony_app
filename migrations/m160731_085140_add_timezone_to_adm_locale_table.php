<?php

use yii\db\Migration;

/**
 * Handles adding timezone to table `adm_locale_table`.
 */
class m160731_085140_add_timezone_to_adm_locale_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('adm_locales_table', 'timezone', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('adm_locales_table', 'timezone');
    }
}
