<?php

use yii\db\Schema;
use yii\db\Migration;

class m160323_132853_drop_id_decision_conclusion extends Migration
{
    public function up()
    {
      $this->dropColumn('dev_conclusion','id_decision');
    }

    public function down()
    {
        echo "m160323_132853_drop_id_decision_conclusion cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
