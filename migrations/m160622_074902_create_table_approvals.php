<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_approvals`.
 */
class m160622_074902_create_table_approvals extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('dev_approvals', [
            'id_approval' => $this->primaryKey(),
            'deviation_id' => $this->integer(),
            'manager' => $this->integer(),
            'approvalDate' => $this->dateTime(),
            'expert' => $this->integer(),
            'expertiseDate' => $this->dateTime(),
            'investigator'=> $this->integer(),
            'investigationDate' => $this->dateTime(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('dev_approvals');
    }
}
