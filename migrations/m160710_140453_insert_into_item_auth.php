<?php

use yii\db\Migration;

class m160710_140453_insert_into_item_auth extends Migration
{
    public function up()
    {
      $this->insert('auth_item', [
            'name' => 'index',
            'type' => '2',
            'description' => 'index',
        ]);
    }

    public function down()
    {
      $this->delete('auth_item', ['name' => 'index']);

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
