<?php

use yii\db\Schema;
use yii\db\Migration;

class m160505_080843_create_capa_state_table extends Migration
{
    public function up()
    {
      $this->createTable('capa_state',[
        'id'=>$this->primaryKey(),
        'name'=>$this->string()
      ]);

      $this->batchInsert('capa_state',['name'],[['Planning'],['Implementation'],['Efficacity review']]);
    }

    public function down()
    {
      $this->dropTable('capa_state');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
