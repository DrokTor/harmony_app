<?php

use yii\db\Migration;

/**
 * Handles the creation for table `dev_monthly_closing_table`.
 */
class m160717_141733_create_dev_monthly_closing_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('dev_monthly_closing', [
            'id' => $this->primaryKey(),
            'date' => $this->date(),
            'closing_rate' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('dev_monthly_closing');
    }
}
