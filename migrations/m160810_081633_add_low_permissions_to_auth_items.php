<?php

use yii\db\Migration;

/**
 * Handles adding low_permissions to table `auth_items`.
 */
class m160810_081633_add_low_permissions_to_auth_items extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

      $this->batchInsert('auth_item', ['name','type','description'],
       [['deviations excel','2','deviations excel'],
        ['deviations filter','2','deviations filter'],

      ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
      $this->delete('auth_item', ['name' => 'deviations excel']);
      $this->delete('auth_item', ['name' => 'deviations filter']);
    }
}
