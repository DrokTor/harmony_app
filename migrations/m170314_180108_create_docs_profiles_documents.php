<?php

use yii\db\Migration;

/**
 * Handles the creation for table `docs_profiles_documents`.
 */
class m170314_180108_create_docs_profiles_documents extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('docs_profiles_documents', [
            'id' => $this->primaryKey(),
            'path' => $this->string(),
            'profile_id' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('docs_profiles_documents');
    }
}
