<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tr_planning`.
 */
class m170219_142108_create_tr_planning extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tr_planning', [
            'id' => $this->primaryKey(),
            'start_date' => $this->date(),
            'end_date' => $this->date(),
            'training_id' => $this->integer(),
            'description' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tr_planning');
    }
}
