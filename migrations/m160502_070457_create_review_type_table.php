<?php

use yii\db\Schema;
use yii\db\Migration;

class m160502_070457_create_review_type_table extends Migration
{
    public function up()
    {
      $this->createTable('capa_review_type',[
        'id'=>$this->primaryKey(),
        'name'=>$this->string()

      ]);
    }

    public function down()
    {
        // echo "m160502_070457_create_review_type_table cannot be reverted.\n";
        //
        // return false;
        $this->dropTable('capa_review_type');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
