<?php

namespace app\models;


use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\modules\deviation\models\DevDeviation;
use app\modules\administration\models\Department;
use app\modules\administration\models\Position;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    /*public $id_user;
    public $username;
    public $password;
     public $authKey;
    public $accessToken;

  private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];*/

    public $password_repeat;
    const SCENARIO_LOGIN = 'login';
    //const SCENARIO_DEFAULT = 'default';
    const SCENARIO_RESET = 'reset';

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_LOGIN] = ['username', 'password'];
        $scenarios[self::SCENARIO_DEFAULT] = ['username','first_name','last_name', 'email','department_id','position_id'];
        $scenarios[self::SCENARIO_RESET] = [ 'password','password_repeat'];

        return $scenarios;
    }

    public static function tableName()
    {
        return 'adm_users';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
      return [
            [['username','first_name','last_name','email','department_id','position_id'],'required', 'on' => self::SCENARIO_DEFAULT],
            ['username','unique', 'on' => self::SCENARIO_DEFAULT],
            [['username','password'],'required', 'on' => self::SCENARIO_LOGIN],
            [['password'],'required', 'on' => self::SCENARIO_RESET],
            //[['password'],'compare', 'on' => self::SCENARIO_RESET],
            [['password'],'match', 'pattern' =>'$\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*$', 'on' => self::SCENARIO_RESET],
            [['password_repeat'], 'required', 'on' => self::SCENARIO_RESET],
            ['password_repeat', 'compare', 'compareAttribute'=>'password','message'=>'Passwords don\'t match !', 'on' => self::SCENARIO_RESET],
        ];
    }
    public function beforeSave($insert)
    {
       if(isset($this->password) && $this->password!='')
        $this->password = \Yii::$app->getSecurity()->generatePasswordHash($this->password);

        //dd($this->password);
        return parent::beforeSave($insert);
    }
    public static function findIdentity($id)
    {
        //return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
        return static::findOne($id);
        //return static::findOne(['username'=>$username,'password'=>$password]);

    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        /*foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;*/
      return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        /*foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;*/
      //$user = User::find()->where(['username' => $username])->one();//I don't know if this is correct i am   //checing value 'becky' in username column of my user table.
   // echo "<script>alert('blabla');</script>";
     return static::findOne(['username'=>$username]);
         //return static::find()->asArray()->where(['username'=>$username])->one();
       // return $user;





    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id_user;
    }

    public function isAdmin()
    {
      $isAdmin=\Yii::$app->authManager;
      $isAdmin=$isAdmin->getAssignment('administrator',$this->getId());

        return $isAdmin;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {

       // $new=static::findOne(['username'=>$user]);


       return \Yii::$app->getSecurity()->validatePassword($password, $this->password);
        //return $this->password === $password;
    }

    public static  function  get_user(){

	      $usr = user::find()->all();

	      $usr = ArrayHelper::map($usr, 'id_user', 'username');

	      return $usr;

	}
    public function getDevDeviation()
    {
        return $this->hasMany(getDevDeviation::className(), ['creator' => 'id_user']);
    }

    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['user_id' => 'id_user']);
    }
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['id' => 'position_id']);
    }
}
