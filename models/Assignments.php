<?php
namespace app\models;

use yii\db\ActiveRecord;
use app\modules\deviation\models\DevActions;

class Assignments extends ActiveRecord
{
  public static function tableName()
  {
    return 'auth_assignment';//'dev_profiles_actions';
  }

  public function getUser()
  {
    return $this->hasOne(User::className(),['id_user'=>'user_id']);
  }
  public function getProfiles()
  {
    return $this->hasOne(Profiles::className(),['id_profile'=>'id_profile']);
  }
  public function getActions()
  {
    return $this->hasOne(DevActions::className(),['id_action'=>'id_action']);
  }
}
 ?>
