<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dev_conclusion".
 *
 * @property integer $id_conclusion
 * @property integer $id_deviation
 * @property integer $id_decision
 * @property string $comment
 * @property string $final_decision
 * @property integer $id_signature
 */
class DevConclusion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dev_conclusion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_deviation',  'comment', 'final_decision', ], 'required'],
            [['id_deviation',  ], 'integer'],
            [['comment', 'final_decision'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_conclusion' => 'Id Conclusion',
            'id_deviation' => 'Id Deviation',
 
            'comment' => 'Comment',
            'final_decision' => 'Final Decision',
        ];
    }
}
