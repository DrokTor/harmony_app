<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dev_method".
 *
 * @property integer $id_method
 * @property string $nm_method
 * @property string $desc_method
 */
class Method extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dev_method';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nm_method'], 'required'],
            [['desc_method'], 'string'],
            [['nm_method'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_method' => Yii::t('app', 'ID Method'),
            'nm_method' => Yii::t('app', 'Method'),
            'desc_method' => Yii::t('app', 'Description'),
        ];
    }
}
