<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\models\AccountState;
use app\models\User;



class Account extends ActiveRecord
{

    const ACCOUNT_ACTIVE='1';
    const ACCOUNT_INACTIVE='2';
    const ACCOUNT_ON_HOLD='3';
    
    public static function tableName()
    {
        return 'adm_accounts';
    }
    /**
     * @inheritdoc
     */

    public function getRState()
    {
        return $this->hasOne(AccountState::className(), ['id' => 'state']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'user_id']);
    }
}
