<?php

$params = require(__DIR__ . '/params.php');
$beforeAction = require(__DIR__ . '/beforeAction.php');
//$language = require(__DIR__ . '/language.php');
use app\modules\administration\models\AdmLocale;


 $config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'on beforeRequest' => function ($event) {
        $locales=AdmLocale::find()->with('rLanguage')->all();//,'rCountry'
          //dd($locales[0]['rLanguage']['code']);
          Yii::$app->language=(!empty($locales[0]->language))?$locales[0]->rLanguage->code:"1";
          Yii::$app->timezone=(!empty($locales[0]->timezone))?$locales[0]->timezone:"Europe/Amsterdam";
          //dd();
      },
    'on beforeAction' => function($action){ beforeAction($action) ;},
    'components' => [

        'formatter' => [
        'class' => 'yii\i18n\Formatter',
        'nullDisplay' =>'',
        ],
        /**/'authManager' => [
            'class' => 'yii\rbac\DbManager',
          ],
        'urlManager' => [
          'showScriptName' => false,
          'enablePrettyUrl' => true
                  ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'mJyqCYdCJh97i6g0R9IAAoR9jEAvuleN',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'session' => [
            'class' => 'yii\web\DbSession',
            'writeCallback' => function ($session) {

                return [
                   'user_id' => Yii::$app->user->id,
                   'last_write' => date('Y-m-d H:i:s'),
                ];
            },
            'timeout'=>'5600', // remove the 5 :)
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            //'enableAutoLogin' => true,
            'authTimeout'=>'5600', // remove the 5 :)
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            //'useFileTransport' => false,
            'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => 'smtp.gmail.com',//'smtp-mail.outlook.com',
            'username' => 'optidevdz@gmail.com',
            'password' => '***REMOVED***',
            'port' => '587',//  '587',
            'encryption' => 'tls',
                        ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'logFile'=>'@app/harmony/logs/harmony.log',
                    //'logFile'=>'@runtime/logs/harmony.log',
                     'maxFileSize' => '5120',
                     'enableRotation' => true,
                     'categories'=>['user'],
                     //'except'=>['yii\db\*','yii\db\Connection','yii\web\Session::open'],
                     'logVars'=>[],
                     'prefix' => function ($message) {
                         $user = Yii::$app->has('user', true) ? Yii::$app->get('user') : null;
                         $userID = $user ? $user->getId(false) : '-';
                         $username = $user ? $user->identity->username : '-';
                         return "[$userID][$username]";
                     }
                  ]
                    ],

                ],
        'db' => require(__DIR__ . '/db.php'),
        'view' => [
            'class' => 'yii\web\View',
            'renderers' => [
                'twig' => [
                    'class' => 'yii\twig\ViewRenderer',
                    'cachePath' => '@runtime/Twig/cache',
                    // Array of twig options:
                    'options' => [
                        'auto_reload' => true,
                    ],
                    'globals' => ['html' => '\yii\helpers\Html'],
                    'uses' => ['yii\bootstrap'],
                  ],
                  // ...
              ],
            ],

          //'language'=>$language,
          //'timezone'=>$timezone,

          'i18n' => [
          'translations' => [
              'app*' => [
                  'class' => 'yii\i18n\PhpMessageSource',
                  //'basePath' => '@app/messages',
                  //'sourceLanguage' => 'en-US',
                  'fileMap' => [
                      'app' => 'app.php',
                      'admin' => 'admin.php',
                      'app/error' => 'error.php',
                              ],
                          ],
              'admin*' => [
                  'class' => 'yii\i18n\PhpMessageSource',
                  //'basePath' => '@app/messages',
                  //'sourceLanguage' => 'en-US',
                  'fileMap' => [
                      'admin' => 'admin.php',
                      'app/error' => 'error.php',
                              ],
                          ],
                      ],
                  ],

    ],
    'modules' => [
        'gridview' =>  [
        'class' => '\kartik\grid\Module',
        // enter optional module parameters below - only if you need to
        // use your own export download action or custom translation
        // message source
         'downloadAction' => 'gridview/export/download',
        // 'i18n' => []
        ],
        'deviation' => [
            'class' => 'app\modules\deviation\Deviation',
            // ... other configurations for the module ...
            /*'components'=>[
            'assetManager' => [
             'class' => 'app\modules\deviation\Deviation',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,   // do not publish the bundle
                    'js' => [
                        '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js',
                            ]
                                        ],
                          ],
                        ],
                    ],*/
          ],
          'capa'=>[
            'class'=> 'app\modules\capa\Capa',
          ],
          'administration'=>[
            'class'=> 'app\modules\administration\Administration',
          ],
          'training'=>[
            'class'=> 'app\modules\training\Training',
          ],
          'documents'=>[
            'class'=> 'app\modules\documents\Documents',
          ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
