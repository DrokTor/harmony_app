<?php

use app\modules\deviation\models\DevDeviation;
use app\modules\deviation\models\CapaDraft;
use app\modules\deviation\models\DevDraft;
use yii\web\View;
use yii\base\Event;

function beforeaction($action) {
  //dd($action->action->controller);
   
      $controller=$action->action->controller;
      $act= $action->action->id;

      //dd($controller->id);
      //dd($act);
      if(($controller->id=="site" && $act=="login") || ($controller->id=="account" && $act=="activate") || $act=="not-allowed")
      {
        return true;
      }
      elseif(! \yii::$app->user->isGuest )
      {
        $isAdmin=\Yii::$app->authManager;
        $isAdmin=$isAdmin->getAssignment('administrator',\yii::$app->user->getId());

        if($controller->module->id=='deviation')
        {

        Event::on(View::className(), View::EVENT_BEFORE_RENDER, function() use ($isAdmin) {
        $userId=  Yii::$app->user->identity->id_user;
        $draftsWhere=(!$isAdmin)?['creator'=>$userId]:'1';
        //$capadraftsWhere=(!$isAdmin)?['user_id'=>$userId]:'1';
        //dd($isAdmin);
        $inv=DevDeviation::find()->where(['stateIn'=>DECLARATION])->count();
        $clsd=DevDeviation::find()->where(['stateIn'=>INVESTIGATION])->count();
        //$drft=DevDeviation::find()->where(['stateIn'=>DRAFT,'creator'=>$userId])->count();
        $drft=DevDraft::find()->where($draftsWhere)->count();//->where(['creator'=>$userId])
        $capadrft=CapaDraft::find()->where(1)->count();//->where(['creator'=>$userId])
//print_r($drft);exit();
        Yii::$app->view->params['draft'] = $drft;
        Yii::$app->view->params['capadraft'] = $capadrft;
        Yii::$app->view->params['investigate'] = $inv;
        Yii::$app->view->params['close'] = $clsd;
      });
      }

      $act=str_replace('-',' ',$act);
      $auth = \Yii::$app->authManager;
      $id=Yii::$app->user->getId();
      $allow= \Yii::$app->user->can($act);
        //dd($isAdmin);

      if ((! $allow && $isAdmin==null) || ($controller->module->id=='administration' && $isAdmin==null))
      {

        //throw  new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
        //dd($controller->getModules());
        //dd(\Yii::$app->request->isAjax);
        if(\Yii::$app->request->isAjax)
        {
          $notallowed=\yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-danger text-center',
                'style'=>'margin-top:100px',
            ],
            'body' => 'You do not have the access level to view this content.',
            ]);
          echo $notallowed;exit(1);
        }
        else
        {
          //dd($controller);
          if($controller->module->id=='administration' || $controller->id=='site')
          {
            return $controller->redirect('/site/not-allowed');
          }
          else
          {
            return $controller->redirect('not-allowed');
          }

        }

          }
      else//if()
      {//dd($allow);
        return true;
      }


      }
      else{//dd($controller->redirect(['/site/login']));
        //if(!\Yii::$app->user->identity)
        //dd(Yii::$app->getResponse()->redirect('/site/login')->send());
        \Yii::$app->getResponse()->redirect('/site/login')->send();
        exit(1);
        //return;
        //return $controller->redirect(['/site/login']);
      }
  }
