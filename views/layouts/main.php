<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;


/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/images/harmony_icon.png" type="image/x-icon" />
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div id="myWrap" class="container-fluid">
        <?php
            NavBar::begin([
                'brandLabel' => '<img class="col-md-7 img-responsive" src="'.Yii::getAlias('@web').'/images/logo.png" />',
                'brandUrl' => Yii::$app->homeUrl,
                'brandOptions' => ['id'=>'harmony_logo'],
                'options' => [
                  'id'=> 'main_bar',
                  'class' => 'navbar-inverse row ',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                  Yii::$app->user->isGuest ?'':
                    ['label' => 'Modules',
                      'items'=>
                      [
                        ['label' => \Yii::t('app','Deviation'), 'url' => ['/deviation/data/indicators']],
                        (Yii::$app->user->identity->isAdmin())?['label' => 'Administration', 'url' => ['/administration']]:'',
                      ]
                    ],
                    Yii::$app->user->isGuest ?
                        ['label' => \Yii::t('app','Login'), 'url' => ['/site/login']] :
                        '',
                    Yii::$app->user->isGuest ?
                        '':
                        ['label' => ''.\Yii::t('app','Account').' (' . Yii::$app->user->identity->username . ')',
                            //'url' => ['/site/logout'],
                            //'linkOptions' => ['data-method' => 'post'],
                            'items'=>
                            [
                              //['label' => 'Update account', 'url' => ['/users/update']],
                              //['label' => 'Change password', 'url' => ['/users/update']],
                              ['label' => \Yii::t('app','Logout'), 'url' => ['/site/login'],
                              'url' => ['/site/logout'],
                              'linkOptions' => ['data-method' => 'post'],
                            ],
                            ],
                          ] ,

                ],
            ]);/**/
            NavBar::end();
        ?>




            <?= $content ?>


    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; HarmonyQMS <?= date('Y') ?></p>

        </div>
    </footer>

<?php $this->endBody() ?>
</body>
<script src="http://malsup.github.com/jquery.form.js"></script>
 

</html>
<?php $this->endPage() ?>
