<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="login-div" class="site-login center-block no-float col-xs-6">

    <?php  echo isset($msg)?  '<div class="row">'.$msg.'</div>':'' ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to login:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'action'=>'/site/login',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-10\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-10 control-label text-left '],
        ],
    ]); ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?php /* $form->field($model, 'rememberMe', ["options"=>["class"=>"col-xs-12 row"],
        'template' => "<div class=\"col-lg-offset-10 col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
    ])->checkbox() */?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-9">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary  pull-right', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
<!---
    <div class="col-lg-offset-3" style="color:#999;">
        You may login with <strong>admin/admin</strong> or <strong>demo/demo</strong>.<br>
        To modify the username/password, please check out the code <code>app\models\User::$users</code>.
    </div>---->
</div>
