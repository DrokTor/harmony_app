<?php
/* @var $this yii\web\View */
$this->title = 'Modules';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= \Yii::t('app','Welcome to Harmony') ?></h1>

        <p class="lead"><?= \Yii::t('app','Your quality management system.' )?></p>


    </div>

    <div class="body-content col-xs-12 no-float">

        <div class="row">
            <div class="col-lg-12">
                <h2><?= \Yii::t('app','Select a module') ?></h2>



                <p class="modules col-md-3"><a class="btn btn-default" href="./deviation/data/"> <?= \Yii::t('app','Deviation management')?>  </a></p> <!--&raquo;-->
                <p class="modules col-md-3"><a class="btn btn-default" href="./training/training/"> <?= \Yii::t('app','Training management')?>  </a></p>
                <p class="modules col-md-3"><a class="btn btn-default" href="./documents/documents/"> <?= \Yii::t('app','Documents management')?>  </a></p>
                <p class="modules col-md-3"><a class="btn btn-default" href="./administration/">  Administration   </a></p>
            </div>

        </div>

    </div>
</div>
