<?php

echo \yii\bootstrap\Alert::widget([
    'options' => [
        'class' => 'alert-danger text-center col-md-6 col-md-offset-3',
        'style'=>'margin-top:150px',
    ],
    'body' => \Yii::t('app','<strong>[ ACCESS RESTRICTED ]</strong><br>You do not have the access level to view this content.'),
]);

 ?>
