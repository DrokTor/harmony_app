<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = \Yii::t('app','Activate your account');//\Yii::t('adm','Activate your account');
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="login-div" class="site-login center-block no-float col-xs-6">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Enter your password to activate your account.</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-10\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-10 control-label text-left '],
        ],
    ]); ?>


    <h3>Username:</h3> <?= $model->username ?>
    <h3>Name:</h3> <?= $model->first_name.', '.$model->last_name ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'password_repeat')->passwordInput() ?>



    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-9">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary  pull-right', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
<!---
    <div class="col-lg-offset-3" style="color:#999;">
        You may login with <strong>admin/admin</strong> or <strong>demo/demo</strong>.<br>
        To modify the username/password, please check out the code <code>app\models\User::$users</code>.
    </div>---->
</div>
