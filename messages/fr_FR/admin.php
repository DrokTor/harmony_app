<?php

  return [

    'Account management'=>'Gestion de compte',
    'Roles'=>'Roles',
    'Users'=>'Utilisateurs',
    'Users profiles'=>'Profils utilisateurs',
    'Organization'=>'Organisation',
    'Departments'=>'Départements',
    'Positions'=>'Postes',
    'Tracking'=>'Suivi',
    'Logs'=>'Historique',
    'Objectives'=>'Objectifs',
    'Localization'=>'Localisation',
    'Global config'=>'Configuration générale',


    'Administration home'=>'Administration accueil',
    '<strong>Welcome to your administration panel,</strong><br> you can choose from the menu above to proceed to the configuration.'=>    '<strong>Bienvenue dans votre panneau d\'administration,</strong><br> vous pouvez choisir dans le menu en haut pour procéder à la configuration.',
    'Create'=>'Créer',
    'Update'=>'Mettre à jour',
    'Create Department'=>'Ajouter un département',
    'Departments'=>'Départements',
    'Update {modelClass}: '=>'Mis à jour: ',
    'department'=>'département',
    'Delete'=>"Supprimer",
    'Are you sure you want to delete this item?'=>"Êtes-vous sûr de vouloir supprimer cet élément?",

    'Create Equipment'=>"Ajouter un équipment",
    'Equipments'=>"Équipements",
    'equipments'=>"équipements",

    'Create Location'=>'Ajouter une Localisation',
    'Locations'=> 'Localisations',

    'Create Method'=>'Ajouter une Méthode d\'investigation',
    'Methods'=>'Méthodes d\'investigation',

    'Create Process'=>"Ajouter un Procédé",
    'Processes'=>"Procédés",

    'Create Product'=>"Ajouter un Produit",
    'Products'=>"Produits",

    'Create User'=>"Ajouter un Utilisateur",
    'Users'=>"Utilisateurs",

    "Username"=>"Nom d'utilisateur",
    "First name"=>"Prénom",
    "Last name"=>"Nom",

    'Create objectives'=>"Ajouter un Objectif",
    'Objective'=>"Objectif",

    'Create Position'=>'Ajouter un Poste',

    'Roles assignments'=>'Affectation des Roles',
    'Save'=>'Sauvegarder',
    'Assignments'=>'Affectations',
    'Permissions'=>'autorisations',
    'Create Roles & Permissions'=>'Créer les Roles & autorisations',
    'Roles & Permissions'=>'Roles & autorisations',

    'Code'=>'Code',
    'Name'=>'Nom',
    'Description'=>'Description',
    'Indicator'=>"Indicateurs",
    'Language'=>'Langue',
    'Timezone'=>'Fuseau horaire',



  ];

?>
