<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Account;
use app\models\User;

class AccountController extends Controller
{
    /*public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }*/

    /*public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }*/


    public function actionActivate($id)
    {

      try {



      if($post=Yii::$app->request->post())
      {
        //dd($post);
        $account= Account::findOne(['token'=>$id]);//$post['token']
        if($account)
        {
          //dd($post);
            $user= User::findOne(['id_user'=>$account->user_id]);
            $user->password=$post['User']['password'];
            $user->password_repeat=$post['User']['password_repeat'];
            $user->scenario= User::SCENARIO_RESET;
            if($user->save())
            {
              $account->state=Account::ACCOUNT_ACTIVE;
              $account->save();
              $model = new LoginForm();
              $msg="<h4 class='col-md-10 alert alert-info'>".\Yii::t('app','<strong>Account activated !</strong> you can now login to your account.')."</h4>";
              Yii::$app->user->logout();
              return $this->render('//site/login', [
                  'model' => $model,
                  'msg'=>$msg,
              ]);
            }
            else
            {
              $user->addErrors($user->errors);
              return $this->render('refresh_account',['model'=>$user]);
            }
        }

      }
      else
      {

        $account= Account::findOne(['token'=>$id]);

        if($account && $account->state=='3')//ACCOUNT_ON_HOLD
        {
          $user= User::findOne(['id_user'=>$account->user_id]);
          Yii::$app->user->logout();
          return $this->render('refresh_account',['model'=>$user]);
        }
        else {
          return 'activation error.';
        }
      }
    } catch (Exception $e) {
        dd($e->getMessage());
    }
    }


}
