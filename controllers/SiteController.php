<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Account;
use app\models\Session;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionNotAllowed()
    {
      return $this->render('_not_allowed');
    }
    public function actionIndex()
    {
      if(! \yii::$app->user->isGuest)
        {
          return $this->render('modules');
        }

      $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }


    }

    public function actionLogin()
    {
      //$getcookies = Yii::$app->session->getId();
      //dd($getcookies);
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();

        }

        $model = new LoginForm();
        $post=Yii::$app->request->post();
        $user= $post? User::findOne(['username'=>$post['LoginForm']['username']]):null;
        //dd($user->account->state);


        $cookies = Yii::$app->request->cookies;
        $session=($post && $user)?Session::findOne(['user_id'=>$user->id_user]):null;
        if( $user && $session && /*(null!==$cookies->get('QMSSESSID')) &&*/ $cookies->get('QMSSESSID')!= $session->id) // to sort this out at the end ..
        {
          if((strtotime('now')-$session->expire)<0)
          {
            $msg='<h4 class="col-md-10 alert alert-warning">'.\Yii::t('app','<strong>This account is already logged in !</strong> please login with another account.').'</h4>';

            return $this->render('login', [
                'model' => $model,
                'msg'=>$msg,
            ]);
          }
          else
          {
              $session->delete();
          }
        }
        elseif($session) {
          $session->delete();
        }
        if($post && $user && $user->account->state==Account::ACCOUNT_ACTIVE)
        {
          //$user->id_user!=
          if ( $model->load($post) && $model->login()) {
            $session= Yii::$app->session;
            $session->open();
            //dd($session);
            //$session=Session::findOne(['user_id'=>$user->id_user]);
            //dd($session->id);
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
              'name' => 'QMSSESSID',
              'value' => $session->id ,
              'expire' => time() + 60 * 10,//$session->timeout ,//'1497295719' ,
              'secure' => true,
            ]));
              //dd($cookies);
              //delete null entries in DbSession

              Session::deleteAll(['user_id' => null]);

              return $this->goBack();
          }
          elseif (!$model->login())
          {
            return $this->render('login', [
                'model' => $model,
                //'msg'=>$msg,
            ]);
          }

        }
        elseif(!$post)
        {
           return $this->render('login', [
               'model' => $model,
           ]);
        }
        elseif($post && !$user)
        {
            $model->addErrors(['username'=>\Yii::t('app',"Incorrect username or password."),'password'=>\Yii::t('app',"Incorrect username or password.")]);
           return $this->render('login', [
               'model' => $model,
           ]);
        }
        elseif( $user && $user->account->state==Account::ACCOUNT_INACTIVE)
        {
          $msg='<h4 class="col-md-10 alert alert-danger">'.\Yii::t('app','<strong>Your account is INACTIVE !</strong> please contact your administrator for more information.').'</h4>';

          return $this->render('login', [
              'model' => $model,
              'msg'=>$msg,
          ]);
        }
        elseif($user && $user->account->state==Account::ACCOUNT_ON_HOLD)
        {
          $msg='<h4 class="col-md-10 alert alert-warning">'.\Yii::t('app','<strong>Your account has been RESET !</strong> please check for the activation email in your email inbox or contact your administrator.').'</h4>';

          return $this->render('login', [
              'model' => $model,
              'msg'=>$msg,
          ]);
        }

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        $session= Yii::$app->session;
        $session->close();
        return $this->goHome();
    }


}
